DELIMITER $$
CREATE DEFINER=`root`@`127.0.0.1` FUNCTION `recordlink`(node varchar(55),url varchar(55),postdata varchar(55)) RETURNS varchar(255) CHARSET utf8
BEGIN
declare recordlink varchar(255);
set recordlink = concat('onmouseover=\"highlightCell(this)\" onmouseout=\"revertCell(this)\" onclick=\"arc(''',node,''',''',url,''',''recFilter=',HEX(postdata),''',1,1)\"');
RETURN (recordlink);
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`127.0.0.1` FUNCTION `script2text`(script blob) RETURNS blob
    DETERMINISTIC
BEGIN
	declare scriptformatted blob;
	set scriptformatted = REPLACE(REPLACE(UNHEX(script),'<','&lt;'),'>','&gt;');RETURN (scriptformatted);
END$$
DELIMITER ;

