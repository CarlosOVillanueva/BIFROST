## Synopsis
The BIFRÖST is a business management platform designed to reduce an organization's software footprint.

Driving the platform, is a robust, yet simple back-end framework built to make extending and customizing the platform simple and **maintainable**.

## Motivation
Operations typically consists of multiple applications. Integrating these applications is both expensive and risky to any organization. An alternative, is purchasing an enterprise solution that will potentially address stakeholders' requirements. In time, these requirements will change, forcing the organization to explore new technology solutions or invest in internal development.

The BIFRÖST was originally designed to replace these disparate applications with a single, extensible system. The challenge, however, was building a "custom" solution that was supportable. To address this challenge, the heart of the BIFRÖST is its framework - not the UI or forms. The latter, is a product of the framework, so as long as there exists a stable framework to build upon, everything else is trivial and can be changed without comprising the system.

Granted, there are several products on the market that perform this function. The problem, however, is that these systems are very cumbersome to configure and require subject matter experts to build and customize. The BIFRÖST is the 90% solution. Packaged in the software, are forms and modules for the following operational functions:
- Human Resources Information Management
- Inventory and Asset Tracking
- Information Technology Service Management
- Service Desk
- Finance Management
- Time Keeping
- Contact Management - To include built-in e-mail capabilities

All form dependencies and relationships are already built, so very little customization is needed. Nevertheless, the most valuable aspect of the system is its extensibility, so I encourage the open-source community to build modules and turn this into a tool that will empower businesses of all sizes.
## Installation
See [Getting Started](https://gitlab.com/CarlosOVillanueva/BIFROST/wikis/Getting-Started)
## Contributors
- Carlos O. Villanueva - Creator

## Release History
- 2.0.0 - Conversion to GPLv3, 2016 [CURRENT]
- 1.2.0 - Pilot, 2015
- 1.1.0 - Conversion to OOP
- 1.0.0 - Original release, 2010

## Screenshoots
[Click here](https://gitlab.com/CarlosOVillanueva/BIFROST/blob/master/assets/)

## License
Licensed under the [GPLv3](http://www.gnu.org/licenses/gpl-3.0.en.html)

Copyright (c) 2010-2016 Carlos Villanueva
