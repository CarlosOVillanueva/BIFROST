<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

$gdbo -> sql = "SELECT
a.id_hr_degree_level,
concat(a.hr_degree_level,'-',a.hr_degree_level_descr) as hr_degree_level,
b.id_hr_degree_tp,
b.hr_degree_tp
FROM _hr_degree_level a
JOIN _hr_degree_tp b
ON a.id_hr_degree_tp = b.id_hr_degree_tp";
?>
