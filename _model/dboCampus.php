<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

$gdbo -> sql ="SELECT
a.id_cust_branch,
a.cust_branch,
a.id_cust_company,
b.cust_company
FROM
_cust_branch a
LEFT JOIN
_cust_company b
ON a.id_cust_company=b.id_cust_company
WHERE
b.id_cust_company_tp = 6 AND a.id_cust_branch_tp = 4
ORDER BY
b.id_cust_company_tp,a.id_cust_company,a.cust_branch";
?>
