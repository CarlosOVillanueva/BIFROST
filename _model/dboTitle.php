<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

$gdbo -> sql = "SELECT 
a.id_name_title,
a.name_title,
a.id_name_title_tp,
b.name_title_tp
FROM _name_title a
LEFT JOIN _name_title_tp b
ON a.id_name_title_tp = b.id_name_title_tp
ORDER BY b.id_name_title_tp asc, a.name_title asc";
?>
