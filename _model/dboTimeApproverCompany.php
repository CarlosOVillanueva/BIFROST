<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

$gdbo -> sql = "SELECT distinct c.id_cust_company,b.cust_company
FROM _sys_user_emp a
LEFT JOIN _cust_contact c ON a.id_cust_contact=c.id_cust_contact
LEFT JOIN _cust_company b ON c.id_cust_company=b.id_cust_company
LEFT JOIN _sys_user d on a.id_sys_user=d.id_sys_user
WHERE d.id_sys_status=21
ORDER BY b.cust_company";
?>
