#Getting Started
The following set of instructions are meant to help you stand up a new instance of the BIFROST.

##Step One - Machine Set Up
###Option 1 - Bare metal or Virtual Machine Setup
[Download and install Ubuntu 14.04.03 LTS (Trusty Tahr)](http://www.ubuntu.com/download/server) 

After download, burn ISO to a CD or mount it within a hypervisor (i.e., VMWare, VirtualBox, HyperV, or Qemu/KVM). Use the ISO to perform a fresh install of the operating system. I will eventually include a download link to a pre-configured image and preseed files for deployment.

###Option 2 - Vagrant Install
[Download and install Vagrant](https://www.vagrantup.com/downloads.html)

Download Vagrant image for Ubuntu

	#Create Directory to store images
	mkdir ~/VagrantBoxes/Bifrost -p

	#Initialize Vagrant Box
	cd ~/VagrantBoxes/Bifrost
	vagrant init ubuntu/trusty64; vagrant up --provider virtualbox

	#Log into Vagrant Box
	vagrant ssh

Please note, to access the http or https service from your host machine, you will have to create a NAT rule in virtual box to map 127.0.0.1:8080 to port 80 on the VM and 127.0.0.1:8443 to port 443.

##Step Two - Download application dependencies
This will eventually be converted to a debian package, but for now, please use the following set of instructions:

	#Download Packages
	apt-get install apache2 gzip imagemagick imagemagick-common mime-support mysql-client-5.5 mysql-client-core-5.5 mysql-common mysql-server mysql-server-5.5 mysql-server-core-5.5 openssh-client openssh-server openssh-sftp-server openssl php-pear php5 php5-cli php5-common php5-dev php5-json php5-mcrypt php5-memcache php5-mysql php5-pgsql php5-readline php5-sybase postfix unzip wget git

	#During MySQL install, please provide a password
	#During PostFix install, please select Internet Site

	#Download PEAR modules
	pear install Package Archive_Tar Console_Getargs Console_Getopt Console_Table Event_Dispatcher File_Find Log Mail_Mime Mail_mimeDecode Net_IPv4 PEAR PHP_CompatInfo PHP_DocBlockGenerator Structures_Graph XML_Util 

	#Get BIFROST
	wget https://github.com/CarlosOVillanueva/BIFROST/archive/master.zip -O /opt/bifrost.zip
	unzip /opt/bifrost.zip
	mkdir /opt/bifrost/_uploads
	mkdir /opt/bifrost/_avatar
	chown www-data.www-data /opt/bifrost -R

##Step Three - Configure Applications
The main applications that require configuration are MySql and Apache. There is also a postfix configuration, but I will add that at a later time.

	#Configure MySQL. Please be sure to enter the password you set when you installed MySQL
	mysql -u root -p 
	mysql> CREATE DATABASE bifrost;
	mysql> quit

	#Import BIFROST schema
	mysql -y root -p < /opt/bifrost/_mysql/bifrost.sql

	#Enable PHP Modules
	php5enmod mcrypt
	php5enmod mysql
	php5enmod json
	php5enmod memcache
	php5enmod pgsql
	php5enmod mssql

	#Configure Apache
	service apache2 stop
	cd /etc/apache2/

At this point, replace the /var/www/ Directory in /etc/apache2/apache2.conf with the following:

	#<Directory /var/www>
    #    Options Indexes FollowSymLinks
    #    AllowOverride None
    #    Require all granted
	#</Directory>

	<Directory /opt/bifrost>
        Options Indexes FollowSymLinks
        AllowOverride all
        Require all granted
	</Directory>

Lastly, update the Virtual Host settings for /etc/apache2/sites-enabled/000-default.conf

	sed -i 's/var\/www/opt\/bifrost/gi' /etc/apache2/sites-enabled/000-default.conf
	service apache2 restart

##Set Four - Configure BIFROST settings
As long as your files are under /opt/bifrost, there is minimal configuration. The only thing that needs updating is the password for MySQL. To edit this, all you need to do is edit /opt/bifrost/_lib/php/config.php

	$avatarWritePath = "/opt/bifrost/_avatar";
	$avatarfolder = "/_avatar";
	$filefolder = "/_uploads/";
	$fileWritePath = "/opt/bifrost/_uploads/";
	$serverroot = '/\/opt\/bifrost/';
	$serverrootpath = '/opt/bifrost/';
	$fsorootpat='/opt/bifrost';
	$globalDB = 'bifrost';
	$globalDBTP = 'mysql';
	$globalDBCON = "localhost,null,root,cleartextpassword";

Just update the $globalDBCON variable. The last two parameters are for the user and the password. If the directories happen to change, you will need to update the other parameters and the .htaccess file under /opt/bifrost.

At this point, you should be able to launch the application in the web browser. The user name is *admin* and the password is *L3tmein!*



