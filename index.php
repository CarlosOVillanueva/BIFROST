<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#includes
require_once("bootstrap.php");
#post variables
filterQrypost("frmUsername");
filterQrypost("frmPassword");
filterQrypost("uBit");
filterQrypost("password");
#lookup user
if (isset($frmUsername)) {
$gdbo -> sql = "
SELECT a.sys_user, a.sys_user_pw, a.id_sys_user FROM _sys_user a WHERE a.sys_user='$frmUsername' AND a.sys_user_pw='" . encrypt($frmPassword) . "' AND a.id_sys_status=21";
$gdbo -> getRec();
$aUserName = $gdbo -> dbData;
#user found
if (count($aUserName) > 0) {
$login = true;
$sys_user = $aUserName[0][0];
$id_sys_user = $aUserName[0][2];
#get user group membership
$gdbo -> sql = "
SELECT a.id_sys_group FROM _sys_group_user a WHERE a.id_sys_user=$id_sys_user";
$gdbo -> getRec();
$aUserGroup = $gdbo -> dbData;
#update password if set
if ($uBit == 1) {
$gdbo -> dbTable = "_sys_user";
$gdbo -> sql = "sys_user_pw='" . encrypt($password)."'";
$gdbo -> updateRec("id_sys_user=".$id_sys_user);}} else {
$login = false;}
#auditing
if ($login === true) {
#add login to log
$gdbo -> sql = "insert into _sys_user_log (id_sys_user,sys_user_log_sessionID,sys_user_log_bit) values (" . $id_sys_user . ",'" . session_id() . "',1)";
#build cookie
setcookie('PHPSESSIDU', encrypt($id_sys_user));
setcookie('PHPSESSIDG', encrypt(implode($aUserGroup)));
header('location:arc.php');
}} else {
#add fail to log
$gdbo -> sql = "insert into _sys_user_log (sys_user,sys_user_log_sessionID,sys_user_log_bit,sys_user_pw) values ('" . $frmUsername . "','" . session_id() . "',0,'" . bin2hex($frmPassword) . "')";}
$gdbo -> execQuery();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>BIFROST SOFTWARE LLC</title>
<meta http-equiv="content-type"
content="text/html;charset=utf-8" />
<META NAME="COPYRIGHT" CONTENT="&copy; 2014 Bifrost Softrware LLC">
<?php
$dirs = array($path.'_lib/jquery');
$file_ext = array("js");
$files = scanDir::scan($dirs, $file_ext);
$linkedFiles = "";
foreach ($files as $fso) {

$fso = str_replace($serverrootpath,"",$fso);

$ext = trim(end(explode(".", $fso)));
switch ($ext) {
case "js":
$linkedFiles .= '<script type="text/javascript" src="'.$fso.'"></script>';
break;
case "css":
$linkedFiles .= '<link type="text/css" rel="stylesheet" href="'.$fso.'"/>';
break;
default:
break;}}
echo $linkedFiles
?>
<script type="text/javascript" src="_lib/js/ArcJS.js"></script>
<link href="/_css/login.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<div id="login">
<img style="margin-bottom:20px" src="_img/interface/applogo.png"/>
<?php
if (isset($login) && $login === false) {echo "<div id=\"error\">login failed</div>";} elseif (isset($login) && $login === true) {echo "login successful";} else {}
?>
<form id="frm_login" method="POST" action="index.php">
<div>
<label>User ID</label><input id="frmUsername" size="20" maxlength="25" style="width:200px" name="frmUsername" type="text" />
</div>
<div>
<label>Password</label><input id="frmPassword" size="20" maxlength="20" style="width:200px" name="frmPassword" type="password" />
</div>
<div id="v_pw" style="display:none">
<label>New Password</label>
<input size="20" maxlength="20" style="width:200px" type="password" id="password_" name="password_" onblur="validateElement('password',this)"/>
<label>Confirm Password</label>
<input size="20" maxlength="20" style="width:200px" type="password" disabled="disabled" id="password" name="password" onblur="validateElement('password',this)"/>
</div>
<div class="login">
<input type="hidden" id="uBit" name="uBit" value="0"/><input type="submit" id="button_submit" value="Login" onclick="validate()"/><input type="button" id="button_pw" value="Change Password" onclick="updatePW(this)"/></div>
</form>
<div>Username: admin<br>Password: L3tmein!</div>
<div id="recovery">&nbsp;</div>
<div class="recovercreds"><a onclick="recoverCreds()">Lost Password?</a></div>
<div class="copyright">Copyright © 2010-2016 BIFRÖST SOFTWARE,LLC.<br/>All rights reserved.</div>
</div>
</body>
</html>
