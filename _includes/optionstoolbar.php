<script type="text/javascript">
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

CONTENTWINDOW=$('#content');
FIELDSETS=$(CONTENTWINDOW).find('fieldset');
OPTIONS='';
LABELS= new Array();
for (i=0;i<FIELDSETS.length;i++) {
if (FIELDSETS[i].id!=''){
LABELS.push(FIELDSETS[i]);
}}
for (i=0;i<LABELS.length;i++) {
MENUID=LABELS[i].id;
LABELID='_'+LABELS[i].id;
LABEL=LABELS[i].id.replace(/_/gi,' ');
if (i==0 ) {ACTIVETAB='activeHeading'} else {ACTIVETAB='menuheadingbutton'}
OPTIONS=OPTIONS+'<span id="'+LABELID+'" class="'+ACTIVETAB+'" onclick="subMenu(this,\''+MENUID+'\')">'+LABEL+'</span>';
if (i!=0) {LABELS[i].style.display='none'}
}
if(LABELS.length === 0) {icoSave = "";} 
$('#menu').html(window.mainmenu+OPTIONS);
<?=$icoAdd?>;
<?=$icoSave?>;
$('#oOptions').html(icoAdd+icoSave);
</script>