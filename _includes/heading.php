<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

if ($mod == 0) {$mod = 15;echo '<script>collapseObj(\'mod_11_sub\',\'navsubcontainer\',null,\'navmod\',this);</script>';$path="_mod/smod_15/";}
if($mod != ""){
$gdbo -> sql = "SELECT
a.id_sys_module,
a.sys_module,
b.id_sys_module_sub,
b.sys_module_sub,
b.sys_module_sub_href,
UNHEX(b.sys_module_sub_icon) as sys_module_sub_icon
FROM _sys_module a
JOIN _sys_module_sub b ON a.id_sys_module=b.id_sys_module
WHERE b.id_sys_module_sub=".$mod." ORDER BY a.sys_module_order asc,b.sys_module_sub_order";
$gdbo -> getRec();
$recModules = $gdbo -> getAssociative();
$menu = 
'$("#menu").html("<div class=\"menuheadingmain\"><i class=\"fa fa-cogs\"></i> '.$recModules[0]["sys_module"].
'</div><div class=\"menuheading\" title=\"'.$recModules[0]["sys_module_sub"].'\"'.
' onclick=\"arc(\'content\',\''.$path.'index.php\')\">'.str_replace("\"","\\\"",$recModules[0]["sys_module_sub_icon"]).' '.$recModules[0]["sys_module_sub"].'</div>");';}
else {$menu = "";}
?>
<script type='text/javascript'>
<?=$icoAdd;?>
$("#oOptions").html(icoAdd);
<?=$menu;?>
window.mainmenu = $("#menu").html();
</script>
