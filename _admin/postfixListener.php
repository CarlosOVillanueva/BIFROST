#!/usr/bin/php
<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

$require_once("_lib/php/bootstrap/bootstrap.php");
$fd = fopen("php://stdin", "r");
$email = "";
while (!feof($fd)) {
$email .= fread($fd, 1024);
}
fclose($fd);
$mime=new mime_parser_class;
$mime->ignore_syntax_errors = 1;
$parameters=array(
'Data'=>$email,
);
$mime->Decode($parameters, $decoded);
//---------------------- GET EMAIL HEADER INFO -----------------------//
//get the name and email of the sender
$fromName = $decoded[0]['ExtractedAddresses']['from:'][0]['name'];
$fromEmail = $decoded[0]['ExtractedAddresses']['from:'][0]['address'];
//get the name and email of the recipient
$toEmail = $decoded[0]['ExtractedAddresses']['to:'][0]['address'];
$toName = $decoded[0]['ExtractedAddresses']['to:'][0]['name'];
//get the subject
$subject = $decoded[0]['Headers']['subject:'];
$removeChars = array('<','>');
//get the message id
$messageID = str_replace($removeChars,'',$decoded[0]['Headers']['message-id:']);
//get the reply id
$replyToID = str_replace($removeChars,'',$decoded[0]['Headers']['in-reply-to:']);
//---------------------- FIND THE BODY -----------------------//
//get the message body
if(substr($decoded[0]['Headers']['content-type:'],0,strlen('text/plain')) == 'text/plain' && isset($decoded[0]['Body'])){
$body = $decoded[0]['Body'];
} elseif(substr($decoded[0]['Parts'][0]['Headers']['content-type:'],0,strlen('text/plain')) == 'text/plain' && isset($decoded[0]['Parts'][0]['Body'])) {
$body = $decoded[0]['Parts'][0]['Body'];
} elseif(substr($decoded[0]['Parts'][0]['Parts'][0]['Headers']['content-type:'],0,strlen('text/plain')) == 'text/plain' && isset($decoded[0]['Parts'][0]['Parts'][0]['Body'])) {
$body = $decoded[0]['Parts'][0]['Parts'][0]['Body'];
}
/*********************************************************************/
/*Get Time Card Data*/
require_once("bootstrap.php");
date_default_timezone_set('UTC');
$gmtOffset="-21600"; //Central Time
$dtCurrent = date("l, F j, Y e");
$dtTimeCurrent=date("Y-m-d H:i:s",(strtotime(date("Y-m-d H:i:s"))+$gmtOffset));
$id_tm_timesheet=decrypt(trim(preg_replace("/.+?\|.+?\|(.*)/","$1",$subject)));
$arc=new ArcDb;
$arc -> dbType = $globalDBTP;
$arc -> dbSchema = $globalDB; 
$arc -> dbConStr=$globalDBCON;
$arc -> sql="
SELECT
c.id_cust_contact,
concat(c.cust_contact_givenName,' ',c.cust_contact_familyName) as fullName,
c.cust_contact_givenName,
c.cust_contact_familyName
FROM
_sys_user_emp a 
LEFT JOIN _hr_emp b ON a.id_cust_contact=b.id_cust_contact
LEFT JOIN _cust_contact c ON a.id_cust_contact=c.id_cust_contact
LEFT JOIN _sys_user d ON a.id_sys_user=d.id_sys_user
WHERE d.id_sys_status=21 AND d.sys_user_tmEmail='".$fromEmail."'";
$arc -> getRec();
$approver=$arc -> getAssociative();
$approverFull=$approver[0]["fullName"];
$approverFirst=$approver[0]["cust_contact_givenName"];
$approverLast=$approver[0]["cust_contact_familyName"];
$approverCount=$arc->dbRows;
$arc -> sql="
SELECT
concat(b.cust_contact_givenName,' ',b.cust_contact_familyName) as fullName
FROM
_tm_timesheet a
LEFT JOIN
_hr_emp c ON a.id_hr_emp=c.id_hr_emp
LEFT JOIN
_cust_contact b ON c.id_cust_contact=b.id_cust_contact
WHERE
a.id_tm_timesheet=$id_tm_timesheet
";
$arc -> getRec();
$timesheetdetail=$arc->getAssociative();
if($approverCount> 0){
/*********************************************************************/
/*The correct approver was matched*/
/*********************************************************************/
if(preg_match("/I.+?".$approverFirst.".+?".$approverLast["cust_contact_familyName"].".*/i",$body,$matches))
{
$arc->dbTable="_tm_timesheet";
if(preg_match("/\sapprove|\saccept|\sapproved/i",$matches[0])) {
$response="Thank you for approving the time sheet for ".$timesheetdetail[0]["fullName"].".";
$arc->sql="tm_timesheet_approved=1,tm_timesheet_du=".strtotime(date("Y-m-d H:i:s"));
$arc->updateRec("id_tm_timesheet=".$id_tm_timesheet);
}
elseif(preg_match("/\sreject|\sdo not accept|\srejected/i",$matches[0])) {
$response="You have elected to reject the time sheet for ".$timesheetdetail[0]["fullName"].".";
$arc->sql="tm_timesheet_approved=0,tm_timesheet_du=".strtotime(date("Y-m-d H:i:s"));
$arc->updateRec("id_tm_timesheet=".$id_tm_timesheet);
}
else {
$response="Your response was not entered correctly. Please re-attempt to respond to the original e-mail.";
}
} else {
$response="Your response was not entered correctly. Please re-attempt to respond to the original e-mail.";
}
/*********************************************************************/
/* Send Appropriate Responses */
/*********************************************************************/
/* Email Timesheet Approver */
$message = $approverFull.". $response<p>If you have any questions, please log into http://arc.bifrostapps.com and submit a ticket to our helpdesk.</p>";
$message = wordwrap($message, 70);
$subject = "TIME APPROVAL REQUEST RECEIVED";
$headers = array();
$headers[] = "MIME-Version: 1.0";
$headers[] = "Content-type: text/html; charset=iso-8859-1";
$headers[] = "From: Bifrost Software LLC <donotreply@bifrost-software.com>";
$headers[] = "Subject: {$subject}";
$headers[] = "X-Mailer: PHP/" . phpversion();
// Send
mail($fromEmail, $subject, $message, implode("\r\n", $headers), "-f donotreply@bifrost-software.com");
}
?>
