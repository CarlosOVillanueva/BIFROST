<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

require("config.php");
require($serverrootpath."_lib/php/bootstrap/ArcCore.php");
require($serverrootpath."_lib/php/bootstrap/ArcDb.php");
$gdbo = new ArcDb;
$gdbo -> dbConStr=$globalDBCON;
$gdbo -> dbType = $globalDBTP;
$SOURCE = "_bifrost";
$DESTINATION = $globalDB;
$LASTNAME = "Administrator";
$FIRSTNAME = "System";
$PASSWORD = "password12345";
$RECOVERYEMAIL = "user@host.com";
$USERNAME = "admin";
$COMPANY = "Your Company Name";
$BRANCH = "Headquarters";
$gdbo -> debug = true;
$gdbo -> dbSchema = $DESTINATION;
$gdbo -> sql = "SELECT TABLE_NAME FROM information_schema.tables WHERE TABLE_SCHEMA='$DESTINATION'";
$gdbo -> getRec();
$adms = $gdbo -> dbData;
$debug =1; printArray($gdbo -> dbData);
foreach ($adms as $row => $data) {
echo "Deleting ".$data[0].".";
$gdbo -> sql = "DELETE FROM ".$data[0];
$gdbo -> execQuery();
echo "Resetting Index ".$data[0].".";
$gdbo -> sql = "ALTER TABLE ".$data[0]." AUTO_INCREMENT=1";
$gdbo -> execQuery();
echo "Done.\n";
}
#create admin account
echo "Creating Administrator\n";
$gdbo -> dbTable = "_cust_company";
$gdbo -> sql = "(cust_company,id_cust_company_tp) values ('$COMPANY',1)";
$gdbo -> insertRec();
$id_cust_company = $gdbo -> insertedID;
$gdbo -> dbTable = "_cust_branch";
$gdbo -> sql = "(cust_branch,id_cust_branch_tp,id_cust_company) values ('$BRANCH',1,$id_cust_company )";
$gdbo -> insertRec();
$id_cust_branch = $gdbo -> insertedID;
$gdbo -> dbTable = "_cust_contact";
$gdbo -> sql = "(cust_contact_familyName,cust_contact_givenName,id_cust_contact_tp,id_cust_branch,id_cust_company) values ('".$LASTNAME."','".$FIRSTNAME."',1,$id_cust_branch,$id_cust_company)";
$gdbo -> insertRec();
$id_cust_contact = $gdbo -> insertedID;
$gdbo -> dbTable = "_hr_emp";
$gdbo -> sql = "(id_cust_contact,hr_emp_payFrequency,id_hr_emp_status) values ($id_cust_contact,'weekly',6)";
$gdbo -> insertRec();
$gdbo -> dbTable = "_sys_user";
$gdbo -> sql = "(sys_user,sys_user_recovery,sys_user_pw,id_sys_status) values ('$USERNAME','$RECOVERYEMAIL ','".encrypt($PASSWORD)."',21)";
$gdbo -> insertRec();
$id_sys_user= $gdbo -> insertedID;
$gdbo -> dbTable = "_sys_user_emp";
$gdbo -> sql = "(id_cust_contact,id_sys_user) values ($id_cust_contact,$id_sys_user)";
$gdbo -> insertRec();
#_hr_race
echo "_hr_race";echo "\n";
$gdbo -> sql = "insert into $DESTINATION._hr_race (hr_race) SELECT hr_race FROM $SOURCE._hr_race;";
$gdbo -> execQuery();
#_loc_city
echo "_loc_city";echo "\n";
$gdbo -> sql = "insert into $DESTINATION._loc_city SELECT * FROM $SOURCE._loc_city;";
$gdbo -> execQuery();
#_loc_country
echo "_loc_country";echo "\n";
$gdbo -> sql = "insert into $DESTINATION._loc_country SELECT * FROM $SOURCE._loc_country;";
$gdbo -> execQuery();
#_loc_region
echo "_loc_region";echo "\n";
$gdbo -> sql = "insert into $DESTINATION._loc_region SELECT * FROM $SOURCE._loc_region;";
$gdbo -> execQuery();
#_name_suffix
echo "_name_suffix";echo "\n";
$gdbo -> sql = "insert into $DESTINATION._name_suffix SELECT * FROM $SOURCE._name_suffix;";
$gdbo -> execQuery();
#_name_title
echo "_name_title";echo "\n";
$gdbo -> sql = "insert into $DESTINATION._name_title SELECT * FROM $SOURCE._name_title;";
$gdbo -> execQuery();
#_name_title_tp
echo "_name_title_tp";echo "\n";
$gdbo -> sql = "insert into $DESTINATION._name_title_tp SELECT * FROM $SOURCE._name_title_tp;";
$gdbo -> execQuery();
#_sys_status
echo "_sys_status";echo "\n";
$gdbo -> sql = "insert into $DESTINATION._sys_status SELECT * FROM $SOURCE._sys_status;";
$gdbo -> execQuery();
#_sys_status_tp
echo "_sys_status_tp";echo "\n";
$gdbo -> sql = "insert into $DESTINATION._sys_status_tp SELECT * FROM $SOURCE._sys_status_tp;";
$gdbo -> execQuery();
#_tm_charge
echo "_tm_charge";echo "\n";
$gdbo -> sql = "insert into $DESTINATION._tm_charge SELECT * FROM $SOURCE._tm_charge;";
$gdbo -> execQuery();
#_srv_tp
echo "_srv_tp";echo "\n";
$gdbo -> sql = "insert into $DESTINATION._srv_tp SELECT * FROM $SOURCE._srv_tp;";
$gdbo -> execQuery();
#_srv_src
echo "_srv_src";echo "\n";
$gdbo -> sql = "insert into $DESTINATION._srv_src SELECT * FROM $SOURCE._srv_src;";
$gdbo -> execQuery();
#_srv_severity
echo "_srv_severity";echo "\n";
$gdbo -> sql = "insert into $DESTINATION._srv_severity SELECT * FROM $SOURCE._srv_severity;";
$gdbo -> execQuery();
#_srv_priority
echo "_srv_priority";echo "\n";
$gdbo -> sql = "insert into $DESTINATION._srv_priority SELECT * FROM $SOURCE._srv_priority;";
$gdbo -> execQuery();
#_srv_impact
echo "_srv_impact";echo "\n";
$gdbo -> sql = "insert into $DESTINATION._srv_impact SELECT * FROM $SOURCE._srv_impact;";
$gdbo -> execQuery();
#_hr_pay_tp
echo "_hr_pay_tp";echo "\n";
$gdbo -> sql = "insert into $DESTINATION._hr_pay_tp SELECT * FROM $SOURCE._hr_pay_tp;";
$gdbo -> execQuery();
#_hr_maritalstatus
echo "_hr_maritalstatus";echo "\n";
$gdbo -> sql = "insert into $DESTINATION._hr_maritalStatus SELECT * FROM $SOURCE._hr_maritalStatus;";
$gdbo -> execQuery();
#_hr_ethnicity
echo "_hr_ethnicity";echo "\n";
$gdbo -> sql = "insert into $DESTINATION._hr_ethnicity SELECT * FROM $SOURCE._hr_ethnicity;";
$gdbo -> execQuery();
#_hr_degree_tp
echo "_hr_degree_tp";echo "\n";
$gdbo -> sql = "insert into $DESTINATION._hr_degree_tp SELECT * FROM $SOURCE._hr_degree_tp;";
$gdbo -> execQuery();
#_hr_degree_level
echo "_hr_degree_level";echo "\n";
$gdbo -> sql = "insert into $DESTINATION._hr_degree_level SELECT * FROM $SOURCE._hr_degree_level;";
$gdbo -> execQuery();
#_device_ifspeedduplex
echo "_device_ifspeedduplex";echo "\n";
$gdbo -> sql = "insert into $DESTINATION._device_ifspeedduplex SELECT * FROM $SOURCE._device_ifspeedduplex;";
$gdbo -> execQuery();
#_contract_cat
echo "_contract_cat";echo "\n";
$gdbo -> sql = "insert into $DESTINATION._contract_cat SELECT * FROM $SOURCE._contract_cat;";
$gdbo -> execQuery();
#_contract_cat
echo "_contract_cat";echo "\n";
$gdbo -> sql = "insert into $DESTINATION._contact_method_tp_grp SELECT * FROM $SOURCE._contact_method_tp_grp;";
$gdbo -> execQuery();
#_contact_method_tp
echo "_contact_method_tp";echo "\n";
$gdbo -> sql = "insert into $DESTINATION._contact_method_tp SELECT * FROM $SOURCE._contact_method_tp;";
$gdbo -> execQuery();
#_con_db_tp
echo "_con_db_tp";echo "\n";
$gdbo -> sql = "insert into $DESTINATION._con_db_tp SELECT * FROM $SOURCE._con_db_tp;";
$gdbo -> execQuery();
#_cust_contact_tp
echo "_cust_contact_tp";echo "\n";
$gdbo -> sql = "insert into $DESTINATION._cust_contact_tp (cust_contact_tp) SELECT cust_contact_tp FROM $SOURCE._cust_contact_tp;";
$gdbo -> execQuery();
#_sys_module_sub
echo "_sys_module_sub";echo "\n";
$gdbo -> sql = "insert into $DESTINATION._sys_module_sub SELECT * FROM $SOURCE._sys_module_sub;";
$gdbo -> execQuery();
#_sys_module
echo "_sys_module";echo "\n";
$gdbo -> sql = "insert into $DESTINATION._sys_module SELECT * FROM $SOURCE._sys_module;";
$gdbo -> execQuery();
#_cust_branch_tp
echo "_cust_branch_tp";echo "\n";
$gdbo -> sql = "insert into $DESTINATION._cust_branch_tp SELECT * FROM $SOURCE._cust_branch_tp;";
$gdbo -> execQuery();
#_cust_company_tp
echo "_cust_company_tp";echo "\n";
$gdbo -> sql = "insert into $DESTINATION._cust_company_tp SELECT * FROM $SOURCE._cust_company_tp;";
$gdbo -> execQuery();
#_hr_insurance_cat
echo "_hr_insurance_cat";echo "\n";
$gdbo -> sql = "insert into $DESTINATION._hr_insurance_cat SELECT * FROM $SOURCE._hr_insurance_cat;";
$gdbo -> execQuery();
#_hr_insurance_plan_tp
echo "_hr_insurance_plan_tp";echo "\n";
$gdbo -> sql = "insert into $DESTINATION._hr_insurance_plan_tp SELECT * FROM $SOURCE._hr_insurance_plan_tp;";
$gdbo -> execQuery();
#_tm_work_role
echo "_tm_work_role";echo "\n";
$gdbo -> sql = "insert into $DESTINATION._tm_work_role SELECT * FROM $SOURCE._tm_work_role;";
$gdbo -> execQuery();
#_tm_work_tp
echo "_tm_work_tp";echo "\n";
$gdbo -> sql = "insert into $DESTINATION._tm_work_tp SELECT * FROM $SOURCE._tm_work_tp;";
$gdbo -> execQuery();
#_hr_insurance_coverage
echo "_hr_insurance_coverage";echo "\n";
$gdbo -> sql = "insert into $DESTINATION._hr_insurance_coverage SELECT * FROM $SOURCE._hr_insurance_coverage;";
$gdbo -> execQuery();
#_invoice_payment_tp
echo "_invoice_payment_tp";echo "\n";
$gdbo -> sql = "insert into $DESTINATION._invoice_payment_tp SELECT * FROM $SOURCE._invoice_payment_tp;";
$gdbo -> execQuery();
#_invoice_qty_tp
echo "_invoice_qty_tp";echo "\n";
$gdbo -> sql = "insert into $DESTINATION._invoice_qty_tp SELECT * FROM $SOURCE._invoice_qty_tp;";
$gdbo -> execQuery();
#_purchaseorder_payment_tp
echo "_purchaseorder_payment_tp";echo "\n";
$gdbo -> sql = "insert into $DESTINATION._purchaseorder_payment_tp SELECT * FROM $SOURCE._purchaseorder_payment_tp;";
$gdbo -> execQuery();
#_purchaseorder_qty_tp
echo "_purchaseorder_qty_tp";echo "\n";
$gdbo -> sql = "insert into $DESTINATION._purchaseorder_qty_tp SELECT * FROM $SOURCE._purchaseorder_qty_tp;";
$gdbo -> execQuery();
#_device_grp
echo "_device_grp";echo "\n";
$gdbo -> sql = "insert into $DESTINATION._device_grp SELECT * FROM $SOURCE._device_grp;";
$gdbo -> execQuery();
#_cfg_grp
echo "_cfg_grp";echo "\n";
$gdbo -> sql = "insert into $DESTINATION._cfg_grp SELECT * FROM $SOURCE._cfg_grp;";
$gdbo -> execQuery();
#add permissions
for($i=0;$i<30;$i++) {
$gdbo -> sql = "insert into $DESTINATION._sys_permission (sys_permission,id_sys_module_sub,id_sys_user) values (3,$i,1);";
$gdbo -> execQuery();
}
?>
