<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

require("config.php");
require($serverrootpath."_lib/php/bootstrap/ArcCore.php");
require($serverrootpath."_lib/php/bootstrap/ArcDb.php");
$gdbo = new ArcDb;
$gdbo -> dbConStr=$globalDBCON;
$gdbo -> dbType = $globalDBTP;
$gdbo -> dbSchema = $globalDB;
#create accounts
$users =array(
array("John","Smith","password12345","john.smith@changeit.com","john.smith"),);
for($i=0;$i<count($users);$i++) {
echo "Creating Account ".$users[$i][0]. " " .$users[$i][1]." \n";
$id_cust_branch = 1;
$id_cust_company = 1;
$LASTNAME = $users[$i][1];
$FIRSTNAME = $users[$i][0];
$RECOVERYEMAIL = $users[$i][3];
$USERNAME = $users[$i][4];
$PASSWORD = $users[$i][2];
$gdbo -> dbTable = "_cust_contact";
$gdbo -> sql = "(cust_contact_familyName,cust_contact_givenName,id_cust_contact_tp,id_cust_branch,id_cust_company) values ('".$LASTNAME."','".$FIRSTNAME."',1,$id_cust_branch,$id_cust_company)";
$gdbo -> insertRec();
$id_cust_contact = $gdbo -> insertedID;
$gdbo -> dbTable = "_hr_emp";
$gdbo -> sql = "(id_cust_contact,hr_emp_payFrequency,id_hr_emp_status) values ($id_cust_contact,'weekly',6)";
$gdbo -> insertRec();
$gdbo -> dbTable = "_sys_user";
$gdbo -> sql = "(sys_user,sys_user_recovery,sys_user_pw,id_sys_status) values ('$USERNAME','$RECOVERYEMAIL ','".encrypt($PASSWORD)."',21)";
$gdbo -> insertRec();
$id_sys_user= $gdbo -> insertedID;
$gdbo -> dbTable = "_sys_user_emp";
$gdbo -> sql = "(id_cust_contact,id_sys_user) values ($id_cust_contact,$id_sys_user)";
$gdbo -> insertRec();
#add permissions
for($ii=0;$ii<30;$ii++) {
$gdbo -> sql = "insert into _sys_permission (sys_permission,id_sys_module_sub,id_sys_user) values (3,$ii,$id_cust_contact);";
$gdbo -> execQuery();
}
}
?>
