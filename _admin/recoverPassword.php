<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#includes
require("_lib/php/bootstrap.php");
#lookup user
$gdbo -> sql = "
select 
a.sys_user_pw,
a.sys_user
from _sys_user a
where a.id_sys_status <> 22 AND a.sys_user_recovery='" . $_POST["uAddress"] . "'";
$gdbo -> getRec();
$a = $gdbo -> dbData;
#construct email
if (count($a) > 0) {
$user = $a[0][1];
$pw = decrypt($a[0][0]);
$message = "A request has been made to recover your account credentials.\n
Please use the following to gain access to your account.\n\nUser:$user\nPassword:$pw\n\nFor your safety, please
change your password prior to logging into the system.";
$message = wordwrap($message, 70);
$subject = "BIFROST ARC Password Recovery Request";
$headers = array();
$headers[] = "MIME-Version: 1.0";
$headers[] = "Content-type: text/plain; charset=iso-8859-1";
$headers[] = "From: Support <admin@bifrostapps.com>";
$headers[] = "Subject: {$subject}";
$headers[] = "X-Mailer: PHP/" . phpversion();
#send
mail($_POST["uAddress"], $subject, $message, implode("\r\n", $headers), "-f admin@bifrostapps.com");
echo "<div style=\"border:1px solid #007700;-moz-border-radius:5px;padding:10px;-webkit-border-radius:5px;background-color:#bbffbb;color#007700;margin:5px 0\">Your credentials were successfully sent.</div>";
} else {
echo "<div style=\"border:1px solid #770000;-moz-border-radius:5px;padding:5px;-webkit-border-radius:5px;background-color:#ffbbbb;color:#770000;margin:10px 0\">We apologize for the inconvenience,<br/>but there was an error accessing your account.</div>";
}
?>
