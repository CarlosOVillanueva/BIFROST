<?php 
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#includes
require_once ("_lib/php/auth.php");
#get viewable modules
$modules = array();
$moduleFilter = "";
$debug = 1;
foreach ($sysPermissions as $row => $data) {
	if ($data["sys_permission"] == 0) {
		$modules[] = $data["id_sys_module_sub"];
	}
}
if (count($modules) > 0) {
	$moduleFilter = " AND (";
		$i = 0;
		foreach ($modules as $key => $value) {
			$or = ($i != 0 ? " AND " : "");
			$moduleFilter .= $or . " id_sys_module_sub <> " . $value;
			$i++;
		}
		$moduleFilter .= ")";
}
#get modules
$gdbo -> sql = "
SELECT
a.id_sys_module,
a.sys_module,
b.id_sys_module_sub,
b.sys_module_sub,
b.sys_module_sub_href,
UNHEX(b.sys_module_sub_icon) as sys_module_sub_icon
FROM _sys_module a
JOIN _sys_module_sub b ON a.id_sys_module=b.id_sys_module
WHERE b.sys_module_sub_enabled=1 " . $moduleFilter . "
ORDER BY a.sys_module_order asc,b.sys_module_sub_order";
$gdbo -> getRec();
$aModules = $gdbo -> dbData;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>BIFROST SOFTWARE LLC</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<meta http-equiv="Cache control" content="no-cache"/>
	<meta name="COPYRIGHT" content="&copy; 2014 Bifrost Softrware LLC">
<?php
$dirs = array(
$path.'_lib/jquery',
$path.'_lib/js/',
$path.'_thirdparty/miniupload/assets/js',
$path.'_thirdparty/amcharts',
#$path.'_thirdparty/amcharts/exporting',
);
$file_ext = array(
"css",
"js",
);
$files = scanDir::scan($dirs, $file_ext);
$linkedFiles = "";
foreach ($files as $fso) {
$fso = str_replace($serverrootpath,"",$fso);
$ext = trim(end(explode(".", $fso)));
switch ($ext) {
case "js":
$linkedFiles .= '<script type="text/javascript" src="'.$fso.'"></script>';
break;
case "css":
$linkedFiles .= '<link type="text/css" rel="stylesheet" href="'.$fso.'"/>';
break;
default:
break;}}
echo $linkedFiles;
?>	
<link href="/_lib/fontawesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,700,300,600,800,400' rel='stylesheet' type='text/css'>
<link href="/_css/interface.css" rel="stylesheet" type="text/css"/>
<link href="/_thirdparty/syntaxhighlighter_3.0.83/styles/shThemeDefault.css" rel="stylesheet" type="text/css"/>
<link href="/_thirdparty/syntaxhighlighter_3.0.83/styles/shCoreDefault.css" rel="stylesheet" type="text/css"/>
<script src="/_thirdparty/ckeditor/ckeditor.js" type="text/javascript"></script>
<script src="/_thirdparty/syntaxhighlighter_3.0.83/scripts/shCore.js" type="text/javascript"></script>
<script src="/_thirdparty/syntaxhighlighter_3.0.83/scripts/shLegacy.js" type="text/javascript"></script>
<script src="/_thirdparty/syntaxhighlighter_3.0.83/scripts/shBrushJScript.js" type="text/javascript"></script>
<script src="/_thirdparty/syntaxhighlighter_3.0.83/scripts/shBrushXml.js" type="text/javascript"></script>
	<script type="text/javascript">
	$('#interface').height($(window).height());
	function stopRKey(evt) {
		var evt = (evt) ? evt : ((event) ? event : null);
		var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
		if ((evt.keyCode == 13) && (node.type == "text")) {
			return false;
		}
	}
	document.onkeypress = stopRKey;
	</script>
<!--[if gte IE 9]>
<style type="text/css">
.gradient {
filter: none;
}
</style>
<![endif]-->
</head>
<body onresize="resizePopups()" onkeydown="detectSave(this,event)">
	<table id="interface" summary="parent">
		<tr id="row_0">
			<td id="banner">
				<div class="titleLogo">
					<img src="_img/bifrost.png" height="28"/><strong>BIFROST 2.0</strong><span class="beta"> BETA</span>
				</div>
				<div class="flRight logout" style="margin-right:5px">
					<span class="buttonUser"><i class="fa fa-user"></i><?=$fullName_derived ?></span>
					<span class="buttonLogout" onclick="logout()"><i class="fa fa-sign-out"></i>Log Out</span>
				</div>
			</td>
		</tr>
		<tr id="row_1">
			<td id="menu">&nbsp;</td>
		</tr>
		<tr id="row_2">
			<td id="oOptions">&nbsp;</td>
		</tr>
		<tr id="row_3">
			<td id="contentcell">
				<table id="navandcontent" summary="content">
					<tr>
						<td id="nav">
							<div class="navcontainer">
								<?php
								for ($i = 0; $i < count($aModules); $i++) {
									/* append 0 to module id if less than 10 */
									if ($aModules[$i][2] < 10) {
										$modIncID = "0" . $aModules[$i][2];
									} else {$modIncID = $aModules[$i][2];
									}
									$navJumpLink = (is_null($aModules[$i][4]) ? "/_mod/smod_" . $modIncID . "/index.php" : $aModules[$i][4]);
									$noTopMargin = ($i == 0 ? " style=\"margin-top:0px\" " : " ");
									if ($i == 0 || $aModules[$i][0] != $aModules[$i - 1][0]) {
										echo "<div id=\"mod_" . $aModules[$i][0] . "\" class=\"navmod\" $noTopMargin onclick=\"collapseObj('mod_" . $aModules[$i][0] . "_sub','navsubcontainer',null,'navmod',this);\">" . $aModules[$i][1] . "</div>";
										echo "<div id=\"mod_" . $aModules[$i][0] . "_sub\" style=\"display:none\" class=\"navsubcontainer\">";
									}
									if (($i + 1) % 2 == 0) {$alt = "altNavsub";
								} else {$alt = "navsub";
							}
							$icon = ($aModules[$i][5] != "" ? $aModules[$i][5] : "<i class='fa fa-fw'></i>");
							echo "<div id=\"mod_" . $aModules[$i][0] . "_" . $aModules[$i][2] . "\" class=\"$alt\" onclick=\"arc('content','$navJumpLink',null,1,0,1);clearPop(popArray)\">" . $icon . $aModules[$i][3] . "</div>";
							if ($i == (count($aModules) - 1) || $aModules[$i][0] != $aModules[$i + 1][0]) { echo "</div>";
						}
					}
					?>
				</div>
			</td>
			<td id="navbar" onclick="collapseObj('nav');"></td>
			<td id="contentContainer">
				<div id="content">
					<?php
					require ("_mod/smod_15/index.php");
					?>
				</div></td>
			</tr>
		</table></td>
	</tr>
</table>
<script type="text/javascript">
if (BrowserDetect.browser == "Explorer") {
	document.body.setAttribute("onresize", "ieHeightResize()");
}
var menuBar = document.getElementById("menu");
var menuOptions = document.getElementById("oOptions");
var popArray = ["objCalendar"];
</script>
<div id="statusBlock" style="display:none">
	&nbsp;
</div>
<div id="emailBlock" style="display:none">
	&nbsp;
</div>
<div id="objCalendar" style="display:none">
	&nbsp;
</div>
<div id="popWindow" style="display:none">
	&nbsp;
</div>
<div id="popWindowFilter" style="display:none">
	&nbsp;
</div>
</body>
</html>
