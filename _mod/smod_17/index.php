<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once("_lib/php/auth.php");
require_once("_includes/heading.php");
#icoImport = '<span onclick="arc(\'content\',\'_mod/smod_17/import.php\',null,1,1)"><img alt="Bulk Add" style="margin:0 4px" height="16" width="16" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAV5JREFUeNqcU8FOwzAMddJ2HfSyy5CAI9d9QiW+gWnVJHbrp/QPKD/SddoH8CHtDbiN0aWHaUtD7KqhrQSCWopeHD87ju2w7Xb7IaWcVFUF/xHOOViWtYc0TdVQQV9bI0V8fXsnZIwDnjHGtKaAWxadV1KiVdvqTG9vrokH6/Waos3nD6o4HFSwWKhC1CjKUq1Wj7RwTzbNQS4K+tr1TQCOM4Lx+AKckUb3skatu+6Y7MaGqLl1tgzsdmFsu07XsTmhxRk0F+C+zTHFbCvsD9Xvc0wGTTGbdvb1/t4EaIin04nwfD53AhyPR0Pu20iaLvRluVz+2P8gCL7noAkURRHsdjtQOs2nODYXhGEIvu9DnudQFJ8Qx8+dDHj7fbPZDHSfTepClDCdXkGWZeB5HkhZdZ5LgZIkGTzK+HyOn2Ko0CAJIV42m809tahd3d896TeiL87FnV6TgUnsvwQYAFNCY95fzC9+AAAAAElFTkSuQmCC"/>Bulk Add</span>';
#$("#oOptions").append(icoImport);
?>
<div id="list17" class="cList"><?php require("list.php");?></div>
