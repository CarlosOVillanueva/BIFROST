<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once("_lib/php/auth.php");
require_once("_includes/optionstoolbar.php");
# Recordset
$gdbo -> sql = "SELECT
a.id_part,
a.id_part_model,
a.part_serialnumber,
a.part_internalserialnumber,
a.part_isAsset,
b.id_cust_company
FROM _part a 
LEFT JOIN _part_model b
ON a.id_part_model = b.id_part_model
WHERE a.id_part=".$_POST["id_part"];
$gdbo -> getRec();
$rec = $gdbo -> getAssociative();
# Part Manufacturer 
$gdbo -> sql = "SELECT id_cust_company,cust_company FROM _cust_company WHERE id_cust_company_tp = 7 ";
$gdbo -> getRec();
$listCompany = $gdbo -> dbData;
# Dependent Lists
if (isset($rec[0]["id_cust_company"])){
$gdbo -> sql = "SELECT id_part_model, part_model FROM _part_model WHERE id_cust_company=".$rec[0]["id_cust_company"];
$gdbo -> getRec();
$listPartModel = $gdbo -> dbData;}
else {
$listPartModel = array();
}
# Filterable Objects
## Regions
$partmodeldbo = new ArcDb;
$partmodeldbo -> dbConStr=$globalDBCON;
$partmodeldbo -> dbType = $globalDBTP;
$partmodeldbo -> dbSchema = $globalDB;
$partmodeldbo -> sql = "SELECT id_part_model,part_model FROM _part_model";
$partmodeldbo -> dbFilter = "WHERE id_cust_company=";
$partmodeldbo -> type = "list";
$partmodeldbo -> id = "id_part_model";
$partmodeldbo -> attributes = 'class="chPartModel"';
$partmodelJSON =encrypt(json_encode((array)$partmodeldbo));
# Fieldsets
$fs=
array(
array(
array('Manufacturer', 'id_cust_company','onchange="filterRec(this,\''.$partmodelJSON.'\',\'dPartModel\')"',1,$listCompany,$rec[0]["id_cust_company"]),
array('Part', 'id_part_model','disabled=\'disabled\' class=\'chPartModel\'',1,$listPartModel,$rec[0]["id_part_model"],"dPartModel"),
array(null,'id_part',null,3,null,$rec[0]["id_part"]),
array('Fixed Asset?', 'part_isAsset',null,8,null,$rec[0]["part_isAsset"]),
),
array(
array('MFR Serial Number', 'part_serialnumber',null,0,null,$rec[0]["part_serialnumber"]),
array('Internal Serial Number', 'part_internalserialnumber',null,0,null,$rec[0]["part_internalserialnumber"]),
)
);
?>
<form method="post" name="frmUnit" id="frmUnit" action="javascript:submitFrmVals('content','/_mod/smod_17/sql.php','id_part_model,id_cust_company','&form=frmUnit&action=update','frmUnit')">
<fieldset id="Unit">
<legend>Unit</legend>
<?=frmElements($fs);?>
</fieldset>
</form>
<form action="#">
<fieldset id="Files">
<legend>Files</legend>
<div id="listFiles">
<?php
require("listFiles.php");
?>
</div></fieldset></form>
<script type="text/javascript">
oREC = '{"oRec":[{"module":"17","filter":"id_part","recid":"<?=$_POST["id_part"]?>","formname":"frmUnit"}]}';
<?=$icoDelete;?>
<?=$icoUpload;?>
<?=$icoCollapse;?>
$("#oOptions").append(icoDelete+icoCollapse+icoUpload);
collapseTabs();
</script>
