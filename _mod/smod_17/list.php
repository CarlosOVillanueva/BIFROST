<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once("_lib/php/auth.php");
$arctbl = new ArcTbl;
$arctbl -> dbConStr=$globalDBCON;
$arctbl -> dbLimit = 23;
$arctbl -> dbType = $globalDBTP;
$arctbl -> dbSchema = $globalDB;
$arctbl -> recLink= $path."edit.php";
$arctbl -> actionFilterKey="id_part";
$arctbl -> recIndex="id_part";
$arctbl -> ignoreCols=array("id_part","id_part_model");
$arctbl -> recQuery = "
SELECT a.id_part,
a.id_part_model,
a.part_serialnumber as \"MFR Serial\",
a.part_internalserialnumber as \"Internal Serial\",
CASE a.part_isAsset WHEN 1 THEN 'Yes' Else 'No' END as \"Is Asset\",
b.part_model as \"Part\",
c.cust_company as \"Manufacturer\"
FROM _part a
LEFT JOIN
_part_model b
ON a.id_part_model = b.id_part_model 
LEFT JOIN
_cust_company c
ON
b.id_cust_company = c.id_cust_company";
$arctbl -> actionDestination = "content";
$arctbl -> ajDestination = "list17";
$arctbl -> ajPage = $path."list.php";
$arctbl -> build();
echo hex2str($arctbl -> tblNav);
echo $arctbl->dataTable;
?>
