<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once("_lib/php/auth.php");
require_once("_includes/optionstoolbar.php");
# Part Manufacturer 
$gdbo -> sql = "SELECT id_cust_company,cust_company FROM _cust_company WHERE id_cust_company_tp = 7 ";
$gdbo -> getRec();
$listCompany = $gdbo -> dbData;
# Filterable Objects
## Regions
$partmodeldbo = new ArcDb;
$partmodeldbo -> dbConStr=$globalDBCON;
$partmodeldbo -> dbType = $globalDBTP;
$partmodeldbo -> dbSchema = $globalDB;
$partmodeldbo -> sql = "SELECT id_part_model,part_model FROM _part_model";
$partmodeldbo -> dbFilter = "WHERE id_cust_company=";
$partmodeldbo -> type = "list";
$partmodeldbo -> id = "id_part_model";
$partmodeldbo -> attributes = 'class="chPartModel"';
$partmodelJSON =encrypt(json_encode((array)$partmodeldbo));
# Fieldsets
$fs=
array(
array(
array('Manufacturer', 'id_cust_company','onchange="filterRec(this,\''.$partmodelJSON.'\',\'dPartModel\')"',1,$listCompany),
array('Part', 'id_part_model','disabled=\'disabled\' class=\'chPartModel\'',1,null,null,"dPartModel"),
array('Fixed Asset?', 'part_isAsset',null,8,null,1),
),
array(
array('MFR Serial Number', 'part_serialnumber',null,0),
array('Internal Serial Number', 'part_internalserialnumber',null,0),
)
);
?>
<script type="text/javascript">$(".icoadd").remove();</script>
<form method="post" name="frmUnit" id="frmUnit" action="javascript:submitFrmVals('content','/_mod/smod_17/sql.php','id_part_model,id_cust_company','&form=frmUnit&action=insert','frmUnit')">
<fieldset id="Unit">
<legend>Unit</legend>
<?=frmElements($fs);?>
</fieldset>
</form>
