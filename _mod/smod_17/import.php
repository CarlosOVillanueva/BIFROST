<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once("_lib/php/auth.php");
require_once("_includes/optionstoolbar.php");
# Part Manufacturer 
$gdbo -> sql = "SELECT id_cust_company,cust_company FROM _cust_company WHERE id_cust_company_tp = 7 ";
$gdbo -> getRec();
$listCompany = $gdbo -> dbData;
# Filterable Objects
## Part Models
$partmodeldbo = new ArcDb;
$partmodeldbo -> dbConStr=$globalDBCON;
$partmodeldbo -> dbType = $globalDBTP;
$partmodeldbo -> dbSchema = $globalDB;
$partmodeldbo -> sql = "SELECT id_part_model,part_model FROM _part_model";
$partmodeldbo -> dbFilter = "WHERE id_cust_company=";
$partmodeldbo -> type = "list";
$partmodeldbo -> id = "id_part_model";
$partmodeldbo -> attributes = 'class="chPartModel"';
$partmodelJSON =encrypt(json_encode((array)$partmodeldbo));
$fs=array(
array(
array('Manufacturer', 'id_cust_company','onchange="filterRec(this,\''.$partmodelJSON.'\',\'dPartModel\')"',1,$listCompany),
array('Part', 'id_part_model','disabled=\'disabled\' class=\'chPartModel\'',1,null,null,"dPartModel"),
array('Fixed Asset?', 'part_isAsset',null,8,null,1),
),
array(
array('Generate', 'mmQuantity',null,0),
array(null,null,'onclick="mkMMRows()"',2,null,'Generate'),
)
);
function getBaseElement(){
$elements=
array(
array(
array('MFR Serial Number',null,null,0,null,null,null,null,'id="x1_row" style="display:none"'),
array('Internal Serial Number',null,null,0),
)
);
$baseElements ="";
$baseElements.= frmElements($elements);
return $baseElements;
}
function getFooter() {
$elements = 
array(
array(
array(null,'part_mText',null,6,null,null,null,null,' style="display:none" '),
)
);
$footerElements=frmElements($elements);
return $footerElements;
}
$fieldset = '<fieldset>';
$fieldset .= '<legend>Static Elements</legend>';
$fieldset .= frmElements($fs);
$fieldset .= '</fieldset>';
$fieldset .= '<fieldset class="multiRow" style="margin-top:10px">';
$fieldset .= '<legend>Unit Import Form</legend>';
$fieldset .= '<div class="multimenu">';
$fieldset .= '<input type="button" id="mmAddButton" onClick="multiRowAddElement(this,\'frmrow\')" value="Add"/>';
$fieldset .= '<input type="button" onClick="multiRowRemoveLastElement(this,\'frmrow\')" value="Remove Last"/>';
$fieldset .= '</div>';
$fieldset .= getBaseElement();
$fieldset .= getFooter();
$fieldset .= '</fieldset>';
echo $fieldset;
?>
