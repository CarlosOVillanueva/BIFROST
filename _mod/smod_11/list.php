<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once("_lib/php/auth.php");
if (isset($_POST["filterResults"])){
$filter=" AND ".hex2str($_POST["filterResults"]);
}else {
$filter="";
}
$arctbl = new ArcTbl;
$arctbl -> dbConStr=$globalDBCON;
$arctbl -> dbOffset = 0;
$arctbl -> dbLimit = 23;
$arctbl -> dbType = $globalDBTP;
$arctbl -> dbSchema = $globalDB;
$arctbl -> recLink= $path."edit.php";
$arctbl -> actionFilterKey="id_tm";
$arctbl -> recIndex="id_tm";
$arctbl -> recFilter=" WHERE id_hr_emp=".$id_hr_emp_derived." AND srv_dr is null".$filter;
$arctbl -> ignoreCols=array("id_tm","Charge ID","Service ID","id_hr_emp","srv_dr");
$arctbl -> ignoreFilterCols=array("id_tm","Charge ID","Service ID","id_hr_emp","srv_dr");
$arctbl -> recQuery = "
SELECT
a.id_tm,
concat(c.cust_contact_familyName,',',c.cust_contact_givenName) as \"Employee\",
if(a.id_srv is null,'Overhead',d.srv_summary) \"Summary\",
FROM_UNIXTIME(a.tm_ts+".$gmtOffset.",'%Y-%m-%d') \"Start Date\",
FROM_UNIXTIME(a.tm_ts+".$gmtOffset.",'%H:%i:%s') \"Start Time\",
FROM_UNIXTIME(a.tm_te+".$gmtOffset.",'%Y-%m-%d') \"End Date\",
FROM_UNIXTIME(a.tm_te+".$gmtOffset.",'%H:%i:%s') \"End Time\",
cast((a.tm_te-a.tm_ts-(a.tm_deduction*3600))/3600 as DECIMAL(12,2)) as \"Actual\",
a.id_tm_charge as \"Charge ID\",
a.id_srv as \"Service ID\",
b.id_hr_emp,
d.srv_dr
FROM _tm a
LEFT JOIN
_hr_emp b ON a.id_hr_emp=b.id_hr_emp
LEFT JOIN
_cust_contact c ON b.id_cust_contact=c.id_cust_contact
LEFT JOIN
_srv d ON a.id_srv=d.id_srv
ORDER BY a.tm_ts desc";
$arctbl -> actionDestination = "content";
$arctbl -> ajDestination = "timeEntries";
$arctbl -> ajPage = $path."list.php";
$arctbl -> build();
echo hex2str($arctbl -> tblNav);
echo $arctbl->dataTable;
?>
