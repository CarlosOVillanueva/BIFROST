<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once("_lib/php/auth.php");
filterQryPost("monthID");
filterQryPost("yearID");
if ($payFrequency == "") {
$gdbo -> sql = "SELECT hr_emp_payFrequency FROM _hr_emp WHERE id_hr_emp=".$rec[0]["id_hr_emp"];
$gdbo -> getRec();
$payFrequencyResults = $gdbo -> dbData;
$payFrequency = $payFrequencyResults[0][0];
}
//*************************************/
/* week days */
/*************************************/
$weekdays = array();
$weekdays[] = "Monday";
$weekdays[] = "Tuesday";
$weekdays[] = "Wednesday";
$weekdays[] = "Thursday";
$weekdays[] = "Friday";
$weekdays[] = "Saturday";
$weekdays[] = "Sunday";
/*************************************/
/* years */
/*************************************/
$gdbo -> sql = "SELECT tm_year FROM _tm_year ORDER BY tm_year desc";
$gdbo -> getRec();
$recYears = $gdbo -> dbData;
$years = array();
foreach($recYears as $row => $data) {
	$years[]=array($data[0],$data[0]);
}
/*************************************/
/* application variables */
/*************************************/
$curYear = date('Y',strtotime($dtTimeCurrent));
$curDay = date('j',strtotime($dtTimeCurrent));
$curMonth = date('n',strtotime($dtTimeCurrent));
$yearID = (isset($yearID) && $yearID!="" ? $yearID : $curYear);
$monthID = (isset($monthID) && $monthID!="" ? $monthID : $curMonth);
$firstDayofMonth = date("l", mktime(0, 0, 0, $monthID, 1, $yearID));
$dayCount = cal_days_in_month(CAL_GREGORIAN, $monthID, $yearID);
$todayTime = strtotime("$yearID-$monthID-$curDay 00:00:00 ");
/*************************************/
/* application arrays */
/*************************************/
$aWeeks = array();
/*************************************/
switch($payFrequency) {
/*************************************/
case "bimonthly":
for ($i=1;$i<13;$i++) {
$lastDay=cal_days_in_month(CAL_GREGORIAN, $i, $yearID); 
# Get First Week 
$aWeeks[]=array(
strtotime($yearID."-".$i."-1"),
date("m/d/Y H:i:s", strtotime($yearID."-".$i."-1")),
date("m/d/Y H:i:s", strtotime($yearID."-".$i."-15 23:59:59")),
(strtotime($yearID."-".$i."-15 23:59:59")-strtotime($yearID."-".$i."-1"))/86400,
strtotime($yearID."-".$i."-15 23:59:59")
);
# Get Second Week
$aWeeks[]=array(
strtotime($yearID."-".$i."-16"),
date("m/d/Y H:i:s", strtotime($yearID."-".$i."-15 23:59:59")),
date("m/d/Y H:i:s", strtotime($yearID."-".$i."-$lastDay 23:59:59")),
(strtotime($yearID."-".$i."-$lastDay 23:59:59")-strtotime($yearID."-".$i."-16"))/86400,
strtotime($yearID."-".$i."-$lastDay 23:59:59")
);
}
break;
/*************************************/
case "monthly":
for ($i=1;$i<13;$i++) {
$lastDay=cal_days_in_month(CAL_GREGORIAN, $i, $yearID); 
# Get First Week 
$aWeeks[]=array(
strtotime($yearID."-".$i."-1"),
date("m/d/Y H:i:s", strtotime($yearID."-".$i."-1")),
date("m/d/Y H:i:s", strtotime($yearID."-".$i."-$lastDay 23:59:59")),
cal_days_in_month(CAL_GREGORIAN, $monthID, $yearID),
strtotime($yearID."-".$i."-$lastDay 23:59:59")
);
}
break;
/*************************************/
case "biweekly":
$bwFirstDay = date("l", mktime(0, 0, 0, 1, 1, $yearID));
$wOffset = (7 - (array_search($bwFirstDay, $weekdays))) * 86400;
$w1Offset = $wOffset;

$fiscalStart = $sysCompany[0]["sys_company_fiscalYear"];

/*
if ($fiscalStart != "" && !isset($_POST["yearID"])) {
	$yearID = date("Y",strtotime($fiscalStart));
	$mm = date("m",strtotime($fiscalStart));
	$dd = date("d",strtotime($fiscalStart));
} else {
	$yearID =$yearID;
	$mm = 01;
	$dd = 01;
}
*/
	$yearID =$yearID;
	$mm = 01;
	$dd = 01;

$aWeeks[] = array(strtotime($yearID . "-" . $mm . "-" . $dd), date("m/d/Y H:i:s", strtotime($yearID . "-" . $mm . "-" . $dd)), date("m/d/Y H:i:s", strtotime($yearID . "-" . $mm . "-" . $dd) - 1 + $wOffset), 7 - (array_search($bwFirstDay, $weekdays)), strtotime($yearID . "-" . $mm . "-" . $dd) - 1 + $wOffset);
$startDate = strtotime($yearID . "-" . $mm . "-" . $dd) + $wOffset;
$wOffset = 0;
$aWeeks[] = array($startDate, date("m/d/Y H:i:s", $startDate), date("m/d/Y H:i:s", $startDate - 1 + (7 * 86400)), 7, $startDate - 1 + (7 * 86400));
while ($wOffset + (7 * 86400) < ((date("z", mktime(23, 59, 59, 12, 31, $yearID)))) * 86400) {
$wOffset = $wOffset + (7 * 86400);
$periodStartString = $startDate;
if (date("I", $periodStartString + $wOffset) == 1) {
$periodStart = date("m/d/Y H:i:s", $periodStartString + $wOffset - 3600);
} else {
$periodStart = date("m/d/Y H:i:s", $periodStartString + $wOffset);
}
$inc = 0;
while ($inc < 7 && ($periodStartString + $wOffset + ($inc * 86400)) < strtotime(($yearID + 1) . "-01-01 00:00:00")) {
$inc++;
}
if (date("I", $periodStartString + $wOffset + ($inc * 86400) - 1) == 1) {
$periodEnd = date("m/d/Y H:i:s", $periodStartString + $wOffset + ($inc * 86400) - 1 - 3600);
} else {
$periodEnd = date("m/d/Y H:i:s", $periodStartString + $wOffset + ($inc * 86400) - 1);
}
$aWeeks[] = array($startDate + $wOffset, $periodStart, $periodEnd, $inc, strtotime($periodEnd));
}
break;
case "weekly":
$bwFirstDay = date("l", mktime(0, 0, 0, 1, 1, $yearID));
$wOffset = (7 - (array_search($bwFirstDay, $weekdays))) * 86400;
$w1Offset = $wOffset;
$mm = 01;
$dd = 01;
$aWeeks[] = array(strtotime($yearID . "-" . $mm . "-" . $dd), date("m/d/Y H:i:s", strtotime($yearID . "-" . $mm . "-" . $dd)), date("m/d/Y H:i:s", strtotime($yearID . "-" . $mm . "-" . $dd) - 1 + $wOffset), 7 - (array_search($bwFirstDay, $weekdays)), strtotime($yearID . "-" . $mm . "-" . $dd) - 1 + $wOffset);
$startDate = strtotime($yearID . "-" . $mm . "-" . $dd) + $wOffset;
$wOffset = 0;
$aWeeks[] = array($startDate, date("m/d/Y H:i:s", $startDate), date("m/d/Y H:i:s", $startDate - 1 + (7 * 86400)), 7, $startDate - 1 + (7 * 86400));
while ($wOffset + (7 * 86400) < ((date("z", mktime(23, 59, 59, 12, 31, $yearID)))) * 86400) {
$wOffset = $wOffset + (7 * 86400);
$periodStartString = $startDate;
if (date("I", $periodStartString + $wOffset) == 1) {
$periodStart = date("m/d/Y H:i:s", $periodStartString + $wOffset - 3600);
} else {
$periodStart = date("m/d/Y H:i:s", $periodStartString + $wOffset);
}
$inc = 0;
while ($inc < 7 && ($periodStartString + $wOffset + ($inc * 86400)) < strtotime(($yearID + 1) . "-01-01 00:00:00")) {
$inc++;
}
if (date("I", $periodStartString + $wOffset + ($inc * 86400) - 1) == 1) {
$periodEnd = date("m/d/Y H:i:s", $periodStartString + $wOffset + ($inc * 86400) - 1 - 3600);
} else {
$periodEnd = date("m/d/Y H:i:s", $periodStartString + $wOffset + ($inc * 86400) - 1);
}
$aWeeks[] = array($startDate + $wOffset, $periodStart, $periodEnd, $inc, strtotime($periodEnd));
}
break;
}
?>
