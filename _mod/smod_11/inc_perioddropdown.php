<!--
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
-->
<label>Period</label>
<?php
	echo "<select id='optionPeriod' class='elementIcon'>";
	for ($i = 0; $i < count($aWeeks); $i++) {
		if ($payFrequency == "biweekly") {
			if ($i == 0 || $i % 2 == 0) {
				$inc = ($i == 0 ? 1 : 2);

				if ($i == count($aWeeks) - 1) {
					$end = $i;
				} else {$end = $i + 1;
				}
				
				$periodList = (($i + $inc) / $inc);
				$periodList = ($periodList < 10 ? "0" . (string)$periodList : $periodList);

				$curValue = ($periodLabel) . "." . $periodStart . "." . ($periodEnd);
				$loopValue = (($i + $inc) / $inc) . "." . $aWeeks[$i][0] . "." . ($aWeeks[$end][4]);
				$selected = ($curValue == $loopValue ? "selected = 'selected' " : "");
				echo "<option $selected value=\"" . bin2hex($loopValue) . "\">[ " . $periodList . " ] " . date("F d", $aWeeks[$i][0]) . " - " . date("d", $aWeeks[$end][4] - 1) . "</option>";
			}
		}
		/****************************************************************************/
		if ($payFrequency == "bimonthly" || $payFrequency == "monthly" || $payFrequency == "weekly") {
			$periodList = (($i + $inc) / $inc);
			$periodList = ($periodList < 10 ? "0" . (string)$periodList : $periodList);
			
			$inc = 1;
			$curValue = ($periodLabel) . "." . $aWeeks[$i][0] . "." . ($aWeeks[$i][4]);
			$loopValue = (($i + $inc) / $inc) . "." . $aWeeks[$i][0] . "." . ($aWeeks[$i][4]);
			$selected = ($curValue == $loopValue ? "selected = 'selected' " : "");
			echo "<option $selected value=\"" . bin2hex($loopValue) . "\">[ " . $periodList . " ] " . date("F d", $aWeeks[$i][0]) . " - " . date("d", $aWeeks[$i][4] - 1) . "</option>";
		}

	}

	echo "</select>";
?>
<div class="elementIconBox" onclick="arc('timecard','<?=$path ?>timecardfiltered.php','period='+document.getElementById('optionPeriod').options[document.getElementById('optionPeriod').selectedIndex].value)"><i class="fa fa-binoculars"></i></div>
