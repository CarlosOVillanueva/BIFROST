<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once ("_lib/php/auth.php");
$timecard = preg_replace("/onclick=\".+?\"|onmouseover=\".+?\"|onmouseout=\".+?\"|style=\".+?\"/i", "", hex2str($_POST["data"]));
$gdbo= new ArcDb;
$gdbo-> dbConStr=$globalDBCON;
$gdbo-> dbSchema = $globalDB;
$gdbo-> dbType = $globalDBTP;
/*Get Employee Data*/
$gdbo-> sql = "
SELECT
a.hr_emp_timeApprover
FROM _hr_emp a WHERE id_hr_emp=$id_hr_emp_derived AND a.hr_emp_timeApprover is not null;";
$gdbo-> getRec();
$employee = $gdbo-> getAssociative();
if ($gdbo-> dbRows > 0) {
$gdbo-> sql = "
SELECT 
a.id_sys_user,
a.sys_user_tmEmail,
b.cust_contact_givenName
FROM _sys_user a
LEFT JOIN _sys_user_emp c ON a.id_sys_user=c.id_sys_user
LEFT JOIN _cust_contact b ON c.id_cust_contact=b.id_cust_contact
WHERE b.id_cust_contact=" . $employee[0]["hr_emp_timeApprover"];
$gdbo-> getRec();
$approver = $gdbo-> getAssociative();
$gdbo-> sql = "
SELECT 
id_tm_timesheet,
id_hr_emp,
tm_timesheet_ds,
tm_timesheet_de,
tm_timesheet_approved
FROM _tm_timesheet
WHERE tm_timesheet_ds=" . $_POST["datestart"] . " AND tm_timesheet_de=" . $_POST["dateend"] . " AND id_hr_emp=" . $id_hr_emp_derived;
$gdbo-> getRec();
$timesheet = $gdbo-> getAssociative();
$timesheetCount = $gdbo-> dbRows;
if ($timesheetCount == 0) {
$gdbo-> dbTable = "_tm_timesheet";
$_POST["tm_timesheet_ds"] = $_POST["datestart"];
$_POST["tm_timesheet_de"] = $_POST["dateend"];
$_POST["id_hr_emp"] = $id_hr_emp_derived;
$_POST["tm_timesheet"] = bin2hex($timecard);
$gdbo-> insertRec();
$recordID = $gdbo-> insertedID;
$alert = "<div class='timesheetstatus'>The following timesheet has just been submitted to your time approver:</div>";
$sendmail = TRUE;
} else {
$recordID = $timesheet[0]["id_tm_timesheet"];
switch ($timesheet[0]["tm_timesheet_approved"]) {
case 0 :
/*Not Yet Reviewed*/
$alert = "<div class='timesheeterror'>This timesheet has already been submitted for approval.</div>";
$sendmail = FALSE;
break;
case 1 :
/*Approved*/
$alert = "<div class='timesheeterror'>This timesheet has already been approved.</div>";
$sendmail = FALSE;
break;
case 2 :
/*Rejected*/
$_POST["tm_timesheet"] = bin2hex($timecard);
$_POST["tm_timesheet_du"] = strtotime($dtTimeCurrent);
$_POST["tm_timesheet_approved"] = 0;
$alert = "<div class='timesheeterror'>Your timesheet was just recently rejected. Resending updated timesheet to your approver.</div>";
$gdbo-> dbTable = "_tm_timesheet";
$gdbo-> updateRec("id_tm_timesheet=" . $timesheet[0]["id_tm_timesheet"]);
$sendmail = TRUE;
break;
default :
break;
}
}
$message = "
<style>div,td,caption,th{font-family:sans-serif;font-size:10pt}td{padding:5px;border:1px solid #000}table{margin:auto;border-collapse:collapse}</style>
<p>Please review the following timecard for $fullName_derived. You can approve/reject this timecard by logging into your portal.</p>
$timecard
<p>If you believe you have received this message in error, please contact us at info@bifrostapps.com</p>
<p style=\"font-size:8pt;text-align:center;border-top:1px dotted #dedede\">To view this properly, HTML e-mail must be enabled.<p>
";
$message = wordwrap($message, 70);
$subject = "TIME APPROVAL REQUEST | $fullName_derived |" . encrypt($recordID);
$headers = array();
$headers[] = "MIME-Version: 1.0";
$headers[] = "Content-type: text/html; charset=iso-8859-1";
$headers[] = "From: Bifrost Software LLC <donotreply@bifrostapps.com>";
$headers[] = "Subject: {$subject}";
$headers[] = "X-Mailer: PHP/" . phpversion();
// Send
if ($sendmail == TRUE) {
mail($approver[0]["sys_user_tmEmail"], $subject, $message, implode("
", $headers), "-f donotreply@bifrostapps.com");
}
} else {
$alert = "You cannot submit a timesheet at this time. Please contact HR and have a Timesheet Approver assigned to your profile.";
$timecard = "";
}
?>
<div id="popBox">
<form method="POST" action="#">
<fieldset>
<legend>
Time Sheet Summary
</legend>
<div class="frmRow" style="margin-bottom:20px">
<?=$alert ?>
</div>
<div class="frmRow">
<?=$timecard ?>
</div>
<input type="button" onclick="clearPop();arc('content','/_mod/smod_11/index.php','datestart=<?=$_POST["datestart"]?>&dateend=<?=$_POST["dateend"]?>',1,1)" value="Close"/>
</fieldset>
</form>
</div>
