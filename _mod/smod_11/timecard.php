<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/*************************************/
require_once("_lib/php/auth.php");
/*************************************/
if(isset($payFrequency)){
/*************************************/
require_once("inc_period.php");
/*************************************/
/* Debugging Tools */
/*************************************/
$debug=0;
if($debug==1){
echo "<div style='width:100%;height:200px;overflow:auto;'>";
echo "<div>";
printArray($aWeeks);
echo "</div>";
echo "</div>";}
/*************************************/
require_once("inc_presentation.php");
/*************************************/
/* Database Connection */
/*************************************/
$arc = new ArcDb;
$arc -> dbType = $globalDBTP;
$arc -> dbSchema = $globalDB;
$arc -> dbConStr=$globalDBCON;
/*************************************/
/* Determine if timecard has already been submitted
/*************************************/
$arc -> sql="
SELECT
id_tm_timesheet,
tm_timesheet_ds,
tm_timesheet_de,
id_hr_emp,
tm_timesheet_approved
FROM
_tm_timesheet WHERE
id_hr_emp=".$id_hr_emp_derived." AND
tm_timesheet_ds=".$periodStart." AND
tm_timesheet_de=".$periodEnd;
$arc->getRec();
$timesheet=$arc->getAssociative();
#$timesheet[0]["tm_timesheet_approved"];
/*************************************/
/* Determine State of Timecard
/*************************************/
if ($arc->dbRows>0){
switch ($timesheet[0]["tm_timesheet_approved"]) {
case 0:
$status="Submitted";
break;
case 1:
$status="Approved";
break;
case 2:
$status="Rejected";
break;
default:
break; 
}
} else {
$status="Open";
}
/*************************************/
/* Find all time entries that fall within this time period
/*************************************/
$gdbo -> sql = "
SELECT * FROM (
SELECT
a.id_srv,
a.id_tm_charge,
a.id_tm,
if(a.id_srv is null,'Overhead',c.srv_summary) as \"Type\",
UNIX_TIMESTAMP(FROM_UNIXTIME(a.tm_ts+$gmtOffset,'%Y-%m-%d')) as \"DS\",
UNIX_TIMESTAMP(FROM_UNIXTIME(a.tm_te+$gmtOffset,'%Y-%m-%d')) as \"DE\",
if(UNIX_TIMESTAMP(FROM_UNIXTIME(a.tm_ts+$gmtOffset,'%Y-%m-%d')) <> UNIX_TIMESTAMP(FROM_UNIXTIME(a.tm_te+$gmtOffset,'%Y-%m-%d')),0,1) as \"DE_BIT\",
if(UNIX_TIMESTAMP(FROM_UNIXTIME(a.tm_ts+$gmtOffset,'%Y-%m-%d'))not between $periodStart AND $periodEnd,0,1) as \"P_BIT\",
a.tm_ts+$gmtOffset as \"DST_UNIX\",
a.tm_te+$gmtOffset as \"DET_UNIX\",
(a.tm_te-a.tm_ts-(a.tm_deduction*3600))/3600 as \"Length\",
if(a.id_srv is null,concat((SELECT z.cust_company FROM _cust_company z WHERE z.id_cust_company=1),' | ',b.tm_charge),concat((SELECT z.cust_company FROM _cust_company z WHERE z.id_cust_company=c.id_cust_company),' | ',b.tm_charge)) as \"Company\",
a.tm_ts+$gmtOffset,
a.tm_te+$gmtOffset,
a.id_hr_emp
FROM _tm a
LEFT JOIN
_tm_charge b ON a.id_tm_charge=b.id_tm_charge
LEFT JOIN
_srv c ON a.id_srv = c.id_srv
where a.id_hr_emp=$id_hr_emp_derived AND c.srv_dr is null
) as derived
where
DS between $periodStart AND $periodEnd OR
DE between $periodStart AND $periodEnd
ORDER BY Company,id_srv,id_tm_charge
";
$gdbo -> getRec();
$aTimeCard = $gdbo -> dbData;
/*************************************/
$dTimeCard = array();
/* break apart timecard into daily time card */
foreach ($aTimeCard as $key => $te) {
if ($te[7] == 0) {
/* record was created in a previous pay period */
if ($te[6] == 0) {
/* record ends on a date other than start */
$fstart = $te[4];
$fend = $fstart + 86400;
$fdiff = $fend - $te[8];
$dTimeCard[] = array($te[0], $te[1], $te[2], $te[3], $te[4], $te[5], $te[8], $te[9], $fdiff / 3600, $te[11]);
$remainder = $te[9] - $fend;
if ($remainder > 86400) {
while ($remainder > 86400) {
$dTimeCard[] = array($te[0], $te[1], $te[2], $te[3], $fend, $fend, $fend, $te[9], 24, $te[11]);
$fend = $fend + 86400;
$remainder = $te[9] - $fend;
}
if ($remainder > 0) {
$dTimeCard[] = array($te[0], $te[1], $te[2], $te[3], $fend, $fend, $fend, $te[9], $remainder / 3600, $te[11]);
}
} else {
$dTimeCard[] = array($te[0], $te[1], $te[2], $te[3], $fend, $fend, $fend, $te[9], $remainder / 3600, $te[11]);
}
}
} else {
if ($te[6] == 0) {
/* record ends on a date other than start */
$fstart = $te[4];
$fend = $fstart + 86400;
/*GMT DAY TIME+6 HOURS + 24 Hours*/
$fdiff = $fend - $te[8];
$dTimeCard[] = array($te[0], $te[1], $te[2], $te[3], $te[4], $te[5], $te[8], $te[9], $fdiff / 3600, $te[11]); 
$remainder = $te[9] - $fend;
if ($remainder > 86400) {
while ($remainder > 86400) {
$dTimeCard[] = array($te[0], $te[1], $te[2], $te[3], $fend, $fend, $fend, $te[9], 24, $te[11]);
$fend = $fend + 86400;
$remainder = $te[9] - $fend;
}
if ($remainder > 0) {
$dTimeCard[] = array($te[0], $te[1], $te[2], $te[3], $fend, $fend, $fend, $te[9], $remainder / 3600, $te[11]);
}
} else {
$dTimeCard[] = array($te[0], $te[1], $te[2], $te[3], $fend, $fend, $fend, $te[9], $remainder / 3600, $te[11]);
}
} else {
/* normal daily record */
$dTimeCard[] = array($te[0], $te[1], $te[2], $te[3], $te[4], $te[5], $te[8], $te[9], $te[10], $te[11]);
}
}
}
#printArray($dTimeCard);
/* * ****************************************************** */
/* sort results */
foreach ($dTimeCard as $key => $row) {
$g0[$key] = $row[0];
$g1[$key] = $row[1];
$g2[$key] = $row[2];
$g3[$key] = $row[3];
$g4[$key] = $row[4];
$g5[$key] = $row[5];
$g6[$key] = $row[6];
$g7[$key] = $row[7];
$g8[$key] = $row[8];
$g9[$key] = $row[9];
}
if (count($dTimeCard > 0)) {
#array_multisort($g0, SORT_ASC, $g1, SORT_ASC, $g2, SORT_ASC, $dTimeCard);
}
/* group results */
$group = array();
$k = -1;
foreach ($dTimeCard as $key => $row) {
if ($key == 0 || ($dTimeCard[$key][0] != $dTimeCard[$key - 1][0]) || ($dTimeCard[$key][1] != $dTimeCard[$key - 1][1])) {
$k = $k + 1;
$group[$k][] = $dTimeCard[$key];
} else {
$group[$k][] = $dTimeCard[$key];
}
}
/* * ****************************************************** */
#printArray($group);
#printArray($aWeeks);
/* * ****************************************************** */
if ($debug == 1) {
echo "<br/>";
echo "Days in Period: " . $periodLength;
echo "<br/>";
echo "Start of Period: " . $periodMarker . " [ " . date("Y-m-d", $periodMarker) . " ]";
echo "<br/>";
echo "Weeks in $yearID: " . count($aWeeks);
echo "<br/>";
echo "BiWeekly Calc for $yearID: " . count($aWeeks) / 2;
echo "<br/>";
echo "Current Month: " . date("F", mktime(0, 0, 0, $monthID));
echo "<br/>";
echo "# Days in Month: " . $dayCount;
echo "<br/>";
echo "First Day of Month: " . $firstDayofMonth;
echo "<br/>";
echo "Current Day: " . date("l", mktime(0, 0, 0, $curMonth, $curDay, $curYear));
echo "<br/>";
echo "Days in Current Year:" . (date("z", mktime(0, 0, 0, 12, 31, $yearID)) + 1);
echo "<br/>";
echo "First Day in Year:" . $bwFirstDay;
echo "<br/>";
echo "Week 1 Offset: " . ($w1Offset / 86400) . " day(s)";
echo "<br/>";
}
/* foreach($payPeriods as $key=>$period){
echo "Period: ".$period[0]." ".$period[1]."<br/>";
} */
?>
<div class="cStatus<?=$status;?>" id="tcStatusBlock"><i class="fa fa-info-circle"></i><strong>Status:</strong> <?=$status?></div>
<div class="cList">
<div class="frmrow">
<div class="frmcol">
<label>Year</label>
<?= selList($years,"year","arc('vpPayPeriod','".$path."payperiodfiltered.php','yearID='+this.options[this.selectedIndex].value,0,0)",null,null,null,$yearID,true)?>
</div>
<div class="frmcol" id="vpPayPeriod">
<?php
if ($payFrequency=="biweekly") {
$periodLabel=$periodKey / 2 + 1;
}else {
$periodLabel=$periodKey+1;
}
require_once("_mod/smod_11/inc_perioddropdown.php");
?>
</div>
</div>
<div id="timecard">
<table class="dataGrid" id="tblPayPeriod">
<tr>
<th>Summary</th>
<th>Total</th>
<?php 
$wkdCount = 1;
$loopWkd=$periodStart;
while ($loopWkd < $periodEnd) {;
if (date("m/d",$todayTime)==date("m/d", $loopWkd)) {$hToday="class='thActive'";}else{$hToday="";}
$datefilterresults=bin2hex("`Start Date` = '".date("Y-m-d",$loopWkd)."'");
echo "<th $hToday onclick=\"arc('list11','" . $path . "list.php','filterResults=$datefilterresults',1,1)\"> " . date("D", $loopWkd) . "<div class='payPeriodDay'>" . date("m/d", $loopWkd) . "</div></th>";
$loopWkd = $loopWkd + 86400;
$wkdCount = $wkdCount + 1;
}
?>
</tr>
<?php
if (count($group) > 0) {
$timecardTotal=0;
foreach ($group as $key =>$entry){
foreach($entry as $col)
$timecardTotal=$timecardTotal+$col[8];
}
$timecardTotal=round($timecardTotal,2);
foreach ($group as $key => $entry) {
$rowTotal=0; 
{
foreach($entry as $col)
$rowTotal=$col[8]+$rowTotal; 
}
$rowTotal=round($rowTotal,2);
echo "<tr class=\"tmRow\"><td class=\"tmSummary\">" . $entry[0][9] . ' | <strong>' . $entry[0][3] . "</strong></td><td class=\"clRowTotal\">$rowTotal</td>";
$loopTe = $periodStart;
while ($loopTe < $periodEnd) {
$total = 0;
$ajaxFilter = "";
$seed = 0;
foreach ($entry as $col) {
if ($col[4] == $loopTe) {
if ($seed == 0) {
$cOr = "";
} else {
$cOr = " OR ";
}
$total = round($col[8] + $total, 3);
$ajaxFilter .= " $cOr id_tm=$col[2]";
$seed = $seed + 1;
} else {
$total = $total;
$ajaxFilter = $ajaxFilter;
}
}
if ($ajaxFilter != "") {
$ajaxFilter = bin2hex($ajaxFilter);
$tmJump = " onclick=\"arc('list11','" . $path . "list.php','filterResults=$ajaxFilter',1,1)\" onmouseout=\"updateClass('timeEntryMT',this)\" onmouseover=\"updateClass('timeEntryMR',this)\"";
} else {
if(!isset($timesheet[0]["tm_timesheet_approved"]) || $timesheet[0]["tm_timesheet_approved"]!=0 && $timesheet[0]["tm_timesheet_approved"]!=1){
$tmJump = ' onclick="arc(\'content\',\''.$path.'add.php\',\'tm_timesheet_ds='.$loopTe.'&id_srv='.$entry[0][0].'\',0,0)" onmouseout="updateClass(\'timeEntryMT\',this)" onmouseover="updateClass(\'timeEntryMR\',this)" ';
$total = "+";}
else{
$tmJump="";
$total='<i class="fa fa-lock"></i>';
}
}
echo "<td $tmJump >$total</td>";
$loopTe = $loopTe + 86400;
}
echo "</tr>";
}
echo '<tr class="tmTotalRow"><td class="tmDailyTotals">Totals</td><td class="clRowTotal">'.$timecardTotal.'</td>';
$totalGroup=array();
for($i=0;$i<count($group);$i++){
for($ii=0;$ii<count($group[$i]);$ii++){
$totalGroup[]=array($group[$i][$ii][4],$group[$i][$ii][8]);
}
} 
$subTotal = $periodStart;
while ($subTotal < $periodEnd) {
$total = 0;
foreach ($totalGroup as $col => $sub) {
if ($sub[0] == $subTotal) {
$total = round($sub[1] + $total, 3);
} else {
$total = $total;
}
}
$total=($total!=0?$total:"");
echo "<td>$total</td>";
$subTotal = $subTotal + 86400;
}
echo "</tr>";
} else {
$gdbo -> sql = "
SELECT cust_company,(SELECT tm_charge FROM _tm_charge where id_tm_charge=1) FROM _cust_company WHERE id_cust_company=1";
$gdbo -> getRec();
$aTmDefault = $gdbo -> dbData;
$rowTotal=0;
echo "<tr class=\"tmRow\"><td class=\"tmSummary\">" . $aTmDefault[0][0] . " | ".$aTmDefault[0][1]." | <strong>Overhead</strong></td><td>$rowTotal</td>";
$loopAdd = $periodStart;
while ($loopAdd < $periodEnd) {
$total = 0;
$seed = 0;
$tmJump = ' onclick="arc(\'content\',\''.$path.'add.php\',\'tm_timesheet_ds='.$loopAdd.'\')" onmouseout="updateClass(\'timeEntryMT\',this)" onmouseover="updateClass(\'timeEntryMR\',this)" ';
$total = "+";
echo "<td $tmJump >$total</td>";
$loopAdd = $loopAdd + 86400;
}
echo "</tr>";
}
?>
</table>
<script type="text/javascript">
<?php if(isset($timesheet[0]["tm_timesheet_approved"])&&$timesheet[0]["tm_timesheet_approved"]==0 || $timesheet[0]["tm_timesheet_approved"]==1){;?>
$("#oOptions").html("<span onclick=\"tableToExcel(document.getElementById('tblPayPeriod'),'PayPeriod')\"><?=$icoExportIcon;?>Download</span><span class=\"tcSubmit\" onclick=\"submitForApproval('<?=$periodStart?>','<?=$periodEnd?>')\"><?=$icoSubmitTimeIcon;?>Submit for Approval</span>");
<?php } else {?>
$("#oOptions").html(icoAdd+"<span onclick=\"tableToExcel(document.getElementById('tblPayPeriod'),'PayPeriod')\"><?=$icoExportIcon;?>Download</span><span class=\"tcSubmit\" onclick=\"submitForApproval('<?=$periodStart?>','<?=$periodEnd?>')\"><?=$icoSubmitTimeIcon;?>Submit for Approval</span>");
<?php } ?>
</script>
</div></div>
<?php }else{echo "<div class='clSystemError'>$fullName_derived is either <strong>not</strong> an employee or </br> <strong>pay frequency</strong> is not configured under Human Resources -> Employee -> Time.</div>";} ?>
