<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/*************************************/
/* Find ID OF CURRENT WEEK */
/*************************************/
$curWeek=0;
for ($i = 0; $i < count($aWeeks); $i++) {
if (isset($_POST["datestart"]) && isset($_POST["dateend"])) {
if ($_POST["datestart"] >= $aWeeks[$i][0] && $_POST["datestart"] <= $aWeeks[$i][4]){
$curWeek=$i;
$yearID=date("Y",$_POST["dateend"]);}}else{
if ($todayTime >= $aWeeks[$i][0] && $todayTime <= $aWeeks[$i][4]){
$curWeek=$i;}}
}
/*************************************/
/* TIMECARD PRESENTATION */
/*************************************/
if ($payFrequency=="biweekly"){
if ($curWeek == 0 || $curWeek % 2 == 0) {
$periodKey = $curWeek;
} else {
$periodKey = $curWeek - 1;
}
$periodMarker = $aWeeks[$periodKey][0];
if ($periodKey != count($aWeeks)-1) {
$periodLength = $aWeeks[$periodKey][3] + $aWeeks[$periodKey + 1][3];
} else {
$periodLength = $aWeeks[$periodKey][3];
}
$periodStart = $periodMarker;
$periodEnd = ($periodLength * 86400) + $periodMarker-1;}
/*************************************/
if ($payFrequency=="bimonthly"){
if ($curWeek % 2 == 0) {
$periodKey = $curWeek;
} else {
$periodKey = $curWeek - 1;
}
$periodMarker = $aWeeks[$periodKey][0];
$periodLength = $aWeeks[$periodKey][3];
$periodStart = $periodMarker;
$periodEnd = $aWeeks[$periodKey][4];}
/*************************************/
if ($payFrequency=="monthly"){
$periodKey = $monthID-1;
$periodMarker = $aWeeks[$periodKey][0];
$periodLength = $aWeeks[$periodKey][3];
$periodStart = $periodMarker;
$periodEnd = $aWeeks[$periodKey][4];}
/*************************************/
if ($payFrequency=="weekly"){
$periodKey = $curWeek;
$periodMarker = $aWeeks[$periodKey][0];
$periodLength = $aWeeks[$periodKey][3];
$periodStart = $periodMarker;
$periodEnd = $aWeeks[$periodKey][4];}
/*************************************/
# If datestart and dateend are passed
if (isset($_POST["datestart"]) && isset($_POST["dateend"])) {
$periodEnd=$_POST["dateend"];
$periodStart=$_POST["datestart"];
}
?>
