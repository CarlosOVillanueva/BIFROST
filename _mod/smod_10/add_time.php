<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once ("_lib/php/auth.php");
$rec[0]['gmtDateStart']=(isset($_POST["tm_timesheet_ds"])?date("Y-m-d",$_POST["tm_timesheet_ds"]):"");
if (isset($_POST["id_srv"]) && !empty($_POST["id_srv"])) {
$gdbo -> sql = "SELECT id_srv_board FROM _srv WHERE id_srv=".$_POST["id_srv"];
$gdbo -> getRec();
$listService = $gdbo -> getAssociative();
$rec[0]['id_srv_board'] = $listService[0]["id_srv_board"];
$rec[0]['id_srv'] = $_POST["id_srv"];
$disabled="";
$gdbo -> sql = "SELECT id_srv, srv_summary FROM _srv WHERE id_srv_board=".$rec[0]["id_srv_board"];
$gdbo -> getRec();
$listService = $gdbo -> dbData;
} else {
$rec[0]['id_srv_board'] = "";
$rec[0]['id_srv'] = "";
$disabled='disabled="disabled"';
$listService=array();
}
$gdbo -> sql = "SELECT
id_tm_charge,
tm_charge
FROM _tm_charge
ORDER BY tm_charge";
$gdbo -> getRec();
$listTmCharge = $gdbo -> dbData;
$gdbo -> sql = "SELECT
id_tm_work_role,
tm_work_role
FROM _tm_work_role
ORDER BY tm_work_role";
$gdbo -> getRec();
$listTmWorkRole = $gdbo -> dbData;
$gdbo -> sql = "SELECT
id_tm_work_tp,
tm_work_tp
FROM _tm_work_tp
ORDER BY tm_work_tp";
$gdbo -> getRec();
$listTmWorkTp = $gdbo -> dbData;
$gdbo -> sql = "SELECT
id_srv_board,
srv_board
FROM _srv_board
WHERE srv_board_dr is null";
$gdbo -> getRec();
$listSrvBoard = $gdbo -> dbData;
# Filterable Objects
## Service
$servicedbo = new ArcDb;
$servicedbo -> dbConStr=$globalDBCON;
$servicedbo -> dbType = $globalDBTP;
$servicedbo -> dbSchema = $globalDB;
$servicedbo -> sql = "SELECT id_srv, srv_summary FROM _srv";
$servicedbo -> dbFilter = "WHERE id_srv_board=";
$servicedbo -> type = "list";
$servicedbo -> id = "id_srv";
$servicedboJSON =encrypt(json_encode((array)$servicedbo));
$buttonDate ='<div class="elementIconBox" onclick="openCalendar(this,\'interface\',null,\'dateFld\')"><i class="fa fa-calendar"></i></div>';
$buttonTime ='<div class="elementIconBox" onclick="insertTime(this)"><i class="fa fa-clock-o"></i></div>';
$fsTime = 
array(
array(
array("Role","id_tm_work_role",null,1,$listTmWorkRole,null),
),
array(
array("Charge Code","id_tm_charge",null,1,$listTmCharge,null),
array("Charge Type","id_tm_work_tp",null,1,$listTmWorkTp,null),
),
array(
array("Service Board","id_srv_board",'onchange="filterRec(this,\''.$servicedboJSON.'\',\'dService\')"',1,$listSrvBoard,$rec[0]["id_srv_board"]),
array("Service Ticket","id_srv",$disabled,1,$listService,$rec[0]["id_srv"],"dService"),
),
array(
array("Start Date","tm_ds",'class="dateFld elementIcon" onblur="validateElement(\'date\',this)"',0,null,$rec[0]['gmtDateStart'],null,$buttonDate),
array("Start Time","tm_ts",'class="timeFld elementIcon" onblur="validateElement(\'time\',this)"',0,null,null,null,$buttonTime),
array("Total Time","tm_total",'disabled="disabled"',0),
),
array(
array("End Date","tm_de",'class="dateFld elementIcon" onblur="validateElement(\'date\',this)"',0,null,null,null,$buttonDate),
array("End Time","tm_te",'class="timeFld elementIcon" onblur="validateElement(\'time\',this)"',0,null,null,null,$buttonTime),
array("Deduct","tm_deduction",null,0,null,null),
),
array(
array("Internal Notes","tm_internalNotes",null,6,null,null),
),
array(
array("Public Notes","tm_publicNotes",null,6,null,null),
),
);
?>
<form method="post" id="frmTime" name="frmTime" action="javascript:submitFrmVals('blTime','/_mod/smod_10/sql_timeEntries.php','id_tm_charge,time_dc,tm_ts,tm_ds,id_tm_work_role,id_tm_work_tp,tm_start','&action=insert&form=frmTime','frmTime')">
<legend>
Time Entry
</legend>
<?=frmElements($fsTime);?>
</form>
<script type="text/javascript">
CKEDITOR.replace("tm_internalNotes");
CKEDITOR.replace("tm_publicNotes");
$('.dateFld,.timeFld,#tm_deduction').each(function() {var elem = $(this);$(elem).on("propertychange change click keyup input paste", function(){estTime();});});
</script>
