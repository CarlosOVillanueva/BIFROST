<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once("_lib/php/auth.php");
require_once("_includes/heading.php");
require_once("_includes/optionstoolbar.php");
# Contacts 
$gdbo -> sql = "SELECT
id_cust_contact,
contact,
IF(id_cust_company IS NULL,'0',id_cust_company) as id_cust_company,
IF(cust_company IS NULL,'General Contacts',cust_company) as cust_company
FROM
(
SELECT 
id_cust_contact,
concat(cust_contact_familyName,',',cust_contact_givenName) AS \"Contact\",
a.id_cust_company,
b.cust_company
FROM _cust_contact a
LEFT JOIN _cust_company b ON a.id_cust_company = b.id_cust_company
ORDER BY b.cust_company,cust_contact_familyName,cust_contact_givenName)
as derived ORDER by cust_company";
$gdbo -> getRec();
$listContact = $gdbo -> dbData;
#Work Role
$gdbo -> sql = "SELECT
id_tm_work_role,
tm_work_role
FROM _tm_work_role;";
$gdbo -> getRec();
$listTmWorkRole = $gdbo -> dbData;
#Work Type
$gdbo -> sql = "SELECT
id_tm_work_tp,
tm_work_tp
FROM _tm_work_tp;";
$gdbo -> getRec();
$listTmWorkTp = $gdbo -> dbData;
#Charge Code
$gdbo -> sql = "SELECT
id_tm_charge,
tm_charge
FROM _tm_charge;";
$gdbo -> getRec();
$listTmCharge = $gdbo -> dbData;
#Type
$gdbo -> sql = "SELECT
id_srv_tp,
srv_tp
FROM _srv_tp;";
$gdbo -> getRec();
$listType = $gdbo -> dbData;
#Source
$gdbo -> sql = "SELECT
id_srv_src,
srv_src
FROM _srv_src;";
$gdbo -> getRec();
$listSource = $gdbo -> dbData;
#Group
$gdbo -> sql = "SELECT
id_sys_group,
sys_group
FROM _sys_group;";
$gdbo -> getRec();
$listGroup = $gdbo -> dbData;
#impact
$gdbo -> sql = "SELECT
id_srv_impact,
srv_impact
FROM _srv_impact;";
$gdbo -> getRec();
$listImpact = $gdbo -> dbData;
#severity
$gdbo -> sql = "SELECT
id_srv_severity,
srv_severity
FROM _srv_severity;";
$gdbo -> getRec();
$listSeverity = $gdbo -> dbData;
#priority
$gdbo -> sql = "SELECT
id_srv_priority,
concat(id_srv_priority,' - ',srv_priority) as \"priority\"
FROM _srv_priority;";
$gdbo -> getRec();
$listPriority = $gdbo -> dbData;
#user filters
$companyFilter="";
$serviceboardFilter="";
if($isEmployee === false) {
$filter = "id_cust_company=".$id_cust_company_derived;
$companyFilter ="WHERE ".$filter;
$serviceboardFilter = "AND ".$filter;
}
#company
$gdbo -> sql = "SELECT
id_cust_company,
cust_company
FROM _cust_company ".$companyFilter;
$gdbo -> getRec();
$listCompany = $gdbo -> dbData;
#service.board
$gdbo -> sql = "SELECT
id_srv_board,
srv_board
FROM _srv_board
WHERE srv_board_dr is null ".$serviceboardFilter;
$gdbo -> getRec();
$listBoard = $gdbo -> dbData;
#service.status
$gdbo -> sql = "SELECT
id_sys_status,
sys_status
FROM _sys_status
WHERE id_sys_status_tp=4
ORDER BY sys_status_order;";
$gdbo -> getRec();
$listServiceStatus = $gdbo -> dbData;
# Filterable Objects
## Branches
$branchesdbo = new ArcDb;
$branchesdbo -> dbConStr=$globalDBCON;
$branchesdbo -> dbType = $globalDBTP;
$branchesdbo -> dbSchema = $globalDB;
$branchesdbo -> sql = "SELECT id_cust_branch, cust_branch FROM _cust_branch";
$branchesdbo -> dbFilter = "WHERE id_cust_company=";
$branchesdbo -> type = "list";
$branchesdbo -> id = "id_cust_branch";
$branchesdbo -> attributes = 'class="chBranch"';
$branchesdboJSON =encrypt(json_encode((array)$branchesdbo));
# Contracts
$gdbo -> sql = "SELECT
a.id_contract,
a.contract,
b.id_cust_company,
b.cust_company
FROM _contract a
LEFT JOIN _cust_company b ON a.id_cust_company = b.id_cust_company
ORDER BY b.cust_company,a.contract";
$gdbo -> getRec();
$listContract = $gdbo -> dbData;
$fsService=
array(
array(
array('Point of Contact','id_cust_contact',null,1,$listContact,null,"dPOC"),
array('Organization','id_cust_company','onchange="filterRec(this,\''.$branchesdboJSON.'\',\'dBranch\')"',1,$listCompany),
array('Branch','id_cust_branch','disabled=\'disabled\' class=\'chBranch\'',1,null,null,"dBranch"),
),
array(
array("Contract","id_contract",null,1,$listContract),
array("Service Board","id_srv_board",null,1,$listBoard),
array("Status","id_srv_status",null,1,$listServiceStatus),
),
array(
array("Severity","id_srv_severity",null,1,$listSeverity),
array("Impact","id_srv_impact",null,1,$listImpact),
),
array(
array("Type","id_srv_tp",null,1,$listType),
array("Source","id_srv_src",null,1,$listSource),
),
array(
array("Summary","srv_summary",'style="width:100% !important"',0,null,null,null,null,null,null,null,'style="width:100% !important"'),
),
);
?>
<!-- begin forms -->
<script type="text/javascript">$(".icoadd").remove();</script>
<form method="post" id="frmService" name="frmService" action="javascript:submitFrmVals('content','/_mod/smod_10/sql.php','service_detail.id_cust_company,service_detail.id_cust_branch,service_detail.id_srv_board,service_detail.id_srv_status,service_detail.id_srv_severity,service_detail.id_srv_impact,service_detail.id_srv_tp,service_detail.srv_summary,service_detail.srv_descr','&form=frmService&action=insert','frmService')">
<fieldset id="service_detail">
<legend>Service Detail</legend>
<?=frmElements($fsService)?>
</fieldset>
</form>
