<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once("_lib/php/auth.php");
if(isset($_POST["uid"])){
$uid = hex2str($_POST["uid"]);
$arrUser = explode(",", $uid);
# Find existing users
$gdbo -> sql = "SELECT id_sys_user FROM _srv_res WHERE id_srv=".$_POST["id_srv"];
$gdbo -> getRec();
$recordset = $gdbo -> dbData;
$existing = array_column($recordset, 0);
$newusers = array_diff($arrUser,$existing);
$users = "";
foreach ($newusers as $key => $value) {
if ($key != 0) {
$comma = ",";
} else {
$comma = "";
}
$users.=$comma . "(" . $_POST["id_srv"] . ",$value)";
}}
$action = $_POST['action'];
$form = $_POST['form'];
$formAction = $form . "," . $action;
# Begin SQL Functions
switch ($formAction) {
case "frmResource,insert":
if (count($newusers) > 0) {
$gdbo -> sql = "insert into _srv_res(id_srv,id_sys_user) values $users";
$gdbo -> dbTable ="_srv_res";
$gdbo -> execQuery();}
break;
case "frmResource,delete":
$gdbo -> dbTable ="_srv_res";
$gdbo -> deleteRec("id_srv_res=".$_POST["id_srv_res"]);
break;
}
include("listResources.php");
?>
