<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once ("_lib/php/auth.php");
$gdbo -> sql = "SELECT
a.id_tm,
a.id_hr_emp,
a.tm_deduction,
UNHEX(a.tm_internalNotes) as tm_internalNotes ,
a.id_srv,
a.id_srv_workflow_task,
a.id_tm_work_role,
a.id_tm_work_tp,
a.id_tm_charge,
a.tm_dc,
a.tm_ts+$gmtOffset as tm_ts,
a.tm_te+$gmtOffset as tm_te,
UNHEX(a.tm_publicNotes) as tm_publicNotes,
b.id_srv_board,
cast((a.tm_te-a.tm_ts-(a.tm_deduction*3600))/3600 as DECIMAL(12,2)) as \"actual\"
FROM
_tm a
LEFT JOIN _srv b ON a.id_srv=b.id_srv
WHERE id_tm=".$_POST["id_tm"];
$gdbo -> getRec();
$rec=$gdbo -> getAssociative();
$rec[0]['gmtTimeStart']=date("H:i:s",$rec[0]['tm_ts']);
$rec[0]['gmtDateStart']=date("Y-m-d",$rec[0]['tm_ts']);
if ($rec[0]['tm_te']!=""){
$rec[0]['gmtDateEnd']=date("Y-m-d",$rec[0]['tm_te']);
$rec[0]['gmtTimeEnd']=date("H:i:s",$rec[0]['tm_te']);
}
else {
$rec[0]['gmtDateEnd']="";
$rec[0]['gmtTimeEnd']="";
}
$periodDetails=array();
$_POST["yearID"] = date("Y",$rec[0]['tm_ts']);
$_POST["monthID"] = date("n",$rec[0]['tm_ts']);
require_once("_mod/smod_11/inc_period.php");
for ($i=0;$i<count($aWeeks);$i++) {
if ( 
($rec[0]['tm_ts'] >= $aWeeks[$i][0] && $rec[0]['tm_ts'] <= $aWeeks[$i][4]) || 
($rec[0]['tm_te'] >= $aWeeks[$i][0] && $rec[0]['tm_te'] <= $aWeeks[$i][4])) {
$periodDetails[]=array($i+1,$aWeeks[$i][0],$aWeeks[$i][4]);
}
}
$gdbo -> sql="
SELECT
id_tm_timesheet,
tm_timesheet_ds,
tm_timesheet_de,
id_hr_emp,
tm_timesheet_approved
FROM
_tm_timesheet WHERE
id_hr_emp=".$rec[0]["id_hr_emp"]." AND
(tm_timesheet_ds between ".$periodDetails[0][1]." AND ".$periodDetails[0][2].") OR 
(tm_timesheet_de between ".$periodDetails[0][1]." AND ".$periodDetails[0][2].")";
$gdbo -> getRec();
$timesheet = $gdbo -> getAssociative();
if ($gdbo->dbRows>0){
switch ($timesheet[0]["tm_timesheet_approved"]) {
case 0:
$status="Submitted";
break;
case 1:
$status="Approved";
break;
case 2:
$status="Rejected";
break;
default:
break; 
}
} else {
$status="Open";
}
$gdbo -> sql = "SELECT
id_tm_charge,
tm_charge
FROM _tm_charge
ORDER BY tm_charge";
$gdbo -> getRec();
$listTmCharge = $gdbo -> dbData;
$gdbo -> sql = "SELECT
id_tm_work_role,
tm_work_role
FROM _tm_work_role
ORDER BY tm_work_role";
$gdbo -> getRec();
$listTmWorkRole = $gdbo -> dbData;
$gdbo -> sql = "SELECT
id_tm_work_tp,
tm_work_tp
FROM _tm_work_tp
ORDER BY tm_work_tp";
$gdbo -> getRec();
$listTmWorkTp = $gdbo -> dbData;
$gdbo -> sql = "SELECT
id_srv_board,
srv_board
FROM _srv_board
WHERE srv_board_dr is null";
$gdbo -> getRec();
$listSrvBoard = $gdbo -> dbData;
# Filterable Objects
## Service
$servicedbo = new ArcDb;
$servicedbo -> dbConStr=$globalDBCON;
$servicedbo -> dbType = $globalDBTP;
$servicedbo -> dbSchema = $globalDB;
$servicedbo -> sql = "SELECT id_srv, srv_summary FROM _srv";
$servicedbo -> dbFilter = "WHERE id_srv_board=";
$servicedbo -> type = "list";
$servicedbo -> id = "id_srv";
$servicedboJSON =encrypt(json_encode((array)$servicedbo));
## Service Recordset
if (isset($rec[0]["id_srv_board"])){
$gdbo -> sql = "SELECT id_srv, srv_summary FROM _srv WHERE id_srv_board=".$rec[0]["id_srv_board"];
$gdbo -> getRec();
$listService = $gdbo -> dbData;
$disabled = "";
}
else {
$disabled='disabled="disabled"';
$listService = array();
}
$buttonDate ='<div class="elementIconBox" onclick="openCalendar(this,\'interface\',null,\'dateFld\')"><i class="fa fa-calendar"></i></div>';
$buttonTime ='<div class="elementIconBox" onclick="insertTime(this)"><i class="fa fa-clock-o"></i></div>';
$fsTime = 
array(
array(
array("Role","id_tm_work_role",null,1,$listTmWorkRole,$rec[0]["id_tm_work_role"]),
array("Time Card Status",null,'disabled="disabled"',0,null,$status),
array(null, 'id_tm',null,3,null,$rec[0]["id_tm"]),
),
array(
array("Charge Code","id_tm_charge",null,1,$listTmCharge,$rec[0]["id_tm_charge"]),
array("Charge Type","id_tm_work_tp",null,1,$listTmWorkTp,$rec[0]["id_tm_work_tp"]),
),
array(
array("Service Board","id_srv_board",'onchange="filterRec(this,\''.$servicedboJSON.'\',\'dService\')"',1,$listSrvBoard,$rec[0]["id_srv_board"]),
array("Service Ticket","id_srv",$disabled,1,$listService,$rec[0]["id_srv"],"dService"),
),
array(
array("Start Date","tm_ds",'class="dateFld elementIcon" onblur="validateElement(\'date\',this)"',0,null,$rec[0]["gmtDateStart"],null,$buttonDate),
array("Start Time","tm_ts",'class="timeFld elementIcon" onblur="validateElement(\'time\',this)"',0,null,$rec[0]["gmtTimeStart"],null,$buttonTime),
array("Total Time","tm_total",'disabled="disabled"',0,null,$rec[0]["actual"]),
),
array(
array("End Date","tm_de",'class="dateFld elementIcon" onblur="validateElement(\'date\',this)"',0,null,$rec[0]["gmtDateEnd"],null,$buttonDate),
array("End Time","tm_te",'class="timeFld elementIcon" onblur="validateElement(\'time\',this)"',0,null,$rec[0]["gmtTimeEnd"],null,$buttonTime),
array("Deduct","tm_deduction",null,0,null,$rec[0]["tm_deduction"]),
),
array(
array("Internal Notes","tm_internalNotes",null,6,null,$rec[0]["tm_internalNotes"]),
),
array(
array("Public Notes","tm_publicNotes",null,6,null,$rec[0]["tm_publicNotes"]),
),
);
?>
<form method="post" id="frmTime" name="frmTime" action="javascript:submitFrmVals('blTime','/_mod/smod_10/sql_timeEntries.php','id_tm_charge,time_dc,tm_ts,tm_ds,id_tm_work_role,id_tm_work_tp,tm_start','&action=update&form=frmTime','frmTime')">
<legend>
Time Entry
</legend>
<?=frmElements($fsTime);?>
</form>
<script type="text/javascript">
CKEDITOR.replace("tm_internalNotes");
CKEDITOR.replace("tm_publicNotes");
<?php if ($isEmployee == true) {?>
a=document.getElementById("tmDeleteButton");
a.disabled=false;
a.onclick=function(){delTm(<?=$rec[0]["id_tm"] ?>,<?= $rec[0]["id_srv"]?>)};
<?php }?> 
$('.dateFld,.timeFld,#tm_deduction').each(function() {var elem = $(this);$(elem).on("propertychange change click keyup input paste", function(){estTime();});});
</script>
