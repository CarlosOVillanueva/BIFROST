<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once("_lib/php/auth.php");
$arctbl = new ArcTbl;
$arctbl -> dbConStr=$globalDBCON;
$arctbl -> dbOffset = 0;
$arctbl -> dbLimit = 23;
$arctbl -> dbType = $globalDBTP;
$arctbl -> dbSchema = $globalDB;
$arctbl -> recLink= $path."edit_time.php";
$arctbl -> actionFilterKey="id_tm";
$arctbl -> recIndex="id_tm";
$arctbl -> ignoreCols=arraY("id_tm","Service ID","Internal Notes","Public Notes");
$arctbl -> ignoreFilterCols=arraY("id_tm","Service ID"); 
$arctbl -> moreActions=";tmButtonControl(3)";
if (isset($_POST["id_srv"]))
$arctbl -> recFilter="where `Service ID`=".$_POST["id_srv"];
$arctbl -> recQuery = "
SELECT
a.id_tm,
concat(c.cust_contact_familyName,',',c.cust_contact_givenName) as \"Employee\",
FROM_UNIXTIME(a.tm_ts+$gmtOffset,'%Y-%m-%d') as \"Start Date\",
FROM_UNIXTIME(a.tm_ts+$gmtOffset,'%H:%i:%s') as \"Start Time\",
FROM_UNIXTIME(a.tm_te+$gmtOffset,'%Y-%m-%d') as \"End Date\",
FROM_UNIXTIME(a.tm_te+$gmtOffset,'%H:%i:%s') as \"End Time\",
cast((a.tm_te-a.tm_ts-(a.tm_deduction*3600))/3600 as DECIMAL(12,2)) as \"Actual\",
SCRIPT2TEXT(a.tm_internalNotes) as \"Internal Notes\",
SCRIPT2TEXT(a.tm_publicNotes) as \"Public Notes\",
id_srv as \"Service ID\"
FROM _tm a
LEFT JOIN
_hr_emp b ON a.id_hr_emp=b.id_hr_emp
LEFT JOIN
_cust_contact c ON b.id_cust_contact=c.id_cust_contact";
$arctbl -> actionDestination = "blTime";
$arctbl -> ajDestination = "blTime";
$arctbl -> ajPage = $path."listTimeEntries.php";
$arctbl -> build();
echo hex2str($arctbl -> tblNav);
echo $arctbl->dataTable;
?>
