<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once ("_lib/php/auth.php");
$fsCommands = array( array( array(null, null, "onclick='clearPop()'", 2, null, 'Close'), ));
$gdbo -> sql = "SELECT
a.id_tm ID,
concat(c.cust_contact_givenName,' ',c.cust_contact_familyName) AS Employee,
FROM_UNIXTIME(a.tm_ts+$gmtOffset,'%M %d, %Y') 'Start Date',
FROM_UNIXTIME(a.tm_ts+$gmtOffset,'%H:%i:%s') 'Start Time',
FROM_UNIXTIME(a.tm_te+$gmtOffset,'%Y-%m-%d') 'End Date',
FROM_UNIXTIME(a.tm_te+$gmtOffset,'%H:%i:%s') 'End Time',
UNHEX(a.tm_internalNotes) as 'Internal Notes',
UNHEX(a.tm_publicNotes) as tm_publicNotes,
cast((a.tm_te-a.tm_ts-(a.tm_deduction*3600))/3600 as DECIMAL(12,2)) Actual,
id_srv as 'Service ID',
(SELECT concat('<img class=''avatarSmall'' style=''vertical-align:middle;margin-right:10px'' src=''/_avatar/thumb_',fso,''' height=''32'' width=''32''/>') as fso FROM _fso where fso_pkcol='id_cust_contact' AND fso_pk=c.id_cust_contact AND fso_originalname='avatar') as Avatar
FROM _tm a
LEFT JOIN
_hr_emp b ON a.id_hr_emp=b.id_hr_emp
LEFT JOIN
_cust_contact c ON b.id_cust_contact=c.id_cust_contact
WHERE id_srv = " . $_POST["id_srv"] . " ORDER BY a.tm_ts+$gmtOffset desc";
$gdbo -> getRec();
$timedata = $gdbo -> dbData;
$debug = 1;
$timereport = '<div class="clReportContainer">';
if ($gdbo -> dbRows > 0) {
for ($i = 0; $i < count($timedata); $i++) {
$end = (int)count($timedata) - 1;
$next = $i + 1;
$prev = $i - 1;
///////////////////////////////////////////////////////////////////
$startdate = '<div class="clReportStartDate"><h2>' . $timedata[$i][2] . '</h2></div>';
$userdata = '<div class="clReportUser">'.$timedata[$i][10].'<span class="user">' . $timedata[$i][1] . '</span> - <span class="time">' . $timedata[$i][8] . ' Hours</span></div>';
$publicnotes = ($timedata[$i][6] != "" ? '<div class="public notesarea">' . $timedata[$i][6].'</div>': "");
$privatenotes = ($timedata[$i][7] != "" ? '<div class="private notesarea">' . $timedata[$i][7].'</div>' : "");
$internalnotes = ($timedata[$i][6] != "" || $timedata[$i][7] != "" ? '<div class="clReportNotes">' . $publicnotes . $privatenotes . '</div>' : '<div class="clReportNotes"><div class="public notesarea">No Data</div></div>');
///////////////////////////////////////////////////////////////////
if ($i == 0 || $timedata[$i][2] != $timedata[$prev][2]) {
$timereport .= '<div class="clReportBody">' . $startdate;
}
$timereport .= $userdata . $internalnotes;
if ($i == $end || $timedata[$i][2] != $timedata[$next][2]) {
$timereport . "</div>";
}
}
$timereport .= "</div>";
} else {
$timereport = '<div class="clReportContainer"><h1>There are no time entries</h1>';
}
?>
<form class="popBox" method="POST" action="#">
<fieldset class="timesummary">
<legend>Time Summary</legend>
<?=$timereport;?>
<?=frmElements($fsCommands);?>
</fieldset>
</form>
<script type="text/javascript">
$(function() {
$(".popBox").resizable({
});
$(".popBox").resize(function() {
lineWrap();
});
});
</script>
<style>
.syntaxhighlighter table td.code .line {
white-space: pre-wrap !important;
word-wrap: break-word !important;
}
@media screen and (max-width: 680px) {
.syntaxhighlighter .gutter {
display:none;
}
</style>
<script type="text/javascript">
$("pre").removeAttr("style");
$("pre").removeClass("text");
$("pre").addClass("brush: js"); 
SyntaxHighlighter.highlight();
SyntaxHighlighter.defaults['auto-links'] = false;
SyntaxHighlighter.defaults['smart-tabs'] = false;
SyntaxHighlighter.defaults['tab-size'] = 4;
SyntaxHighlighter.defaults['html-script'] = true;
$(".toolbar").remove();
lineWrap()
</script>
