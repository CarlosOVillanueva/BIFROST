<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once("_lib/php/auth.php");
$arctbl = new ArcTbl;
$arctbl -> dbConStr=$globalDBCON;
$arctbl -> tblID = "dashboard";
$arctbl -> dbOffset = 0;
$arctbl -> dbLimit = 23;
$arctbl -> dbType = $globalDBTP;
$arctbl -> dbSchema = $globalDB;
$arctbl -> recIndex="id_srv_board";
$arctbl -> recFilter=" WHERE `Date Removed` is null";
$arctbl -> ignoreCols=array("id_srv_board","Date Removed");
$arctbl -> ignoreFilterCols=array("id_srv_board","Date Removed");
$arctbl -> recQuery = "SELECT
a.id_srv_board,
a.srv_board_dr \"Date Removed\",
CONCAT('<div ',RECORDLINK('list10','".$path."list.php',CONCAT('WHERE id_cust_department=',CAST(a.id_cust_department as char CHARACTER SET utf8))),'>',CAST(b.cust_department as char CHARACTER SET utf8),'</div>') as \"Department\",
CONCAT('<div ',RECORDLINK('list10','".$path."list.php',CONCAT('WHERE id_srv_board=',CAST(a.id_srv_board as char CHARACTER SET utf8))),'>',CAST(a.srv_board as char CHARACTER SET utf8),'</div>') as \"Service Board\",
CONCAT('<div ',RECORDLINK('list10','".$path."list.php',CONCAT('WHERE id_srv_status=16 AND id_srv_board=',a.id_srv_board)),'>',(SELECT count(id_srv) FROM _srv WHERE id_srv_board = a.id_srv_board and id_srv_status=16),'</div>') as \"Pending\",
CONCAT('<div ',RECORDLINK('list10','".$path."list.php',CONCAT('WHERE id_srv_status=17 AND id_srv_board=',a.id_srv_board)),'>',(SELECT count(id_srv) FROM _srv WHERE id_srv_board = a.id_srv_board and id_srv_status=17),'</div>') as \"Assigned\",
CONCAT('<div ',RECORDLINK('list10','".$path."list.php',CONCAT('WHERE id_srv_status=18 AND id_srv_board=',a.id_srv_board)),'>',(SELECT count(id_srv) FROM _srv WHERE id_srv_board = a.id_srv_board and id_srv_status=18),'</div>') as \"Working\",
CONCAT('<div ',RECORDLINK('list10','".$path."list.php',CONCAT('WHERE id_srv_status=19 AND id_srv_board=',a.id_srv_board)),'>',(SELECT count(id_srv) FROM _srv WHERE id_srv_board = a.id_srv_board and id_srv_status=19),'</div>') as \"Closed\",
CONCAT('<div ',RECORDLINK('list10','".$path."list.php',CONCAT('WHERE id_srv_board=',a.id_srv_board)),'>',(SELECT count(id_srv) FROM _srv WHERE id_srv_board = a.id_srv_board),'</div>') as \"Total\"
FROM _srv_board a
LEFT JOIN _cust_department b ON a.id_cust_department=b.id_cust_department";
$arctbl -> ajDestination = "listServiceBoard";
$arctbl -> ajPage = $path."listServiceBoards.php";
$arctbl -> build();
echo hex2str($arctbl -> tblNav);
echo $arctbl->dataTable;
?>
