<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once("_lib/php/auth.php");
filterQryPost("id");
#Group Members
$gdbo -> sql = "
SELECT
a.id_sys_user,
c.cust_contact_givenName,
c.cust_contact_familyName,
d.sys_user
FROM
_sys_user_emp a 
LEFT JOIN _hr_emp b ON a.id_cust_contact=b.id_cust_contact
LEFT JOIN _cust_contact c ON a.id_cust_contact=c.id_cust_contact
LEFT JOIN _sys_user d ON a.id_sys_user=d.id_sys_user
LEFT JOIN _sys_group_user e ON a.id_sys_user=e.id_sys_user
WHERE d.id_sys_status !=22 AND e.id_sys_group=$id;
";
$gdbo -> getRec();
$aUser = $gdbo -> getAssociative();
/* create table */
for ($i = 0; $i < count($aUser); $i++) {
?>
<div class="frmrow">
<div class="frmcol"><input class="clRes" type="checkbox"checked="checked" value="<?php echo $aUser[$i]["id_sys_user"] ?>"/>
<?php echo "[ ". $aUser[$i]["sys_user"]." ] ". $aUser[$i]["cust_contact_givenName"].' '.$aUser[$i]["cust_contact_familyName"] ?></div>
</div>
<?php
}
?>
