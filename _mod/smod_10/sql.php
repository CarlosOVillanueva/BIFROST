<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
# Includes
require_once("_lib/php/auth.php");
# Parse Form Action
$action = $_POST['action'];
$form = $_POST['form'];
$formAction = $form . "," . $action;
$_POST["srv_du"]=$dtTimeUTC;
switch ($formAction) {
case "frmService,insert":
$gdbo -> dbTable = "_srv";
$gdbo -> insertRec();
$_POST["id_srv"] = $gdbo -> insertedID;
include("edit.php");
break;
case "frmService,update":
$gdbo -> dbTable = "_srv";
$gdbo -> updateRec("id_srv=".$_POST["id_srv"]);
include("edit.php");
break;
case "frmService,delete":
$gdbo -> dbTable = "_srv";
$gdbo -> deleteRec("id_srv=".$_POST["id_srv"]);
$gdbo -> dbTable = "_srv_descr";
$gdbo -> deleteRec("id_srv=".$_POST["id_srv"]);
$gdbo -> dbTable = "_srv_analysis";
$gdbo -> deleteRec("id_srv=".$_POST["id_srv"]);
$gdbo -> dbTable = "_srv_resolution";
$gdbo -> deleteRec("id_srv=".$_POST["id_srv"]);
$gdbo -> dbTable = "_tm";
$gdbo -> deleteRec("id_srv=".$_POST["id_srv"]);
require_once("index.php");
break;
}
include("_error/status.php");
?>
