<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once("_lib/php/auth.php");
$_POST["id_srv"] = (isset($_POST["id_srv"])?$_POST["id_srv"]:"");
$arctbl = new ArcTbl;
$arctbl -> dbConStr=$globalDBCON;
$arctbl -> dbOffset = 0;
$arctbl -> dbLimit = 23;
$arctbl -> dbType = $globalDBTP;
$arctbl -> dbSchema = $globalDB;
$arctbl -> recIndex="id_srv";
$arctbl -> ignoreCols=array("id_srv","id_srv_res","id_sys_user","id_cust_contact","Date Removed");
$arctbl -> recQuery = "SELECT
a.id_srv_res,
a.id_srv,
a.id_sys_user,
b.id_cust_contact,
(SELECT concat('<img class=''avatarSmall'' src=''/_avatar/thumb_',fso,''' height=''32'' width=''32''/>') as fso FROM _fso where fso_pkcol='id_cust_contact' AND fso_pk=b.id_cust_contact AND fso_originalname='avatar') as \"Avatar\",
c.cust_contact_givenName as \"First\",
c.cust_contact_familyName as \"Last\",
d.sys_user as \"Logon\",
case when a.srv_res_ack then 'Yes' else 'No' end  as \"Acknowledged\",
FROM_UNIXTIME(UNIX_TIMESTAMP(a.srv_res_dc)+$gmtOffset,'%M %d,%Y | %h:%i:%s %p') as \"Date Acknowledged\",
concat('<a class=\"button\" onclick=\"removeResource(',a.id_srv_res,',',a.id_srv,')\"><i class=\"fa fa-trash fa-fw\"></i>Remove</a>') as \"\"
FROM
_srv_res a
LEFT JOIN
_sys_user_emp b on a.id_sys_user=b.id_sys_user
LEFT JOIN
_cust_contact c on b.id_cust_contact=c.id_cust_contact
LEFT JOIN
_sys_user d ON a.id_sys_user = d.id_sys_user
WHERE d.id_sys_status !=22 AND a.id_srv=".$_POST["id_srv"];
$arctbl -> ajDestination = "listResource";
$arctbl -> ajPage = $path."listResources.php";
$arctbl -> build();
echo hex2str($arctbl -> tblNav);
echo $arctbl->dataTable;
?>
