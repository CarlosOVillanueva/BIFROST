<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once("_lib/php/auth.php");
$_POST["id_hr_emp"]=$id_hr_emp_derived;
if(isset($_POST["tm_ts"])){
$_POST["tm_ts"]=strtotime($_POST["tm_ds"].' '.$_POST["tm_ts"])-$gmtOffset;}
if(isset($_POST["tm_te"]) && $_POST["tm_te"]!=""){
$_POST["tm_te"]=strtotime($_POST["tm_de"].' '.$_POST["tm_te"])-$gmtOffset;
} 
if (isset($_POST["tm_deduction"]) && ($_POST["tm_deduction"]!=''|$_POST["tm_deduction"]!=null)){
$_POST["tm_deduction"]=$_POST["tm_deduction"];
} else {
$_POST["tm_deduction"]='0';
}
$action = $_POST['action'];
$form = $_POST['form'];
$formAction = $form . "," . $action;
switch ($formAction) {
case "frmTime,insert":
$gdbo -> dbTable = "_tm";
$gdbo -> insertRec();
$insertedID = $gdbo -> insertedID;
$_POST['id_tm'] = $insertedID;
include('edit_time.php');
break;
/* ********************************************************************** */
case "frmTime,update":
/* ********************************************************************** */
$gdbo -> dbTable = "_tm";
$gdbo -> updateRec("id_tm=".$_POST['id_tm']);
include('edit_time.php');
break;
/* ********************************************************************** */
case "frmTime,delete":
/* ********************************************************************** */
# Purge all records that may be linked to this company
# IMPORTANT! All tickets associated with the company will be purged; however, time counted against the company will remain
$gdbo -> sql ="SELECT TABLE_NAME FROM information_schema.columns WHERE COLUMN_NAME='id_tm'";
$gdbo -> getRec();
$delTables = $gdbo -> dbData;
foreach($delTables as $row => $column) {
foreach($column as $data => $value) {
$gdbo -> dbTable = $value;
$gdbo -> deleteRec("id_tm=".$_POST["id_tm"]);
}
}
include('listTimeEntries.php');
break;
}
include("_error/status.php");
?>
