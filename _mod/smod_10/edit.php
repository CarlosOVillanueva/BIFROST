<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once("_lib/php/auth.php");
require_once("_includes/heading.php");
require_once("_includes/optionstoolbar.php");
/************************************************/
# Resource
$gdbo -> sql = "SELECT id_srv_res, id_srv, id_sys_user,srv_res_ack FROM _srv_res WHERE id_sys_user=".$id_sys_user." AND id_srv=".$_POST["id_srv"];
$gdbo -> getRec();
$recResources = $gdbo -> getAssociative();
# Update ACK
$_POST["srv_res_ack"] = 1;
$gdbo -> sql = null;
$gdbo -> dbTable = "_srv_res";
$gdbo -> updateRec("id_srv_res=".$recResources[0]["id_srv_res"]);
/************************************************/
# Contacts 
$gdbo -> sql = "SELECT
id_cust_contact,
contact,
IF(id_cust_company IS NULL,'0',id_cust_company) as id_cust_company,
IF(cust_company IS NULL,'General Contacts',cust_company) as \"cust_company\"
FROM
(
SELECT 
id_cust_contact,
concat(cust_contact_familyName,',',cust_contact_givenName) AS \"Contact\",
a.id_cust_company,
b.cust_company
FROM _cust_contact a
LEFT JOIN _cust_company b ON a.id_cust_company = b.id_cust_company
ORDER BY b.cust_company,cust_contact_familyName,cust_contact_givenName)
as derived ORDER by cust_company";
$gdbo -> getRec();
$listContact = $gdbo -> dbData;
# Severity
$gdbo -> sql= "SELECT
id_srv_severity,
srv_severity
FROM _srv_severity;";
$gdbo -> getRec();
$listSeverity = $gdbo -> dbData;
# Service Type
$gdbo -> sql= "SELECT
id_srv_tp,
srv_tp
FROM _srv_tp;";
$gdbo -> getRec();
$listServiceType = $gdbo -> dbData;
# Service Source
$gdbo -> sql= "SELECT
id_srv_src,
srv_src
FROM _srv_src;";
$gdbo -> getRec();
$listServiceSource = $gdbo -> dbData;
# System Group
$gdbo -> sql= "SELECT
id_sys_group,
sys_group
FROM _sys_group;";
$gdbo -> getRec();
$listSystemGroup = $gdbo -> dbData;
# Service Impact
$gdbo -> sql= "SELECT
id_srv_impact,
srv_impact
FROM _srv_impact;";
$gdbo -> getRec();
$listServiceImpact = $gdbo -> dbData;
# Service Priority
$gdbo -> sql= "SELECT
id_srv_priority,
concat(id_srv_priority,' - ',srv_priority) as \"priority\"
FROM _srv_priority;";
$gdbo -> getRec();
$listServicePriority = $gdbo -> dbData;
# Filters
$companyFilter="";
$serviceboardFilter="";
if($isEmployee === false) {
$filter = "id_cust_company=".$id_cust_company_derived;
$companyFilter ="WHERE ".$filter;
$serviceboardFilter = "AND ".$filter;
}
# Company
$gdbo -> sql= "SELECT
id_cust_company,
cust_company
FROM _cust_company ".$companyFilter;
$gdbo -> getRec();
$listCompany = $gdbo -> dbData;
# Service Board
$gdbo -> sql= "SELECT
id_srv_board,
srv_board
FROM _srv_board
WHERE srv_board_dr is null ".$serviceboardFilter;
$gdbo -> getRec();
$listServiceBoard = $gdbo -> dbData;
# Service Status
$gdbo -> sql= "SELECT
id_sys_status,
sys_status
FROM _sys_status
WHERE id_sys_status_tp=4
ORDER BY sys_status_order;";
$gdbo -> getRec();
$listServiceStatus = $gdbo -> dbData;
# Service Record
$gdbo -> sql= "
SELECT
id_cust_contact,
id_srv,
id_srv_board,
id_srv_tp,
id_srv_loc,
id_srv_src,
id_cust_company,
id_contract,
id_srv_t,
srv_budgeted_hours,
FROM_UNIXTIME(UNIX_TIMESTAMP(srv_dc)+$gmtOffset) as srv_dc,
srv_dr,
srv_summary,
id_srv_severity,
id_srv_impact,
id_srv_priority,
id_sys_user,
id_srv_status,
id_srv_updatedBy,
id_cust_branch,
FROM_UNIXTIME(UNIX_TIMESTAMP(srv_du)+$gmtOffset) as srv_du
FROM 
_srv
WHERE
id_srv=".$_POST["id_srv"];
$gdbo -> getRec();
$rec= $gdbo -> getAssociative();
# Filterable Objects
## Branches
$branchesdbo = new ArcDb;
$branchesdbo -> dbConStr=$globalDBCON;
$branchesdbo -> dbType = $globalDBTP;
$branchesdbo -> dbSchema = $globalDB;
$branchesdbo -> sql = "SELECT id_cust_branch, cust_branch FROM _cust_branch";
$branchesdbo -> dbFilter = "WHERE id_cust_company=";
$branchesdbo -> type = "list";
$branchesdbo -> id = "id_cust_branch";
$branchesdbo -> attributes = 'class="chBranch"';
$branchesdboJSON =encrypt(json_encode((array)$branchesdbo));
# Branch 
if (isset($rec[0]["id_cust_company"])){
$gdbo -> sql = "SELECT id_cust_branch, cust_branch FROM _cust_branch WHERE id_cust_company=".$rec[0]["id_cust_company"];
$gdbo -> getRec();
$listBranch = $gdbo -> dbData;}
else {
$listBranch = array();
}
# Contracts
$gdbo -> sql = "SELECT
a.id_contract,
a.contract,
b.id_cust_company,
b.cust_company
FROM _contract a
LEFT JOIN _cust_company b ON a.id_cust_company = b.id_cust_company
ORDER BY b.cust_company,a.contract
";
$gdbo -> getRec();
$listContract = $gdbo -> dbData;
$fsService=
array(
array(
array('Point of Contact','id_cust_contact',null,1,$listContact,$rec[0]["id_cust_contact"],"dPOC"),
array('Organization','id_cust_company','onchange="filterRec(this,\''.$branchesdboJSON.'\',\'dBranch\')"',1,$listCompany,$rec[0]["id_cust_company"]),
array('Branch','id_cust_branch',' class=\'chBranch\'',1,$listBranch,$rec[0]["id_cust_branch"],"dBranch"),
),
array(
array("Contract","id_contract",null,1,$listContract,$rec[0]["id_contract"]),
array("Service Board","id_srv_board",null,1,$listServiceBoard,$rec[0]["id_srv_board"]),
array("Status","id_srv_status",null,1,$listServiceStatus,$rec[0]["id_srv_status"]),
),
array(
array("Severity","id_srv_severity",null,1,$listSeverity,$rec[0]["id_srv_severity"]),
array("Impact","id_srv_impact",null,1,$listServiceImpact,$rec[0]["id_srv_impact"]),
array("Date Created",null,'disabled="disabled"',0,null,$rec[0]["srv_dc"]),
),
array(
array("Type","id_srv_tp",null,1,$listServiceType,$rec[0]["id_srv_tp"]),
array("Source","id_srv_src",null,1,$listServiceSource,$rec[0]["id_srv_src"]),
array("Date Updated",null,'disabled="disabled"',0,null,$rec[0]["srv_du"]),
),
array(
array("Summary","srv_summary",'style="width:100% !important"',0,null,$rec[0]["srv_summary"],null,null,null,null,null,'style="width:100% !important"'),
),
);
?>
<!-- begin forms -->
<form method="post" id="frmService" name="frmService" action="javascript:submitFrmVals('content','/_mod/smod_10/sql.php','service_detail.id_cust_company,service_detail.id_cust_branch,service_detail.id_srv_board,service_detail.id_srv_status,service_detail.id_srv_severity,service_detail.id_srv_impact,service_detail.id_srv_tp,service_detail.srv_summary,service_detail.srv_descr','&id_srv=<?php echo $_POST["id_srv"]?>&form=frmService&action=update','frmService')">
<fieldset id="service_detail">
<legend>Service Detail</legend>
<?=frmElements($fsService);?>
</fieldset>
<fieldset id="description">
<legend>Description</legend>
<div id="control_descr">
<div>
<input type="button" value="Add" id="add_descr" onclick="srvAdd('descr')"/>
<input type="button" value="Save" id="save_descr" disabled="disabled" onclick="srvSave('descr')"/>
<input type="button" disabled="disabled" value="Close" id="close_descr" onclick="srvCancel('descr')"/>
<input type="button" value="Delete" id="delete_descr" disabled="disabled" onclick="srvDelete('descr')"/>
<input type="checkbox" id="private_descr" disabled="disabled"/>Mark Private
<input type="hidden" id="rid_descr"/>
<input type="hidden" id="sid_descr" value="<?php echo bin2hex($_POST["id_srv"]) ?>"/>
</div>
</div>
<div class="frmrow" id="ir_descr" style="display:none">
<textarea id="it_descr" name="it_descr" onkeydown="detectTab(this,event)"></textarea>
</div>
<div class="frmrow" id="rec_descr">
<?php
$gdbo -> sql= "SELECT
a.id_srv_descr,
script2text(a.srv_descr) as notes,
FROM_UNIXTIME(UNIX_TIMESTAMP(a.srv_descr_dc)+$gmtOffset) as srv_descr_dc,
FROM_UNIXTIME(UNIX_TIMESTAMP(a.srv_descr_du)+$gmtOffset,'%M %D, %Y') as srv_descr_du,
concat(d.cust_contact_givenName,' ',d.cust_contact_familyName),
a.id_sys_user,
a.srv_descr_privateBit,
(SELECT concat('<img class=''avatarSmall'' src=''/_avatar/thumb_',fso,''' height=''32'' width=''32''/>') as fso FROM _fso where fso_pkcol='id_cust_contact' AND fso_pk=b.id_cust_contact AND fso_originalname='avatar') as Avatar
FROM
_srv_descr a 
LEFT JOIN _sys_user_emp b ON a.id_sys_user=b.id_sys_user
LEFT JOIN _cust_contact d ON b.id_cust_contact=d.id_cust_contact
WHERE a.id_srv=".$_POST["id_srv"]." ORDER BY
a.srv_descr_dc DESC";
$gdbo -> getRec();
$aDescriptions = $gdbo -> dbData;
if (count($aDescriptions) > 0) {
$debug = 1;
foreach ($aDescriptions as $key => $row) {
$isPrivate=($row[6]==1?" [Private Note]":"");
$editRecord=($row[5]==$id_sys_user ? " onclick=\"editRow('descr'," . $row[0] . ")\"":"");
echo "<div class=\"frmrowh\" $editRecord>";
foreach ($row as $col => $value) {
if ($col == 1) {
$notes=($row[6]==1&&$row[5]!=$id_sys_user?encrypt($value):$value);
$notes=wordwrap($notes,128, "\n",true);
echo
"<div class=\"srvDate\" id=\"descr_head_" . $row[0] . "\">
<div class=\"flLeft\">".
$row[7]." Last updated " . $row[3] . " by ".$row[4]."</div>
<div class=\"flRight\">$isPrivate</div></div>";
echo "<div class=\"srvData\" id=\"descr_" . $row[0] . "\">" . $notes . "</div>";
}
}
echo "</div>";
}
} else {
echo "";
}
?>
</div>
</fieldset>
<fieldset id="analysis">
<legend>Analysis</legend>
<div id="control_analysis">
<div>
<input type="button" value="Add" id="add_analysis" onclick="srvAdd('analysis')"/>
<input type="button" value="Save" id="save_analysis" disabled="disabled" onclick="srvSave('analysis')"/>
<input type="button" disabled="disabled" value="Close" id="close_analysis" onclick="srvCancel('analysis')"/>
<input type="button" value="Delete" id="delete_analysis" disabled="disabled" onclick="srvDelete('analysis')"/>
<input type="checkbox" id="private_analysis" disabled="disabled"/>Mark Private
<input type="hidden" id="rid_analysis"/>
<input type="hidden" id="sid_analysis" value="<?php echo bin2hex($_POST["id_srv"]) ?>"/>
</div>
</div>
<div class="frmrow" id="ir_analysis" style="display:none">
<textarea id="it_analysis" name="it_analysis" onkeydown="detectTab(this,event)"></textarea>
</div>
<div class="frmrow" id="rec_analysis">
<?php
$gdbo -> sql= "SELECT
a.id_srv_analysis,
script2text(a.srv_analysis) as notes,
FROM_UNIXTIME(UNIX_TIMESTAMP(a.srv_analysis_dc)+$gmtOffset) as srv_analysis_dc,
FROM_UNIXTIME(UNIX_TIMESTAMP(a.srv_analysis_du)+$gmtOffset,'%M %D, %Y') as srv_analysis_du,
concat(d.cust_contact_givenName,' ',d.cust_contact_familyName),
a.id_sys_user,
a.srv_analysis_privateBit,
(SELECT concat('<img class=''avatarSmall'' src=''/_avatar/thumb_',fso,''' height=''32'' width=''32''/>') as fso FROM _fso where fso_pkcol='id_cust_contact' AND fso_pk=b.id_cust_contact AND fso_originalname='avatar') as Avatar
FROM
_srv_analysis a
LEFT JOIN _sys_user_emp b ON a.id_sys_user=b.id_sys_user
LEFT JOIN _cust_contact d ON b.id_cust_contact=d.id_cust_contact
WHERE a.id_srv=".$_POST["id_srv"]."
ORDER BY
a.srv_analysis_dc DESC";
$gdbo -> getRec();
$aAnalysis = $gdbo -> dbData;
if (count($aAnalysis) > 0) {
$debug = 1;
foreach ($aAnalysis as $key => $row) {
$isPrivate=($row[6]==1?" [Private Note]":"");
$editRecord=($row[5]==$id_sys_user ? " onclick=\"editRow('analysis'," . $row[0] . ")\"":"");
echo "<div class=\"frmrowh\" $editRecord>";
foreach ($row as $col => $value) {
if ($col==1){
$notes=($row[6]==1&&$row[5]!=$id_sys_user?encrypt(hex2str($value)):$value);
$notes=wordwrap($notes,128, "\n",true);
echo "<div class=\"srvDate\" id=\"analysis_head_" . $row[0] . "\">".
"<div class=\"flLeft\">".
$row[7]." Last updated " . $row[3] . " by ".$row[4]."</div>".
"<div class=\"flRight\">$isPrivate</div></div>";
echo "<div class=\"srvData\" id=\"analysis_" . $row[0] . "\">" . $notes . "</div>";
}
}
echo "</div>";
}
} else {
echo "";
}
?>
</div>
</fieldset>
<fieldset id="resolution">
<legend>Resolution</legend>
<div id="control_resolution">
<div>
<input type="button" value="Add" id="add_resolution" onclick="srvAdd('resolution')"/>
<input type="button" value="Save" id="save_resolution" disabled="disabled" onclick="srvSave('resolution')"/>
<input type="button" disabled="disabled" value="Close" id="close_resolution" onclick="srvCancel('resolution')"/>
<input type="button" value="Delete" id="delete_resolution" disabled="disabled" onclick="srvDelete('resolution')"/>
<input type="checkbox" id="private_resolution" disabled="disabled"/>Mark Private
<input type="hidden" id="rid_resolution"/>
<input type="hidden" id="sid_resolution" value="<?php echo bin2hex($_POST["id_srv"]) ?>"/>
</div>
</div>
<div class="frmrow" id="ir_resolution" style="display:none">
<textarea id="it_resolution" name="it_resolution" onkeydown="detectTab(this,event)"></textarea>
</div>
<div class="frmrow" id="rec_resolution">
<?php
$gdbo -> sql= "SELECT
a.id_srv_resolution,
script2text(a.srv_resolution) as Notes,
FROM_UNIXTIME(UNIX_TIMESTAMP(a.srv_resolution_dc)+$gmtOffset) as srv_resolution_dc,
FROM_UNIXTIME(UNIX_TIMESTAMP(a.srv_resolution_du)+$gmtOffset,'%M %D, %Y') as srv_resolution_du,
concat(d.cust_contact_givenName,' ',d.cust_contact_familyName),
a.id_sys_user,
a.srv_resolution_privateBit,
(SELECT concat('<img class=''avatarSmall'' src=''/_avatar/thumb_',fso,''' height=''32'' width=''32''/>') as fso FROM _fso where fso_pkcol='id_cust_contact' AND fso_pk=b.id_cust_contact AND fso_originalname='avatar') as Avatar
FROM
_srv_resolution a
LEFT JOIN _sys_user_emp b ON a.id_sys_user=b.id_sys_user
LEFT JOIN _cust_contact d ON b.id_cust_contact=d.id_cust_contact
WHERE a.id_srv=".$_POST["id_srv"]."
ORDER BY
a.srv_resolution_dc DESC";
$gdbo -> getRec();
$aResolution = $gdbo -> dbData;
if (count($aResolution) > 0) {
$debug = 1;
foreach ($aResolution as $key => $row) {
$isPrivate=($row[6]==1?" [Private Note]":"");
$editRecord=($row[5]==$id_sys_user ? " onclick=\"editRow('resolution'," . $row[0] . ")\"":"");
echo "<div class=\"frmrowh\" $editRecord>";
foreach ($row as $col => $value) {
if ($col == 1) {
$notes=($row[6]==1&&$row[5]!=$id_sys_user?encrypt(hex2str($value)):$value);
$notes=wordwrap($notes,128, "\n",true);
echo "<div class=\"srvDate\" id=\"resolution_head_" . $row[0] . "\">
<div class=\"flLeft\">".
$row[7]." Last updated " . $row[3] . " by ".$row[4]."</div>".
"<div class=\"flRight\">$isPrivate</div></div>";
echo "<div class=\"srvData\" id=\"resolution_" . $row[0] . "\">" . $notes . "</div>";
}
}
echo "</div>";
}
} else {
echo "";
}
?>
</div>
</fieldset>
<fieldset id="resources">
<legend>Resources</legend>
<div class="frmrow">
<div class="frmcol">
<label>Import from Group</label>
<?php echo selList($listSystemGroup, "id_sys_group", "arc('fldUser','".$path."filterMembers.php','id='+this.options[this.selectedIndex].value,0,1)","class='elementIcon'") ?>
<span class="elementIconBox" onClick="addResource(<?php echo $_POST["id_srv"]?>)"><i class="fa fa-check"></i></span>
</div>
</div>
<div id="fldUser" style="display:none">&nbsp;</div>
<div id="listResource">
<?php include("listResources.php");?>
</div>
</fieldset>
<fieldset id="time_entry">
<legend>Time Entry</legend>
<div class="frmrow" id="tm_control">
<div class="frmcol">
<input type="button" class="clTimeButton list" disabled="disabled" onclick="tmButtons(0,<?php echo $_POST["id_srv"]?>)" value="List Entries"/>
<?php if ($isEmployee === true) {?>
<input type="button" class="clTimeButton add" onclick="tmButtons(1,<?php echo $_POST["id_srv"]?>)" value="Add Time"/>
<?php }?>
</div>
<?php if ($isEmployee === true && (!isset($status) || $status != "Approved" || $status !="Submitted")) {?>
<div class="frmcol" id="tm_control_edit">
<input type="button" class="clTimeButton save" id="tmSaveButton" disabled="disabled" onclick="document.forms['frmTime'].submit()" value="Save Time"/>
<input type="button" class="clTimeButton delete" id="tmDeleteButton" disabled="disabled" value="Delete"/>
</div>
<?php }?>
</div>
<div id="blTime"><?php include("listTimeEntries.php");?></div>
</fieldset>
</form>
<form action="#">
<fieldset id="Files">
<legend>Files</legend>
<div id="listFiles">
<?php
require("listFiles.php");
?>
</div>
</fieldset>
</form>
<script type="text/javascript">
oREC = '{"oRec":[{"module":"10","filter":"id_srv","recid":"<?=$_POST["id_srv"]?>","formname":"frmService"}]}';
<?=$icoUpload;?>
icoReport = '<span onclick="arc(\'popWindow\',\'<?=$path?>reports/reportTimeEntries.php\',\'id_srv=<?=$_POST["id_srv"]?>\',1,1)"><?=$icoReportIcon?>Time Summary</span>';
<?=$icoDelete;?>
<?=$icoCollapse;?>
<?=$icoPreview;?>
$("#oOptions").append(icoDelete+icoPreview+icoUpload+icoReport+icoCollapse);
collapseTabs();
</script>
