<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once ("_lib/php/auth.php");
printArray($_POST);
filterQryPost("id");
$id = hex2str($id);
filterQryPost("rid");
filterQryPost("val");
filterQryPost("edit");
filterQryPost("pri");
switch ($edit) {
case 0 :
$gdbo -> sql = "insert into _srv_descr (srv_descr_privateBit,id_srv,srv_descr,id_sys_user,srv_descr_du) values ($pri,$id,'$val',$id_sys_user,current_timestamp)";
$gdbo -> dbTable = "_srv_descr";
$gdbo -> execQuery();
break;
case 1 :
$gdbo -> sql = "update _srv_descr set srv_descr_privateBit=$pri,srv_descr_du=current_timestamp,id_srv=$id,srv_descr='$val',id_sys_user=$id_sys_user where id_srv_descr=$rid";
$gdbo -> dbTable = "_srv_descr";
$gdbo -> execQuery();
break;
case 2 :
$gdbo -> sql = "delete from _srv_descr where id_srv_descr=$rid";
$gdbo -> dbTable = "_srv_descr";
$gdbo -> execQuery();
break;
}
$gdbo -> sql = "update _srv set srv_du=current_timestamp where id_srv=$id";
$gdbo -> dbTable = "_srv";
$gdbo -> execQuery();
$gdbo -> sql = "SELECT
a.id_srv_descr,
script2text(a.srv_descr) as \"notes\",
FROM_UNIXTIME(UNIX_TIMESTAMP(a.srv_descr_dc)+$gmtOffset) as \"srv_descr_dc\",
FROM_UNIXTIME(UNIX_TIMESTAMP(a.srv_descr_du)+$gmtOffset,'%M %D, %Y') as \"srv_descr_du\",
concat(d.cust_contact_givenName,' ',d.cust_contact_familyName) as \"fullname\",
a.id_sys_user,
a.srv_descr_privateBit,
(SELECT concat('<img src=''/_avatar/thumb_',fso,''' height=''32'' width=''32''/>') as fso FROM _fso where fso_pkcol='id_cust_contact' AND fso_pk=b.id_cust_contact AND fso_originalname='avatar') as \"Avatar\"
FROM
_srv_descr a 
LEFT JOIN _sys_user_emp b ON a.id_sys_user=b.id_sys_user
LEFT JOIN _cust_contact d ON b.id_cust_contact=d.id_cust_contact
WHERE a.id_srv=" . $id . " ORDER BY
a.srv_descr_dc DESC";
$gdbo -> getRec();
$aDescriptions = $gdbo -> dbData;
if (count($aDescriptions) > 0) {
$debug = 1;
foreach ($aDescriptions as $key => $row) {
$isPrivate = ($row[6] == 1 ? " [Private Note]" : "");
$editRecord = ($row[5] == $id_sys_user ? " onclick=\"editRow('descr'," . $row[0] . ")\"" : "");
echo "<div class=\"frmrowh\" $editRecord>";
foreach ($row as $col => $value) {
if ($col == 1) {
$notes = ($row[6] == 1 && $row[5] != $id_sys_user ? encrypt($value) : $value);
$notes = wordwrap($notes, 128, "
", true);
echo "<div class=\"srvDate\" id=\"descr_head_" . $row[0] . "\">
<div class=\"flLeft\">".
$row[7]." Last updated " . $row[3] . " by ".$row[4]."</div><div class=\"flRight\">$isPrivate</div></div>";
echo "<div class=\"srvData\" id=\"descr_" . $row[0] . "\">" . $notes . "</div>";
}
}
echo "</div>";
}
} else {
echo "";
}
?>
