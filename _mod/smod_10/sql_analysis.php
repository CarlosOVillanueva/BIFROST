<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once ("_lib/php/auth.php");
$debug = 0;
printArray($_POST);
filterQryPost("id");
$id = hex2str($id);
filterQryPost("rid");
filterQryPost("val");
filterQryPost("edit");
filterQryPost("pri");
switch ($edit) {
case 0 :
$gdbo -> sql = "insert into _srv_analysis (srv_analysis_privateBit,id_srv,srv_analysis,id_sys_user,srv_analysis_du) values ($pri,$id,'$val',$id_sys_user,current_timestamp)";
$gdbo -> dbTable = "_srv_analysis";
$gdbo -> execQuery();
break;
case 1 :
$gdbo -> sql = "update _srv_analysis set srv_analysis_privateBit=$pri,srv_analysis_du=current_timestamp,id_srv=$id,srv_analysis='$val',id_sys_user=$id_sys_user where id_srv_analysis=$rid";
$gdbo -> dbTable = "_srv_analysis";
$gdbo -> execQuery();
break;
case 2 :
$gdbo -> sql = "delete from _srv_analysis where id_srv_analysis=$rid";
$gdbo -> dbTable = "_srv_analysis";
$gdbo -> execQuery();
break;
}
/*update service ticket*/
$gdbo -> sql = "update _srv set srv_du=current_timestamp where id_srv=$id";
$gdbo -> dbTable = "_srv";
$gdbo -> execQuery();
/*list results*/
$gdbo -> sql = "SELECT
a.id_srv_analysis,
script2text(a.srv_analysis) as \"notes\",
FROM_UNIXTIME(UNIX_TIMESTAMP(a.srv_analysis_dc)+$gmtOffset) as \"srv_analysis_dc\",
FROM_UNIXTIME(UNIX_TIMESTAMP(a.srv_analysis_du)+$gmtOffset,'%M %D, %Y') as \"srv_analysis_du\",
concat(d.cust_contact_givenName,' ',d.cust_contact_familyName) as \"fullname\",
a.id_sys_user,
a.srv_analysis_privateBit,
(SELECT concat('<img src=''/_avatar/thumb_',fso,''' height=''32'' width=''32''/>') as fso FROM _fso where fso_pkcol='id_cust_contact' AND fso_pk=b.id_cust_contact AND fso_originalname='avatar') as \"Avatar\"
FROM
_srv_analysis a
LEFT JOIN _sys_user_emp b ON a.id_sys_user=b.id_sys_user
LEFT JOIN _cust_contact d ON b.id_cust_contact=d.id_cust_contact
WHERE a.id_srv=" . $id . "
ORDER BY
a.srv_analysis_dc DESC";
$gdbo -> getRec();
$aAnalysis = $gdbo -> dbData;
if (count($aAnalysis) > 0) {
$debug = 1;
foreach ($aAnalysis as $key => $row) {
$isPrivate = ($row[6] == 1 ? " [Private Note]" : "");
$editRecord = ($row[5] == $id_sys_user ? " onclick=\"editRow('analysis'," . $row[0] . ")\"" : "");
echo "<div class=\"frmrowh\" $editRecord>";
foreach ($row as $col => $value) {
if ($col == 1) {
$notes = ($row[6] == 1 && $row[5] != $id_sys_user ? encrypt(hex2str($value)) : $value);
$notes = wordwrap($notes, 128, "
", true);
echo "<div class=\"srvDate\" id=\"analysis_head_" . $row[0] . "\">" . "<div class=\"flLeft\">".
$row[7]." Last updated " . $row[3] . " by ".$row[4]."</div><div class=\"flRight\">$isPrivate</div></div>";
echo "<div class=\"srvData\" id=\"analysis_" . $row[0] . "\">" . $notes . "</div>";
}
}
echo "</div>";
}
} else {
echo "";
}
?>
