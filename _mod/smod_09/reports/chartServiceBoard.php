<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once("_lib/php/auth.php");
$gdbo -> sql ="
SELECT
srv_board as \"Service Board\",
(SELECT count(id_srv) FROM _srv WHERE id_srv_board = a.id_srv_board and id_srv_status=16) as \"Pending\",
(SELECT count(id_srv) FROM _srv WHERE id_srv_board = a.id_srv_board and id_srv_status=17) as \"Assigned\",
(SELECT count(id_srv) FROM _srv WHERE id_srv_board = a.id_srv_board and id_srv_status=18) as \"Working\",
(SELECT count(id_srv) FROM _srv WHERE id_srv_board = a.id_srv_board and id_srv_status=19) as \"Closed\"
FROM _srv_board a
LEFT JOIN _cust_department b ON a.id_cust_department=b.id_cust_department WHERE id_srv_board=".$_POST["id_srv_board"];
$gdbo -> getRec();
$graphData = $gdbo -> getAssociative();
$graphDataJSON = json_encode($graphData);

?>
<script>
var chartData=<?=$graphDataJSON?>;
var chart = AmCharts.makeChart("chartdiv", {
    "type": "serial",
	"theme": "none",
    "legend": {
        "horizontalGap": 10,
        "maxColumns": 1,
        "position": "right",
		"useGraphSettings": true,
		"markerSize": 10
    },
    "dataProvider": chartData,
    "valueAxes": [{
        "stackType": "regular",
        "axisAlpha": 0.5,
        "gridAlpha": 0
    }],
    "graphs": [{
        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
        "fillAlphas": 0.8,
        "labelText": "[[value]]",
        "lineAlpha": 0.3,
        "title": "Pending",
        "type": "column",
		"color": "#000000",
        "valueField": "Pending"
    }, {
        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
        "fillAlphas": 0.8,
        "labelText": "[[value]]",
        "lineAlpha": 0.3,
        "title": "Assigned",
        "type": "column",
		"color": "#000000",
        "valueField": "Assigned"
    }, {
        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
        "fillAlphas": 0.8,
        "labelText": "[[value]]",
        "lineAlpha": 0.3,
        "title": "Working",
        "type": "column",
		"color": "#000000",
        "valueField": "Working"
    }, {
        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
        "fillAlphas": 0.8,
        "labelText": "[[value]]",
        "lineAlpha": 0.3,
        "title": "Closed",
        "type": "column",
		"color": "#000000",
        "valueField": "Closed"
    }],
    "rotate": true,
    "categoryField": "Service Board",
    "categoryAxis": {
        "gridPosition": "start",
        "axisAlpha": 0,
        "gridAlpha": 0,
        "position": "left"
    },
	"exportConfig":{
      "menuTop":"0px",
      "menuItems": [{
      "icon": '/_thirdparty/amcharts/images/export.png',
      "format": 'png'	  
      }]  
    }
});
</script>
<div id="chartdiv" class="chart" style="width: 100%; height: 435px;font-size:11pt"></div>
