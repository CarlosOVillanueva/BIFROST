<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/ /* ********************************************************************** */
require_once ("_lib/php/auth.php");
/* ********************************************************************** */
filterQryPost("edit");
filterQryPost("id_srv_board");
/* ********************************************************************** */
$debug = 0;
/* ********************************************************************** */
switch ($edit) {
case 0 :
$gdbo -> dbTable = "_srv_board";
$gdbo -> insertRec();
$_POST['id_srv_board'] = $gdbo -> insertedID;
include ('edit.php');
include ("_error/status.php");
break;
/* ********************************************************************** */
case 1 :
/* ********************************************************************** */
$gdbo -> dbTable = "_srv_board";
$gdbo -> updateRec('id_srv_board='.$_POST["id_srv_board"]);
$_POST['id_srv_board'] = $_POST["id_srv_board"];
include ('edit.php');
include ("_error/status.php");
break;
/* ********************************************************************** */
case 2 :
if (!isset($_POST["srvConfirmation"])) {$_POST["srvConfirmation"] = 2;
}
switch ($_POST["srvConfirmation"]) {
case 0 :
echo $_POST["id_srv_board_alt"];
if ($_POST["id_srv_board_alt"] != "") {
$gdbo -> dbTable = "_srv_board";
$gdbo -> sql = "update _srv set id_srv_board=" . $_POST["id_srv_board_alt"] . " WHERE id_srv_board=" . $id_srv_board;
$gdbo -> execQuery();
}
break;
case 1 :
$service = new ArcDb;
$service -> dbConStr=$globalDBCON;
$service -> dbType = $globalDBTP;
$service -> dbSchema = $globalDB;
$service -> dbTable = "_srv";
$service -> deleteRec("id_srv_board=" . $id_srv_board);
break;
default :
break;
}
$service = new ArcDb;
$service -> dbConStr=$globalDBCON;
$service -> dbType = $globalDBTP;
$service -> dbSchema = $globalDB;
$service -> dbTable = "_srv_board";
$service -> deleteRec("id_srv_board=" . $id_srv_board);
require ("index.php");
break;
}
?>
<script type="text/javascript">clearPop();</script>
