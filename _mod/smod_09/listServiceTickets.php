<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once("_lib/php/auth.php");
$filterServiceBoard = (isset($_POST["id_srv_board"])?" WHERE a.id_srv_board=".$_POST["id_srv_board"]." ":"");
$arctbl = new ArcTbl;
$arctbl -> dbConStr=$globalDBCON;
$arctbl -> dbOffset = 0;
$arctbl -> dbLimit = 23;
$arctbl -> dbType = $globalDBTP;
$arctbl -> dbSchema = $globalDB;
$arctbl -> recLink= "/_mod/smod_10/edit.php";
$arctbl -> actionFilterKey="id_srv";
$arctbl -> recIndex="Ticket";
$arctbl -> recFilter=(isset($_POST["recFilter"])?hex2str($_POST["recFilter"]):"");
$arctbl -> ignoreCols=array("Date Removed","id_srv_board","id_srv_status","id_cust_company","id_cust_department");
$arctbl -> ignoreFilterCols=array("Date Removed","id_srv_board","id_srv_status","id_cust_company","id_cust_department");
if ($isEmployee === false){
$arctbl -> ignoreFilterCols[] = "Company";
$arctbl -> ignoreFilterCols[] = "Board";}
$arctbl -> recQuery = "
SELECT
a.id_srv_board,
a.id_srv_status,
a.id_srv Ticket,
a.srv_dr 'Date Removed',
d.srv_board Board,
a.id_cust_company,
b.cust_company Company,
c.cust_branch Branch,
concat(e.sys_status) Status,
f.srv_severity Severity,
g.srv_impact Impact,
a.srv_summary Summary,
d.id_cust_department
FROM
_srv a
LEFT JOIN _cust_company b ON a.id_cust_company=b.id_cust_company
LEFT JOIN _cust_branch c ON a.id_cust_branch=c.id_cust_branch
LEFT JOIN _srv_board d ON a.id_srv_board=d.id_srv_board
LEFT JOIN _sys_status e ON a.id_srv_status=e.id_sys_status
LEFT JOIN _srv_severity f ON a.id_srv_severity=f.id_srv_severity
LEFT JOIN _srv_impact g ON a.id_srv_impact=g.id_srv_impact
".$filterServiceBoard."
ORDER BY e.sys_status desc
";
$arctbl -> actionDestination = "content";
$arctbl -> ajDestination = "list09-1";
$arctbl -> ajPage = $path."list.php";
$arctbl -> build();
echo hex2str($arctbl -> tblNav);
echo $arctbl->dataTable;
?>
