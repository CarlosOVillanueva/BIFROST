<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once ("_lib/php/auth.php");
$gdbo -> sql = "SELECT srv_board from _srv_board WHERE id_srv_board=" . $_POST["id_srv_board"];
$gdbo -> getRec();
$aBoard = $gdbo -> dbData;
$gdbo -> sql = "SELECT id_srv from _srv WHERE id_srv_board=" . $_POST["id_srv_board"];
$gdbo -> getRec();
$aSRVCount = $gdbo -> dbData;
$gdbo -> sql = "SELECT id_srv_board,srv_board from _srv_board WHERE id_srv_board!=" . $_POST["id_srv_board"];
$gdbo -> getRec();
$aSrvBoard = $gdbo -> dbData;
?>
<div id="popBox">
<form method="POST" name="frmDeleteSrvBoard" action="javascript:submitFrmVals('content','/_mod/smod_09/insertSQL.php','','&edit=2&id_srv_board=<?php echo $_POST["id_srv_board"]?>','frmDeleteSrvBoard')">
<fieldset>
<legend>
Service Board Deletion Validation
</legend>
<div class="frmrow">
You have selected to delete the following service board: <strong><?php echo $aBoard[0][0]?></strong>
</div>
<?php
if (count($aSRVCount)==0){?>
<div class="frmrow">
There are no service records associated with this service board. It is safe to delete.
</div>
<?php
}else{
?>
<div class="frmrow">
During our validation, we discovered <?php echo count($aSRVCount)?> associated service records. What do you want to do to these records?
</div>
<div class="frmrow" style="margin-top:10px">
<div class="frmcol">
<input type="radio" name="srvConfirmation" value="1" onclick="delSrvBoardOption(1)"/>Remove
</div>
<div class="frmcol">
<input type="radio" name="srvConfirmation" value="2" onclick="delSrvBoardOption(2)"/>Do Nothing
</div>
<div class="frmcol">
<input type="radio" name="srvConfirmation" checked="checked" value="0" onclick="delSrvBoardOption(0)"/>Move
</div>
<div class="frmcol">
<?php echo selList($aSrvBoard,"id_srv_board_alt")?>
</div>
</div>
<div id="delList" style="margin-top:10px">
<?php include($path."dellist.php");?>
</div>
<?php }?>
<div class="frmrow">
<div class="flLeft">
<input type="button" onclick="clearPop()" value="Cancel"/>
</div>
<div class="flRight">
<input type="button" onclick="document.forms['frmDeleteSrvBoard'].submit()" value="Delete"/>
</div>
</div>
<?php ?>
</fieldset>
</form>
</div>
