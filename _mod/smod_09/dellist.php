<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once("_lib/php/auth.php");
$filterServiceBoard = (isset($_POST["id_srv_board"])?" WHERE a.id_srv_board=".$_POST["id_srv_board"]:"");
$arctbl = new ArcTbl;
$arctbl -> dbConStr=$globalDBCON;
$arctbl -> dbOffset = 0;
$arctbl -> dbLimit = 23;
$arctbl -> dbType = $globalDBTP;
$arctbl -> dbSchema = $globalDB;
$arctbl -> recLink= $path."edit.php";
$arctbl -> actionFilterKey="id_srv_board";
$arctbl -> recIndex="id_srv_board";
$arctbl -> ignoreCols=array("id_srv","id_srv_board");
$arctbl -> ignoreFilterCols=array("id_srv","id_srv_board");
$arctbl -> recQuery = "
SELECT
a.id_srv,
d.srv_board Board,
a.id_srv_board,
b.cust_company Company,
c.cust_branch Branch,
a.srv_summary Summary
FROM
_srv a
LEFT JOIN _cust_company b ON a.id_cust_company=b.id_cust_company
LEFT JOIN _cust_branch c ON a.id_cust_branch=c.id_cust_branch
LEFT JOIN _srv_board d ON a.id_srv_board=d.id_srv_board ".$filterServiceBoard ;
$arctbl -> actionDestination = "content";
$arctbl -> ajDestination = "delList";
$arctbl -> ajPage = $path."dellist.php";
$arctbl -> build();
echo hex2str($arctbl -> tblNav);
echo $arctbl->dataTable;
?>
