<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/* ********************************************************************** */
require_once("_lib/php/auth.php");
require_once("_includes/heading.php");
require_once("_includes/optionstoolbar.php");
/* ********************************************************************** */
/* recordset.board */
/* ********************************************************************** */
$gdbo -> sql = "SELECT id_srv_board,id_cust_department, srv_board FROM _srv_board where id_srv_board=".$_POST["id_srv_board"];
$gdbo -> getRec();
$aBoard = $gdbo -> dbData;
/* ********************************************************************** */
/* list.department */
/* ********************************************************************** */
$gdbo -> sql = "SELECT id_cust_department,cust_department FROM _cust_department";
$gdbo -> getRec();
$aDepartment = $gdbo -> dbData;
$requiredFields="id_cust_department,srv_board";
?>
<form method="post" name="editBoard" action="javascript:submitFrmVals('content','/_mod/smod_09/insertSQL.php','<?=$requiredFields?>','&id_srv_board=<?php echo $_POST["id_srv_board"] ?>&edit=1','editBoard')">
<fieldset id="service_board_detail">
<legend>Service Board Detail</legend>
<div class="frmrow">
<div class="frmcol">
<label>*Department</label>
<?php echo selList($aDepartment, "id_cust_department",null,null,null,null,$aBoard[0][1]) ?>
</div>
<div class="frmcol">
<label>*Board Name</label>
<input type="text" name="srv_board" id="srv_board" value="<?php echo $aBoard[0][2]?>"/>
</div>
</div>
</fieldset>
</form>
<div class="cList">
<div id="list09-1" ><?php require("listServiceTickets.php");?></div>
</div>
<script type="text/javascript">
$("#oOptions").append('<span id="icoDelete" onclick="delSrvBoard(<?php echo $_POST["id_srv_board"]?>)"><i class="fa fa-times-circle fa-fw"></i>Delete</span>');
</script>
