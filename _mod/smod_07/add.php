<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once("_lib/php/auth.php");
require_once("_includes/optionstoolbar.php");
# Insurance Provider
require("_model/dboInsuranceProvider.php");
$gdbo -> getRec();
$listInsuranceProvider = $gdbo -> dbData;
# Insurance Plan Type
require("_model/dboInsurancePlanType.php");
$gdbo -> getRec();
$listInsuranceTp = $gdbo -> dbData;
# Fieldsets
$fsInsurance=
array(
array(
array('*Provider', 'id_cust_company',null,1,$listInsuranceProvider),
),
array(
array('*Plan Type', 'id_hr_insurance_plan_tp',null,1,$listInsuranceTp ),
),
array(
array('*Plan Name', 'hr_insurance_plan',null,0),
),
array(
array('Plan Description', 'hr_insurance_plan_descr','onkeydown="detectTab(this,event)"',6),
),
);
$requiredFields="id_cust_company,id_hr_insurance_plan_tp,hr_insurance_plan";
?>
<script type="text/javascript">$(".icoadd").remove();</script>
<form method="post" name="frmInsurance" id="frmInsurance" action="javascript:submitFrmVals('content','/_mod/smod_07/sql.php','<?=$requiredFields?>','&form=frmInsurance&action=insert','frmInsurance')">
<fieldset id="insurance_detail">
<legend>Insurance Detail</legend>
<?=frmElements($fsInsurance)?>
</fieldset>
</form>
<script type="text/javascript">
CKEDITOR.replace("hr_insurance_plan_descr");
</script>
