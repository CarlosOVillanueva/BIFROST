<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
# Includes
require_once("_lib/php/auth.php");
# Parse Form Action
$action = $_POST['action'];
$form = $_POST['form'];
$formAction = $form . "," . $action;
# Begin SQL Functions
switch ($formAction) {
case "frmInsurance,insert":
/*****************************************************************************/
$gdbo -> dbTable = "_hr_insurance_plan";
$gdbo -> insertRec();
$_POST["id_hr_insurance_plan"] = $gdbo -> insertedID;
include('edit.php');
/*****************************************************************************/
break;
case "frmInsurance,update":
/*****************************************************************************/
$gdbo -> dbTable = "_hr_insurance_plan";
$gdbo -> updateRec("id_hr_insurance_plan=".$_POST["id_hr_insurance_plan"]);
include('edit.php');
/*****************************************************************************/
break;
case "frmInsurance,delete":
/*****************************************************************************/
$gdbo -> dbTable = "_hr_insurance_plan";
$gdbo -> deleteRec("id_hr_insurance_plan=".$_POST["id_hr_insurance_plan"]);
/*****************************************************************************/ 
# Purge all records that may be linked to this company
# IMPORTANT! All records associated with the branch will be purged
$gdbo -> sql ="SELECT TABLE_NAME FROM information_schema.columns WHERE COLUMN_NAME='id_hr_insurance_plan'";
$gdbo -> getRec();
$delTables = $gdbo -> dbData;
foreach($delTables as $row => $column) {
foreach($column as $data => $value) {
$gdbo -> dbTable = $value;
$gdbo -> deleteRec("id_hr_insurance_plan=".$_POST["id_hr_insurance_plan"]);
}
}
include("index.php");
break; 
default:
break;
}
include("_error/status.php");
?>
