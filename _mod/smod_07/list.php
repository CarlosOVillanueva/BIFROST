<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once("_lib/php/auth.php");
$arctbl = new ArcTbl;
$arctbl -> dbConStr=$globalDBCON;
$arctbl -> dbOffset = 0;
$arctbl -> dbLimit = 23;
$arctbl -> dbType = $globalDBTP;
$arctbl -> dbSchema = $globalDB;
$arctbl -> recLink= $path."edit.php";
//$arctbl -> recDetail = "hex2str(\$record['Description2'])";
$arctbl -> actionFilterKey="id_hr_insurance_plan";
$arctbl -> recIndex="id_hr_insurance_plan";
$arctbl -> ignoreCols=array("id_hr_insurance_plan","Description","Description2");
$arctbl -> ignoreFilterCols=array("id_hr_insurance_plan","Description2");
$arctbl -> recQuery = "
SELECT
a.id_hr_insurance_plan,
b.cust_company as \"Company\",
a.hr_insurance_plan as \"Plan\",
unhex(a.hr_insurance_plan_descr) as \"Description\",
a.hr_insurance_plan_descr as \"Description2\"
FROM
_hr_insurance_plan a
JOIN
_cust_company b ON a.id_cust_company=b.id_cust_company";
$arctbl -> actionDestination = "content";
$arctbl -> ajDestination = "list07";
$arctbl -> ajPage = $path."list.php";
$arctbl -> build();
echo hex2str($arctbl -> tblNav);
echo $arctbl->dataTable;
?>
