<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once("_lib/php/auth.php");
$arctbl = new ArcTbl;
$arctbl -> dbConStr=$globalDBCON;
$arctbl -> dbOffset = 0;
$arctbl -> dbLimit = 23;
$arctbl -> dbType = $globalDBTP;
$arctbl -> dbSchema = $globalDB;
$arctbl -> recLink= $path."edit.php";
$arctbl -> actionFilterKey="id_hr_position";
$arctbl -> recIndex="id_hr_position";
$arctbl -> ignoreCols=array("id_hr_position");
$arctbl -> ignoreFilterCols=array("id_hr_position");
$arctbl -> recQuery = "
SELECT
a.id_hr_position,
b.cust_department as \"Department\",
a.hr_position as \"Position\"
FROM
_hr_position a
JOIN
_cust_department b ON a.id_cust_department=b.id_cust_department";
$arctbl -> actionDestination = "content";
$arctbl -> ajDestination = "list06";
$arctbl -> ajPage = $path."list.php";
$arctbl -> build();
echo hex2str($arctbl -> tblNav);
echo $arctbl->dataTable;
?>
