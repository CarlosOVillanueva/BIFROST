<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once("_lib/php/auth.php");
require_once("_includes/optionstoolbar.php");
# Branch
require("_model/dboParentBranch.php");
$gdbo -> getRec();
$listParentBranch = $gdbo -> dbData;
# Pay Type
require("_model/dboPayTp.php");
$gdbo -> getRec();
$listPayTp = $gdbo -> dbData;
# Filterable Objects
## Departments
$departmentdbo = new ArcDb;
$departmentdbo -> dbConStr=$globalDBCON;
$departmentdbo -> dbType = $globalDBTP;
$departmentdbo -> dbSchema = $globalDB;
$departmentdbo -> sql = "SELECT id_cust_department, cust_department FROM _cust_department";
$departmentdbo -> dbFilter = "WHERE id_cust_branch=";
$departmentdbo -> type = "list";
$departmentdbo -> id = "id_cust_department";
$departmentdboJSON =encrypt(json_encode((array)$departmentdbo));
# Fieldsets
$fsPosition=
array(
array(
array('*Branch','id_cust_branch','onchange="filterRec(this,\''.$departmentdboJSON.'\',\'dDepartment\')"',1,$listParentBranch),
array('*Department','id_cust_department','disabled="disabled" class="chDepartment"',1,null,null,"dDepartment"),
),
array(
array('*Job Title','hr_position',null,0),
),
array(
array('Compensation Type','id_hr_pay_tp',null,1,$listPayTp),
),
array(
array('Hourly Rate','hr_position_hourlyRate','onblur="validateElement(\'money\',this)"',0),
array('Salary','hr_position_salary','onblur="validateElement(\'money\',this)"',0),
),
);
$fsJobDescription = array(
array(
array(null,'hr_position_descr','onkeydown="detectTab(this,event)"',6),
),
);
$fsDuties = array(
array(
array(null,'hr_position_duties','onkeydown="detectTab(this,event)"',6),
),
);
$fsQualifications = array(
array(
array(null,'hr_position_qualifications','onkeydown="detectTab(this,event)"',6),
),
);
$requiredFields = "id_cust_branch,id_cust_department,hr_position";
?>
<script type="text/javascript">$(".icoadd").remove();</script>
<form method="post" name="frmPosition" id="frmPosition" action="javascript:submitFrmVals('content','/_mod/smod_06/sql.php','<?=$requiredFields?>','&form=frmPosition&action=insert','frmPosition')">
<fieldset id="position_detail">
<legend>Position Detail</legend>
<?=frmElements($fsPosition)?>
</fieldset>
<fieldset id="job_description">
<legend>Job Description</legend>
<?=frmElements($fsJobDescription)?>
</fieldset>
<fieldset id="duties">
<legend>Duties</legend>
<?=frmElements($fsDuties)?>
</fieldset>
<fieldset id="qualifications">
<legend>Qualifications</legend>
<?=frmElements($fsQualifications)?>
</fieldset>
</form>
</div>
<script type="text/javascript">
<?=$icoCollapse;?>
$("#oOptions").append(icoCollapse);
collapseTabs();
CKEDITOR.replace("hr_position_descr");
CKEDITOR.replace("hr_position_duties");
CKEDITOR.replace("hr_position_qualifications");
</script>
