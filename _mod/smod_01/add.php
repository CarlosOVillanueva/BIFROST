<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once("_lib/php/auth.php");
require_once("_includes/heading.php");
require_once("_includes/optionstoolbar.php");
# Company Type 
$gdbo -> sql = "SELECT id_cust_company_tp,cust_company_tp FROM _cust_company_tp WHERE id_cust_company_tp !=1 ORDER BY cust_company_tp;";
$gdbo -> getRec();
$listCompanyTp = $gdbo -> dbData;
# Fieldsets
$fsOrganization=
array(
array(
array('*Organization', 'cust_company',null,0),
array('*Tag', 'id_cust_company_tp',null,1,$listCompanyTp),
)
);
$requiredFields = "cust_company,id_cust_company_tp";
?>
<script type="text/javascript">$(".icoadd").remove();</script>
<form method="post" name="frmOrganization" id="frmOrganization" action="javascript:submitFrmVals('content','/_mod/smod_01/sql.php','<?=$requiredFields?>','&form=frmOrganization&action=insert','frmOrganization')">
<fieldset id="organization_detail">
<legend>Organization Detail</legend>
<?=frmElements($fsOrganization);?>
</fieldset>
</form>
