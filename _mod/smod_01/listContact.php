<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once("_lib/php/auth.php");
$arctbl = new ArcTbl;
$arctbl -> dbConStr=$globalDBCON;
$arctbl -> dbOffset = 0;$arctbl -> dbLimit = 23;
$arctbl -> dbType = $globalDBTP;
$arctbl -> dbSchema = $globalDB;
$arctbl -> recLink= "/_mod/smod_04/edit.php";
$arctbl -> actionFilterKey="id_cust_contact";
$arctbl -> recIndex="id_cust_contact";
$arctbl -> ignoreCols=array("id_cust_contact","Nickname","Middle","Company","id_cust_company");
$arctbl -> ignoreFilterCols=array("id_cust_contact","","id_cust_company");
$arctbl -> recFilter = (isset($_POST["id_cust_company"])?"WHERE id_cust_company=".$_POST["id_cust_company"]:"");
$arctbl -> recQuery = "
SELECT
a.id_cust_contact,
b.id_cust_company,
a.cust_contact_givenName as \"First\",
a.cust_contact_commonName as \"Nickname\",
a.cust_contact_middleName as \"Middle\",
a.cust_contact_familyName as \"Last\",
c.cust_company as \"Company\",
b.cust_branch as \"Branch\",
concat('"
."<a class=\"button\" onclick=\"arc(''popWindow'',''/_lib/php/ArcEmail.View.php"."'',''data=',a.id_cust_contact,''')\"><i class=\"fa fa-envelope fa-fw\"></i>Mail</a>') as \"\"
FROM _cust_contact a
LEFT JOIN _cust_branch b ON a.id_cust_branch=b.id_cust_branch
LEFT JOIN _cust_company c ON b.id_cust_company=c.id_cust_company";
$arctbl -> actionDestination = "content";
$arctbl -> ajDestination = "listContact";
$arctbl -> ajPage = $path."listContact.php";
$arctbl -> build();
echo hex2str($arctbl -> tblNav);
echo $arctbl->dataTable;
?>
