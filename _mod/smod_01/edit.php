<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once("_lib/php/auth.php");
require_once("_includes/heading.php");
require_once("_includes/optionstoolbar.php");
# Record
$gdbo -> sql = "SELECT * FROM _cust_company WHERE id_cust_company=".$_POST["id_cust_company"];
$gdbo -> getRec();
$recOrganization = $gdbo -> getAssociative();
# Company Type
$filterCompanTp = ($recOrganization[0]["id_cust_company_tp"] != 1?" WHERE id_cust_company_tp <> 1":"");
$gdbo -> sql = "SELECT id_cust_company_tp,cust_company_tp FROM _cust_company_tp".$filterCompanTp." ORDER BY cust_company_tp";
$gdbo -> getRec();
$listCompanyTp = $gdbo -> dbData;
# Evaluate Data
$disabled = ($recOrganization[0]["id_cust_company_tp"] == 1?"disabled=\"disabled\"":"");
# Fieldsets
$fsOrganization=
array(
array(
array('*Organization', 'cust_company',null,0,null,$recOrganization[0]["cust_company"]),
array('*Tag', 'id_cust_company_tp',$disabled,1,$listCompanyTp,$recOrganization[0]["id_cust_company_tp"]),
array(null, 'id_cust_company',null,3,null,$recOrganization[0]["id_cust_company"]),
)
);
$requiredFields = "cust_company,id_cust_company_tp";
?>
<form method="post" name="frmOrganization" id="frmOrganization" action="javascript:submitFrmVals('content','/_mod/smod_01/sql.php','<?=$requiredFields?>','&form=frmOrganization&action=update','frmOrganization')">
<fieldset id="organization_detail">
<legend>Organization Detail</legend>
<?=frmElements($fsOrganization);?>
</fieldset>
<fieldset id="Branches">
<legend>Branches</legend>
<div id="listBranch">
<?php include("listBranch.php"); ?>
</div>
</fieldset>
<fieldset id="Contacts">
<legend>Contacts</legend>
<div id="listContact">
<?php include("listContact.php"); ?>
</div>
</fieldset>
</form>
<?php 
if ($recOrganization[0]["id_cust_company_tp"] != 1) {?>
<script type="text/javascript">
<?=$icoCollapse;?>
<?=$icoDelete;?>
oREC = '{"oRec":[{"module":"01","filter":"id_cust_company","recid":"<?=$_POST["id_cust_company"]?>","formname":"frmOrganization"}]}';
$("#oOptions").append(icoDelete);
</script>
<?php } ?>
<script type="text/javascript">
<?=$icoCollapse;?>
<?=$icoPreview;?> 
$("#oOptions").append(icoPreview+icoCollapse);
collapseTabs();
</script>
