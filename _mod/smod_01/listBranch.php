<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once("_lib/php/auth.php");
$arctbl = new ArcTbl;
$arctbl -> dbConStr=$globalDBCON;
$arctbl -> tblKey = "listBranch";
$arctbl -> dbLimit = 23;
$arctbl -> dbType = $globalDBTP;
$arctbl -> dbSchema = $globalDB;
$arctbl -> recLink= "/_mod/smod_02/edit.php";
$arctbl -> actionFilterKey="id_cust_branch";
$arctbl -> recIndex="id_cust_branch";
$arctbl -> ignoreCols=array("id_cust_branch","id_cust_company");
$arctbl -> ignoreFilterCols=array("id_cust_branch","","Organization","id_cust_company");
$arctbl -> recFilter = (isset($_POST["id_cust_company"])?"WHERE id_cust_company=".$_POST["id_cust_company"]:"");
$arctbl -> recQuery = "
SELECT
d.id_cust_branch,
a.id_cust_company,
a.cust_company as \"Organization\",
d.cust_branch as \"Label\",
b.addr_line1 as \"Address 1\",
b.addr_line2 as \"Address 2\",
b.loc_city as \"City\",
c.loc_region as \"Region/State\",
e.loc_country as \"Country\",
b.loc_postal_code as \"Postal Code\",
concat('"
."<a class=\"button\" onclick=\"arc(''popWindow'',''/_lib/php/ArcEmail.View.php"."'',''isBranch=true&data=',d.id_cust_branch,''')\"><i class=\"fa fa-envelope fa-fw\"></i>Mail</a>') as \"\"
FROM _cust_branch d
JOIN _cust_company a on a.id_cust_company=d.id_cust_company
LEFT JOIN _addr b on d.id_addr=b.id_addr
LEFT JOIN _loc_region c on b.id_loc_region=c.id_loc_region
LEFT JOIN _loc_country e on c.id_loc_country=e.id_loc_country";
$arctbl -> actionDestination = "content";
$arctbl -> ajDestination = "listBranch";
$arctbl -> ajPage = $path."list.php";
$arctbl -> build();
echo hex2str($arctbl -> tblNav);
echo $arctbl->dataTable;
?>
