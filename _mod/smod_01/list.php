<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once("_lib/php/auth.php");
$arctbl = new ArcTbl;
$arctbl -> dbConStr=$globalDBCON;
$arctbl -> dbLimit = 23;
$arctbl -> dbType = $globalDBTP;
$arctbl -> dbSchema = $globalDB;
$arctbl -> recLink= $path."edit.php";
$arctbl -> actionFilterKey="id_cust_company";
$arctbl -> recIndex="id_cust_company";
$arctbl -> ignoreCols=array("id_cust_company");
$arctbl -> ignoreFilterCols=array("id_cust_company");
$arctbl -> recQuery = "
SELECT
a.id_cust_company,
a.cust_company as \"Organization\",
b.cust_company_tp as \"Tag\"
FROM _cust_company a
LEFT JOIN _cust_company_tp b ON a.id_cust_company_tp=b.id_cust_company_tp 
ORDER BY a.cust_company";
$arctbl -> actionDestination = "content";
$arctbl -> ajDestination = "list01";
$arctbl -> ajPage = $path."list.php";
$arctbl -> build();
echo hex2str($arctbl -> tblNav);
echo $arctbl->dataTable;
?>
