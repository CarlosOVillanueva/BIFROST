<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once("_lib/php/auth.php");
require_once("_includes/heading.php");
require_once("_includes/optionstoolbar.php");
# Recordset
$gdbo -> sql = "SELECT
a.id_cust_branch,
a.id_cust_company,
a.id_cust_branch_tp,
a.cust_branch,
a.cust_branch_dc,
a.cust_branch_dr,
a.id_addr,
c.id_cust_company_tp,
b.addr_line1,
b.addr_line2,
b.addr_line3,
b.id_loc_region,
b.loc_city,
b.loc_postal_code,
b.addr_dc,
b.addr_dr,
b.addr_label,
b.id_loc_country
FROM _cust_branch a
LEFT JOIN _cust_company c ON a.id_cust_company = c.id_cust_company
LEFT JOIN _addr b ON a.id_addr = b.id_addr 
WHERE a.id_cust_branch = ". $_POST["id_cust_branch"];
$gdbo -> getRec();
$recBranch = $gdbo -> getAssociative();
# Company 
$gdbo -> sql = "SELECT a.id_cust_company,a.cust_company, b.id_cust_company_tp,b.cust_company_tp FROM _cust_company a LEFT JOIN _cust_company_tp b ON a.id_cust_company_tp = b.id_cust_company_tp";
$gdbo -> getRec();
$listCompany = $gdbo -> dbData;
echo "<script type='text/javascript'>window.listcompany=" . json_encode($listCompany) . "</script>";
# Company Type
$gdbo -> sql = "SELECT distinct a.id_cust_company_tp, a.cust_company_tp FROM _cust_company_tp a RIGHT JOIN _cust_company b ON a.id_cust_company_tp = b.id_cust_company_tp ORDER BY a.cust_company_tp";
$gdbo -> getRec();
$listCompanyTp = $gdbo -> dbData;
# Branch Type
$gdbo -> sql = "SELECT id_cust_branch_tp,cust_branch_tp FROM _cust_branch_tp ORDER BY cust_branch_tp";
$gdbo -> getRec();
$listBranchTp = $gdbo -> dbData;
# Fieldsets
$fsBranch=
array(
array(
array('*Tag', 'id_cust_company_tp','onchange="childListFilter(this,\'list\',window.listcompany,2,\'0-1\',\'id_cust_company\',\'chCompany\')"',1,$listCompanyTp,$recBranch[0]["id_cust_company_tp"]),
array('*Organization', 'id_cust_company','disabled=\'disabled\' class=\'chCompany\'',1,$listCompany,$recBranch[0]["id_cust_company"]),
array(null, 'id_cust_branch',null,3,null,$recBranch[0]["id_cust_branch"]),
),
array(
array('*Branch', 'cust_branch',null,0,null,$recBranch[0]["cust_branch"]),
array('*Branch Type', 'id_cust_branch_tp',null,1,$listBranchTp,$recBranch[0]["id_cust_branch_tp"])
)
);
$requiredFields = "id_cust_company,cust_branch,id_cust_company_tp,id_cust_branch_tp"
?>
<form method="post" name="frmBranch" id="frmBranch" action="javascript:submitFrmVals('content','/_mod/smod_02/sql.php','<?=$requiredFields?>','&form=frmBranch&action=update','frmBranch')">
<fieldset id="branch_detail">
<legend>Branch Detail</legend>
<?= frmElements($fsBranch);?>
</fieldset>
<?php 
$contacts = new ContactMethods;
$contacts -> filter = "id_cust_branch";
$contacts -> id = $recBranch[0]["id_cust_branch"];
$contacts -> build();
$address = new ArcAddress;
$address -> filter = "id_addr";
$address -> id = $recBranch[0]["id_addr"];
$address -> build();
?>
<fieldset id="Contacts">
<legend>Contacts</legend>
<div id="listContact">
<?php include("listContact.php"); ?>
</div>
</fieldset>
</form>
<script type="text/javascript">
oREC = '{"oRec":[{"module":"02","filter":"id_cust_branch","recid":"<?=$_POST["id_cust_branch"]?>","formname":"frmBranch"}]}';
<?=$icoCollapse;?>
<?=$icoPreview;?>
<?=$icoDelete;?> 
$("#oOptions").append(icoDelete+icoPreview+icoCollapse);
collapseTabs();
</script>
