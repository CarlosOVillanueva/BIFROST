<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once("_lib/php/auth.php");
require_once("_includes/heading.php");
require_once("_includes/optionstoolbar.php");
# Company 
$gdbo -> sql = "SELECT a.id_cust_company,a.cust_company, b.id_cust_company_tp,b.cust_company_tp FROM _cust_company a LEFT JOIN _cust_company_tp b ON a.id_cust_company_tp = b.id_cust_company_tp";
$gdbo -> getRec();
$listCompany = $gdbo -> dbData;
echo "<script type='text/javascript'>window.listcompany=" . json_encode($listCompany) . "</script>";
# Company Type
$gdbo -> sql = "SELECT distinct a.id_cust_company_tp, a.cust_company_tp FROM _cust_company_tp a RIGHT JOIN _cust_company b ON a.id_cust_company_tp = b.id_cust_company_tp ORDER BY a.cust_company_tp";
$gdbo -> getRec();
$listCompanyTp = $gdbo -> dbData;
# Branch Type
$gdbo -> sql = "SELECT id_cust_branch_tp,cust_branch_tp FROM _cust_branch_tp ORDER BY cust_branch_tp";
$gdbo -> getRec();
$listBranchTp = $gdbo -> dbData;
# Fieldsets
$fsBranch=
array(
array(
array('*Tag', 'id_cust_company_tp','onchange="childListFilter(this,\'list\',window.listcompany,2,\'0-1\',\'id_cust_company\',\'chCompany\')"',1,$listCompanyTp),
array('*Organization', 'id_cust_company','disabled=\'disabled\' class=\'chCompany\'',1,$listCompany),
),
array(
array('*Branch', 'cust_branch',null,0),
array('*Branch Type', 'id_cust_branch_tp',null,1,$listBranchTp)
)
);
$requiredFields = "id_cust_company,cust_branch,id_cust_company_tp,id_cust_branch_tp"
?>
<script type="text/javascript">$(".icoadd").remove();</script>
<form method="post" name="frmBranch" id="frmBranch" action="javascript:submitFrmVals('content','/_mod/smod_02/sql.php','<?=$requiredFields?>','&form=frmBranch&action=insert','frmBranch')">
<fieldset id="branch_detail">
<legend>Branch Detail</legend>
<?=frmElements($fsBranch);?>
</fieldset>
<?php 
$contacts = new ContactMethods;
$contacts -> build();
?>
<?php 
$address = new ArcAddress;
$address -> build();
?>
</form>
<script type="text/javascript">
<?=$icoCollapse;?>
$("#oOptions").append(icoCollapse);
collapseTabs();
</script>
