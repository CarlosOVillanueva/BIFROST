<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once("_lib/php/auth.php");
require_once("_includes/optionstoolbar.php");
# Company 
$gdbo -> sql = "SELECT 
a.id_cust_company,
a.cust_company,
b.id_cust_company_tp,
b.cust_company_tp 
FROM _cust_company a 
LEFT JOIN _cust_company_tp b 
ON a.id_cust_company_tp = b.id_cust_company_tp 
WHERE b.id_cust_company_tp !=1 
ORDER BY b.cust_company_tp asc, a.cust_company asc";
$gdbo -> getRec();
$listCompany = $gdbo -> dbData;
# Title 
require("_model/dboTitle.php");
$gdbo -> getRec();
$listTitle= $gdbo -> dbData;
# Suffix
require("_model/dboSuffix.php");
$gdbo -> getRec();
$listSuffix= $gdbo -> dbData;
# Filterable Objects
## Branches
$branchesdbo = new ArcDb;
$branchesdbo -> dbConStr=$globalDBCON;
$branchesdbo -> dbType = $globalDBTP;
$branchesdbo -> dbSchema = $globalDB;
$branchesdbo -> sql = "SELECT id_cust_branch, cust_branch FROM _cust_branch";
$branchesdbo -> dbFilter = "WHERE id_cust_company=";
$branchesdbo -> type = "list";
$branchesdbo -> id = "id_cust_branch";
$branchesdbo -> attributes = 'class="chBranch"';
$branchesdboJSON =encrypt(json_encode((array)$branchesdbo));
# Fieldset Variables
$dateBirth ='<div class="elementIconBox" onclick="openCalendar(this,\'interface\',null,\'clDateBirth\')"><i class="fa fa-calendar"></i></div>';
# Fieldsets
$fsContact=
array(
array(
array('*First','cust_contact_givenName',null,0),
array('*Last','cust_contact_familyName',null,0),
),
array(
array('Middle','cust_contact_middleName',null,0),
array('Nickname','cust_contact_commonName',null,0),
),
array(
array('Title','id_name_title',null,1,$listTitle),
array('Suffix','id_name_suffix',null,1,$listSuffix),
),
);
$fsBusiness=
array(
array(
array('Job Title','cust_contact_role',null,0),
array('Department','cust_department',null,0),
),
array(
array('Organization','id_cust_company','onchange="filterRec(this,\''.$branchesdboJSON.'\',\'dBranch\')"',1,$listCompany),
array('Branch','id_cust_branch','disabled=\'disabled\' class=\'chBranch\'',1,null,null,"dBranch"),
),
);
$fsPrivate=
array(
array(
array('Birthdate','cust_contact_bdate','class="clDateBirth elementIcon" validateElement(\'date\',this)',0,null,null,null,$dateBirth,null,'YYYY-MM-DD'),
),
array(
array('Notes','cust_contact_notes','onkeydown="detectTab(this,event)"',6),
),
);
$requiredFields = "cust_contact_givenName,cust_contact_familyName";
?>
<script type="text/javascript">$(".icoadd").remove();</script>
<form method="post" id="frmContact" name="frmContact" action="javascript:submitFrmVals('content','/_mod/smod_04/sql.php','<?=$requiredFields?>','&action=insert&form=frmContact','frmContact')">
<fieldset id="detail">
<legend>Detail</legend>
<?=frmElements($fsContact);?>
</fieldset>
<?php 
$contacts = new ContactMethods;
$contacts -> build();
?>
<fieldset id="business">
<legend>Business</legend>
<?=frmElements($fsBusiness);?>
</fieldset>
<fieldset id="personal">
<legend>Personal</legend>
<?=frmElements($fsPrivate);?>
</fieldset>
<?php 
$address = new ArcAddress;
$address -> build();
?>
</form>
<script type="text/javascript">
<?=$icoCollapse;?>
$("#oOptions").append(icoCollapse);
collapseTabs();
CKEDITOR.replace("cust_contact_notes");
</script>
