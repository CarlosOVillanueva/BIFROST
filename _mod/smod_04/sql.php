<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
# Includes
require_once ("_lib/php/auth.php");
# Parse Contact Methods
if (isset($_POST["contact_method_mText"])) {
	$contactMethods = hex2str($_POST["contact_method_mText"]);
	$contactMethods = json_decode($contactMethods);
	$contactMethodsInsertStr = array2sqlstring($contactMethods, "0,2");
};
# Multi Form Columns
$contactMethodsCols = "contact_method,contact_method_notes,id_contact_method_tp,id_cust_contact";
# Parse Form Action
$action = $_POST['action'];
$form = $_POST['form'];
$formAction = $form . "," . $action;
# Begin SQL Functions
switch ($formAction) {
	case "frmContact,insert" :
		/*****************************************************************************/
		# Insert Address Record
		$gdbo -> dbTable = "_addr";
		$gdbo -> insertRec();
		$_POST["id_addr"] = $gdbo -> insertedID;
		# Insert Contact Record
		$gdbo -> dbTable = "_cust_contact";
		$gdbo -> insertRec();
		$id_cust_contact = $gdbo -> insertedID;
		# Insert Contact Methods
		if (!empty($contactMethodsInsertStr)) {
			$contactMethodsInsertStr = str_replace("~id~", $id_cust_contact, $contactMethodsInsertStr);
			$gdbo -> dbTable = "_contact_method";
			$gdbo -> sql = "(" . $contactMethodsCols . ") values " . $contactMethodsInsertStr;
			$gdbo -> insertRec();
		};
		$_POST['id_cust_contact'] = $id_cust_contact;
		include ('edit.php');
		/*****************************************************************************/
		break;
	case "frmContact,update" :
		/*****************************************************************************/
		# Update Address Record
		if (!empty($_POST["id_addr"])) {
			$gdbo -> dbTable = "_addr";
			$gdbo -> updateRec("id_addr=" . $_POST["id_addr"]);
		} else {
			$gdbo -> dbTable = "_addr";
			$gdbo -> insertRec();
			$_POST["id_addr"] = $gdbo -> insertedID;
		}
		# Update Contact Record
		$gdbo -> dbTable = "_cust_contact";
		$gdbo -> updateRec("id_cust_contact=" . $_POST["id_cust_contact"]);
		# Update Contact Methods
		$gdbo -> dbTable = "_contact_method";
		$gdbo -> deleteRec("id_cust_contact=" . $_POST["id_cust_contact"]);
		if (!empty($contactMethodsInsertStr)) {
			$gdbo -> dbTable = "_contact_method";
			$contactMethodsInsertStr = str_replace("~id~", $_POST["id_cust_contact"], $contactMethodsInsertStr);
			$gdbo -> sql = "(" . $contactMethodsCols . ") values " . $contactMethodsInsertStr;
			$gdbo -> insertRec();
		}
		include ('edit.php');
		/*****************************************************************************/
		break;
	case "frmContact,delete" :
		/*****************************************************************************/
		$gdbo -> dbTable = "_cust_contact";
		$gdbo -> deleteRec("id_cust_contact=" . $_POST["id_cust_contact"]);
		/*****************************************************************************/
		# Purge all records that may be linked to this company
		# IMPORTANT! All records associated with the branch will be purged
		$gdbo -> sql = "SELECT TABLE_NAME FROM information_schema.columns WHERE COLUMN_NAME='id_cust_contact'";
		$gdbo -> getRec();
		$delTables = $gdbo -> dbData;
		foreach ($delTables as $row => $column) {
			foreach ($column as $data => $value) {
				$gdbo -> dbTable = $value;
				$gdbo -> deleteRec("id_cust_contact=" . $_POST["id_cust_contact"]);
			}
		}
		include ("index.php");
		/*****************************************************************************/
		break;
	default :
		break;
}
include ("_error/status.php");
?>
