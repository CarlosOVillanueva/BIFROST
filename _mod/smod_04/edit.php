<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once("_lib/php/auth.php");
require_once("_includes/heading.php");
require_once("_includes/optionstoolbar.php");
# Recordset
$gdbo -> sql = "SELECT 
a.id_cust_contact,
a.id_cust_contact_tp,
a.id_cust_branch,
a.id_name_title,
a.cust_contact_familyName,
a.cust_contact_middleName,
a.cust_contact_givenName,
a.cust_contact_maidenName,
a.cust_contact_commonName,
a.id_name_suffix,
a.cust_contact_role,
a.cust_contact_dc,
a.cust_contact_dr,
UNHEX(a.cust_contact_notes) as cust_contact_notes,
a.cust_contact_bdate,
a.cust_department,
a.id_cust_company,
a.id_addr,
b.id_hr_emp,
b.id_hr_position,
b.id_hr_gender,
b.id_hr_race,
b.id_hr_ethnicity,
b.hr_emp_pto,
b.hr_emp_sick,
b.hr_emp_hRate,
b.hr_emp_salary,
b.hr_emp_accrRate,
b.hr_emp_ssn,
b.hr_emp_disabled,
b.hr_emp_date_hire,
b.hr_emp_dc,
b.hr_emp_dr,
b.id_hr_insurance_plan,
b.hr_emp_insMemberNo,
b.hr_emp_insGroupNo,
b.hr_emp_insPerscriptionNo,
b.hr_emp_insDateEffective,
b.hr_emp_timeApprover,
b.hr_emp_expenseApprover,
b.id_hr_emp_status,
b.hr_emp_citizenBit,
b.id_hr_maritalStatus,
b.hr_emp_timeApproverOrg,
b.hr_emp_payFrequency
FROM _cust_contact a
LEFT JOIN _hr_emp b ON a.id_cust_contact = b.id_cust_contact
WHERE a.id_cust_contact =" . $_POST["id_cust_contact"];
$gdbo -> getRec();
$rec = $gdbo -> getAssociative();
$gdbo -> sql = "SELECT fso FROM _fso WHERE fso_pk=".$_POST["id_cust_contact"]." AND fso_originalname='avatar' AND fso_pkcol='id_cust_contact'";
$gdbo -> getRec();
$fsoRec = $gdbo -> dbData;
if (isset($fsoRec[0][0]) && $fsoRec[0][0]) {
$base64=shell_exec("base64 ".$avatarWritePath.$fsoRec[0][0]);
$avatar="data:image/png;base64,".$base64;
} else {
$avatar="/_img/avatar.png";
}
# Rules
## Disable elements linked to parent company
$disabled = ($rec[0]["id_cust_company"] == 1 ? 'disabled="disabled"' : null); 
$filterParent = ($rec[0]["id_cust_company"] <> 1 ? 'where a.id_cust_company <> 1' : '');
## Pull position from parent company
if ($rec[0]["id_hr_position"] != "" ){
$gdbo -> sql = "SELECT
a.id_hr_position,
a.id_cust_department,
a.hr_position,
a.hr_position_descr,
a.hr_position_duties,
a.hr_position_qualifications,
a.hr_position_hourlyRate,
a.hr_position_salary,
a.id_hr_pay_tp,
b.id_cust_branch,
b.cust_department
FROM _hr_position a
LEFT JOIN _cust_department b ON a.id_cust_department = b.id_cust_department
WHERE a.id_hr_position=" . $rec[0]["id_hr_position"];
$gdbo -> getRec();
$recPosition = $gdbo -> getAssociative();
$position = $recPosition[0]["hr_position"];
$department = $recPosition[0]["cust_department"];
} else {
$position = $rec[0]["cust_contact_role"];
$department = $rec[0]["cust_department"];
}
# Company 
$gdbo -> sql = "SELECT 
a.id_cust_company,
a.cust_company,
b.id_cust_company_tp,
b.cust_company_tp 
FROM _cust_company a 
LEFT JOIN _cust_company_tp b 
ON a.id_cust_company_tp = b.id_cust_company_tp " . $filterParent . " ORDER BY b.cust_company_tp asc, a.cust_company asc";
$gdbo -> getRec();
$listCompany = $gdbo -> dbData;
# Branch 
if (isset($rec[0]["id_cust_company"])){
$gdbo -> sql = "SELECT id_cust_branch, cust_branch FROM _cust_branch WHERE id_cust_company=".$rec[0]["id_cust_company"];
$gdbo -> getRec();
$listBranch = $gdbo -> dbData;}
else {
$listBranch = array();
}
# Title 
require("_model/dboTitle.php");
$gdbo -> getRec();
$listTitle= $gdbo -> dbData;
# Suffix
require("_model/dboSuffix.php");
$gdbo -> getRec();
$listSuffix= $gdbo -> dbData;
# Countries
require("_model/dboCountries.php");
$gdbo -> getRec();
$listCountries = $gdbo -> dbData;
# Filterable Objects
## Branches
$branchesdbo = new ArcDb;
$branchesdbo -> dbConStr=$globalDBCON;
$branchesdbo -> dbType = $globalDBTP;
$branchesdbo -> dbSchema = $globalDB;
$branchesdbo -> sql = "SELECT id_cust_branch, cust_branch FROM _cust_branch";
$branchesdbo -> dbFilter = "WHERE id_cust_company=";
$branchesdbo -> type = "list";
$branchesdbo -> id = "id_cust_branch";
$branchesdbo -> attributes = 'class="chBranch"';
$branchesdboJSON =encrypt(json_encode((array)$branchesdbo));
# Fieldset Variables
$dateBirth ='<div class="elementIconBox" onclick="openCalendar(this,\'interface\',null,\'clDateBirth\')"><i class="fa fa-calendar"></i></div>';
# Fieldsets
$fsContact=
array(
array(
array('*First','cust_contact_givenName',null,0,null,$rec[0]["cust_contact_givenName"]),
array('*Last','cust_contact_familyName',null,0,null,$rec[0]["cust_contact_familyName"]),
),
array(
array('Middle','cust_contact_middleName',null,0,null,$rec[0]["cust_contact_middleName"]),
array('Nickname','cust_contact_commonName',null,0,null,$rec[0]["cust_contact_commonName"]),
array(null, 'id_cust_contact',null,3,null,$rec[0]["id_cust_contact"]),
),
array(
array('Title','id_name_title',null,1,$listTitle,$rec[0]["id_name_title"]),
array('Suffix','id_name_suffix',null,1,$listSuffix,$rec[0]["id_name_suffix"]),
),
);
$fsBusiness=
array(
array(
array('Job Title','cust_contact_role',$disabled,0,null,$position),
array('Department','cust_department',$disabled,0,null,$department),
),
array(
array('Organization','id_cust_company',$disabled.' onchange="filterRec(this,\''.$branchesdboJSON.'\',\'dBranch\')"',1,$listCompany,$rec[0]["id_cust_company"]),
array('Branch','id_cust_branch',$disabled.' class=\'chBranch\'',1,$listBranch,$rec[0]["id_cust_branch"],"dBranch"),
),
);
$fsPrivate=
array(
array(
array('Birthdate','cust_contact_bdate','class="clDateBirth elementIcon" validateElement(\'date\',this)',0,null,$rec[0]["cust_contact_bdate"],null,$dateBirth,null,'YYYY-MM-DD'),
),
array(
array('Notes','cust_contact_notes','onkeydown="detectTab(this,event)"',6,null,$rec[0]["cust_contact_notes"]),
),
);
$requiredFields = "cust_contact_givenName,cust_contact_familyName";
# Plugins
## Email Form
$emailForm = new ArcEmailForm;
$emailForm -> receiverid = $_POST["id_cust_contact"];
$emailForm -> senderid = $id_cust_contact_derived;
$emailForm -> sendername = $fullName_derived;
$emailForm -> className = "popBox";
$emailFormData = bin2hex($emailForm -> buildForm());
?>
<form method="post" id="frmContact" name="frmContact" action="javascript:submitFrmVals('content','/_mod/smod_04/sql.php','<?=$requiredFields?>','&action=update&form=frmContact','frmContact')">
<fieldset id="detail">
<legend>Detail</legend>
<div>
<div style="float:left">
<?=frmElements($fsContact);?>
</div>
<div style="float:right;text-align:center;"><?php include("add_avatar.php");?></div>
</div>
</fieldset>
<?php 
$contacts = new ContactMethods;
$contacts -> filter = "id_cust_contact";
$contacts -> id = $rec[0]["id_cust_contact"];
$contacts -> build();
?>
<fieldset id="business">
<legend>Business</legend>
<?=frmElements($fsBusiness);?>
</fieldset>
<fieldset id="personal">
<legend>Personal</legend>
<?=frmElements($fsPrivate);?>
</fieldset>
<?php
$address = new ArcAddress;
$address -> filter = "id_addr";
$address -> id = $rec[0]["id_addr"];
$address -> build();
?>
</form>
<form action="#">
<fieldset id="Files">
<legend>Files</legend>
<div id="listFiles">
<?php
require("listFiles.php");
?>
</div>
</fieldset>
</form>
<script type="text/javascript">
window.oEmailData = <?="'".$emailFormData."'"?>;
oREC = '{"oRec":[{"module":"04","filter":"id_cust_contact","recid":"<?=$_POST["id_cust_contact"]?>","formname":"frmContact"}]}';
<?=$icoUpload;?>
<?=$icoCollapse;?>
<?=$icoEmail;?>
<?=$icoPreview;?> 
<?=$icoDelete;?>
$("#oOptions").append(icoDelete+icoEmail+icoPreview+icoUpload+icoCollapse);
collapseTabs();
CKEDITOR.replace("cust_contact_notes");
</script>
