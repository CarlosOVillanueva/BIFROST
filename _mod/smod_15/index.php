<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once ("_lib/php/auth.php");
require_once ("_includes/heading.php");
?>
<style type="text/css">
#modSplash .spCol {
border:1px solid #fff;
overflow: hidden;
text-align: center;
padding: 10px;
background-color: #fff;
margin: 0 0 10px 0;
cursor: default;
background-color: #ffffff; /* Old browsers */
-webkit-box-shadow: 0px 0px 1px 0px rgba(0,0,0,0.75);
-moz-box-shadow: 0px 0px 1px 0px rgba(0,0,0,0.75);
box-shadow: 0px 0px 1px 0px rgba(0,0,0,0.75);
}
#modSplash .spCol h1 {
text-shadow:.05em .05em #fff!important;
}
#modSplash .spCol .fa {
text-shadow:.02em .02em #fff!important;
}
#modSplash .spCol .num {
text-shadow:none;
}
#modSplash .address {
color:#cc0011!important;

}
#modSplash .address .fa {
color:#cc0011;
}
#modSplash .address .fa .num,#modSplash .address h1 {
color:#cc0011!important;
}
#modSplash .business {
color:#ee4400!important;

}
#modSplash .business .fa {
color:#ee4400;
}
#modSplash .business .fa .num,#modSplash .business h1 {
color:#ee4400!important;
}
#modSplash .contracts {
color:#ff8811!important;

}
#modSplash .contracts .fa {
color:#ff8811;
}
#modSplash .contracts .fa .num,#modSplash .contracts h1 {
color:#ff8811!important;
}
.faLine {
float:left;
margin:0 5px;
width:250px;
}
#modSplash .finance {
color:#22bb00!important;

}
#modSplash .finance .fa {
color:#22bb00;
}
#modSplash .finance .fa .num,#modSplash .finance h1 {
color:#22bb00!important;
}
#modSplash .it {
color:#6600dd!important;

}
#modSplash .it .fa {
color:#6600dd;
}
#modSplash .it .fa .num,#modSplash .it h1 {
color:#6600dd!important;
}
#modSplash .logistics {
color:#cc00bb!important;

}
#modSplash .logistics .fa {
color:#cc00bb;
}
#modSplash .logistics.fa .num,#modSplash .logistics h1 {
color:#cc00bb!important;
}
#modSplash .service {
color:#2200ee!important;

}
#modSplash .service .fa {
color:#2200ee;
}
#modSplash .service .fa .num,#modSplash .service h1 {
color:#2200ee!important;
}
.spCol .fa {
font-size:50px;
line-height:36pt;
margin-right:10px;
position:relative;
vertical-align:middle;
}
.spCol .fa .num {
color:#484848;
font-family:sans-serif;
font-size:24pt;
font-weight:700;
height:50px;
left:50%;
margin-left:-25px;
margin-top:-25px;
position:absolute;
text-align:center;
top:50%;
width:50px;
}
.spCol .faLine {
font-size:11pt;
font-weight:700;
line-height:36pt;
margin-bottom:12px;
text-align:left;
}
.spCol .faLine:hover {
text-decoration:underline;
}
.spCol h1 {
color:#585858;
font-size:14pt;
font-weight:700;
margin-bottom:10px;
text-shadow:.05em .05em #fff;
}
.spColContainer {
float:left;
height:300px;
margin-right:10px;
width:250px;
}
#modSplash .time {
color:#0077cc!important;

}
#modSplash .time .fa {
color:#0077cc;
}
#modSplash .time .fa .num,#modSplash .time h1 {
color:#0077cc!important;
}
</style>
<?php
function modLink($MOD) {
$link = "";
foreach ($GLOBALS["sysPermissions"] as $row => $data) {
if ($data["id_sys_module_sub"] == $MOD && $data["sys_permission"] != 0) {
$link = 'onclick="arc(\'content\',\'/_mod/smod_' . $MOD . '/index.php\')"';
}
}
return $link;
}
function modDisabled($MOD) {
$disabled = false;
foreach ($GLOBALS["sysPermissions"] as $row => $data) {
if ($data["id_sys_module_sub"] == $MOD && $data["sys_permission"] == 0) {
$disabled = true;
}
}
return $disabled;
}
function topModDisabled($MOD) {
$total = array();
foreach ($GLOBALS["sysPermissions"] as $row => $data) {
if ($data["id_sys_module"] == $MOD && $data["sys_permission"] != 0) {
$total[] = $data["id_sys_module_sub"];
}
}
if (count($total)==0) {
$disabled = true;
} else {
$disabled = false;
}
return $disabled;
}
?>
<div class="cList" id="modSplash">
<?php if (topModDisabled('01')===false) {?>
<div class="spCol address">
<h1>Populate Address Book</h1>
<?php if (modDisabled('01')===false) {
?>
<div class="faLine" <?= modLink('01'); ?>>
<i class="fa fa-globe fa-fw"><span class="num"></span></i>Organizations
</div>
<?php } ?>
<?php if (modDisabled('02')===false) {
?>
<div class="faLine" <?= modLink('02'); ?>>
<i class="fa fa-building fa-fw"><span class="num"></span></i>Branches
</div>
<?php } ?>
<?php if (modDisabled('04')===false) {
?>
<div class="faLine" <?= modLink('04'); ?>>
<i class="fa fa-user fa-fw"><span class="num"></span></i>Contacts
</div>
<?php } ?>
</div>
<?php } ?>
<?php if (topModDisabled('02')===false) {?>
<div class="spCol business">
<h1>Define Your Business</h1>
<?php if (modDisabled('03')===false) {
?>
<div class="faLine" <?= modLink('03'); ?>>
<i class="fa fa-cog fa-fw"><span class="num"></span></i>Departments
</div>
<?php } ?>
<?php if (modDisabled('06')===false) {
?>
<div class="faLine" <?= modLink('06'); ?>>
<i class="fa fa-briefcase fa-fw"><span class="num"></span></i>Positions
</div>
<?php } ?>
<?php if (modDisabled('07')===false) {
?>
<div class="faLine" <?= modLink('07'); ?>>
<i class="fa fa-ambulance fa-fw"><span class="num"></span></i>Insurance
</div>
<?php } ?>
<?php if (modDisabled('05')===false) {
?>
<div class="faLine" <?= modLink('05'); ?>>
<i class="fa fa-user fa-fw"><span class="num"></span></i>Employees
</div>
<?php } ?>
</div>
<?php } ?>
<?php if (topModDisabled('04')===false) {?>
<div class="spCol contracts">
<h1>Monitor Contracts</h1>
<?php if (modDisabled('08')===false) {
?>
<div class="faLine" <?= modLink('08'); ?>>
<i class="fa fa-file-text fa-fw"></i>Contracts
</div>
<?php } ?>
</div>
<?php } ?>
<?php if (topModDisabled('13')===false) {?>
<div class="spCol finance">
<h1>Manage Finances</h1>
<?php if (modDisabled('27')===false) {
?>
<div class="faLine" <?= modLink('27'); ?>>
<i class="fa fa-money fa-fw"></i>Invoices
</div>
<?php } ?>
<?php if (modDisabled('29')===false) {?>
<div class="faLine" <?= modLink('29'); ?>>
<i class="fa fa-calculator fa-fw"></i>Purchases
</div>
<?php } ?>
</div>
<?php } ?>
<?php if (topModDisabled('03')===false) {?>
<div class="spCol time">
<h1>Track Your Time</h1>
<?php if (modDisabled('11')===false) {
?>
<div class="faLine" <?= modLink('11'); ?>>
<i class="fa fa-clock-o fa-fw"></i>Time Card
</div>
<?php } ?>
<?php if (modDisabled('25')===false) {
?>
<div class="faLine" <?= modLink('25'); ?>>
<i class="fa fa-book fa-fw"></i>Time Entries
</div>
<?php } ?>
<?php if (modDisabled('16')===false) {
?>
<div class="faLine" <?= modLink('16'); ?>>
<i class="fa fa-check-square-o fa-fw"></i>My Approvals
</div>
<?php } ?>
</div>
<?php } ?>
<?php if (topModDisabled('07')===false) {?>
<div class="spCol service">
<h1>Service and Support</h1>
<?php if (modDisabled('09')===false) {
?>
<div class="faLine" <?= modLink('09'); ?>>
<i class="fa fa-life-ring fa-fw"></i>Service Boards
</div>
<?php } ?>
<?php if (modDisabled('10')===false) {
?>
<div class="faLine" <?= modLink('10'); ?>>
<i class="fa fa-dashboard fa-fw"></i>Tickets
</div>
<?php } ?>
</div>
<?php } ?>
<?php if (topModDisabled('12')===false) {?>
<div class="spCol it">
<h1>IT Service Management</h1>
<?php if (modDisabled('18')===false) {
?>
<div class="faLine" <?= modLink('18'); ?>>
<i class="fa fa-book fa-fw"></i>Device Catalogs
</div>
<?php } ?>
<?php if (modDisabled('26')===false) {
?>
<div class="faLine" <?= modLink('26'); ?>>
<i class="fa fa-cube fa-fw"></i>Parts Catalog
</div>
<?php } ?>
</div>
<?php } ?>
<?php if (topModDisabled('08')===false) {?>
<div class="spCol logistics">
<h1>Logistics</h1>
<?php if (modDisabled('17')===false) {?>
<div class="faLine" <?= modLink('17'); ?>>
<i class="fa fa-cubes fa-fw"></i>Inventory Catalog
</div>
<?php }?>
<?php if (modDisabled('23')===false) {?>
<div class="faLine" <?= modLink('23'); ?>>
<i class="fa fa-cube fa-fw"></i>Parts Catalog
</div>
<?php }?>
</div>
<?php }?>
</div>
<script type="text/javascript">
$(".icoadd").remove();
</script>
