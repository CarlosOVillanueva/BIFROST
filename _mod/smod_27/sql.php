<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
# Includes
require_once("_lib/php/auth.php");
if (isset($_POST["lineItem_mText"])){
$lineItems = hex2str($_POST["lineItem_mText"]);
$lineItems = json_decode($lineItems);
$lineItemsInsertStr = array2sqlstring($lineItems,'0,2,3,4');
};
if (isset($_POST["payment_mText"])){
$paymentItems = hex2str($_POST["payment_mText"]);
$paymentItems = json_decode($paymentItems);
$paymentItemsInsertStr = array2sqlstring($paymentItems,'0,4');
};
# Multi Form Columns
$lineItemsCols="invoice_item,invoice_item_dservice,invoice_item_qty,invoice_item_rate,invoice_item_amount,invoice_item_percentTax,invoice_item_totalAmount,id_contract,id_invoice_qty_tp,id_invoice";
$paymentItemsCols ="invoice_payment,invoice_payment_refNumber,invoice_payment_dreceived,invoice_payment_dcleared,id_invoice_payment_tp,id_contract,id_invoice";
# Parse Form Action
$action = $_POST['action'];
$form = $_POST['form'];
$formAction = $form . "," . $action;
# Begin SQL Functions
switch ($formAction) {
case "frmInvoice,insert":
/*****************************************************************************/
$_POST["invoice_du"]=$dtTimeUTC;
$_POST["id_sys_user"]=$id_sys_user;
# Insert Invoice Record
$gdbo -> dbTable = "_invoice";
$gdbo -> insertRec();
$id_invoice = $gdbo -> insertedID;
$_POST['id_invoice']=$id_invoice;
##
# Insert Line Items
if (!empty($lineItemsInsertStr)){
$gdbo -> dbTable = "_invoice_item";
$lineItemsInsertStr = str_replace("~id~", $_POST["id_invoice"], $lineItemsInsertStr);
$gdbo -> sql = "(".$lineItemsCols.") values ".$lineItemsInsertStr;
$gdbo -> insertRec();
}
# Insert Payments
if (!empty($paymentItemsInsertStr)){
$gdbo -> dbTable = "_invoice_payment";
$paymentItemsInsertStr = str_replace("~id~", $_POST["id_invoice"], $paymentItemsInsertStr);
$gdbo -> sql = "(".$paymentItemsCols.") values ".$paymentItemsInsertStr;
$gdbo -> insertRec();
}
include('edit.php');
/*****************************************************************************/
break;
case "frmInvoice,update":
/*****************************************************************************/
$_POST["invoice_du"]=$dtTimeUTC;
$_POST["invoice_lastUpdatedBy"]=$id_sys_user;
# Update Contact Record
$gdbo -> dbTable = "_invoice";
$gdbo -> updateRec("id_invoice=".$_POST["id_invoice"]);
##
# Update Line Items
$gdbo -> dbTable = "_invoice_item";
$gdbo -> deleteRec("id_invoice=".$_POST["id_invoice"]);
if (!empty($lineItemsInsertStr)){
$gdbo -> dbTable = "_invoice_item";
$lineItemsInsertStr = str_replace("~id~", $_POST["id_invoice"], $lineItemsInsertStr);
$gdbo -> sql = "(".$lineItemsCols.") values ".$lineItemsInsertStr;
$gdbo -> insertRec();
}
# Update Payments
$gdbo -> dbTable = "_invoice_payment";
$gdbo -> deleteRec("id_invoice=".$_POST["id_invoice"]);
if (!empty($paymentItemsInsertStr)){
$gdbo -> dbTable = "_invoice_payment";
$paymentItemsInsertStr = str_replace("~id~", $_POST["id_invoice"], $paymentItemsInsertStr);
$gdbo -> sql = "(".$paymentItemsCols.") values ".$paymentItemsInsertStr;
$gdbo -> insertRec();
}
include('edit.php');
/*****************************************************************************/
break;
case "frmInvoice,delete":
/*****************************************************************************/
$gdbo -> sql ="SELECT TABLE_NAME FROM information_schema.columns WHERE COLUMN_NAME='id_invoice'";
$gdbo -> getRec();
$contact_tables = $gdbo -> dbData;
foreach($contact_tables as $row => $column) {
foreach($column as $data => $value) {
$gdbo -> dbTable = $value;
$gdbo -> deleteRec("id_invoice=".$_POST["id_invoice"]);
}
}
include('index.php');
/*****************************************************************************/ 
break; 
default:
break;
}
include("_error/status.php");
?>
