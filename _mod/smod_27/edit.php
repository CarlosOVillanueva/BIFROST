<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once("_lib/php/auth.php");
require_once("_includes/optionstoolbar.php");
/*
SELECT _invoice.id_invoice,
_invoice.invoice,
_invoice.invoice_dc,
_invoice.invoice_du,
_invoice.invoice_ddue,
_invoice.invoice_dpaid,
_invoice.invoice_dsubmitted,
_invoice.invoice_dcleared,
_invoice.id_sys_user,
_invoice.invoice_lastModifiedBy,
_invoice.id_cust_company,
_invoice.id_cust_branch,
_invoice.invoice_poc,
_invoice.invoice_department,
_invoice.invoice_percentDiscount,
_invoice.invoice_totalTax,
_invoice.invoice_totalAmount,
_invoice.invoice_amount,
_invoice.id_sys_status
FROM _invoice;
*/
# Record
$gdbo -> sql = "SELECT
id_invoice,
invoice,
FROM_UNIXTIME(UNIX_TIMESTAMP(invoice_dc)+$gmtOffset) as invoice_dc,
FROM_UNIXTIME(UNIX_TIMESTAMP(invoice_du)+$gmtOffset) as invoice_du,
invoice_ddue,
invoice_dsubmitted,
invoice_dcleared,
invoice_dpaid,
id_sys_user,
id_sys_status,
id_cust_company,
id_cust_branch,
invoice_department,
invoice_poc,
invoice_percentDiscount,
invoice_amount,
invoice_totalAmount,
invoice_totalTax,
invoice_lastModifiedBy
FROM _invoice 
WHERE id_invoice=".$_POST["id_invoice"];
$gdbo -> getRec();
$rec = $gdbo -> getAssociative();
# Branch 
if (isset($rec[0]["id_cust_company"])){
$gdbo -> sql = "SELECT id_cust_branch, cust_branch FROM _cust_branch WHERE id_cust_company=".$rec[0]["id_cust_company"];
$gdbo -> getRec();
$listBranch = $gdbo -> dbData;}
else {
$listBranch = array();
}
# Rules
## Disable elements linked to parent company
$disabled = ($rec[0]["id_cust_company"] == 1 ? 'disabled="disabled"' : null); 
# Status
$gdbo -> sql = "SELECT 
id_sys_status,
sys_status
FROM _sys_status 
WHERE id_sys_status_tp = 8
ORDER BY sys_status_order;";
$gdbo -> getRec();
$listStatusType = $gdbo -> dbData;
# Company 
$gdbo -> sql = "SELECT 
a.id_cust_company,
a.cust_company,
b.id_cust_company_tp,
b.cust_company_tp 
FROM _cust_company a 
LEFT JOIN _cust_company_tp b 
ON a.id_cust_company_tp = b.id_cust_company_tp 
ORDER BY b.cust_company_tp asc, a.cust_company asc";
$gdbo -> getRec();
$listCompany = $gdbo -> dbData;
# Contacts 
$gdbo -> sql = "SELECT
id_cust_contact,
contact,
IF(id_cust_company IS NULL,'0',id_cust_company) as id_cust_company,
IF(cust_company IS NULL,'General Contacts',cust_company) as cust_company
FROM
(
SELECT 
x.id_cust_contact,
concat(x.cust_contact_familyName,',',x.cust_contact_givenName) AS \"Contact\",
x.id_cust_company,
y.cust_company
FROM _cust_contact x
LEFT JOIN _cust_company y ON x.id_cust_company = y.id_cust_company
ORDER BY y.cust_company,x.cust_contact_familyName,x.cust_contact_givenName)
as derived ORDER BY cust_company";
$gdbo -> getRec();
$listContact = $gdbo -> dbData;
# Filterable Objects
## Branches
$branchesdbo = new ArcDb;
$branchesdbo -> dbConStr=$globalDBCON;
$branchesdbo -> dbType = $globalDBTP;
$branchesdbo -> dbSchema = $globalDB;
$branchesdbo -> sql = "SELECT id_cust_branch, cust_branch FROM _cust_branch";
$branchesdbo -> dbFilter = "WHERE id_cust_company=";
$branchesdbo -> type = "list";
$branchesdbo -> id = "id_cust_branch";
$branchesdbo -> attributes = 'class="chBranch"';
$branchesdboJSON =encrypt(json_encode((array)$branchesdbo));
# Fieldset Variables
$dateDue ='<div class="elementIconBox" onclick="openCalendar(this,\'interface\',null,\'clDueDate\')"><i class="fa fa-calendar"></i></div>';
$datePaid ='<div class="elementIconBox" onclick="openCalendar(this,\'interface\',null,\'clPaidDate\')"><i class="fa fa-calendar"></i></div>';
$dateSubmitted ='<div class="elementIconBox" onclick="openCalendar(this,\'interface\',null,\'clSubmitDate\')"><i class="fa fa-calendar"></i></div>';
$dateCleared='<div class="elementIconBox" onclick="openCalendar(this,\'interface\',null,\'clClearedDate\')"><i class="fa fa-calendar"></i></div>';
# Fieldsets
$fsCustomer=
array(
array(
array('*Organization','id_cust_company','onchange="filterRec(this,\''.$branchesdboJSON.'\',\'dBranch\')"',1,$listCompany,$rec[0]['id_cust_company']),
array('*Branch','id_cust_branch',$disabled.' class=\'chBranch\'',1,$listBranch,$rec[0]['id_cust_branch'],"dBranch"),
),
array(
array('Point of Contact','invoice_poc',null,1,$listContact,$rec[0]['invoice_poc'],"dPOC"),
array('Department','invoice_department',null,0,null,$rec[0]['invoice_department']),
)
);
$fsInvoice=
array(
array(
array('*Status','id_sys_status',null,1,$listStatusType,$rec[0]['id_sys_status']),
array('Last Updated','invoice_du','disabled="disabled"',0,null,$rec[0]['invoice_du']),
),
array(
array('*Invoice Number','invoice',null,0,null,$rec[0]['invoice']),
array('Amount','invoice_amount','disabled="disabled"',0,null,$rec[0]['invoice_amount']),
array(null, 'id_invoice',null,3,null,$rec[0]["id_invoice"]),
),
array(
array('Tax','invoice_totalTax','disabled="disabled"',0,null,$rec[0]['invoice_totalTax']),
array('Total Amount','invoice_totalAmount','disabled="disabled"',0,null,$rec[0]['invoice_totalAmount']),
), 
array(
array('Date Submitted','invoice_dsubmitted','class="clSubmitDate elementIcon" validateElement(\'date\',this)',0,null,$rec[0]['invoice_dsubmitted'],null,$dateSubmitted,null,'YYYY-MM-DD'),
array('Date Due','invoice_ddue','class="clDueDate elementIcon" validateElement(\'date\',this)',0,null,$rec[0]['invoice_ddue'],null,$dateDue,null,'YYYY-MM-DD'),
),
array( 
array('Date Paid','invoice_dpaid','class="clPaidDate elementIcon" validateElement(\'date\',this)',0,null,$rec[0]['invoice_dpaid'],null,$datePaid,null,'YYYY-MM-DD'),
array('Date Cleared','invoice_dcleared','class="clClearedDate elementIcon" validateElement(\'date\',this)',0,null,$rec[0]['invoice_dcleared'],null,$dateCleared,null,'YYYY-MM-DD'),
),
);
$requiredFields = "id_cust_company,id_cust_branch,id_sys_status,invoice";
?>
<form method="post" id="frmInvoice" name="frmInvoice" action="javascript:submitFrmVals('content','/_mod/smod_27/sql.php','<?=$requiredFields?>','&action=update&form=frmInvoice','frmInvoice')">
<fieldset id="customer">
<legend>Customer</legend>
<?=frmElements($fsCustomer);?>
</fieldset>
<fieldset id="summary">
<legend>Summary</legend>
<?=frmElements($fsInvoice);?>
</fieldset>
<?php
$lineItem = new LineItem;
$lineItem -> filter = "id_invoice";
$lineItem -> id = $rec[0]["id_invoice"];
$lineItem -> build();
$payment = new InvoicePayment;
$payment -> filter = "id_invoice";
$payment -> id = $rec[0]["id_invoice"];
$payment -> build();
?> 
</form>
<form action="#">
<fieldset id="Files">
<legend>Files</legend>
<div id="listFiles">
<?php
require("listFiles.php");
?>
</div></fieldset></form>
<script type="text/javascript">
<?=$icoCollapse;?>
<?=$icoPreview;?> 
oREC = '{"oRec":[{"module":"27","filter":"id_invoice","recid":"<?=$_POST["id_invoice"]?>","formname":"frmInvoice"}]}';
<?=$icoDelete;?>
$("#oOptions").append(icoDelete);
<?=$icoUpload;?>
$("#oOptions").append(icoUpload+icoPreview+icoCollapse);
collapseTabs();
</script>
