<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once("_lib/php/auth.php");
require_once("_includes/optionstoolbar.php");
/*
SELECT _invoice.id_invoice,
_invoice.invoice,
_invoice.invoice_dc,
_invoice.invoice_du,
_invoice.invoice_ddue,
_invoice.invoice_dpaid,
_invoice.invoice_dsubmitted,
_invoice.invoice_dcleared,
_invoice.id_sys_user,
_invoice.invoice_lastModifiedBy,
_invoice.id_cust_company,
_invoice.id_cust_branch,
_invoice.invoice_poc,
_invoice.invoice_department,
_invoice.invoice_percentDiscount,
_invoice.invoice_totalTax,
_invoice.invoice_totalAmount,
_invoice.invoice_amount,
_invoice.id_sys_status
FROM _invoice;
*/
# Branch 
$listBranch = array(); 
# Status
$gdbo -> sql = "SELECT
id_sys_status,
sys_status
FROM _sys_status
WHERE id_sys_status_tp = 8
ORDER BY sys_status_order";
$gdbo -> getRec();
$listStatusType = $gdbo -> dbData;
# Company 
$gdbo -> sql = "SELECT 
a.id_cust_company,
a.cust_company,
b.id_cust_company_tp,
b.cust_company_tp 
FROM _cust_company a 
LEFT JOIN _cust_company_tp b 
ON a.id_cust_company_tp = b.id_cust_company_tp 
ORDER BY b.cust_company_tp asc, a.cust_company asc";
$gdbo -> getRec();
$listCompany = $gdbo -> dbData;
# Contacts 
$gdbo -> sql = "SELECT
id_cust_contact,
contact,
IF(id_cust_company IS NULL,'0',id_cust_company) as id_cust_company,
IF(cust_company IS NULL,'General Contacts',cust_company) as cust_company
FROM
(
SELECT 
x.id_cust_contact,
concat(x.cust_contact_familyName,',',x.cust_contact_givenName) AS \"Contact\",
x.id_cust_company,
y.cust_company
FROM _cust_contact x
LEFT JOIN _cust_company y ON x.id_cust_company = y.id_cust_company
ORDER BY y.cust_company,x.cust_contact_familyName,x.cust_contact_givenName)
as derived ORDER BY cust_company";
$gdbo -> getRec();
$listContact = $gdbo -> dbData;
# Filterable Objects
## Branches
$branchesdbo = new ArcDb;
$branchesdbo -> dbConStr=$globalDBCON;
$branchesdbo -> dbType = $globalDBTP;
$branchesdbo -> dbSchema = $globalDB;
$branchesdbo -> sql = "SELECT id_cust_branch, cust_branch FROM _cust_branch";
$branchesdbo -> dbFilter = "WHERE id_cust_company=";
$branchesdbo -> type = "list";
$branchesdbo -> id = "id_cust_branch";
$branchesdbo -> attributes = 'class="chBranch"';
$branchesdboJSON =encrypt(json_encode((array)$branchesdbo));
# Fieldset Variables
$dateDue ='<div class="elementIconBox" onclick="openCalendar(this,\'interface\',null,\'clDueDate\')"><i class="fa fa-calendar"></i></div>';
$dateSubmitted ='<div class="elementIconBox" onclick="openCalendar(this,\'interface\',null,\'clSubmitDate\')"><i class="fa fa-calendar"></i></div>';
# Fieldsets
$fsCustomer=
array(
array(
array('*Organization','id_cust_company','onchange="filterRec(this,\''.$branchesdboJSON.'\',\'dBranch\')"',1,$listCompany),
array('*Branch','id_cust_branch','disabled="disabled" class=\'chBranch\'',1,$listBranch,null,"dBranch"),
),
array(
array('Point of Contact','invoice_poc',null,1,$listContact,null,"dPOC"),
array('Department','invoice_department',null,0,null,null),
)
);
$fsInvoice=
array(
array(
array('*Status','id_sys_status',null,1,$listStatusType,null),
array('Last Updated','invoice_du','disabled="disabled"',0,null,null),
),
array(
array('*Invoice Number','invoice',null,0),
array('Amount','invoice_amount','disabled="disabled"',0,null,null),
),
array(
array('Tax','invoice_totalTax','disabled="disabled"',0,null,null),
array('Total Amount','invoice_totalAmount','disabled="disabled"',0,null,null),
), 
array(
array('Date Submitted','invoice_dsubmitted','class="clSubmitDate elementIcon" validateElement(\'date\',this)',0,null,null,null,$dateSubmitted,null,'YYYY-MM-DD'),
array('Date Due','invoice_ddue','class="clDueDate elementIcon" validateElement(\'date\',this)',0,null,null,null,$dateDue,null,'YYYY-MM-DD'),
),
);
$requiredFields = "id_cust_company,id_cust_branch,id_sys_status,invoice";
?>
<script type="text/javascript">$(".icoadd").remove();</script>
<form method="post" id="frmInvoice" name="frmInvoice" action="javascript:submitFrmVals('content','/_mod/smod_27/sql.php','<?=$requiredFields?>','&action=insert&form=frmInvoice','frmInvoice')">
<fieldset id="customer">
<legend>Customer</legend>
<?=frmElements($fsCustomer);?>
</fieldset>
<fieldset id="summary">
<legend>Summary</legend>
<?=frmElements($fsInvoice);?>
</fieldset>
<?php
$lineItem = new LineItem;
$lineItem -> build();
$payment = new InvoicePayment;
$payment -> build();
?> 
</form>
<script type="text/javascript">
<?=$icoCollapse;?>
$("#oOptions").append(icoCollapse);
collapseTabs();
</script>
