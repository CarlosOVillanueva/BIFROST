<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
# Includes
require_once("_lib/php/auth.php");
if (isset($_POST["contact_method_mText"])){
$contactMethods = hex2str($_POST["contact_method_mText"]);
$contactMethods = json_decode($contactMethods);
$contactMethodsInsertStr = array2sqlstring($contactMethods,"0,2");
};
if (isset($_POST["degree_mText"])){
$degree = hex2str($_POST["degree_mText"]);
$degree = json_decode($degree);
$degreeInsertStr = array2sqlstring($degree,"0,1,2,4,5,6");
};
if (isset($_POST["certification_mText"])){
$certification = hex2str($_POST["certification_mText"]);
$certification = json_decode($certification);
$certificationInsertStr = array2sqlstring($certification,"3,4,5");
};
if (isset($_POST["insurance_mText"])){
$insurance = hex2str($_POST["insurance_mText"]);
$insurance = json_decode($insurance);
$insuranceInsertStr = array2sqlstring($insurance,"0,2,3,4");
};
# Multi Form Columns
$contactMethodsCols="contact_method,contact_method_notes,id_contact_method_tp,id_cust_contact";
$degreeCols="hr_degree_hours,hr_degree,hr_degree_ds,hr_degree_da,id_hr_degree_status,id_hr_degree_level,id_cust_company,id_cust_branch,id_hr_emp";
$certificationCols="hr_certification_hours,hr_certification_ds,hr_certification_da,id_hr_certification_status,id_hr_certification_provider,id_hr_certification_tp,id_hr_emp";
$insuranceCols="hr_insurance_ds,hr_insurance_de,id_cust_company,id_hr_insurance_plan,id_hr_insurance_coverage,id_hr_emp";
# Parse Form Action
$action = $_POST['action'];
$form = $_POST['form'];
$formAction = $form . "," . $action;
# Begin SQL Functions
switch ($formAction) {
case "frmEmp,insert":
/*****************************************************************************/
# Insert Address Record
$gdbo -> dbTable = "_addr";
$gdbo -> insertRec();
$_POST["id_addr"]= $gdbo -> insertedID;
# Insert Contact Record
$gdbo -> dbTable = "_cust_contact";
$gdbo -> insertRec();
$id_cust_contact = $gdbo -> insertedID;
$_POST['id_cust_contact']=$id_cust_contact;
# Insert HR
$gdbo -> dbTable = "_hr_emp";
$gdbo -> insertRec();
$id_hr_emp = $gdbo -> insertedID;
$_POST['id_hr_emp']=$id_hr_emp;
##
# Insert Insurance
$gdbo -> dbTable = "_hr_insurance";
if (!empty($insuranceInsertStr)){
$gdbo -> dbTable = "_hr_insurance";
$insuranceInsertStr = str_replace("~id~", $_POST["id_hr_emp"], $insuranceInsertStr);
$gdbo -> sql = "(".$insuranceCols.") values ".$insuranceInsertStr; 
$gdbo -> insertRec();
}
# Insert Education
$gdbo -> dbTable = "_hr_degree";
if (!empty($degreeInsertStr)){
$gdbo -> dbTable = "_hr_degree";
$degreeInsertStr = str_replace("~id~", $_POST["id_hr_emp"], $degreeInsertStr);
$gdbo -> sql = "(".$degreeCols.") values ".$degreeInsertStr; 
$gdbo -> insertRec();
}
# Insert Certification
$gdbo -> dbTable = "_hr_certification";
if (!empty($certificationInsertStr)){
$gdbo -> dbTable = "_hr_certification";
$certificationInsertStr = str_replace("~id~", $_POST["id_hr_emp"], $certificationInsertStr);
$gdbo -> sql = "(".$certificationCols.") values ".$certificationInsertStr; 
$gdbo -> insertRec();
}
# Insert Contact Methods
if (!empty($contactMethodsInsertStr)){
$gdbo -> dbTable = "_contact_method";
$contactMethodsInsertStr = str_replace("~id~", $_POST["id_cust_contact"], $contactMethodsInsertStr);
$gdbo -> sql = "(".$contactMethodsCols.") values ".$contactMethodsInsertStr;
$gdbo -> insertRec();
};
include('edit.php');
/*****************************************************************************/
break;
case "frmEmp,update":
/*****************************************************************************/
# Update Address Record
if (!empty($_POST["id_addr"])){
$gdbo -> dbTable = "_addr";
$gdbo -> updateRec("id_addr=".$_POST["id_addr"]);
} else { 
$gdbo -> dbTable = "_addr";
$gdbo -> insertRec();
$_POST["id_addr"]= $gdbo -> insertedID;
}
# Update HR Record
$gdbo -> dbTable = "_hr_emp";
$gdbo -> updateRec("id_hr_emp=".$_POST["id_hr_emp"]);
# Update Contact Record
$gdbo -> dbTable = "_cust_contact";
$gdbo -> updateRec("id_cust_contact=".$_POST["id_cust_contact"]);
##
# Update Insurance
$gdbo -> dbTable = "_hr_insurance";
$gdbo -> deleteRec("id_hr_emp=".$_POST["id_hr_emp"]);
if (!empty($insuranceInsertStr)){
$gdbo -> dbTable = "_hr_insurance";
$insuranceInsertStr = str_replace("~id~", $_POST["id_cust_contact"], $insuranceInsertStr);
$gdbo -> sql = "(".$insuranceCols.") values ".$insuranceInsertStr; 
$gdbo -> insertRec();
}
# Update Contact Methods
$gdbo -> dbTable = "_contact_method";
$gdbo -> deleteRec("id_cust_contact=".$_POST["id_cust_contact"]);
if (!empty($contactMethodsInsertStr)){
$gdbo -> dbTable = "_contact_method";
$contactMethodsInsertStr = str_replace("~id~", $_POST["id_cust_contact"], $contactMethodsInsertStr);
$gdbo -> sql = "(".$contactMethodsCols.") values ".$contactMethodsInsertStr; 
$gdbo -> insertRec();
}
# Update Education
$gdbo -> dbTable = "_hr_degree";
$gdbo -> deleteRec("id_hr_emp=".$_POST["id_hr_emp"]);
if (!empty($degreeInsertStr)){
$gdbo -> dbTable = "_hr_degree";
$degreeInsertStr = str_replace("~id~", $_POST["id_hr_emp"], $degreeInsertStr);
$gdbo -> sql = "(".$degreeCols.") values ".$degreeInsertStr; 
$gdbo -> insertRec();
}
# Update Certification
$gdbo -> dbTable = "_hr_certification";
$gdbo -> deleteRec("id_hr_emp=".$_POST["id_hr_emp"]);
if (!empty($certificationInsertStr)){
$gdbo -> dbTable = "_hr_certification";
$certificationInsertStr = str_replace("~id~", $_POST["id_hr_emp"], $certificationInsertStr);
$gdbo -> sql = "(".$certificationCols.") values ".$certificationInsertStr; 
$gdbo -> insertRec();
}
include('edit.php');
/*****************************************************************************/
break;
case "frmEmp,delete":
/*****************************************************************************/
$gdbo -> sql ="SELECT id_cust_contact FROM _hr_emp WHERE id_hr_emp=".$_POST["id_hr_emp"];
$gdbo -> getRec();
$contact_record = $gdbo -> dbData;
$id_cust_contact = $contact_record[0][0];
$gdbo -> sql ="SELECT TABLE_NAME FROM information_schema.columns WHERE COLUMN_NAME='id_cust_contact'";
$gdbo -> getRec();
$contact_tables = $gdbo -> dbData;
foreach($contact_tables as $row => $column) {
foreach($column as $data => $value) {
$gdbo -> dbTable = $value;
$gdbo -> deleteRec("id_cust_contact=".$id_cust_contact);
}
}
$gdbo -> sql ="SELECT TABLE_NAME FROM information_schema.columns WHERE COLUMN_NAME='id_hr_emp'";
$gdbo -> getRec();
$empTables = $gdbo -> dbData;
foreach($empTables as $row => $column) {
foreach($column as $data => $value) {
$gdbo -> dbTable = $value;
$gdbo -> deleteRec("id_hr_emp=".$_POST["id_hr_emp"]);
}
}
include('index.php');
/*****************************************************************************/ 
break; 
default:
break;
}
include("_error/status.php");
?>
