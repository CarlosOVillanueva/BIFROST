<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once("_lib/php/auth.php");
require_once("_includes/heading.php");
require_once("_includes/optionstoolbar.php");
# Recordset
$gdbo -> sql = "SELECT
a.id_hr_emp,
a.id_cust_contact,
a.id_hr_position,
a.id_hr_gender,
a.id_hr_race,
a.id_hr_ethnicity,
a.hr_emp_pto,
a.hr_emp_sick,
a.hr_emp_hRate,
a.hr_emp_salary,
a.hr_emp_accrRate,
a.hr_emp_ssn,
a.hr_emp_disabled,
a.hr_emp_date_hire,
a.hr_emp_dc,
a.hr_emp_dr,
a.id_hr_insurance_plan,
a.hr_emp_insMemberNo,
a.hr_emp_insGroupNo,
a.hr_emp_insPerscriptionNo,
a.hr_emp_insDateEffective,
a.hr_emp_timeApprover,
a.hr_emp_expenseApprover,
a.id_hr_emp_status,
c.id_cust_contact_tp,
c.id_cust_branch,
c.id_name_title,
c.cust_contact_familyName,
c.cust_contact_middleName,
c.cust_contact_givenName,
c.cust_contact_maidenName,
c.cust_contact_commonName,
c.id_name_suffix,
c.cust_contact_role,
c.cust_contact_dc,
c.cust_contact_dr,
c.cust_contact_notes,
c.cust_contact_bdate,
j.cust_contact_tp,
k.cust_branch,
d.id_cust_department,
l.cust_department,
c.id_addr,
UNHEX(a.hr_emp_notes) as hr_emp_notes,
a.hr_emp_citizenBit,
a.id_hr_maritalStatus,
a.hr_emp_timeApproverOrg,
a.hr_emp_payFrequency
FROM
_hr_emp a
LEFT JOIN _cust_contact c ON a.id_cust_contact=c.id_cust_contact
LEFT JOIN _hr_position d ON a.id_hr_position=d.id_hr_position
LEFT JOIN _hr_gender e ON a.id_hr_gender=e.id_hr_gender
LEFT JOIN _hr_race f ON a.id_hr_race=f.id_hr_race
LEFT JOIN _hr_ethnicity g ON a.id_hr_ethnicity=g.id_hr_ethnicity
LEFT JOIN _hr_insurance_plan h ON a.id_hr_insurance_plan=h.id_hr_insurance_plan
LEFT JOIN _sys_status i ON a.id_hr_emp_status=i.id_sys_status
LEFT JOIN _cust_contact_tp j ON c.id_cust_contact_tp=j.id_cust_contact_tp
LEFT JOIN _cust_branch k ON c.id_cust_branch=k.id_cust_branch
LEFT JOIN _cust_department l ON d.id_cust_department=l.id_cust_department
WHERE
a.id_hr_emp=".$_POST["id_hr_emp"];
$gdbo -> getRec();
$rec = $gdbo -> getAssociative();
$gdbo -> sql = "SELECT CONCAT(fso) as fso FROM _fso WHERE fso_pk=".$rec[0]["id_cust_contact"]." AND fso_originalname='avatar' AND fso_pkcol='id_cust_contact'";
$gdbo -> getRec();
$fsoRec = $gdbo -> dbData;
if (isset($fsoRec[0][0]) && $fsoRec[0][0]) {
$base64=shell_exec("base64 ".$avatarWritePath.$fsoRec[0][0]);
$avatar="data:image/png;base64,".$base64;
} else {
$avatar="/_img/avatar.png";
}
# Marital Status
require("_model/dboMaritalStatus.php");
$gdbo -> getRec();
$listMaritalStatus = $gdbo -> dbData;
# Branch
require("_model/dboParentBranch.php");
$gdbo -> getRec();
$listParentBranch = $gdbo -> dbData;
# Employment Status
require("_model/dboEmploymentStatus.php");
$gdbo -> getRec();
$listEmploymentStatus = $gdbo -> dbData;
# Education Status
require("_model/dboEducationStatus.php");
$gdbo -> getRec();
$listEducationStatus = $gdbo -> dbData;
# Race
require("_model/dboRace.php");
$gdbo -> getRec();
$listRace = $gdbo -> dbData;
# Ethnicity
require("_model/dboEthnicity.php");
$gdbo -> getRec();
$listEthnicity = $gdbo -> dbData;
# Title 
require("_model/dboTitle.php");
$gdbo -> getRec();
$listTitle= $gdbo -> dbData;
# Suffix
require("_model/dboSuffix.php");
$gdbo -> getRec();
$listSuffix= $gdbo -> dbData;
# Contact Type
$gdbo -> sql = "SELECT id_cust_contact_tp,cust_contact_tp FROM _cust_contact_tp WHERE id_cust_contact_tp<> 1";
$gdbo -> getRec();
$listContactType = $gdbo -> dbData;
# Time Approver Company
require("_model/dboTimeApproverCompany.php");
$gdbo -> getRec();
$listTimeApproverCompany = $gdbo -> dbData;
# Pay Frequency
$listPayFrequency=array();
$listPayFrequency[]=array("biweekly","Bi-Weekly");
$listPayFrequency[]=array("bimonthly","Bi-Monthly");
$listPayFrequency[]=array("monthly","Monthly");
$listPayFrequency[]=array("weekly","Weekly");
# Dependent Arrays
## Department
if (!empty($rec[0]["id_cust_branch"])) {
$gdbo -> sql = "SELECT id_cust_department, cust_department FROM _cust_department WHERE id_cust_branch=".$rec[0]["id_cust_branch"];
$gdbo -> getRec();
$listDepartment= $gdbo -> dbData;} else {$listDepartment=array();}
## Position
if (!empty($rec[0]["id_cust_department"])) {
$gdbo -> sql = "SELECT id_hr_position, hr_position FROM _hr_position WHERE id_cust_department=".$rec[0]["id_cust_department"];
$gdbo -> getRec();
$listPosition= $gdbo -> dbData;} else {$listPosition=array();}
## Approver
if (!empty($rec[0]["hr_emp_timeApproverOrg"])) {
$gdbo -> sql = "SELECT
a.id_cust_contact,concat(a.cust_contact_familyName,',',a.cust_contact_givenName)
FROM
_cust_contact a
WHERE
EXISTS (SELECT e.id_cust_contact FROM _sys_user_emp e WHERE e.id_cust_contact=a.id_cust_contact) AND a.id_cust_company=".$rec[0]["hr_emp_timeApproverOrg"];
$gdbo -> getRec();
$listApprover = $gdbo -> dbData;} else {$listApprover=array();}
# Filterable Objects
## Positions
$positiondbo = new ArcDb;
$positiondbo -> dbConStr=$globalDBCON;
$positiondbo -> dbType = $globalDBTP;
$positiondbo -> dbSchema = $globalDB;
$positiondbo -> sql = "SELECT id_hr_position, hr_position FROM _hr_position";
$positiondbo -> dbFilter = "WHERE id_cust_department=";
$positiondbo -> type = "list";
$positiondbo -> id = "id_hr_position";
$positiondbo -> attributes = 'class="chposition"';
$positiondboJSON =encrypt(json_encode((array)$positiondbo));
## Departments
$departmentdbo = new ArcDb;
$departmentdbo -> dbConStr=$globalDBCON;
$departmentdbo -> dbType = $globalDBTP;
$departmentdbo -> dbSchema = $globalDB;
$departmentdbo -> sql = "SELECT id_cust_department, cust_department FROM _cust_department";
$departmentdbo -> dbFilter = "WHERE id_cust_branch=";
$departmentdbo -> type = "list";
$departmentdbo -> id = "id_cust_department";
$departmentdbo -> attributes = 'onchange="filterRec(this,\''.$positiondboJSON.'\',\'dPosition\')" class="chDepartment"';
$departmentdboJSON =encrypt(json_encode((array)$departmentdbo));
## Approvers
$approverdbo = new ArcDb;
$approverdbo -> dbConStr=$globalDBCON;
$approverdbo -> dbType = $globalDBTP;
$approverdbo -> dbSchema = $globalDB;
$approverdbo -> sql = "SELECT
a.id_cust_contact as hr_emp_timeApprover,concat(a.cust_contact_familyName,',',a.cust_contact_givenName)
FROM
_cust_contact a
WHERE
EXISTS (SELECT e.id_cust_contact FROM _sys_user_emp e WHERE e.id_cust_contact=a.id_cust_contact)";
$approverdbo -> dbFilter = "AND id_cust_company=";
$approverdbo -> type = "list";
$approverdbo -> id = "hr_emp_timeApprover";
$approverdbo -> attributes = 'class="chApprover"';
$approverdboJSON =encrypt(json_encode((array)$approverdbo)); 
# Fieldset Variables
$dateHire ='<div class="elementIconBox" onclick="openCalendar(this,\'interface\',null,\'clDateHire\')"><i class="fa fa-calendar"></i></div>';
$dateBirth='<div class="elementIconBox" onclick="openCalendar(this,\'interface\',null,\'clDateBirth\')"><i class="fa fa-calendar"></i></div>';
$buttonReveal='<div class="elementIconBox" onclick="showPasswordText(\'hr_emp_ssn\',this)"><i class="fa fa-eye"></i></div>';
# Fieldsets
$fsContact=
array(
array(
array('*First','cust_contact_givenName',null,0,null,$rec[0]["cust_contact_givenName"]),
array('*Last','cust_contact_familyName',null,0,null,$rec[0]["cust_contact_familyName"]),
),
array(
array('Middle','cust_contact_middleName',null,0,null,$rec[0]["cust_contact_middleName"]),
array('Nickname','cust_contact_commonName',null,0,null,$rec[0]["cust_contact_commonName"]),
array(null, 'id_cust_contact',null,3,null,$rec[0]['id_cust_contact']),
array(null, 'id_hr_emp',null,3,null,$rec[0]['id_hr_emp']),
),
array(
array('Title','id_name_title',null,1,$listTitle,$rec[0]["id_name_title"]),
array('Suffix','id_name_suffix',null,1,$listSuffix,$rec[0]["id_name_suffix"]),
),
);
$fsBusiness=
array(
array(
array('*Employment Status','id_hr_emp_status',null,1,$listEmploymentStatus,$rec[0]["id_hr_emp_status"]),
array('*Date of Hire','hr_emp_date_hire','class="clDateHire elementIcon" validateElement(\'date\',this)',0,null,$rec[0]["hr_emp_date_hire"],null,$dateHire,null,'YYYY-MM-DD'),
),
array(
array('Hourly Rate','hr_emp_hRate','onblur="validateElement(\'money\',this)"',0,null,$rec[0]["hr_emp_hRate"]),
array('Salary','hr_emp_salary','onblur="validateElement(\'money\',this)"',0,null,$rec[0]["hr_emp_salary"]),
),
array(
array('Branch','id_cust_branch','onchange="disableChild(this,\'id_hr_position,id_cust_department\');filterRec(this,\''.$departmentdboJSON.'\',\'dDepartment\')"',1,$listParentBranch,$rec[0]["id_cust_branch"]),
array('Department','id_cust_department','onchange="filterRec(this,\''.$positiondboJSON.'\',\'dPosition\')" class=\'chDepartment\'',1,$listDepartment,$rec[0]["id_cust_department"],"dDepartment"),
array('Job Title','id_hr_position','class=\'chPosition\'',1,$listPosition,$rec[0]["id_hr_position"],"dPosition"),
)
);
$listGender = array(array(1,"Male"),array(2,"Female"));
$fsPrivate=
array(
array(
array('Birthdate','cust_contact_bdate','class="clDateBirth elementIcon" onblur="validateElement(\'date\',this)"',0,null,$rec[0]["cust_contact_bdate"],null,$dateBirth,null,'YYYY-MM-DD'),
),
array(
array('Social Security Number','hr_emp_ssn','class="elementIcon" onblur="validateElement(\'ssn\',this)"',7,null,$rec[0]["hr_emp_ssn"],null,$buttonReveal),
),
array(
array('Gender','id_hr_gender',null,4,$listGender,$rec[0]["id_hr_gender"]),
),
array(
array('Marital Status','id_hr_maritalStatus',null,1,$listMaritalStatus,$rec[0]["id_hr_maritalStatus"]),
),
array(
array('Citizenship','hr_emp_citizenBit',null,8,null,$rec[0]["hr_emp_citizenBit"],null,"US Citizen?"),
array('Disability','hr_emp_disabled',null,8,null,$rec[0]["hr_emp_disabled"],null,"Qualifies for Disability"),
),
array( 
array('Ethnicity','id_hr_ethnicity',null,1,$listEthnicity,$rec[0]["id_hr_ethnicity"]),
array('Race','id_hr_race',null,1,$listRace,$rec[0]["id_hr_race"]),
),
array(
array('Notes','hr_emp_notes','onkeydown="detectTab(this,event)"',6,null,$rec[0]["hr_emp_notes"]),
),
);
$fsTimeKeeping=
array(
array(
array('Time Approver Organization','hr_emp_timeApproverOrg','onchange="filterRec(this,\''.$approverdboJSON.'\',\'dApprover\')"',1,$listTimeApproverCompany,$rec[0]["hr_emp_timeApproverOrg"]),
),
array(
array('Time Approver','hr_emp_timeApprover','class=\'chApprover\'',1,$listApprover,$rec[0]["hr_emp_timeApprover"],"dApprover"),
),
array(
array('Pay Frequency','hr_emp_payFrequency',null,1,$listPayFrequency,$rec[0]["hr_emp_payFrequency"]),
),
);
$requiredFields = "cust_contact_givenName,cust_contact_familyName,id_hr_emp_status,hr_emp_date_hire";
# Plugins
## Email Form
$emailForm = new ArcEmailForm;
$emailForm -> receiverid = $rec[0]["id_cust_contact"];
$emailForm -> senderid = $id_cust_contact_derived;
$emailForm -> sendername = $fullName_derived;
$emailForm -> className = "popBox";
$emailFormData = bin2hex($emailForm -> buildForm());
?>
</script>
<form method="post" name="frmEmp" id="frmEmp" action="javascript:submitFrmVals('content','/_mod/smod_05/sql.php','<?=$requiredFields?>','&form=frmEmp&action=update','frmEmp')">
<fieldset id="detail">
<legend>Detail</legend>
<div>
<div style="float:left">
<?=frmElements($fsContact);?>
</div>
<div style="float:right;text-align:center;"><?php include("add_avatar.php");?></div>
</div>
</fieldset>
<?php 
$contacts = new ContactMethods;
$contacts -> filter = "id_cust_contact";
$contacts -> id = $rec[0]["id_cust_contact"];
$contacts -> build();
?>
<fieldset id="business">
<legend>Business</legend>
<?=frmElements($fsBusiness);?>
</fieldset>
<fieldset id="private">
<legend>Private</legend>
<?=frmElements($fsPrivate);?>
</fieldset>
<fieldset id="timekeeping">
<legend>Time Keeping</legend>
<?=frmElements($fsTimeKeeping);?>
</fieldset>
<?php
$address = new ArcAddress;
$address -> filter = "id_addr";
$address -> id = $rec[0]["id_addr"];
$address -> build();
?>
<?php 
$education = new Education;
$education -> filter = "id_hr_emp";
$education -> id = $rec[0]["id_hr_emp"];
$education -> build();
?>
<?php 
$certification = new Certification;
$certification -> filter = "id_hr_emp";
$certification -> id = $rec[0]["id_hr_emp"];
$certification -> build();
?>
<?php 
$insurance = new Insurance;
$insurance -> filter = "id_hr_emp";
$insurance -> id = $rec[0]["id_hr_emp"];
$insurance -> build();
?> 
</form>
<script type="text/javascript">
oREC = '{"oRec":[{"module":"05","filter":"id_hr_emp","recid":"<?=$_POST["id_hr_emp"]?>","formname":"frmEmp"}]}';
<?=$icoCollapse;?>
<?=$icoPreview;?> 
<?=$icoDelete;?>
<?=$icoEmail;?>
window.oEmailData = <?="'".$emailFormData."'"?>;
$("#oOptions").append(icoDelete+icoEmail+icoPreview+icoCollapse);
collapseTabs();
CKEDITOR.replace("hr_emp_notes");
</script>
