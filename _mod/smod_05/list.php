<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once("_lib/php/auth.php");
$arctbl = new ArcTbl;
$arctbl -> dbConStr=$globalDBCON;
$arctbl -> dbOffset = 0;
$arctbl -> dbLimit = 23;
$arctbl -> dbType = $globalDBTP;
$arctbl -> dbSchema = $globalDB;
$arctbl -> recLink= $path."edit.php";
$arctbl -> actionFilterKey="id_hr_emp";
$arctbl -> recIndex="id_hr_emp";
$arctbl -> ignoreCols=array("id_hr_emp","id_cust_contact","Middle","Nickname");
$arctbl -> ignoreFilterCols=array("id_hr_emp","","id_cust_contact");
$arctbl -> recQuery = "
SELECT
a.id_hr_emp,
a.id_cust_contact,
(SELECT concat('<img class=''avatarSmall'' src=''/_avatar/thumb_',fso,''' height=''32'' width=''32''/>') as fso FROM _fso where fso_pkcol='id_cust_contact' AND fso_pk=a.id_cust_contact AND fso_originalname='avatar') as \"Avatar\",
b.cust_contact_familyName as \"Last\",
b.cust_contact_middleName as \"Middle\",
b.cust_contact_givenName as \"First\",
b.cust_contact_commonName as \"Nickname\",
h.cust_branch as \"Branch\",
i.cust_department as \"Department\",
f.hr_position as \"Position\",
concat('"
."<a class=\"button\" onclick=\"arc(''popWindow'',''/_lib/php/ArcEmail.View.php"."'',''data=',a.id_cust_contact,''')\"><i class=\"fa fa-envelope fa-fw\"></i>Mail</a>') as \"\"
FROM
_hr_emp as a
LEFT JOIN _cust_contact b ON a.id_cust_contact=b.id_cust_contact
LEFT JOIN _hr_ethnicity c ON a.id_hr_ethnicity=c.id_hr_ethnicity
LEFT JOIN _hr_gender d ON a.id_hr_gender=d.id_hr_gender
LEFT JOIN _hr_position f ON a.id_hr_position=f.id_hr_position
LEFT JOIN _hr_pay_tp e ON f.id_hr_pay_tp=e.id_hr_pay_tp
LEFT JOIN _hr_race g ON a.id_hr_race=g.id_hr_race
LEFT JOIN _cust_branch h ON b.id_cust_branch=h.id_cust_branch
LEFT JOIN _cust_department i ON f.id_cust_department = i.id_cust_department";
$arctbl -> actionDestination = "content";
$arctbl -> ajDestination = "list05";
$arctbl -> ajPage = $path."list.php";
$arctbl -> build();
echo hex2str($arctbl -> tblNav);
echo $arctbl->dataTable;
?>
