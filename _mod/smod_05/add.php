<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once("_lib/php/auth.php");
require_once("_includes/optionstoolbar.php");
# Marital Status
require("_model/dboMaritalStatus.php");
$gdbo -> getRec();
$listMaritalStatus = $gdbo -> dbData;
# Branch
require("_model/dboParentBranch.php");
$gdbo -> getRec();
$listParentBranch = $gdbo -> dbData;
# Employment Status
require("_model/dboEmploymentStatus.php");
$gdbo -> getRec();
$listEmploymentStatus = $gdbo -> dbData;
# Education Status
require("_model/dboEducationStatus.php");
$gdbo -> getRec();
$listEducationStatus = $gdbo -> dbData;
# Race
require("_model/dboRace.php");
$gdbo -> getRec();
$listRace = $gdbo -> dbData;
# Ethnicity
require("_model/dboEthnicity.php");
$gdbo -> getRec();
$listEthnicity = $gdbo -> dbData;
# Title 
require("_model/dboTitle.php");
$gdbo -> getRec();
$listTitle= $gdbo -> dbData;
# Suffix
require("_model/dboSuffix.php");
$gdbo -> getRec();
$listSuffix= $gdbo -> dbData;
# Contact Type
$gdbo -> sql = "SELECT id_cust_contact_tp,cust_contact_tp FROM _cust_contact_tp WHERE id_cust_contact_tp<> 1";
$gdbo -> getRec();
$listContactType = $gdbo -> dbData;
# Time Approver Company
require("_model/dboTimeApproverCompany.php");
$gdbo -> getRec();
$listTimeApproverCompany = $gdbo -> dbData;
# Pay Frequency
$listPayFrequency=array();
$listPayFrequency[]=array("biweekly","Bi-Weekly");
$listPayFrequency[]=array("bimonthly","Bi-Monthly");
$listPayFrequency[]=array("monthly","Monthly");
$listPayFrequency[]=array("weekly","Weekly");
# Filterable Objects
## Positions
$positiondbo = new ArcDb;
$positiondbo -> dbConStr=$globalDBCON;
$positiondbo -> dbType = $globalDBTP;
$positiondbo -> dbSchema = $globalDB;
$positiondbo -> sql = "SELECT id_hr_position, hr_position FROM _hr_position";
$positiondbo -> dbFilter = "WHERE id_cust_department=";
$positiondbo -> type = "list";
$positiondbo -> id = "id_hr_position";
$positiondbo -> attributes = 'class="chposition"';
$positiondboJSON =encrypt(json_encode((array)$positiondbo));
## Departments
$departmentdbo = new ArcDb;
$departmentdbo -> dbConStr=$globalDBCON;
$departmentdbo -> dbType = $globalDBTP;
$departmentdbo -> dbSchema = $globalDB;
$departmentdbo -> sql = "SELECT id_cust_department, cust_department FROM _cust_department";
$departmentdbo -> dbFilter = "WHERE id_cust_branch=";
$departmentdbo -> type = "list";
$departmentdbo -> id = "id_cust_department";
$departmentdbo -> attributes = 'onchange="filterRec(this,\''.$positiondboJSON.'\',\'dPosition\')" class="chDepartment"';
$departmentdboJSON =encrypt(json_encode((array)$departmentdbo));
## Approvers
$approverdbo = new ArcDb;
$approverdbo -> dbConStr=$globalDBCON;
$approverdbo -> dbType = $globalDBTP;
$approverdbo -> dbSchema = $globalDB;
$approverdbo -> sql = "SELECT
a.id_cust_contact as hr_emp_timeApprover,concat(a.cust_contact_familyName,',',a.cust_contact_givenName)
FROM
_cust_contact a
WHERE
EXISTS (SELECT e.id_cust_contact FROM _sys_user_emp e WHERE e.id_cust_contact=a.id_cust_contact)";
$approverdbo -> dbFilter = "AND id_cust_company=";
$approverdbo -> type = "list";
$approverdbo -> id = "hr_emp_timeApprover";
$approverdbo -> attributes = 'class="chApprover"';
$approverdboJSON =encrypt(json_encode((array)$approverdbo)); 
# Fieldset Variables
$dateHire ='<div class="elementIconBox" onclick="openCalendar(this,\'interface\',null,\'clDateHire\')"><i class="fa fa-calendar"></i></div>';
$dateBirth='<div class="elementIconBox" onclick="openCalendar(this,\'interface\',null,\'clDateBirth\')"><i class="fa fa-calendar"></i></div>';
$buttonReveal='<div class="elementIconBox" onclick="showPasswordText(\'hr_emp_ssn\',this)"><i class="fa fa-eye"></i></div>';
# Fieldsets
$fsContact=
array(
array(
array('*First','cust_contact_givenName',null,0),
array('*Last','cust_contact_familyName',null,0),
),
array(
array('Middle','cust_contact_middleName',null,0),
array('Nickname','cust_contact_commonName',null,0),
array(null, 'id_cust_contact',null,3),
array(null, 'id_hr_emp',null,3),
array(null, 'id_cust_company',null,3,null,1),
),
array(
array('Title','id_name_title',null,1,$listTitle),
array('Suffix','id_name_suffix',null,1,$listSuffix),
),
);
$fsBusiness=
array(
array(
array('*Employment Status','id_hr_emp_status',null,1,$listEmploymentStatus),
array('*Date of Hire','hr_emp_date_hire','class="clDateHire elementIcon" validateElement(\'date\',this)',0,null,null,null,$dateHire,null,'YYYY-MM-DD'),
),
array(
array('Hourly Rate','hr_emp_hRate','onblur="validateElement(\'money\',this)"',0),
array('Salary','hr_emp_salary','onblur="validateElement(\'money\',this)"',0),
),
array(
array('Branch','id_cust_branch','onchange="disableChild(this,\'id_hr_position,id_cust_department\');filterRec(this,\''.$departmentdboJSON.'\',\'dDepartment\')"',1,$listParentBranch),
array('Department','id_cust_department','disabled="disabled" class="chDepartment"',1,null,null,"dDepartment"),
array('Job Title','id_hr_position','disabled="disabled" class="chPosition"',1,null,null,"dPosition"),
)
);
$listGender = array(array(1,"Male"),array(2,"Female"));
$fsPrivate=
array(
array(
array('Birthdate','cust_contact_bdate','class="clDateBirth elementIcon" validateElement(\'date\',this)',0,null,null,null,$dateBirth,null,'YYYY-MM-DD'),
),
array(
array('Social Security Number','hr_emp_ssn','class="elementIcon" onblur="validateElement(\'ssn\',this)"',7,null,null,null,$buttonReveal),
),
array(
array('Gender','id_hr_gender',null,4,$listGender),
),
array(
array('Marital Status','id_hr_maritalStatus',null,1,$listMaritalStatus),
),
array(
array('Citizenship','hr_emp_citizenBit',null,8,null,null,null,"US Citizen?"),
array('Disability','hr_emp_disabled',null,8,null,null,null,"Qualifies for Disability"),
),
array( 
array('Ethnicity','id_hr_ethnicity',null,1,$listEthnicity),
array('Race','id_hr_race',null,1,$listRace),
),
array(
array('Notes','hr_emp_notes','onkeydown="detectTab(this,event)"',6),
),
);
$fsTimeKeeping=
array(
array(
array('Time Approver Organization','hr_emp_timeApproverOrg','onchange="filterRec(this,\''.$approverdboJSON.'\',\'dApprover\')"',1,$listTimeApproverCompany,null),
),
array(
array('Time Approver','hr_emp_timeApprover','disabled=\'disabled\' class=\'chApprover\'',1,null,null,"dApprover"),
),
array(
array('Pay Frequency','hr_emp_payFrequency',null,1,$listPayFrequency),
),
);
$requiredFields = "cust_contact_givenName,cust_contact_familyName,id_hr_emp_status,hr_emp_date_hire";
?>
<script type="text/javascript">$(".icoadd").remove();</script>
<form method="post" name="frmEmp" id="frmEmp" action="javascript:submitFrmVals('content','/_mod/smod_05/sql.php','<?=$requiredFields?>','&form=frmEmp&action=insert','frmEmp')">
<fieldset id="detail">
<legend>Detail</legend>
<?=frmElements($fsContact);?>
</fieldset>
<?php 
$contacts = new ContactMethods;
$contacts -> build();
?>
<fieldset id="business">
<legend>Business</legend>
<?=frmElements($fsBusiness);?>
</fieldset>
<fieldset id="private">
<legend>Private</legend>
<?=frmElements($fsPrivate);?>
</fieldset>
<fieldset id="timekeeping">
<legend>Time Keeping</legend>
<?=frmElements($fsTimeKeeping);?>
</fieldset>
<?php
$address = new ArcAddress;
$address -> build();
$education = new Education;
$education -> build();
$certification = new Certification;
$certification -> build();
$insurance = new Insurance;
$insurance -> build();
?> 
</form>
<script type="text/javascript">
<?=$icoCollapse;?>
$("#oOptions").append(icoCollapse);
collapseTabs();
CKEDITOR.replace("hr_emp_notes");
</script>
