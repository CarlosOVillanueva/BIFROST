<!--
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
-->
<div id="avatar"><img class="avatarMedium" src="<?=$avatar?>" height="200" width="200"/></div>
<script type="text/javascript">oREC2='{"oRec":[{"module":"05","filter":"id_cust_contact","recid":"<?=$rec[0]["id_cust_contact"]?>","formname":"frmEmp"}]}';</script>
<input type="button" class="avatarbutton" value="Change Photo" onclick="uploadFile(oREC2)">
