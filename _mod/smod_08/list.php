<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once("_lib/php/auth.php");
$arctbl = new ArcTbl;
$arctbl -> dbConStr=$globalDBCON;
$arctbl -> dbOffset = 0;
$arctbl -> dbLimit = 23;
$arctbl -> dbType = $globalDBTP;
$arctbl -> dbSchema = $globalDB;
$arctbl -> recLink= $path."edit.php";
$arctbl -> actionFilterKey="id_contract";
$arctbl -> recIndex="id_contract";
$arctbl -> ignoreCols=array("id_contract","Category ID","Status ID","Type ID","Billing Notes","Under Review","Removed","Amount Paid","Purchase Order","End","Removed","Execution","Awarded","Company ID","Renewal Num","Notes","Parent ID");
$arctbl -> recQuery = "
SELECT
a.id_contract,
b.cust_company as \"Company\",
a.contract as \"Contract\",
a.contract_label as \"Label\",
c.contract_cat as \"Category\",
e.contract_tp as \"Type\",
d.sys_status as \"Status\",
DATE_FORMAT(a.contract_dc,'%m-%d-%Y') as \"Created\",
DATE_FORMAT(a.contract_da,'%m-%d-%Y') as \"Awarded\",
DATE_FORMAT(a.contract_ds,'%m-%d-%Y') as \"Execution\",
DATE_FORMAT(a.contract_de,'%m-%d-%Y') as \"End\",
DATE_FORMAT(a.contract_dr,'%m-%d-%Y') as \"Removed\",
a.id_cust_company as \"Company ID\",
a.contract_value as \"Value\",
(SELECT sum(invoice_payment) FROM _invoice_payment z WHERE z.id_contract = a.id_contract) as \"Paid\",
a.contract_parentID as \"Parent ID\",
a.contract_notes as \"Notes\",
a.contract_renewalNumber as \"Renewal Num\",
a.contract_du as \"Last Update\",
a.contract_review as \"Under Review\",
a.contract_billingNotes as \"Billing Notes\",
a.id_contract_tp as \"Type ID\",
a.id_sys_status as \"Status ID\",
a.id_contract_cat as \"Category ID\"
from _contract a
LEFT JOIN _cust_company b ON a.id_cust_company=b.id_cust_company
LEFT JOIN _contract_cat c ON a.id_contract_cat=c.id_contract_cat
LEFT JOIN _sys_status d ON a.id_sys_status=d.id_sys_status
LEFT JOIN _contract_tp e ON a.id_contract_tp=e.id_contract_tp";
$arctbl -> actionDestination = "content";
$arctbl -> ajDestination = "list08";
$arctbl -> ajPage = $path."list.php";
$arctbl -> build();
echo hex2str($arctbl -> tblNav);
echo $arctbl->dataTable;
?>
