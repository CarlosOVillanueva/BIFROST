<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once("_lib/php/auth.php");
require_once("_includes/heading.php");
require_once("_includes/optionstoolbar.php");
# Recordset
$gdbo -> sql ="SELECT 
a.id_contract,
a.contract,
FROM_UNIXTIME(UNIX_TIMESTAMP(a.contract_dc)+$gmtOffset) as contract_dc,
FROM_UNIXTIME(UNIX_TIMESTAMP(a.contract_dr)+$gmtOffset) as contract_dr,
a.contract_ds,
a.contract_de,
a.id_cust_company,
a.contract_value,
a.contract_parentID,
a.contract_notes,
a.contract_renewalNumber,
FROM_UNIXTIME(UNIX_TIMESTAMP(a.contract_du)+$gmtOffset) as contract_du,
a.contract_review,
a.contract_billingNotes,
a.id_contract_tp,
a.id_sys_status,
a.id_contract_cat,
a.contract_label,
a.contract_da,
(SELECT sum(invoice_payment) FROM _invoice_payment b WHERE b.id_contract = a.id_contract) as contract_paid
FROM _contract a
WHERE a.id_contract=".$_POST["id_contract"];
$gdbo -> getRec();
$rec = $gdbo -> getAssociative();
# Company
require("_model/dboCompany.php");
$gdbo -> getRec();
$listCompany = $gdbo -> dbData;
# Contract Type
require("_model/dboContractTp.php");
$gdbo -> getRec();
$listContractTp = $gdbo -> dbData;
# Contract Category
require("_model/dboContractCat.php");
$gdbo -> getRec();
$listContractCat = $gdbo -> dbData;
# Contract Status
require("_model/dboContractStatus.php");
$gdbo -> getRec();
$listContractStatus = $gdbo -> dbData;
# Fieldset Variables
$buttonDate ='<div class="elementIconBox" onclick="openCalendar(this,\'interface\',null,\'dateFld\')"><i class="fa fa-calendar"></i></div>';
# Fieldsets
$fsContract=array(
array(
array("*Organization","id_cust_company",null,1,$listCompany,$rec[0]["id_cust_company"]),
array("*Status","id_sys_status",null,1,$listContractStatus,$rec[0]["id_sys_status"]),
array(null,"id_contract",null,3,null,$rec[0]["id_contract"]),
),
array(
array("*Type","id_contract_tp",null,1,$listContractTp,$rec[0]["id_contract_tp"]),
array("*Category","id_contract_cat",null,1,$listContractCat,$rec[0]["id_contract_cat"]),
),
array(
array("*Contract","contract",null,0,null,$rec[0]["contract"]),
array("*Label","contract_label",null,0,null,$rec[0]["contract_label"]),
),
array(
array("*Value","contract_value",'onblur="validateElement(\'money\',this)"',0,null,$rec[0]["contract_value"]),
array("Paid to Date","paid",'disabled="disabled"',0,null,$rec[0]["contract_paid"]),
),
array(
array('Award Date','contract_da','class="dateFld elementIcon" onblur="validateElement(\'date\',this)"',0,null,$rec[0]["contract_da"],null,$buttonDate,null,'YYYY-MM-DD'),
array("Date Updated","contract_du",'disabled="disabled"',0,null,$rec[0]["contract_du"]),
),
array(
array('Execution Date','contract_ds','class="dateFld elementIcon" onblur="validateElement(\'date\',this)"',0,null,$rec[0]["contract_ds"],null,$buttonDate,null,'YYYY-MM-DD'),
array("Date Terminated","contract_dr",'disabled="disabled"',0,null,$rec[0]["contract_dr"]),
),
array(
array('Expiration Date','contract_de','class="dateFld elementIcon" onblur="validateElement(\'date\',this)"',0,null,$rec[0]["contract_ds"],null,$buttonDate,null,'YYYY-MM-DD'),
),
array(
array('General Notes','contract_notes','onkeydown="detectTab(this,event)"',6,null,hex2str($rec[0]["contract_notes"])),
),
array(
array('Billing Notes','contract_billingNotes','onkeydown="detectTab(this,event)"',6,null,hex2str($rec[0]["contract_billingNotes"])),
),
);
$requiredFields = "contract_value,id_cust_company,id_sys_status,contract_label,contract,id_contract_cat,id_contract_tp";
?>
<form method="post" id="frmContract" name="frmContract" action="javascript:submitFrmVals('content','/_mod/smod_08/sql.php','<?=$requiredFields?>','&form=frmContract&action=update','frmContract')">
<fieldset id="contract_detail">
<legend>Contract Detail</legend>
<?=frmElements($fsContract);?>
</fieldset>
</form>
<form action="#">
<fieldset id="Files">
<legend>Files</legend>
<div id="listFiles">
<?php
require("listFiles.php");
?>
</div></fieldset></form>
<script type="text/javascript">
oREC = '{"oRec":[{"module":"08","filter":"id_contract","recid":"<?=$_POST["id_contract"]?>","formname":"frmContract"}]}';
<?=$icoDelete;?>
<?=$icoUpload;?>
<?=$icoCollapse;?>
<?=$icoPreview;?>
$("#oOptions").append(icoDelete+icoPreview+icoUpload+icoCollapse);
collapseTabs();
</script>
<script type="text/javascript">
CKEDITOR.replace("contract_billingNotes");
CKEDITOR.replace("contract_notes");
</script>
