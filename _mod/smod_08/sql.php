<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
# Includes
require_once("_lib/php/auth.php");
# Parse Form Action
$action = $_POST['action'];
$form = $_POST['form'];
$formAction = $form . "," . $action;
# Static Variables
$_POST["contract_du"]=$dtTimeUTC;
if (isset($_POST["id_sys_status"]) && $_POST["id_sys_status"]==14 && isset($_POST["contract_dr"]) && $_POST["contract_dr"] =="") {
$_POST["contract_dr"]=$dtTimeUTC;
} elseif ($_POST["id_sys_status"]!=14 && isset($_POST["contract_dr"]) && $_POST["contract_dr"] !="") {
$_POST["contract_dr"]=''; 
} else {
$_POST["contract_dr"]=(isset($_POST["contract_dr"])?$_POST["contract_dr"]:null);
}
# Parse
if (isset($_POST["payment_mText"])){
$payments = hex2str($_POST["payment_mText"]);
$payments = json_decode($payments);
$paymentsFormatted = array();
foreach ($payments as $payment => $entry) {
$paymentProcessed = (!empty($entry[2])?strtotime($entry[2]):NULL);
$paymentsFormatted[]=array($entry[0],strtotime($entry[1]),$paymentProcessed,$entry[3]);
}
$paymentsInsertStr = array2sqlstring($paymentsFormatted,"0,1");
};
# Multi Form Columns
$paymentCols="contract_payment,contract_payment_dr,contract_payment_dp,id_contract";
switch ($formAction) {
case "frmContract,insert":
/*****************************************************************************/
$gdbo -> dbTable = "_contract";
$gdbo -> insertRec();
$_POST["id_contract"] = $gdbo -> insertedID;
# Insert Payments
$gdbo -> dbTable = "_contract_payment";
if (!empty($paymentsInsertStr)){
$gdbo -> dbTable = "_contract_payment";
$paymentsInsertStr = str_replace("~id~", $_POST["id_contract"], $paymentsInsertStr);
$gdbo -> sql = "(".$paymentCols.") values ".$paymentsInsertStr; 
$gdbo -> insertRec();
}
include('edit.php');
/*****************************************************************************/
break;
case "frmContract,update":
/*****************************************************************************/
$gdbo -> dbTable = "_contract";
$gdbo -> updateRec("id_contract=".$_POST["id_contract"]);
##
# Update Payments
$gdbo -> dbTable = "_contract_payment";
$gdbo -> deleteRec("id_contract=".$_POST["id_contract"]);
if (!empty($paymentsInsertStr)){
$gdbo -> dbTable = "_contract_payment";
$paymentsInsertStr = str_replace("~id~", $_POST["id_contract"], $paymentsInsertStr);
$gdbo -> sql = "(".$paymentCols.") values ".$paymentsInsertStr; 
$gdbo -> insertRec();
}
include('edit.php');
/*****************************************************************************/
break;
case "frmContract,delete":
/*****************************************************************************/
$gdbo -> dbTable = "_contract";
$gdbo -> deleteRec("id_contract=".$_POST["id_contract"]);
/*****************************************************************************/ 
# Purge all records that may be linked to this company
# IMPORTANT! All records associated with the branch will be purged
$gdbo -> sql ="SELECT TABLE_NAME FROM information_schema.columns WHERE COLUMN_NAME='id_contract'";
$gdbo -> getRec();
$delTables = $gdbo -> dbData;
foreach($delTables as $row => $column) {
foreach($column as $data => $value) {
$gdbo -> dbTable = $value;
$gdbo -> deleteRec("id_contract=".$_POST["id_contract"]);
}
}
include("index.php");
break; 
default:
break;
}
include("_error/status.php");
?>
