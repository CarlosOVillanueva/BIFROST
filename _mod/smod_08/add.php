<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once("_lib/php/auth.php");
require_once("_includes/optionstoolbar.php");
# Company
require("_model/dboCompany.php");
$gdbo -> getRec();
$listCompany = $gdbo -> dbData;
# Contract Type
require("_model/dboContractTp.php");
$gdbo -> getRec();
$listContractTp = $gdbo -> dbData;
# Contract Category
require("_model/dboContractCat.php");
$gdbo -> getRec();
$listContractCat = $gdbo -> dbData;
# Contract Status
require("_model/dboContractStatus.php");
$gdbo -> getRec();
$listContractStatus = $gdbo -> dbData;
# Fieldset Variables
$buttonDate ='<div class="elementIconBox" onclick="openCalendar(this,\'interface\',null,\'dateFld\')"><i class="fa fa-calendar"></i></div>';
# Fieldsets
$fsContract=array(
array(
array("*Organization","id_cust_company",null,1,$listCompany),
array("*Status","id_sys_status",null,1,$listContractStatus),
),
array(
array("*Type","id_contract_tp",null,1,$listContractTp),
array("*Category","id_contract_cat",null,1,$listContractCat),
),
array(
array("*Contract","contract",null,0),
array("*Label","contract_label",null,0),
),
array(
array("*Value","contract_value",'onblur="validateElement(\'money\',this)"',0),
),
array(
array('Award Date','contract_da','class="dateFld elementIcon" onblur="validateElement(\'date\',this)"',0,null,null,null,$buttonDate,null,'YYYY-MM-DD'),
),
array(
array('Execution Date','contract_ds','class="dateFld elementIcon" onblur="validateElement(\'date\',this)"',0,null,null,null,$buttonDate,null,'YYYY-MM-DD'),
),
array(
array('Expiration Date','contract_de','class="dateFld elementIcon" onblur="validateElement(\'date\',this)"',0,null,null,null,$buttonDate,null,'YYYY-MM-DD'),
),
array(
array('General Notes','contract_notes','onkeydown="detectTab(this,event)"',6),
),
array(
array('Billing Notes','contract_billingNotes','onkeydown="detectTab(this,event)"',6),
),
);
$requiredFields = "contract_value,id_cust_company,id_sys_status,contract_label,contract,id_contract_cat,id_contract_tp";
?>
<script type="text/javascript">$(".icoadd").remove();</script>
<form method="post" id="frmContract" name="frmContract" action="javascript:submitFrmVals('content','/_mod/smod_08/sql.php','<?=$requiredFields?>','&form=frmContract&action=insert','frmContract')">
<fieldset id="contract_detail">
<legend>Contract Detail</legend>
<?=frmElements($fsContract);?>
</fieldset>
</form>
<script type="text/javascript">
CKEDITOR.replace("contract_billingNotes");
CKEDITOR.replace("contract_notes");
</script>
