<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once("_lib/php/auth.php");
require_once("_includes/optionstoolbar.php");
/**************************************************/
$arcdb = new ArcDb;
$arcdb -> dbConStr=$globalDBCON;
$arcdb -> dbType = $globalDBTP;
$arcdb -> dbSchema = $globalDB;
/**************************************************/
$arcdb -> sql = "
SELECT
a.id_con,
a.con_alias,
a.id_con_db_tp,
b.con_db_tp_alias
FROM _con a
LEFT JOIN _con_db_tp b ON a.id_con_db_tp=b.id_con_db_tp
ORDER BY a.id_con_db_tp,a.con_alias";
$arcdb -> getRec();
$connections=$arcdb -> dbData;
$eleConnection = array(
array(
array('Connection', 'con','onchange="buildTableView(this)"',1,$connections),
),
);
?>
<script type="text/javascript">
document.getElementById('oOptions').innerHTML="";
</script>
<form>
<fieldset id="schema_browser">
<legend>Schema Browser</legend>
<?php buildElements($eleConnection);?>
<div id="list19" style="margin-top:10px;display:none">&nbsp;</div>
</fieldset>
</form>
<?php include("view_query.php")?>
