<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once("_lib/php/auth.php");
$sql=(isset($_POST["sqlbox"])?hex2str($_POST["sqlbox"]):"");
if (isset($_POST["id_con"])&&$_POST["id_con"]!=""){
$arcdb = new ArcDb;
$arcdb -> dbConStr=$globalDBCON;
$arcdb -> dbType = $globalDBTP;
$arcdb -> dbSchema = $globalDB;
/**************************************************/
$arcdb -> sql = "
SELECT
a.id_con, 
a.con, 
a.id_con_db_tp,
a.con_alias,
a.con_user,
a.con_enabledbit,
a.con_password,
a.con_host,
a.con_port,
a.con_catalog,
a.con_options,
b.con_db_tp
FROM _con a
LEFT JOIN _con_db_tp b ON a.id_con_db_tp=b.id_con_db_tp
WHERE a.id_con =".$_POST["id_con"];
$arcdb -> getRec();
$con=$arcdb -> getAssociative();
}
$arcCon = new ArcTbl;
$arcCon -> dbLimit = 23;
$arcCon -> dbOffset = 0;
$arcCon -> dbCatalog = (isset($con[0]["con_catalog"])?$con[0]["con_catalog"]:$arcCon -> dbCatalog);
$arcCon -> oCon = (isset($con[0]["con"])?$con[0]["con"]:$arcCon -> oCon);
$arcCon -> dbTable = $arcCon -> oCon;
$arcCon -> dbSchema = (isset($con[0]["con"])?$con[0]["con"]:$arcCon -> dbSchema);
$arcCon -> oUser = (isset($con[0]["con_user"])?$con[0]["con_user"]:$arcCon -> oUser);
$arcCon -> oPassword =(isset($con[0]["con_password"]) && !empty($con[0]["con_password"])?decrypt($con[0]["con_password"]):$arcCon -> oPassword);
$arcCon -> oHost = (isset($con[0]["con_host"])?$con[0]["con_host"]:$arcCon -> oHost);
$arcCon -> oPort = (isset($con[0]["con_port"])?$con[0]["con_port"]:$arcCon -> oPort);
$arcCon -> oOptions =(isset($con[0]["con_options"])?$con[0]["con_options"]:$arcCon -> oOptions);
$arcCon -> dbType = (isset($con[0]["con_db_tp"])?$con[0]["con_db_tp"]:$arcCon -> dbType);
$arcCon -> ajDestination = "queryReport";
$arcCon -> ajPage = $path."view_customquery.php";
$arcCon -> ignoreCols=array();
$arcCon -> ignoreFilterCols=array();
$arcCon -> post = (isset($_POST["arctbl"])?$_POST["arctbl"]:null);
$arcCon -> recQuery = $sql;
$arcCon -> build(); 
echo hex2str($arcCon -> tblNav);
echo $arcCon->dataTable;
?> 
