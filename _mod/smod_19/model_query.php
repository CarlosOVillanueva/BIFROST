<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once ("_lib/php/auth.php");
if (isset($_POST["id_con"]) && $_POST["id_con"] != "") {
	$arcdb = new ArcDb;
	$arcdb -> dbConStr = $globalDBCON;
	$arcdb -> dbType = $globalDBTP;
	$arcdb -> dbSchema = $globalDB;
	/**************************************************/
	$arcdb -> sql = "
SELECT
a.id_con, 
a.con, 
a.id_con_db_tp,
a.con_alias,
a.con_user,
a.con_enabledbit,
a.con_password,
a.con_host,
a.con_port,
a.con_catalog,
a.con_options,
b.con_db_tp
FROM _con a
LEFT JOIN _con_db_tp b ON a.id_con_db_tp=b.id_con_db_tp
WHERE a.id_con =" . $_POST["id_con"];
	$arcdb -> getRec();
	$con = $arcdb -> getAssociative();
	/**************************************************/
}
$arcCon = new ArcDb;
$arcCon -> dbCatalog = (isset($con[0]["con_catalog"]) ? $con[0]["con_catalog"] : $arcCon -> dbCatalog);
$arcCon -> oCon = (isset($con[0]["con"]) ? $con[0]["con"] : $arcCon -> oCon);
$arcCon -> dbTable = $arcCon -> oCon;
$arcCon -> dbSchema = (isset($con[0]["con"]) ? $con[0]["con"] : $arcCon -> dbSchema);
$arcCon -> oUser = (isset($con[0]["con_user"]) ? $con[0]["con_user"] : $arcCon -> oUser);
$arcCon -> oPassword = (isset($con[0]["con_password"]) && !empty($con[0]["con_password"]) ? decrypt($con[0]["con_password"]) : $arcCon -> oPassword);
$arcCon -> oHost = (isset($con[0]["con_host"]) ? $con[0]["con_host"] : $arcCon -> oHost);
$arcCon -> oPort = (isset($con[0]["con_port"]) ? $con[0]["con_port"] : $arcCon -> oPort);
$arcCon -> oOptions = (isset($con[0]["con_options"]) ? $con[0]["con_options"] : $arcCon -> oOptions);
$arcCon -> dbType = (isset($con[0]["con_db_tp"]) ? $con[0]["con_db_tp"] : $arcCon -> dbType);
/*Make Connection*/
if ($arcCon -> oCon == "" | $arcCon -> dbType == "") {die();
}
switch ($arcCon -> dbType ) {
	case "mysql" :
		$arcCon -> dbSchema = "information_schema";
		$sql = "SELECT TABLE_SCHEMA as `Schema`,TABLE_NAME as `Table`,COLUMN_NAME as `Column`,DATA_TYPE as `Type` FROM COLUMNS WHERE TABLE_SCHEMA='" . $arcCon -> oCon . "' ORDER BY TABLE_NAME,COLUMN_NAME";
		break;
	case "mssql" :
		$sql = "SELECT TABLE_SCHEMA as [Schema],TABLE_NAME as [Table],COLUMN_NAME as [Column],DATA_TYPE as [Type] FROM INFORMATION_SCHEMA.COLUMNS ORDER BY TABLE_NAME,COLUMN_NAME";
		$arcCon -> recOrderCol = "[Column]";
		break;
	case "pgsql" :
		$sql = "SELECT TABLE_Catalog as \"Database Schema\",TABLE_SCHEMA as \"Table Schema\",TABLE_NAME as \"Table\",COLUMN_NAME as \"Column\",DATA_TYPE as \"Type\" FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='" . $arcCon -> dbCatalog . "' AND TABLE_CATALOG='" . $arcCon -> oCon . "' ORDER BY TABLE_NAME,COLUMN_NAME";
		break;
}
$arcCon -> sql = $sql;
$arcCon -> getRec();
$schemaCols = $arcCon -> dbData;
$schemaColsList = "";
for ($i = 0; $i < count($schemaCols); $i++) {
	$next = $i + 1;
	$prev = $i - 1;
	if ($i == 0 || $schemaCols[$i][1] != $schemaCols[$prev][1]) {
		$topLevel = '<ul class="fa-ul"><li onclick="toggleChild(this)"><i class="fa-li fa fa-chevron-circle-up"></i>' . $schemaCols[$i][1] . '</li>';
		$subLevelTop = '<ul style="display:none" class="childlist">';
		$closeTopLevel = '';
		$closeSubLevel = '';
	} else {$topLevel = '';
		$subLevelTop = '';
	}

	if ($i == (count($schemaCols) - 1) || $schemaCols[$i][1] != $schemaCols[$next][1]) {
		$closeTopLevel = '</ul>';
		$closeSubLevel = '</ul>';
	} else {$closeTopLevel = '';
	}
	$subLevel = '<li>' . $schemaCols[$i][2] . ' [' . $schemaCols[$i][3] . ']</li>';
	$schemaColsList .= $topLevel . $subLevelTop . $subLevel . $closeSubLevel . $closeTopLevel;
}
?>
<style>
	#colSchema li, ul {
		cursor: default;
		padding: 0 !important;
		border: 0 !important;
	}
	#colSchema ul ul > li {
		margin: 4px 0 4px 16px !important;
	}
	#colSchema .fa {
		margin-right: 10px;
	}
	#colSchema .submitreport {
		display: block;
		width: 100% !important;
	}
	#colSchema .schemaMenu {
		border: 1px solid #91A8B6;
		border-radius: 5px;
		color: #1D445C;
		-moz-border-radius: 5px;
		-webkit-border-radius: 5px;
		-webkit-box-shadow: 0px 0px 2px 0px rgba(50, 50, 50, 0.25);
		-moz-box-shadow: 0px 0px 2px 0px rgba(50, 50, 50, 0.25);
		box-shadow: 0px 0px 2px 0px rgba(50, 50, 50, 0.25);
		padding: 5px;
		background-color: #FBFCFE;
	}
	#tableQueryBuilder {
		border-collapse: collapse;
		width: 100%;
		padding:0;
		margin:0;
	}
	#colSchema {
		width: 400px;
		padding:0;
	}
	#colQuery textarea {
		width: 100% !important;
	}
	#colQuery {
		vertical-align: top;
		padding:0;
	}
	#colQuery .runreport {
		display: block;
		float: right;
	}
	#queryReport {
		margin: 10px;
	}

</style>
<table id="tableQueryBuilder">
	<tr>
		<td id="colSchema">
		<div class="schemaMenu">
			<?=$schemaColsList; ?>
		</div></td>
		<td id="colQuery">
		<form method="post" name="frmQuery" id="frmQuery" action="javascript:submitFrmVals('queryReport','/_mod/smod_19/view_customquery.php','','&id_con='+$('#oCon').val(),'frmQuery')">
			<fieldset style="margin-top:0" id="sql_window">
				<legend>
					SQL Window
				</legend>
				<textarea id="sqlbox" name="sqlbox"></textarea>
				<span onclick="prepSaveChanges('frmQuery')" class="button runreport"><i class="fa fa-cog fa-fw"></i>Run Report</span>
			</fieldset>
		</form><div id="queryReport"></div></td>
	</tr>
</table>
<script>disableSelForTag("li,ol")</script>
