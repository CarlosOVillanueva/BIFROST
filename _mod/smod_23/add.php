<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once("_lib/php/auth.php");
require_once("_includes/optionstoolbar.php");
# Company 
$gdbo -> sql = "SELECT id_cust_company,cust_company FROM _cust_company where id_cust_company_tp=7";
$gdbo -> getRec();
$listCompany = $gdbo -> dbData;
# Filterable Objects
# Fieldsets
$fs=
array(
array(
array('Manufacturer', 'id_cust_company',null,1,$listCompany),
array('Model', 'part_model',null,0),
),
array(
array('Manufacturer Part Number', 'part_model_partnumber',null,0),
array('Internal Part Number', 'part_model_internalpartnumber',null,0),
),
array(
array('Description', 'part_model_descr',null,6),
)
);
?>
<script type="text/javascript">$(".icoadd").remove();</script>
<form method="post" name="frmPartModel" id="frmPartModel" action="javascript:submitFrmVals('content','/_mod/smod_23/sql.php','part_model,id_cust_company','&form=frmPartModel&action=insert','frmPartModel')">
<fieldset id="Part">
<legend>Part</legend>
<?=frmElements($fs);?>
</fieldset>
</form>
<script type="text/javascript">
CKEDITOR.replace("part_model_descr");
</script>
