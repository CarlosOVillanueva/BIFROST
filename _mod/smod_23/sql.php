<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
# Includes
require_once("_lib/php/auth.php");
# Parse Form Action
$action = $_POST['action'];
$form = $_POST['form'];
$formAction = $form . "," . $action;
# Begin SQL Functions
switch ($formAction) {
case "frmPartModel,insert":
/*****************************************************************************/
# Insert Part Model
$gdbo -> dbTable = "_part_model";
$gdbo -> insertRec();
$id_part_model = $gdbo -> insertedID;
$_POST['id_part_model']=$id_part_model;
include('edit.php');
/*****************************************************************************/
break;
case "frmPartModel,update":
/*****************************************************************************/
# Update Part Model
$gdbo -> dbTable = "_part_model";
$gdbo -> updateRec("id_part_model=".$_POST['id_part_model']);
include('edit.php');
/*****************************************************************************/
break;
case "frmPartModel,delete":
/*****************************************************************************/
$gdbo -> dbTable = "_part_model";
$gdbo -> deleteRec("id_part_model=".$_POST["id_part_model"]);
/*****************************************************************************/ 
include("index.php");
break; 
default:
break;
}
include("_error/status.php");
?>
