<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once ("_lib/php/auth.php");
$_POST["tm_timesheet_approvedBy"] = $id_cust_contact_derived;
$arc=new ArcDb;
$arc -> dbType = $globalDBTP;
$arc -> dbSchema = $globalDB; 
$arc -> dbConStr=$globalDBCON;
$arc -> dbTable = "_tm_timesheet";
switch ($_POST["edit"]){
case 1: 
$arc -> updateRec("id_tm_timesheet=".$_POST["id_tm_timesheet"]);
break;
case 2: 
$arc -> deleteRec("id_tm_timesheet=".$_POST["id_tm_timesheet"]);
break;
default:
break;
}
?>
<div class="cList" id="list16">
<?php include("list.php");?>
</div>
