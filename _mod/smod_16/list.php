<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/* application includes */
require_once("_lib/php/auth.php");
$arctbl = new ArcTbl;
$arctbl -> dbConStr=$globalDBCON;
$arctbl -> dbLimit = 23;
$arctbl -> recDetail = "hex2str(\$record['tm_timesheet'])";
$arctbl -> dbType = $globalDBTP;
$arctbl -> dbSchema = $globalDB;
$arctbl -> recQuery = "SELECT
a.id_tm_timesheet,
a.tm_timesheet,
FROM_UNIXTIME(a.tm_timesheet_ds,'%Y-%m-%d') as \"Period Start\",
FROM_UNIXTIME(a.tm_timesheet_de,'%Y-%m-%d') as \"Period End\",
FROM_UNIXTIME(UNIX_TIMESTAMP(a.tm_timesheet_dc)+$gmtOffset,'%Y-%m-%d %H:%i:%s') as \"Submitted\",
a.id_hr_emp as \"Employee ID\",
c.cust_contact_givenName as \"First\",
c.cust_contact_familyName as \"Last\",
b.hr_emp_timeApprover,
(SELECT sum(cast((tm_te-tm_ts-(tm_deduction*3600))/3600 as DECIMAL(12,2))) as Actual FROM _tm where id_hr_emp=a.id_hr_emp AND
(tm_ts >= a.tm_timesheet_ds AND tm_te <=a.tm_timesheet_de)) as Hours,
CASE a.tm_timesheet_approved
WHEN 0 THEN 'Pending Approval'
WHEN 1 THEN 'Approved'
ELSE 'Rejected'
END as \"Approved\",
concat('<select class=''timeapprovalselect'' id=''',a.id_tm_timesheet,'_timeAction'' onchange=''setAction(',a.id_tm_timesheet,')''>
<option>--</option>
<option value=''Approve''>Approve</option>
<option value=''Reject''>Reject</option>
<option value=''Delete''>Delete</option>') as \"\" from _tm_timesheet a
LEFT JOIN _hr_emp b ON a.id_hr_emp=b.id_hr_emp
LEFT JOIN _cust_contact c ON b.id_cust_contact=c.id_cust_contact";
$arctbl -> ajDestination = "list16";
$arctbl -> ajPage = $path."list.php";
$arctbl -> ignoreCols=array("id_tm_timesheet","Employee ID","hr_emp_timeApprover","tm_timesheet");
$arctbl -> ignoreFilterCols=array("Approver","id_tm_timesheet","Employee ID","hr_emp_timeApprover","");
$arctbl -> recFilter=" WHERE hr_emp_timeApprover=".$id_cust_contact_derived;
$arctbl -> recIndex = "id_tm_timesheet";
$arctbl -> build();
echo hex2str($arctbl -> tblNav);
echo $arctbl->dataTable;
?>
