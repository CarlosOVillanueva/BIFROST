<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once("_lib/php/auth.php");
$filterGroup = (isset($_POST["id_sys_group"])?" WHERE a.id_sys_group=".$_POST["id_sys_group"]:"");
$arctbl = new ArcTbl;
$arctbl -> dbConStr=$globalDBCON;
$arctbl -> dbOffset = 0;$arctbl -> dbLimit = 23;
$arctbl -> dbType = $globalDBTP;
$arctbl -> dbSchema = $globalDB;
$arctbl -> recIndex="id_sys_group_user";
$arctbl -> ignoreCols=array("id_sys_group_user","id_sys_user","id_sys_group");
$arctbl -> recQuery = "SELECT 
a.id_sys_group_user,
a.id_sys_user,
a.id_sys_group,
b.sys_user as Member,
concat('<a class=\"button\" onclick=\"arc(\'content\',\'/_mod/smod_24/sql.php\',\'form=frmMember&action=delete&id_sys_group=',a.id_sys_group,'&id_sys_group_user=',a.id_sys_group_user,'\')\"><i class=\"fa fa-trash fa-fw\"></i>Remove</a>') as '' 
FROM 
_sys_group_user a 
LEFT JOIN
_sys_user b ON a.id_sys_user = b.id_sys_user".$filterGroup;
$arctbl -> actionDestination = "content";
$arctbl -> ajDestination = "list24-01";
$arctbl -> ajPage = $path."listMembers.php";
$arctbl -> build();
echo hex2str($arctbl -> tblNav);
echo $arctbl->dataTable;
?>
