<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once("_lib/php/auth.php");
require_once("_includes/optionstoolbar.php");
# Record
$gdbo -> sql = "SELECT id_sys_group,sys_group from _sys_group where id_sys_group=".$_POST["id_sys_group"];
$gdbo -> getRec();
$rec = $gdbo -> getAssociative();
# List Users
$gdbo -> sql = "SELECT 
id_sys_user,
sys_user 
FROM 
_sys_user 
WHERE 
sys_user_dr is null AND not exists 
(SELECT id_sys_user 
FROM _sys_group_user 
WHERE id_sys_user=_sys_user.id_sys_user AND id_sys_group=".$_POST["id_sys_group"].")";
$gdbo -> getRec();
$listUser = $gdbo -> dbData;
# Fieldset Variables
$buttonAdd ='<div class="elementIconBox" onclick="arc(\'content\',\''.$path.'sql.php\',\'form=frmMember&action=insert&id_sys_group=\'+$(\'#id_sys_group\').val()+\'&id_sys_user=\'+$(\'#id_sys_user\').val())"><i class="fa fa-plus"></i></div>';
# Fieldsets
$fsGroup=
array(
array(
array('Group', 'sys_group',null,0,null,$rec[0]["sys_group"]),
array(null, 'id_sys_group',null,3,null,$rec[0]["id_sys_group"]),
)
);
$fsMember=array(
array(
array('Add Member', 'id_sys_user','class="elementIcon"',1,$listUser,null,null,$buttonAdd ),
)
);
?>
<form method="post" name="frmGroup" id="frmGroup" action="javascript:submitFrmVals('content','/_mod/smod_24/sql.php',null,'&form=frmGroup&action=update','frmGroup')">
<fieldset id="group">
<legend>Group</legend>
<?=frmElements($fsGroup);?>
</fieldset>
<fieldset id="members">
<legend>Members</legend>
<?=frmElements($fsMember);?>
<div id="list24-01">
<?php include("listMembers.php");?>
</div>
</fieldset>
<fieldset id="permissions">
<legend>
Permissions
</legend>
<?php include("editPermissions.php");?>
</fieldset>
</form>
<script type="text/javascript">
oREC = '{"oRec":[{"module":"24","filter":"id_sys_group","recid":"<?=$_POST["id_sys_group"]?>","formname":"frmGroup"}]}';
<?=$icoCollapse;?>
<?=$icoDelete;?>
$("#oOptions").append(icoDelete+icoCollapse);
collapseTabs();
</script>
