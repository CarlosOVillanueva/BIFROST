<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once("_lib/php/auth.php");
$dbo = new ArcDb;
$dbo -> dbConStr=$globalDBCON;
$dbo -> dbType = $globalDBTP;
$dbo -> dbSchema = $globalDB;
$action = $_POST['action'];
$form = $_POST['form'];
$formAction = $form . "," . $action;
if(isset($_POST["con_password"]) && $_POST["con_password"]!=""){
$_POST["con_password"]=encrypt($_POST["con_password"]);}
switch ($formAction) {
/* ********************************************************************** */
case "dbcon,insert":
/* ********************************************************************** */
$dbo -> dbTable = "_con";
$dbo -> insertRec();
$id_con = $dbo -> insertedID;
$_POST["id_con"]=$id_con;
include("edit.php");
break;
/* ********************************************************************** */
case "dbcon,update":
/* ********************************************************************** */
$dbo -> dbTable = "_con";
$dbo -> updateRec("id_con=".$_POST["id_con"]);
include("edit.php");
break;
/* ********************************************************************** */
case "dbcon,delete":
/* ********************************************************************** */
$dbo -> dbTable = "_con";
$dbo -> deleteRec("id_con=".$_POST["id_con"]);
include("index.php");
break;
}
include("_error/status.php");
?>
