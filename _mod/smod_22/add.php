<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once("_lib/php/auth.php");
require_once("_includes/optionstoolbar.php");
/**************************************************/
$arcdb = new ArcDb;
$arcdb -> dbConStr=$globalDBCON;
$arcdb -> dbType = $globalDBTP;
$arcdb -> dbSchema = $globalDB;
/**************************************************/
$arcdb -> sql = "
SELECT 
id_con_db_tp,
con_db_tp_alias
FROM _con_db_tp
";
$arcdb -> getRec();
$dbtypes=$arcdb -> dbData;
/**************************************************/
$enabledBoo=array(array(1,"Enable"),array(0,"Disable"));
$conFields =
array(
array(
array('Friendly Name', 'con_alias',null,0),
array('Database Type', 'id_con_db_tp',null,1,$dbtypes),
array('Status', 'con_enabledbit',null,4,$enabledBoo,0)
)
,
array(
array('Database', 'con',null,0),
array('Table Schema', 'con_catalog',null,0,null),
)
,
array(
array('Username', 'con_user',null,0),
array('Password', 'con_password',null,7)
)
,
array(
array('Host', 'con_host',null,0),
array('Port', 'con_port',null,0)
)
,
array(
array('Options','con_options',null,6)
)
);
?>
<script type="text/javascript">$(".icoadd").remove();</script>
<form method="post" name="dbcon" id="dbcon" action="javascript:submitFrmVals('content','/_mod/smod_22/sql.php',null,'&form=dbcon&action=insert','dbcon')">
<fieldset id="database_connection">
<legend>Database Connection</legend>
<?= frmElements($conFields);?>
</div>
</fieldset>
</form>
