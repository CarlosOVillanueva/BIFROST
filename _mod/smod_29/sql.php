<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
# Includes
require_once("_lib/php/auth.php");
if (isset($_POST["lineItem_mText"])){
$lineItems = hex2str($_POST["lineItem_mText"]);
$lineItems = json_decode($lineItems);
$lineItemsInsertStr = array2sqlstring($lineItems,'0,2,3,4');
};
if (isset($_POST["payment_mText"])){
$paymentItems = hex2str($_POST["payment_mText"]);
$paymentItems = json_decode($paymentItems);
$paymentItemsInsertStr = array2sqlstring($paymentItems,'0,4');
};
# Multi Form Columns
$lineItemsCols="purchaseorder_item,purchaseorder_item_dservice,purchaseorder_item_qty,purchaseorder_item_rate,purchaseorder_item_amount,purchaseorder_item_percentTax,purchaseorder_item_totalAmount,id_contract,id_purchaseorder_qty_tp,id_purchaseorder";
$paymentItemsCols ="purchaseorder_payment,purchaseorder_payment_refNumber,purchaseorder_payment_dreceived,purchaseorder_payment_dcleared,id_purchaseorder_payment_tp,id_contract,id_purchaseorder";
# Parse Form Action
$action = $_POST['action'];
$form = $_POST['form'];
$formAction = $form . "," . $action;
# Begin SQL Functions
switch ($formAction) {
case "frmpurchaseorder,insert":
/*****************************************************************************/
$_POST["purchaseorder_du"]=$dtTimeUTC;
$_POST["id_sys_user"]=$id_sys_user;
# Insert purchaseorder Record
$gdbo -> dbTable = "_purchaseorder";
$gdbo -> insertRec();
$id_purchaseorder = $gdbo -> insertedID;
$_POST['id_purchaseorder']=$id_purchaseorder;
##
# Insert Line Items
if (!empty($lineItemsInsertStr)){
$gdbo -> dbTable = "_purchaseorder_item";
$lineItemsInsertStr = str_replace("~id~", $_POST["id_purchaseorder"], $lineItemsInsertStr);
$gdbo -> sql = "(".$lineItemsCols.") values ".$lineItemsInsertStr;
$gdbo -> insertRec();
}
# Insert Payments
if (!empty($paymentItemsInsertStr)){
$gdbo -> dbTable = "_purchaseorder_payment";
$paymentItemsInsertStr = str_replace("~id~", $_POST["id_purchaseorder"], $paymentItemsInsertStr);
$gdbo -> sql = "(".$paymentItemsCols.") values ".$paymentItemsInsertStr;
$gdbo -> insertRec();
}
include('edit.php');
/*****************************************************************************/
break;
case "frmpurchaseorder,update":
/*****************************************************************************/
$_POST["purchaseorder_du"]=$dtTimeUTC;
$_POST["purchaseorder_lastUpdatedBy"]=$id_sys_user;
# Update Contact Record
$gdbo -> dbTable = "_purchaseorder";
$gdbo -> updateRec("id_purchaseorder=".$_POST["id_purchaseorder"]);
##
# Update Line Items
$gdbo -> dbTable = "_purchaseorder_item";
$gdbo -> deleteRec("id_purchaseorder=".$_POST["id_purchaseorder"]);
if (!empty($lineItemsInsertStr)){
$gdbo -> dbTable = "_purchaseorder_item";
$lineItemsInsertStr = str_replace("~id~", $_POST["id_purchaseorder"], $lineItemsInsertStr);
$gdbo -> sql = "(".$lineItemsCols.") values ".$lineItemsInsertStr;
$gdbo -> insertRec();
}
# Update Payments
$gdbo -> dbTable = "_purchaseorder_payment";
$gdbo -> deleteRec("id_purchaseorder=".$_POST["id_purchaseorder"]);
if (!empty($paymentItemsInsertStr)){
$gdbo -> dbTable = "_purchaseorder_payment";
$paymentItemsInsertStr = str_replace("~id~", $_POST["id_purchaseorder"], $paymentItemsInsertStr);
$gdbo -> sql = "(".$paymentItemsCols.") values ".$paymentItemsInsertStr;
$gdbo -> insertRec();
}
include('edit.php');
/*****************************************************************************/
break;
case "frmpurchaseorder,delete":
/*****************************************************************************/
$gdbo -> sql ="SELECT TABLE_NAME FROM information_schema.columns WHERE COLUMN_NAME='id_purchaseorder'";
$gdbo -> getRec();
$contact_tables = $gdbo -> dbData;
foreach($contact_tables as $row => $column) {
foreach($column as $data => $value) {
$gdbo -> dbTable = $value;
$gdbo -> deleteRec("id_purchaseorder=".$_POST["id_purchaseorder"]);
}
}
include('index.php');
/*****************************************************************************/ 
break; 
default:
break;
}
include("_error/status.php");
?>
