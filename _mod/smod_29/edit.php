<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once("_lib/php/auth.php");
require_once("_includes/optionstoolbar.php");
/*
SELECT _purchaseorder.id_purchaseorder,
_purchaseorder.purchaseorder,
_purchaseorder.purchaseorder_dc,
_purchaseorder.purchaseorder_du,
_purchaseorder.purchaseorder_ddue,
_purchaseorder.purchaseorder_dpaid,
_purchaseorder.purchaseorder_dsubmitted,
_purchaseorder.purchaseorder_dcleared,
_purchaseorder.id_sys_user,
_purchaseorder.purchaseorder_lastModifiedBy,
_purchaseorder.id_cust_company,
_purchaseorder.id_cust_branch,
_purchaseorder.purchaseorder_poc,
_purchaseorder.purchaseorder_department,
_purchaseorder.purchaseorder_percentDiscount,
_purchaseorder.purchaseorder_totalTax,
_purchaseorder.purchaseorder_totalAmount,
_purchaseorder.purchaseorder_amount,
_purchaseorder.id_sys_status
FROM _purchaseorder;
*/
# Record
$gdbo -> sql = "SELECT
id_purchaseorder,
purchaseorder,
FROM_UNIXTIME(UNIX_TIMESTAMP(purchaseorder_dc)+$gmtOffset) as purchaseorder_dc,
FROM_UNIXTIME(UNIX_TIMESTAMP(purchaseorder_du)+$gmtOffset) as purchaseorder_du,
purchaseorder_ddue,
purchaseorder_dsubmitted,
purchaseorder_dcleared,
purchaseorder_dpaid,
id_sys_user,
id_sys_status,
id_cust_company,
id_cust_branch,
purchaseorder_department,
purchaseorder_poc,
purchaseorder_percentDiscount,
purchaseorder_amount,
purchaseorder_totalAmount,
purchaseorder_totalTax,
purchaseorder_lastModifiedBy
FROM _purchaseorder 
WHERE id_purchaseorder=".$_POST["id_purchaseorder"];
$gdbo -> getRec();
$rec = $gdbo -> getAssociative();
# Branch 
if (isset($rec[0]["id_cust_company"])){
$gdbo -> sql = "SELECT id_cust_branch, cust_branch FROM _cust_branch WHERE id_cust_company=".$rec[0]["id_cust_company"];
$gdbo -> getRec();
$listBranch = $gdbo -> dbData;}
else {
$listBranch = array();
}
# Rules
## Disable elements linked to parent company
$disabled = ($rec[0]["id_cust_company"] == 1 ? 'disabled="disabled"' : null); 
# Status
$gdbo -> sql = "SELECT 
id_sys_status,
sys_status
FROM _sys_status 
WHERE id_sys_status_tp = 8
ORDER BY sys_status_order;";
$gdbo -> getRec();
$listStatusType = $gdbo -> dbData;
# Company 
$gdbo -> sql = "SELECT 
a.id_cust_company,
a.cust_company,
b.id_cust_company_tp,
b.cust_company_tp 
FROM _cust_company a 
LEFT JOIN _cust_company_tp b 
ON a.id_cust_company_tp = b.id_cust_company_tp 
ORDER BY b.cust_company_tp asc, a.cust_company asc";
$gdbo -> getRec();
$listCompany = $gdbo -> dbData;
# Contacts 
$gdbo -> sql = "SELECT
id_cust_contact,
contact,
IF(id_cust_company IS NULL,'0',id_cust_company) as id_cust_company,
IF(cust_company IS NULL,'General Contacts',cust_company) as cust_company
FROM
(
SELECT 
x.id_cust_contact,
concat(x.cust_contact_familyName,',',x.cust_contact_givenName) AS \"Contact\",
x.id_cust_company,
y.cust_company
FROM _cust_contact x
LEFT JOIN _cust_company y ON x.id_cust_company = y.id_cust_company
ORDER BY y.cust_company,x.cust_contact_familyName,x.cust_contact_givenName)
as derived ORDER BY cust_company";
$gdbo -> getRec();
$listContact = $gdbo -> dbData;
# Filterable Objects
## Branches
$branchesdbo = new ArcDb;
$branchesdbo -> dbConStr=$globalDBCON;
$branchesdbo -> dbType = $globalDBTP;
$branchesdbo -> dbSchema = $globalDB;
$branchesdbo -> sql = "SELECT id_cust_branch, cust_branch FROM _cust_branch";
$branchesdbo -> dbFilter = "WHERE id_cust_company=";
$branchesdbo -> type = "list";
$branchesdbo -> id = "id_cust_branch";
$branchesdbo -> attributes = 'class="chBranch"';
$branchesdboJSON =encrypt(json_encode((array)$branchesdbo));
# Fieldset Variables
$dateDue ='<div class="elementIconBox" onclick="openCalendar(this,\'interface\',null,\'clDueDate\')"><i class="fa fa-calendar"></i></div>';
$datePaid ='<div class="elementIconBox" onclick="openCalendar(this,\'interface\',null,\'clPaidDate\')"><i class="fa fa-calendar"></i></div>';
$dateSubmitted ='<div class="elementIconBox" onclick="openCalendar(this,\'interface\',null,\'clSubmitDate\')"><i class="fa fa-calendar"></i></div>';
$dateCleared='<div class="elementIconBox" onclick="openCalendar(this,\'interface\',null,\'clClearedDate\')"><i class="fa fa-calendar"></i></div>';
# Fieldsets
$fsCustomer=
array(
array(
array('*Organization','id_cust_company','onchange="filterRec(this,\''.$branchesdboJSON.'\',\'dBranch\')"',1,$listCompany,$rec[0]['id_cust_company']),
array('*Branch','id_cust_branch',$disabled.' class=\'chBranch\'',1,$listBranch,$rec[0]['id_cust_branch'],"dBranch"),
),
array(
array('Point of Contact','purchaseorder_poc',null,1,$listContact,$rec[0]['purchaseorder_poc'],"dPOC"),
array('Department','purchaseorder_department',null,0,null,$rec[0]['purchaseorder_department']),
)
);
$fspurchaseorder=
array(
array(
array('Status','id_sys_status',null,1,$listStatusType,$rec[0]['id_sys_status']),
array('Last Updated','purchaseorder_du','disabled="disabled"',0,null,$rec[0]['purchaseorder_du']),
),
array(
array('PO Number','purchaseorder',null,0,null,$rec[0]['purchaseorder']),
array('Amount','purchaseorder_amount','disabled="disabled"',0,null,$rec[0]['purchaseorder_amount']),
array(null, 'id_purchaseorder',null,3,null,$rec[0]["id_purchaseorder"]),
),
array(
array('Tax','purchaseorder_totalTax','disabled="disabled"',0,null,$rec[0]['purchaseorder_totalTax']),
array('Total Amount','purchaseorder_totalAmount','disabled="disabled"',0,null,$rec[0]['purchaseorder_totalAmount']),
), 
array(
array('Date Submitted','purchaseorder_dsubmitted','class="clSubmitDate elementIcon" validateElement(\'date\',this)',0,null,$rec[0]['purchaseorder_dsubmitted'],null,$dateSubmitted,null,'YYYY-MM-DD'),
array('Date Due','purchaseorder_ddue','class="clDueDate elementIcon" validateElement(\'date\',this)',0,null,$rec[0]['purchaseorder_ddue'],null,$dateDue,null,'YYYY-MM-DD'),
),
array( 
array('Date Paid','purchaseorder_dpaid','class="clPaidDate elementIcon" validateElement(\'date\',this)',0,null,$rec[0]['purchaseorder_dpaid'],null,$datePaid,null,'YYYY-MM-DD'),
array('Date Cleared','purchaseorder_dcleared','class="clClearedDate elementIcon" validateElement(\'date\',this)',0,null,$rec[0]['purchaseorder_dcleared'],null,$dateCleared,null,'YYYY-MM-DD'),
),
);
$requiredFields = "id_cust_company,id_cust_branch";
?>
<script type="text/javascript">$(".icoadd").remove();</script>
<form method="post" id="frmpurchaseorder" name="frmpurchaseorder" action="javascript:submitFrmVals('content','/_mod/smod_29/sql.php','<?=$requiredFields?>','&action=update&form=frmpurchaseorder','frmpurchaseorder')">
<fieldset id="vendor">
<legend>Vendor</legend>
<?=frmElements($fsCustomer);?>
</fieldset>
<fieldset id="summary">
<legend>Summary</legend>
<?=frmElements($fspurchaseorder);?>
</fieldset>
<?php
$lineItem = new POLineItem;
$lineItem -> filter = "id_purchaseorder";
$lineItem -> id = $rec[0]["id_purchaseorder"];
$lineItem -> build();
$payment = new POPayment;
$payment -> filter = "id_purchaseorder";
$payment -> id = $rec[0]["id_purchaseorder"];
$payment -> build();
?> 
</form>
<form action="#">
<fieldset id="Files">
<legend>Files</legend>
<div id="listFiles">
<?php
require("listFiles.php");
?>
</div></fieldset></form>
<script type="text/javascript">
oREC = '{"oRec":[{"module":"29","filter":"id_purchaseorder","recid":"<?=$_POST["id_purchaseorder"]?>","formname":"frmpurchaseorder"}]}';
<?=$icoDelete;?>
<?=$icoCollapse;?>
<?=$icoPreview;?> 
<?=$icoUpload;?>
$("#oOptions").append(icoDelete+icoUpload+icoPreview+icoCollapse);
collapseTabs();
</script>
