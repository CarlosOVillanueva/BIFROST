<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once("_lib/php/auth.php");
$gdbo -> sql ="
SELECT
COALESCE((SELECT sum(purchaseorder_payment) FROM _purchaseorder_payment),0) as `Paid`,
COALESCE((SELECT sum(invoice_payment) FROM _invoice_payment),0) as `Earned`";
$gdbo -> getRec();
$graphData = $gdbo -> getAssociative();
$graphDataJSON = json_encode($graphData);

?>
<script>
var chartProfit = AmCharts.makeChart("chartdiv2", {
    "type": "pie",
    "theme": "light",
       "colors":["#0077aa","#ee8800"],
    "dataProvider": [{
        "title": "Money Collected",
        "value": <?=$graphData[0]['Earned']?>
    }, {
        "title": "Money Spent",
        "value": <?=$graphData[0]['Paid']?>

    }],
    "titleField": "title",
    "valueField": "value",
    "labelRadius": 5,

    "radius": "42%",
    "innerRadius": "60%",
    "labelText": "[[title]]: [[percents]]%",

    "radius": "42%",
    "innerRadius": "60%",
    "labelText": "[[title]]: [[percents]]%",
     "exportConfig":{
		"menuTop":"20px",
        "menuRight":"20px",
        "menuItems": [{
        "icon": '/_thirdparty/amcharts/images/export.png',
        "format": 'png'	  
        }]  
    }
});
</script>
<div id="chartdiv2" class="chart" style="width: 100%; height: 435px;font-size:11pt"></div>
