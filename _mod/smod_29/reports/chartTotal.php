<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once("_lib/php/auth.php");
$gdbo -> sql ="
SELECT
b.cust_company as `Customer`,
COALESCE((SELECT sum(a1.purchaseorder_payment) FROM _purchaseorder_payment a1 LEFT JOIN _purchaseorder b1 ON a1.id_purchaseorder=b1.id_purchaseorder WHERE a.id_cust_company=b1.id_cust_company ),0) as `Paid`,
sum(a.purchaseorder_totalAmount) - COALESCE((SELECT sum(a1.purchaseorder_payment) FROM _purchaseorder_payment a1 LEFT JOIN _purchaseorder b1 ON a1.id_purchaseorder=b1.id_purchaseorder WHERE a.id_cust_company=b1.id_cust_company ),0) as `Owe`
FROM _purchaseorder a
LEFT JOIN _cust_company b ON a.id_cust_company = b.id_cust_company
GROUP BY b.cust_company";
$gdbo -> getRec();
$graphData = $gdbo -> getAssociative();
$graphDataJSON = json_encode($graphData);

?>
<script>
var chartData=<?=$graphDataJSON?>;
var chart = AmCharts.makeChart("chartdiv", {
    "type": "serial",
	"theme": "none",
     "colors":["#0077aa","#ee8800"],
    "legend": {
        "horizontalGap": 10,
        "maxColumns": 1,
        "position": "right",
		"useGraphSettings": true,
		"markerSize": 10
    },
    
    "dataProvider": chartData,
    "valueAxes": [{
        "stackType": "regular",
        "axisAlpha": 0.3,
        "gridAlpha": 0
    }],
    "graphs": [{
        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
        "fillAlphas": 0.8,
        "labelText": "$[[value]]",
        "lineAlpha": 0.3,
        "title": "Paid",
        "type": "column",
		"color": "#000000",
        "valueField": "Paid"
    }, {
        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
        "fillAlphas": 0.8,
        "labelText": "$[[value]]",
        "lineAlpha": 0.3,
        "title": "Owe",
        "type": "column",
		"color": "#000000",
        "valueField": "Owe"
    }],
    "categoryField": "Customer",
    "categoryAxis": {
        "gridPosition": "start",
        "axisAlpha": 0,
        "gridAlpha": 0,
        "position": "left"
    },
	"exportConfig":{
		"menuTop":"20px",
        "menuRight":"20px",
        "menuItems": [{
        "icon": '/_thirdparty/amcharts/images/export.png',
        "format": 'png'	  
        }]  
    }
});
</script>
<div id="chartdiv" class="chart"  style="width: 100%; height: 435px;font-size:11pt"></div>
