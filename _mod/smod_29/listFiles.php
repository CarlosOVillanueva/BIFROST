<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once("_lib/php/auth.php");
if (isset($_POST["id_purchaseorder"])){
$_POST["modRecID"]=$_POST["id_purchaseorder"];
}
$filter = (isset($_POST["modRecID"])?" AND fso_pk=".$_POST["modRecID"]:"");
$arctbl = new ArcTbl;
$arctbl -> dbConStr=$globalDBCON;
$arctbl -> dbOffset = 0;
$arctbl -> dbLimit = 23;
$arctbl -> dbType = $globalDBTP;
$arctbl -> dbSchema = $globalDB;
$arctbl -> recIndex="id_fso";
$arctbl -> recFilter=" WHERE fso_pkcol='id_purchaseorder'".$filter; 
$arctbl -> ignoreCols=array("id_fso","fso_path","fso_pkcol","fso_pk");
$arctbl -> ignoreFilterCols=array("","id_fso","fso_path","fso_pkcol","fso_pk");
$arctbl -> recQuery = "
SELECT
a.id_fso,
concat('<a target=''_blank'' href=''/_lib/php/ArcFileUpload.Download.php?fso=',a.fso,'''>',a.fso_originalname,'</a>') as \"File Name\",
a.fso_filesize as \"Size\",
a.fso_ext as \"Type\",
FROM_UNIXTIME(a.fso_datemodified+$gmtOffset,'%Y-%m-%d %h:%i:%s %p') as \"Date Modified\",
concat('<input type=\"button\" value=\"Delete\" onclick=\"deleteFSO(',cast(a.id_fso as char),',\'29\')\"/>') as \"\",
a.fso_path,
a.fso_pkcol,
a.fso_pk
FROM _fso a WHERE a.fso_originalname <> 'avatar'";
$arctbl -> ajDestination = "listFiles";
$arctbl -> ajPage ="/_mod/smod_29/listFiles.php";
$arctbl -> build();
echo hex2str($arctbl -> tblNav);
echo $arctbl->dataTable;
?>
