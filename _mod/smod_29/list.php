<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once("_lib/php/auth.php");
$arctbl = new ArcTbl;
$arctbl -> dbConStr=$globalDBCON;
$arctbl -> dbLimit = 23;
$arctbl -> dbType = $globalDBTP;
$arctbl -> dbSchema = $globalDB;
$arctbl -> recLink= $path."edit.php";
$arctbl -> actionFilterKey="id_purchaseorder";
$arctbl -> recIndex="id_purchaseorder";
$arctbl -> ignoreCols=array("id_purchaseorder");
$arctbl -> ignoreFilterCols=array("id_purchaseorder");
$arctbl -> recQuery = "
SELECT
a.id_purchaseorder,
a.purchaseorder as \"Purchase Order\",
b.cust_company as \"Vendor\",
CONCAT('$',CAST(a.purchaseorder_amount as CHAR)) as \"Amount\",
CONCAT('$',CAST(a.purchaseorder_totalAmount as CHAR)) as \"Total\",
CONCAT('$',(SELECT sum(purchaseorder_payment) FROM _purchaseorder_payment WHERE id_purchaseorder=a.id_purchaseorder)) as \"Paid\",
purchaseorder_dsubmitted as \"Date Submitted\",
purchaseorder_ddue as \"Due Date\",
c.sys_status as \"Status\"
FROM _purchaseorder a
LEFT JOIN _cust_company b ON a.id_cust_company = b.id_cust_company
LEFT JOIN _sys_status c ON a.id_sys_status = c.id_sys_status
";
$arctbl -> actionDestination = "content";
$arctbl -> ajDestination = "list29";
$arctbl -> ajPage = $path."list.php";
$arctbl -> build();
echo hex2str($arctbl -> tblNav);
echo $arctbl->dataTable;
?>
