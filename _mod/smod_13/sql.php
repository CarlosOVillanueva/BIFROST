<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
# Includes
require_once("_lib/php/auth.php");
# Parse Form Action
$action = $_POST['action'];
$form = $_POST['form'];
$formAction = $form . "," . $action;
# Begin SQL Functions
switch ($formAction) {
case "frmCompany,update":
/*****************************************************************************/
$gdbo -> sql ="SELECT * FROM _sys_company";
$gdbo -> getRec();
if(count($gdbo->dbData) > 0) {
$gdbo -> dbTable = "_sys_company";
$gdbo -> updateRec("id_sys_company=1");}
else {
$gdbo -> dbTable = "_sys_company";
$gdbo -> insertRec();
}
/*****************************************************************************/
include("index.php");
break;
}
include("_error/status.php");
?>
