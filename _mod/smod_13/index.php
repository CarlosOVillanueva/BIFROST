<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once("_lib/php/auth.php");
require_once("_includes/heading.php");
# Records
$gdbo -> sql = "SELECT
id_sys_company,
sys_company_tin,
sys_company_tin_tp,
sys_company_registeredAgent,
sys_company_payrollContact,
sys_company_naicsCode,
sys_company_stateTaxID
FROM _sys_company;";
$gdbo -> getRec();
$rec = $gdbo -> getAssociative();
# Contacts
$gdbo -> sql = "SELECT id_cust_contact,concat(_cust_contact.cust_contact_familyName,',',
_cust_contact.cust_contact_givenName) as Name FROM _cust_contact WHERE id_cust_company = 1";
$gdbo -> getRec();
$listContacts = $gdbo -> dbData;
# Variables
$dateFiscal='<div class="elementIconBox" onclick="openCalendar(this,\'interface\',null,\'clFiscal\')"><i class="fa fa-calendar"></i></div>';
$dateTax='<div class="elementIconBox" onclick="openCalendar(this,\'interface\',null,\'clTax\')"><i class="fa fa-calendar"></i></div>';
# Arrays
$listTIN = array(
array("EIN","Employer Identification Number"),
array("SSN","Social Security Number"),
array("ITIN","Individual Taxpayer Identification Number"),
array("ATIN","Taxpayer Identification Number for Pending U.S. Adoptions"),
array("PTIN","Preparer Taxpayer Identification Number "),
);
# Fieldsets
$fsTaxData = array (
array(
array("TIN Type","sys_company_tin_tp",null,1,$listTIN,$rec[0]["sys_company_tin_tp"]),
array("Tax Payer Identification","sys_company_tin",null,0,null,$rec[0]["sys_company_tin"]),
),
array(
array("NAICS Code","sys_company_naicsCode",null,0,null,$rec[0]["sys_company_naicsCode"]),
array("State TaxID","sys_company_stateTaxID",null,0,null,$rec[0]["sys_company_stateTaxID"]),
),
);
$fsPayroll = array (
array(
array("Payroll Contact","sys_company_payrollContact",null,1,$listContacts,$rec[0]["sys_company_payrollContact"]),
array("Registered Agent","sys_company_registeredAgent",null,1,$listContacts,$rec[0]["sys_company_registeredAgent"]),
),
);
?>
<form method="post" id="frmCompany" name="frmCompany" action="javascript:submitFrmVals('content','/_mod/smod_13/sql.php',null,'&action=update&form=frmCompany','frmCompany')">
<fieldset>
<legend>Tax Data</legend>
<?=frmElements($fsTaxData);?>
</fieldset>
<fieldset>
<legend>Company Contacts</legend>
<?=frmElements($fsPayroll);?>
</fieldset>
</form>
<script type="text/javascript">
icoSave = '<span class="icoSave" onclick="prepSaveChanges()"><img alt="save" style="margin:0 4px" height="16" width="16" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAsdJREFUeNp8U0tIlFEU/u7jn/nnn4ej5LvSBve9oGihYYo7N4URRgguqk0bKaJo0SpoG7QwaFEEKZJCUrhpKbXwVWGFhOOkoThNOqmN48x/b+dejYgeFw73v+f+53zf+c657MGTYSilDmutbyutWqDx52IAZ/wFY+wq53x8x2VNUnCv4zjn6+vqUVoShxsKgZL9imUMm7kcVrKrLXOpubFCoXBPcH5BcsBhGux+/5A+uP8QDDIxgP4LA7bNwEJOvZmAKwULS6Bz0gM3aPv21CAej4JzRv8oa5xMGGMKjmAoK42ioa4GDiGFhca5KQ8xqSEZBb0aG0dDoh41VeUIh1xbW5CCJN2ZQz6Xx2p2DdNTE4gQ8llCDttg0sDUaFjMpRYQcIJw3SDiYQ83Rj9j4O2iLc2arYPKkEF4sqCLdCb0VWlIkyiQQkAI2qUwNWJgegkvL7djq6jQVCt/02R00YcXEGi7OxLnK18zz959mMFSOo31jQ0Si1ktDGKQ+Wh+lERmJYu+wWFr5rvp4SyEKlL7qQzHCeQ7OzrwcTaJxcVlpFKfsOYFLVI6p1AZEfALCkePNVqf+Ta+9KZv9ZHhSOTk2NRrK2IiUYcIiegK+vP5U6zmfFR4EiOpPI5XbZcxslC0vmzOdIoSmCy+72M2OY9gIICQ66IiFrad+FbUKKNsvTNF3HlfJC01JaeWuhxZYmIZGBFNvZIEFFyA026GxmixsaUQC3J0JiROHyinlAp9k2k8Tiq60xaEr2Qyg18yX7Ce+w5fKUokbUfMfGwqhmjIQeteF9eu38T88jpO1AYQdSXd7byFnovdPWe6uocam1svhTzvCNuZNEeEiJhAdcRFWUkA1bsiqIy7ELpAPtKD7gxLZl5j16l2j5LtJov+7HX0Sv+tUDTWpmjIippDm4GjtjF6L/YhcTNIWGP490qQleL/y/8hwAB52QNlkuILOAAAAABJRU5ErkJggg==">Save</span>';
$("#oOptions").html(icoSave); 
</script>
