<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once ("_lib/php/auth.php");
$ajCfgDevice = null;
$ajCfgDevice = (isset($_POST["id_cfg_device"])?"&id_cfg_device=".$_POST["id_cfg_device"]:"");
$ajCfgDevice.= (isset($_POST["id_device"])?"&id_device=".$_POST["id_device"]:"");
$ajCfgDevice.= (isset($_POST["id_cfg"])?"&id_cfg=".$_POST["id_cfg"]:"");
/**************************************************/
$netgroup=new ArcDb;
$netgroup->dbConStr=$globalDBCON;
$netgroup->dbType = $globalDBTP;
$netgroup->dbSchema=$globalDB;
$netgroup->sql="SELECT id_cfg_device_netgroup,cfg_device_netgroup from _cfg_device_netgroup order by cfg_device_netgroup";
$netgroup->getRec();
$listnetgroup=$netgroup->dbData;
/**************************************************/
$netgroup->sql="
SELECT 
inet_ntoa(a.cfg_device_ip4netaddress) as \"Network\",
a.cfg_device_ip4netaddress_nmbits as \"Netmask\",
a.id_cfg_device_netgroup,
b.cfg_device_netgroup
FROM _cfg_device_ip4netaddress a
INNER JOIN _cfg_device_netgroup b
ON a.id_cfg_device_netgroup=b.id_cfg_device_netgroup 
WHERE cfg_device_ip4netaddress_enabled=1";
$netgroup->getRec();
$listnetwork=$netgroup->dbData;
echo "<script type='text/javascript'>window.listNetwork=".json_encode($listnetwork).";</script>";
/**************************************************/
$buttonAdd ='<div class="elementIconBox" onclick="prepSaveChanges(\'frmNetGroup\')"><i class="fa fa-plus"></i></div>';
$mnunetgroup = array(
array( 
array("Network Group", "cfg_device_netgroup", "class=\"elementIcon\"", 0,null,null,null,$buttonAdd,null,'Network Group'), 
));
/**************************************************/
?>
<legend>
Add Network Group
</legend>
<form name="frmNetGroup" id="frmNetGroup" method="post" action="javascript:submitFrmVals('fs_network_groups','/_mod/smod_18/sql.php','cfg_device_netgroup','<?=$ajCfgDevice?>&action=insert&form=frmNetGroup','frmNetGroup')">
<?=frmElements($mnunetgroup); ?>
</form>
<h3>Configured Network Groups</h3>
<div id="tblnetgroup" class="fieldsettable">
<?php include("list_netgroup.php");?>
</div>
