<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once ("_lib/php/auth.php");
# POST variables
$ajCfgDevice = null;
$ajCfgDevice = (isset($_POST["id_cfg_device"])?"&id_cfg_device=".$_POST["id_cfg_device"]:"");
$ajCfgDevice.= (isset($_POST["id_device"])?"&id_device=".$_POST["id_device"]:"");
$ajCfgDevice.= (isset($_POST["id_cfg"])?"&id_cfg=".$_POST["id_cfg"]:"");
$definedNetworks = new ArcTbl;
$definedNetworks -> dbConStr=$globalDBCON;
$definedNetworks -> dbType = $globalDBTP;
$definedNetworks -> dbSchema = $globalDB;
$definedNetworks -> recQuery = "
SELECT
id_cfg_device_ip4netaddress,
inet_ntoa(cfg_device_ip4netaddress) as \"Network\",
cfg_device_ip4netaddress_nmbits as \"Netmask\",
cfg_device_netgroup as \"NetGroup\",
CONCAT('<input type=\"button\" value=\"Delete\" onclick=\"updateTable(this,''_cfg_device_ip4netaddress'',''id_cfg_device_ip4netaddress'',',id_cfg_device_ip4netaddress,',2,''".$path."sql.php'',''fs_networks'',''&form=frmNetwork&action=delete".$ajCfgDevice."'')\"/>') as \"\" 
FROM _cfg_device_ip4netaddress
INNER JOIN _cfg_device_netgroup on _cfg_device_ip4netaddress.id_cfg_device_netgroup=_cfg_device_netgroup.id_cfg_device_netgroup
WHERE cfg_device_ip4netaddress_enabled=1";
$definedNetworks->recIndex="id_cfg_device_ip4netaddress";
$definedNetworks->ignoreCols=array("id_cfg_device_ip4netaddress","Enabled");
$definedNetworks->ignoreFilterCols=array("id_cfg_device_ip4netaddress","");
$definedNetworks -> recOrderCol = "Network";
$definedNetworks -> recOrder = "asc";
$definedNetworks -> dbLimit = 10;
$definedNetworks -> dbOffset = 0;
$definedNetworks -> ajDestination = "tblnetwork";
$definedNetworks -> ajPage = "/_mod/smod_18/list_network.php";
$definedNetworks -> build();
echo hex2str($definedNetworks->tblNav);
echo $definedNetworks->dataTable;
?>
