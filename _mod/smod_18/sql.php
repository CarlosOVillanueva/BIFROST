<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once("_lib/php/auth.php");
$dbo = new ArcDb;
$dbo -> dbConStr=$globalDBCON;
$dbo -> dbType = $globalDBTP;
$dbo -> dbSchema = $globalDB;
$action = $_POST['action'];
$form = $_POST['form'];
$formAction = $form . "," . $action;
# POST variables
$ajCfgDevice = null;
$ajCfgDevice = (isset($_POST["id_cfg_device"])?"&id_cfg_device=".$_POST["id_cfg_device"]:"");
$ajCfgDevice.= (isset($_POST["id_device"])?"&id_device=".$_POST["id_device"]:"");
$ajCfgDevice.= (isset($_POST["id_cfg"])?"&id_cfg=".$_POST["id_cfg"]:"");
$_POST["cfg_device_du"] = $dtTimeUTC;
$_POST["cfg_du"] = $dtTimeUTC;
if($form!="frmConfig" && $form!="frmDevice"){
# Ajax actions
switch ($form){
case "frmIFLabel":
$labels="true";$networks="false";$netgroups="false";$interfaces="false";
break;
case "frmInterface":
$labels="false";$networks="false";$netgroups="false";$interfaces="true";
break;
case "frmNetGroup":
$labels="false";$networks="false";$netgroups="true";$interfaces="false";
break;
case "frmNetwork":
$labels="false";$networks="true";$netgroups="false";$interfaces="false";
break;
}
$updateTabs = '
<script text="javascript">
arc("fs_labels","/_mod/smod_18/inc_iflabel.php","'.$ajCfgDevice.'",0,0,'.$labels.');
arc("fs_networks","/_mod/smod_18/inc_network.php","'.$ajCfgDevice.'",0,0,'.$networks.');
arc("fs_network_groups","/_mod/smod_18/inc_netgroup.php","'.$ajCfgDevice.'",0,0,'.$netgroups.');
arc("fs_interfaces","/_mod/smod_18/inc_interface.php","'.$ajCfgDevice.'",0,0,'.$interfaces.');
arc("tblinterface","/_mod/smod_18/list_interface.php","'.$ajCfgDevice.'",0,0,'.$interfaces.');
arc("list18-1","/_mod/smod_18/list_interface_filtered.php","'.$ajCfgDevice.'",0,0,'.$interfaces.');
</script>';
}
switch($formAction) {
# frmConfig
case("frmConfig,insert") :
$dbo -> dbTable = "_cfg";
$dbo -> insertRec();
$_POST['id_cfg']=$dbo -> insertedID;
include("edit.php");
break;
case("frmConfig,edit") :
$dbo -> dbTable = "_cfg";
$dbo -> updateRec("id_cfg=" . $_POST["id_cfg"]);
include("edit.php");
break;
case("frmConfig,delete") :
$dbo -> dbTable = "_cfg";
$dbo -> deleteRec("id_cfg=" . $_POST["id_cfg"]);
$dbo -> sql = "SELECT id_cfg_device FROM _cfg_device WHERE id_cfg=".$_POST["id_cfg"];
$dbo -> getRec();
$devices=$dbo->dbData;
$dbo -> dbTable = "_cfg_device_interface";
foreach ($devices as $record => $cols) {
$dbo -> deleteRec("id_cfg_cdevice=". $cols[0]);
}
$dbo -> dbTable = "_cfg_device";
$dbo -> deleteRec("id_cfg=" . $_POST["id_cfg"]);
include("index.php");
break;
# frmDevice
case("frmDevice,insert") :
$dbo -> dbTable = "_cfg_device";
$dbo -> insertRec();
$_POST['id_cfg_device']=$dbo -> insertedID;
include("edit_cfgdevice.php");
echo "<script type=\"text/javascript\">arc('list18-0','".$path."list_cfgdevice.php','id_cfg=".$_POST["id_cfg"]."',1,0);</script>";
break;
case("frmDevice,edit") :
$dbo -> dbTable = "_cfg_device";
$_POST['cfg_device_du']=$dtTimeUTC;
$dbo -> updateRec("id_cfg_device=" . $_POST["id_cfg_device"]);
include("edit_cfgdevice.php");
echo "<script type=\"text/javascript\">arc('list18-0','".$path."list_cfgdevice.php','id_cfg=".$_POST["id_cfg"]."',1,0);</script>";
break;
case("frmDevice,delete") :
$dbo -> dbTable = "_cfg_device";
$dbo -> deleteRec("id_cfg_device=" . $_POST["id_device"]);
$dbo -> dbTable = "_cfg_device_interface";
$dbo -> deleteRec("id_cfg_cdevice=".$_POST["id_device"]);
$_POST['id']=$_POST['id_cfg'];
include("list_cfgdevice.php");
break;
# frmNetGroup
case("frmNetGroup,insert") :
$dbo -> dbTable = "_cfg_device_netgroup";
$dbo -> insertRec();
echo $updateTabs; 
break;
case("frmNetGroup,edit") :
$dbo -> dbTable = "_cfg_device_netgroup";
$dbo -> updateRec("id_cfg_device_netgroup=" . $_POST["id_cfg_device_netgroup"]);
echo $updateTabs;
break;
case("frmNetGroup,delete") :
$dbo -> dbTable = "_cfg_device_netgroup";
$dbo -> deleteRec("id_cfg_device_netgroup=" . $_POST["id_cfg_device_netgroup"]);
echo $updateTabs;
break;
# frmNetwork
case("frmNetwork,insert") :
$_POST["cfg_device_ip4netaddress"]=ip2long($_POST["cfg_device_ip4netaddress"]);
$_POST["cfg_device_ip4netaddress_enabled"]=1;
$dbo -> dbTable = "_cfg_device_ip4netaddress"; 
$dbo -> insertRec();
echo $updateTabs;
break;
case("frmNetwork,delete") :
$dbo -> dbTable = "_cfg_device_ip4netaddress";
$dbo -> deleteRec("id_cfg_device_ip4netaddress=" . $_POST["id_cfg_device_ip4netaddress"]);
echo $updateTabs;
break;
# frmIFLabel
case("frmIFLabel,insert") :
$dbo -> dbTable = "_device_interfacelabel"; 
$dbo -> insertRec();
echo $updateTabs;
break;
case("frmIFLabel,delete") :
$dbo -> dbTable = "_device_interfacelabel"; 
$dbo -> deleteRec("id_device_interfacelabel=" . $_POST["id_device_interfacelabel"]);
echo $updateTabs;
break;
case("frmIFLabel,edit") :
$dbo -> dbTable = "_device_interfacelabel"; 
echo $_POST["id_device_interfacelabel"];
$dbo -> updateRec("id_device_interfacelabel=" . $_POST["id_device_interfacelabel"]);
echo $updateTabs;
break;
# Missing Delete
# frmInterface
case("frmInterface,insert") :
$_POST["cfg_device_interface_ip4hostaddress"]=ip2long($_POST["cfg_device_interface_ip4hostaddress"]);
$dbo -> dbTable = "_cfg_device_interface"; 
$dbo -> insertRec();
echo $updateTabs;
break;
case("frmInterface,delete") :
echo $_POST["id_cfg_device_interface"];
$dbo -> dbTable = "_cfg_device_interface";
$dbo -> deleteRec("id_cfg_device_interface=" . $_POST["id_cfg_device_interface"]);
echo $updateTabs;
break;
# Missing Delete
}
include("_error/status.php");
?>
