<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/* application includes */
require_once("_lib/php/auth.php");
$arctbl = new ArcTbl;
$arctbl -> dbConStr=$globalDBCON;
$arctbl -> dbLimit = 23;
$arctbl -> dbOffset = 0;
$arctbl -> dbType = $globalDBTP;
$arctbl -> dbSchema = $globalDB;
$arctbl -> recOrder="asc";
$arctbl -> recOrderCol="id_cfg";
$arctbl -> recLink= $path."edit.php";
$arctbl -> actionFilterKey="id_cfg";
$arctbl -> recQuery = "
SELECT
a.id_cfg,
b.cust_branch as \"Branch\",
a.cfg as \"Catalog\",
c.cfg_grp as \"Category\",
a.cfg_dc as \"Date Created\",
d.sys_status as \"Status\"
FROM _cfg a
LEFT JOIN _cfg_grp c
ON a.id_cfg_grp = c.id_cfg_grp 
LEFT JOIN _sys_status d
ON a.id_sys_status = d.id_sys_status 
LEFT JOIN _cust_branch b
ON a.id_cust_branch= b.id_cust_branch
";
$arctbl -> ajDestination = "list18";
$arctbl -> actionDestination = "content";
$arctbl -> ajPage = $path."list.php";
$arctbl -> ignoreCols=array("id_cfg");
$arctbl -> ignoreFilterCols=array("id_cfg");
$arctbl -> recFilter=null;
$arctbl -> recIndex = "id_cfg";
$arctbl -> build();
echo hex2str($arctbl -> tblNav);
echo $arctbl->dataTable;
?>
