<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once ("_lib/php/auth.php");
/**************************************************/
$interfacerec=new ArcDb;
$interfacerec->dbConStr=$globalDBCON;
$interfacerec->dbType = $globalDBTP;
$interfacerec->dbSchema=$globalDB;
/**************************************************/
#netgroup
$interfacerec->sql="SELECT a.id_cfg_device_netgroup,a.cfg_device_netgroup from _cfg_device_netgroup a
WHERE EXISTS (SELECT b.id_cfg_device_netgroup FROM _cfg_device_ip4netaddress b WHERE a.id_cfg_device_netgroup=b.id_cfg_device_netgroup AND b.cfg_device_ip4netaddress_enabled = 1)
order by cfg_device_netgroup";
$interfacerec->getRec();
$listnetgroup=$interfacerec->dbData;
/**************************************************/
#interfaces
$interfacerec->sql="SELECT
a.id_cfg_device_ip4netaddress,
concat(RTRIM(cast(inet_ntoa(a.cfg_device_ip4netaddress) as char)),'/',RTRIM(cast(a.cfg_device_ip4netaddress_nmbits as char))) as net,
a.id_cfg_device_netgroup,b.cfg_device_netgroup
FROM _cfg_device_ip4netaddress a LEFT JOIN _cfg_device_netgroup b ON a.id_cfg_device_netgroup=b.id_cfg_device_netgroup
WHERE a.cfg_device_ip4netaddress_enabled=1 ORDER BY a.id_cfg_device_netgroup,a.cfg_device_ip4netaddress";
$interfacerec->getRec();
$listnetworkinterfaces=$interfacerec->dbData;
echo "<script type='text/javascript'>window.listNetworkInterface=".json_encode($listnetworkinterfaces).";</script>";
/**************************************************/
#speedandduplex
$interfacerec->sql="SELECT id_device_ifspeedduplex,device_ifspeedduplex FROM _device_ifspeedduplex";
$interfacerec->getRec();
$listifspeedduplex=$interfacerec->dbData;
/**************************************************/
#label
$interfacerec->sql="
SELECT 
a.id_device_interfacelabel,
a.device_interfacelabel 
FROM _device_interfacelabel a
WHERE NOT EXISTS 
(SELECT b.id_device_interfacelabel 
FROM _cfg_device_interface b 
WHERE a.id_device_interfacelabel=b.id_device_interfacelabel 
AND b.id_cfg_device=".$_POST["id_cfg_device"]." 
AND b.cfg_device_interface_enabled=1) AND id_device=".$_POST["id_device"];
$interfacerec->getRec();
$listiflabel=$interfacerec->dbData;
/**************************************************/
#netmask
$listnetmask=array();
for($i=15;$i<33;$i++) {
$net=Net_IPv4::parseAddress("0.0.0.0/".$i);
$net->bitmask=$i;
$netmask=$net->netmask;
$listNetmask[]=array($i,$netmask." / ".$i);
}
/**************************************************/
# Filterable Objects
## Network
$gdbo = new ArcDb;
$gdbo -> dbConStr=$globalDBCON;
$gdbo -> dbType = $globalDBTP;
$gdbo -> dbSchema = $globalDB;
$gdbo -> sql = "SELECT
id_cfg_device_ip4netaddress,
CONCAT(CAST(inet_ntoa(cfg_device_ip4netaddress) as CHAR),'/',CAST(cfg_device_ip4netaddress_nmbits as CHAR)) as Network
FROM _cfg_device_ip4netaddress";
$gdbo -> dbFilter = " WHERE id_cfg_device_netgroup=";
$gdbo -> type = "list";
$gdbo -> id = "id_cfg_device_ip4netaddress";
$gdbo -> attributes = 'class="chNetwork" onchange="IPInNetwork(document.getElementById(\'cfg_device_interface_ip4hostaddress\'),document.getElementById(\'id_cfg_device_ip4netaddress\'))"';
$networkdboJSON = encrypt(json_encode((array)$gdbo));
$buttonAdd ='<a id="addInterface" onclick="IPIsUnique(document.getElementById(\'cfg_device_interface_ip4hostaddress\'),\'id_cfg_device_ip4netaddress\',null,null,\'frminterfacesubmit\',\'frmInterface\')" class="elementIconBox" title="Add Interface"><i class="fa fa-plus"></i></a>';
# Fieldsets
$mnuinterface = array( 
array(
array('Interface Label','id_device_interfacelabel','class="clInterfaceLabel"',1,$listiflabel),
array('Speed/Duplex','id_device_ifspeedduplex','',1,$listifspeedduplex),
array("MAC Address", "cfg_device_interface_mac",'onblur="validateElement(\'mac\',this)"', 0),
array(null, 'id_device',null,3,null,$_POST["id_device"]),
array(null, 'id_cfg',null,3,null,$_POST["id_cfg"]),
array(null, 'id_device',null,3,null,$_POST["id_cfg_device"]),
),
array(
array('NetGroup','id_cfg_device_netgroup','class="clNetGroups" onChange="filterRec(this,\''.$networkdboJSON.'\',\'dNetwork\');IPInNetwork(document.getElementById(\'cfg_device_interface_ip4hostaddress\'),document.getElementById(\'id_cfg_device_ip4netaddress\'))"',1,$listnetgroup),
array('Network Assignment','id_cfg_device_ip4netaddress','class="chNetwork" disabled="disabled"',1,null,null,'dNetwork'),
array('Address','cfg_device_interface_ip4hostaddress','onblur="validateElement(\'ip\',this)" disabled="disabled" class="elementIcon"',0,null,null,null,$buttonAdd),
),
);
/**************************************************/
?>
<legend>
Add Interface
</legend>
<?=frmElements($mnuinterface); ?>
<h3>Configured Interfaces</h3>
<div id="tblinterface" class="fieldsettable">
<?php include("list_interface.php");?>
</div>
<div id="intfInsertError" style="display:none"></div>
