<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once ("_lib/php/auth.php");
# POST variables
$ajCfgDevice = null;
$ajCfgDevice = (isset($_POST["id_cfg_device"])?"&id_cfg_device=".$_POST["id_cfg_device"]:"");
$ajCfgDevice.= (isset($_POST["id_device"])?"&id_device=".$_POST["id_device"]:"");
$ajCfgDevice.= (isset($_POST["id_cfg"])?"&id_cfg=".$_POST["id_cfg"]:"");
if(!isset($_POST["id_cfg_device"])){
$_POST["id_cfg_device"]="";
}
$ifrecordset = new ArcTbl;
$ifrecordset -> dbConStr=$globalDBCON;
$ifrecordset -> dbLimit = 23;
$ifrecordset -> dbOffset = 0;
$ifrecordset -> dbType = $globalDBTP;
$ifrecordset -> dbSchema = $globalDB;
$ifrecordset -> recOrder="asc";
$ifrecordset -> recOrderCol="id_cfg_device_interface";
$ifrecordset -> recIndex ="id_cfg_device_interface";
$ifrecordset -> recQuery = "
SELECT
c.cfg_device as \"Device Label\",
a.device_interfacelabel as \"IF Label\",
g.cfg_device_interface_mac as \"MAC\",
b.device_ifspeedduplex as \"Speed/Duplex\",
f.cfg_device_netgroup as \"NetGroup\",
d.device as \"Device\",
id_cfg_device_interface,
concat(inet_ntoa(e.cfg_device_ip4netaddress),'/',cast(e.cfg_device_ip4netaddress_nmbits as char)) as \"Network\",
inet_ntoa(cfg_device_interface_ip4hostaddress) as Address,
g.id_cfg_device,
cfg_device_interface_enabled as \"Enabled\",
c.id_cfg,
CONCAT('<input type=\"button\" value=\"Delete\" onclick=\"updateTable(this,''_cfg_device_interface'',''id_cfg_device_interface'',',id_cfg_device_interface,',2,''".$path."sql.php'',''tblinterface'',''&id_cfg_device_interface=',id_cfg_device_interface,'&form=frmInterface&action=delete".$ajCfgDevice."'')\"/>') as \"\",
c.id_device
FROM
_cfg_device_interface g
LEFT JOIN _device_interfacelabel a ON g.id_device_interfacelabel=a.id_device_interfacelabel
LEFT JOIN _device_ifspeedduplex b ON g.id_device_ifspeedduplex=b.id_device_ifspeedduplex
LEFT JOIN _cfg_device c ON g.id_cfg_device=c.id_cfg_device
LEFT JOIN _device d ON c.id_device=d.id_device
LEFT JOIN _cfg_device_ip4netaddress e ON g.id_cfg_device_ip4netaddress=e.id_cfg_device_ip4netaddress
LEFT JOIN _cfg_device_netgroup f ON e.id_cfg_device_netgroup=f.id_cfg_device_netgroup
where cfg_device_interface_enabled=1 AND g.id_cfg_device=".$_POST["id_cfg_device"];
$ifrecordset -> ajDestination = "tblinterface";
$ifrecordset -> ajPage = "/_mod/smod_18/list_interface.php";
$ifrecordset -> ignoreCols=array("id_cfg_device_interface","id_cfg_device","id_device","Enabled","id_cfg");
$ifrecordset -> ignoreFilterCols=array("id_cfg_device_interface","id_cfg_device","id_device","Action","Enabled","id_cfg");
$ifrecordset->build();
echo hex2str($ifrecordset->tblNav);
echo $ifrecordset->dataTable;
?>
