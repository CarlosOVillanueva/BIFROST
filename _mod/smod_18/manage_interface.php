<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once ("_lib/php/auth.php");
$ajCfgDevice = null;
$ajCfgDevice = (isset($_POST["id_cfg_device"])?"&id_cfg_device=".$_POST["id_cfg_device"]:"");
$ajCfgDevice.= (isset($_POST["id_device"])?"&id_device=".$_POST["id_device"]:"");
$ajCfgDevice.= (isset($_POST["id_cfg"])?"&id_cfg=".$_POST["id_cfg"]:"");
$mnuwindow = array(array( array(null, 'windowifclose', 'onclick="clearPop()"', 2, null, 'Close')));
/**************************************************/
?>
<style type="text/css">
#vpinterface {
clear:both;
}
#popoptions {
background-color:#5f86a6;
padding:10px 10px 0 10px;
clear:both;
overflow:hidden;
}
#popoptions .tab {
float:left;
background-color:#d0d0d0;
cursor:pointer;
color:#a0a0a0;
padding:10px;
text-transform:capitalize;
font-size:10pt;
border-bottom:1px solid #b0b0b0;
font-weight:600;
}
#popoptions .tabActive {
background-color:#fff;
color:#585858;
border-bottom:1px solid #fff;
border-left:1px solid #b0b0b0;
border-right:1px solid #b0b0b0;
}
#popBox {
border:4px solid #343434;
}
</style>
<div id="popBox">
<script type="text/javascript">
tabMenu('popBox','popoptions','tabActive','tabDead');
</script>
<div id="popoptions"></div>
<div id="vpinterface" class="childform">
<form name="frmInterface" id="frmInterface" method="post" action="javascript:submitFrmVals('fs_interfaces','/_mod/smod_18/sql.php','','<?=$ajCfgDevice?>&action=insert&form=frmInterface','frmInterface')">
<fieldset id="fs_interfaces">
<?php require_once("inc_interface.php");?>
</fieldset>
</form>
</div>
<div id="vpnetwork" class="childform">
<form name="frmNetwork" id="frmNetwork" method="post" action="javascript:submitFrmVals('fs_networks','/_mod/smod_18/sql.php','','<?=$ajCfgDevice?>&action=insert&form=frmNetwork','frmNetwork')">
<fieldset id="fs_networks">
<?php require_once("inc_network.php");?>
</fieldset>
</form>
</div>
<div id="vpiflabel" class="childform">
<fieldset id="fs_labels">
<?php require_once("inc_iflabel.php");?>
</fieldset>
</div>
<div id="vpnetgroup" class="childform">
<fieldset id="fs_network_groups">
<?php require_once("inc_netgroup.php");?>
</fieldset>
</div>
<div class="cList">
<?=frmElements($mnuwindow);?>
</div>
</div>
