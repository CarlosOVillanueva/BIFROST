<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once ("_lib/php/auth.php");
require_once ("_includes/optionstoolbar.php");
$gdbo = new ArcDb;
$gdbo -> dbConStr=$globalDBCON;
$gdbo -> dbType = $globalDBTP;
$gdbo -> dbSchema = $globalDB;
# Record
$gdbo -> sql = "SELECT
a.id_cfg,
a.cfg,
UNHEX(a.cfg_descr) as \"cfg_descr\",
a.cfg_dc,
a.cfg_dr,
a.id_cfg_grp,
a.id_sys_status,
a.cfg_timezone,
FROM_UNIXTIME(UNIX_TIMESTAMP(a.cfg_du)+$gmtOffset) as cfg_du,
UNHEX(a.cfg_notes) as cfg_notes,
a.id_cust_branch,
b.id_cust_company
FROM _cfg a
LEFT JOIN _cust_branch b on a.id_cust_branch=b.id_cust_branch WHERE id_cfg=".$_POST["id_cfg"];
$gdbo -> getRec();
$rec=$gdbo -> getAssociative();
# Dependent Lists
## Branches
$filter = ($rec[0]['id_cust_company']!=''?" WHERE id_cust_company = ".$rec[0]['id_cust_company']:null);
$gdbo -> sql = "SELECT
id_cust_branch,cust_branch
FROM _cust_branch ".$filter;
$gdbo -> getRec();
$listBranches = $gdbo -> dbData;
# Companies
$gdbo -> sql = "SELECT
id_cust_company,cust_company
FROM _cust_company
ORDER BY cust_company";
$gdbo -> getRec();
$listCompanies = $gdbo -> dbData;
# Config Groups
$gdbo -> sql = "SELECT
id_cfg_grp,cfg_grp
FROM _cfg_grp ORDER BY cfg_grp ASC";
$gdbo -> getRec();
$listConfigGroups= $gdbo -> dbData;
# Status
$gdbo -> sql = "SELECT
id_sys_status,sys_status
FROM _sys_status
WHERE id_sys_status_tp=6";
$gdbo -> getRec();
$listStatus = $gdbo -> dbData;
# Filterable Objects
## Branches
$gdbo = new ArcDb;
$gdbo -> dbConStr=$globalDBCON;
$gdbo -> dbType = $globalDBTP;
$gdbo -> dbSchema = $globalDB;
$gdbo -> sql = "SELECT
id_cust_branch,
cust_branch
FROM _cust_branch";
$gdbo -> dbFilter = " WHERE id_cust_company=";
$gdbo -> type = "list";
$gdbo -> id = "id_cust_branch";
$gdbo -> attributes = 'class="chBranch"';
$branchdboJSON = encrypt(json_encode((array)$gdbo));
# Fieldsets
$configData =
array(
array(
array('Company', 'id_cust_company', 'onChange="filterRec(this,\''.$branchdboJSON .'\',\'dBranch\')"', 1, $listCompanies,$rec[0]['id_cust_company']),
array('Branch', 'id_cust_branch', 'disabled=\"disabled\" class=\"chBranch\" onchange="cpSelToTxt(this,\'cfg\',16)"', 1,$listBranches,$rec[0]['id_cust_branch'],'dBranch'),
array('Last Update', 'cfg_du', 'disabled="disabled"', 0,null,$rec[0]['cfg_du']),
array(null, 'id_cfg',null,3,null,$rec[0]["id_cfg"]),
),
array(
array('Catalog', 'cfg', null, 0,null,$rec[0]['cfg']),
array('Category', 'id_cfg_grp', null, 1, $listConfigGroups,$rec[0]['id_cfg_grp']),
array('Status', 'id_sys_status', null, 1, $listStatus,$rec[0]['id_sys_status'])
),
array(
array('Description', 'cfg_descr', null, 6,null,$rec[0]['cfg_descr'])
),
);
?>
<script type="text/javascript">$(".icoadd").remove();</script>
<form name="frmConfig" id="frmConfig" method="post" action="javascript:submitFrmVals('content','/_mod/smod_18/sql.php','id_cust_company,id_cust_branch,id_cfg_grp,id_sys_status,cfg','&action=edit&form=frmConfig','frmConfig')">
<fieldset id="catalog">
<legend>Catalog</legend>
<?=frmElements($configData);?>
</fieldset>
<fieldset id="devices">
<legend>Devices</legend>
<div id="list18-0">
<?php include("list_cfgdevice.php");?>
</div>
</fieldset>
<fieldset id="interfaces">
<legend>Interfaces</legend>
<div id="list18-1">
<?php include("list_interface_filtered.php");?>
</div>
</fieldset>
</form>
<script type="text/javascript">
oREC = '{"oRec":[{"module":"18","filter":"id_cfg","recid":"<?=$_POST["id_cfg"]?>","formname":"frmConfig"}]}';
<?=$icoDelete;?>
<?=$icoPreview;?>
<?="icoAddDevice='<span onclick=\"arc(\'popWindow\',\'".$path."add_cfgdevice.php\',\'id_cfg=".$_POST["id_cfg"]."\',1,1)\">".$icoAddDevice."Add Device</span>';";?>
<?=$icoCollapse;?>
$('#oOptions').append(icoDelete+icoAddDevice+icoPreview+icoCollapse);
collapseTabs();
CKEDITOR.replace("cfg_descr");
</script>
