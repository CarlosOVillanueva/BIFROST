<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
# POST variables
$ajCfgDevice = null;
$ajCfgDevice = (isset($_POST["id_cfg_device"])?"&id_cfg_device=".$_POST["id_cfg_device"]:"");
$ajCfgDevice.= (isset($_POST["id_device"])?"&id_device=".$_POST["id_device"]:"");
$ajCfgDevice.= (isset($_POST["id_cfg"])?"&id_cfg=".$_POST["id_cfg"]:"");
if(!isset($_POST["id_device"])){
$_POST["id_device"]="";
}
require_once ("_lib/php/auth.php");
$gdbo=new ArcTbl;
$gdbo->dbConStr=$globalDBCON;
$gdbo->dbLimit=25;
$gdbo->dbOffset=0;
$gdbo->dbType = $globalDBTP;
$gdbo->dbSchema=$globalDB;
#$gdbo->recDetail="hex2str(\$record['Notes'])";
$gdbo->recOrder="asc";
$gdbo->tblKey="iflabel";
$gdbo->elements =array(array("Label","device_interfacelabel"));
$gdbo->recOrderCol="id_device_interfacelabel";
$gdbo->recQuery="
SELECT 
id_device_interfacelabel,
device_interfacelabel as Label,
id_device
FROM _device_interfacelabel WHERE id_device=".$_POST["id_device"];
$gdbo -> dbTable = "_device_interfacelabel";
$gdbo -> recIndex = $gdbo -> getPrimaryKey();
$gdbo -> updateAction = '"updateTable(\'rc".$rowIndex."_".$tblKey."\',\'". $this -> dbTable. "\',\'".$this->primaryKey."\',".$rowIndex.",1,\''.$path.'sql.php\',\'fs_labels\',\'&form=frmIFLabel&action=edit'.$ajCfgDevice.'\')"';
$gdbo -> deleteAction = '"updateTable(\'rc".$rowIndex."_".$tblKey."\',\'". $this -> dbTable. "\',\'".$this->primaryKey."\',".$rowIndex.",2,\''.$path.'sql.php\',\'fs_labels\',\'&form=frmIFLabel&action=delete'.$ajCfgDevice.'\')"';
$gdbo->editable=true;
$gdbo->ajDestination="tbliflabel";
$gdbo->ajPage="/_mod/smod_18/list_iflabel.php";
$gdbo->ignoreCols=array("id_device","id_device_interfacelabel");
$gdbo->ignoreFilterCols=array("id_device_interfacelabel","id_device");
$gdbo->build();
echo hex2str($gdbo->tblNav);
echo $gdbo->dataTable;
?>
