<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once ("_lib/php/auth.php");
/**************************************************/
$listnetmask=array();
for($i=15;$i<33;$i++) {
$net=Net_IPv4::parseAddress("0.0.0.0/".$i);
$net->bitmask=$i;
$netmask=$net->netmask;
$listnetmask[]=array($i,$netmask." / ".$i);
}
/**************************************************/
$netgroup=new ArcDb;
$netgroup->dbConStr=$globalDBCON;
$netgroup->dbType = $globalDBTP;
$netgroup->dbSchema=$globalDB;
$netgroup->sql="SELECT id_cfg_device_netgroup,cfg_device_netgroup from _cfg_device_netgroup order by cfg_device_netgroup";
$netgroup->getRec();
$listnetgroup=$netgroup->dbData;
/**************************************************/
$gdbo -> sql="SELECT 
INET_NTOA(a.cfg_device_ip4netaddress) as \"Network\",
a.cfg_device_ip4netaddress_nmbits as \"Netmask\",
a.id_cfg_device_netgroup,
b.cfg_device_netgroup
FROM _cfg_device_ip4netaddress a
INNER JOIN _cfg_device_netgroup b on a.id_cfg_device_netgroup=b.id_cfg_device_netgroup WHERE a.cfg_device_ip4netaddress_enabled=1";
$gdbo->getRec();
$listNetworks=$gdbo -> dbData;
echo "<script type='text/javascript'>window.listNetwork=".json_encode($listNetworks).";</script>";
/**************************************************/
$buttonAdd ='<a id="buttonAddNetwork" class="elementIconBox disableButton" title="Add Network"><i class="fa fa-plus"></i></a>';
$buttonValidate ='<a id="buttonValidateNetwork" class="elementIconBox" title="Validate Network" onclick="validateNetwork(\'cfg_device_ip4netaddress\',\'cfg_device_ip4netaddress_nmbits\',\'id_cfg_device_netgroup\',window.listNetwork,null,\'frmNetwork\');return false;"><i class="fa fa-check"></i></a>';
$mnunetwork = array(
array( 
array('NetGroup','id_cfg_device_netgroup','class="clNetGroups"',1,$listnetgroup), 
array('Netmask','cfg_device_ip4netaddress_nmbits',null,1,$listnetmask),
array('Network','cfg_device_ip4netaddress','class="elementIcon" style="width:140px!important" onblur="validateElement(\'ip\',this)"',0,null,null,null,$buttonValidate.$buttonAdd,null,"IP Address"), 
)
);
/**************************************************/
?>
<style type="text/css">
.disableButton,.disableButton:hover {
background-color:#fafafa !important;
color:#d5d3d1 !important;
border:1px solid #fff !important;
-moz-box-shadow:none !important;
-webkit-box-shadow:none !important;
box-shadow:v !important;
}
</style>
<legend>
Add Network
</legend>
<?= frmElements($mnunetwork); ?>
<h3>Configured Networks</h3>
<div id="tblnetwork" class="fieldsettable">
<?php include("list_network.php");?>
</div>
<script type="text/javascript">
$("#buttonAddNetwork").unbind( "click" );
</script>
