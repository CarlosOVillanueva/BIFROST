<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once ("_lib/php/auth.php");
$gdbo = new ArcDb;
$gdbo -> dbConStr=$globalDBCON;
$gdbo -> dbType = $globalDBTP;
$gdbo -> dbSchema = $globalDB;
# Device Group
$gdbo -> sql = "SELECT 
id_device_grp,
device_grp
FROM _device_grp";
$gdbo -> getRec();
$listDeviceGroup = $gdbo -> dbData;
# Users
$gdbo -> sql ="SELECT
a.id_hr_emp,
concat(b.cust_contact_givenName,' ',b.cust_contact_familyName) as \"fullname\" 
FROM
_hr_emp as a
LEFT JOIN _cust_contact b ON a.id_cust_contact=b.id_cust_contact
ORDER BY b.cust_contact_givenName,b.cust_contact_familyName";
$gdbo -> getRec();
$listUsers = $gdbo -> dbData;
# Status
$gdbo -> sql = "SELECT 
id_sys_status,
sys_status
FROM
_sys_status 
WHERE id_sys_status_tp=7";
$gdbo -> getRec();
$listStatus = $gdbo -> dbData;
# Configuration
$gdbo -> sql = "SELECT id_cfg,cfg FROM _cfg;";
$gdbo -> getRec();
$listConfiguration = $gdbo -> dbData;
# Filterable Objects
## Devices
$gdbo = new ArcDb;
$gdbo -> dbConStr=$globalDBCON;
$gdbo -> dbType = $globalDBTP;
$gdbo -> dbSchema = $globalDB;
$gdbo -> sql = "SELECT 
a.id_device,
a.device,
a.id_cust_company,
b.cust_company FROM _device a
LEFT JOIN _cust_company b ON a.id_cust_company = b.id_cust_company";
$gdbo -> dbFilter = "WHERE id_device_grp=";
$gdbo -> type = "list";
$gdbo -> id = "id_device";
$gdbo -> attributes = 'class="chDevice"';
$devicedboJSON = encrypt(json_encode((array)$gdbo));
# Actions
$deviceAction = array( array( array(null, 'deviceSubmit', 'onclick="prepSaveChanges(\'frmDevice\')"', 2, null, 'Save Changes'), array(null, 'deviceClose', 'onclick="clearPop()"', 2, null, 'Close')));
# Fieldsets
$deviceForm = 
array(
array (
array('Device Label', 'cfg_device', null, 0,null,null),
array('Operating System', 'cfg_device_os', null, 0,null,null),
array('Administrator', 'id_hr_emp',null,1,$listUsers,null),
),
array(
array('Type', 'id_device_grp', 'onchange="filterRec(this,\''.$devicedboJSON.'\',\'dDevice\')"', 1, $listDeviceGroup, null), 
array('Device', 'id_device', 'disabled="disabled" class="chDevice"', 1,null, null,'dDevice'), 
array(null,'id_cfg',null,3,null,$_POST["id_cfg"]),
), 
array(
array('Serial Number', 'cfg_device_serialnumber', null, 0, null, null),
array('Status', 'id_sys_status', null, 1, $listStatus, null),
),
array(
array('Date Created',null, 'disabled=\"disabled\"', 0, null),
array('Date Updated',null, 'disabled=\"disabled\"', 0, null),
),
array(
array('Notes', 'cfg_device_notes', 'onkeydown="detectTab(this,event)"', 6, null, null),
),
);
?>
<div id="popBox">
<form name="frmDevice" id="frmDevice" method="post" action="javascript:submitFrmVals('popWindow','/_mod/smod_18/sql.php','id_sys_status,id_device_grp,id_device','&action=insert&form=frmDevice','frmDevice')">
<fieldset id="add_device">
<legend>
Add Device
</legend>
<?=frmElements($deviceForm); ?>
<?= frmElements($deviceAction); ?>
</fieldset>
</form>
</div>
<script type="text/javascript">
CKEDITOR.replace("cfg_device_notes");
</script>
