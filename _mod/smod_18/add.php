<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once ("_lib/php/auth.php");
require_once ("_includes/optionstoolbar.php");
$gdbo = new ArcDb;
$gdbo -> dbConStr=$globalDBCON;
$gdbo -> dbType = $globalDBTP;
$gdbo -> dbSchema = $globalDB;
# Companies
$gdbo -> sql = "SELECT
id_cust_company,cust_company
FROM _cust_company
ORDER BY cust_company";
$gdbo -> getRec();
$listCompanies = $gdbo -> dbData;
# Config Groups
$gdbo -> sql = "SELECT
id_cfg_grp,cfg_grp
FROM _cfg_grp ORDER BY cfg_grp ASC";
$gdbo -> getRec();
$listConfigGroups= $gdbo -> dbData;
# Status
$gdbo -> sql = "SELECT
id_sys_status,sys_status
FROM _sys_status
WHERE id_sys_status_tp=6";
$gdbo -> getRec();
$listStatus = $gdbo -> dbData;
# Filterable Objects
## Branches
$gdbo = new ArcDb;
$gdbo -> dbConStr=$globalDBCON;
$gdbo -> dbType = $globalDBTP;
$gdbo -> dbSchema = $globalDB;
$gdbo -> sql = "SELECT
id_cust_branch,
cust_branch
FROM _cust_branch";
$gdbo -> dbFilter = " WHERE id_cust_company=";
$gdbo -> type = "list";
$gdbo -> id = "id_cust_branch";
$gdbo -> attributes = 'class="chBranch"';
$branchdboJSON = encrypt(json_encode((array)$gdbo));
# Fieldsets
$configData =
array(
array(
array('Company', 'id_cust_company', 'onChange="filterRec(this,\''.$branchdboJSON .'\',\'dBranch\')"', 1, $listCompanies),
array('Branch', 'id_cust_branch', 'disabled=\"disabled\" class=\"chBranch\" onchange="cpSelToTxt(this,\'cfg\',16)"', 1,null,null,'dBranch')
),
array(
array('Catalog', 'cfg', null, 0),
array('Category', 'id_cfg_grp', null, 1, $listConfigGroups),
array('Status', 'id_sys_status', null, 1, $listStatus)
),
array(
array('Description', 'cfg_descr', null, 6))
);
?>
<script type="text/javascript">$(".icoadd").remove();</script>
<form name="frmConfig" id="frmConfig" method="post" action="javascript:submitFrmVals('content','/_mod/smod_18/sql.php','id_cust_company,id_cust_branch,id_cfg_grp,id_sys_status,cfg','&action=insert&form=frmConfig','frmConfig')">
<fieldset id="catalog">
<legend>
Catalog
</legend>
<?=frmElements($configData);?>
</fieldset>
</form>
<script type="text/javascript">
CKEDITOR.replace("cfg_descr");
</script>
