<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once ("_lib/php/auth.php");
$gdbo = new ArcDb;
$gdbo -> dbConStr=$globalDBCON;
$gdbo -> dbType = $globalDBTP;
$gdbo -> dbSchema = $globalDB;
# Record
$gdbo -> sql = "SELECT
a.id_cfg_device,
a.id_hr_emp,
a.id_device,
a.cfg_device,
a.id_cfg,
a.cfg_device_serialnumber,
UNHEX(a.cfg_device_notes) as \"cfg_device_notes\",
FROM_UNIXTIME(UNIX_TIMESTAMP(a.cfg_device_dc)+$gmtOffset) as \"cfg_device_dc\",
FROM_UNIXTIME(UNIX_TIMESTAMP(a.cfg_device_du)+$gmtOffset) as \"cfg_device_du\",
a.cfg_device_dr,
a.id_sys_status,
a.cfg_device_keyenabled,
a.cfg_device_key,
b.id_device_grp,
a.cfg_device_os
FROM
_cfg_device a
LEFT JOIN
_device b ON a.id_device=b.id_device
WHERE a.id_cfg_device=" . $_POST["id_cfg_device"];
$gdbo -> getRec();
$rec = $gdbo -> getAssociative();
# Device Group
$gdbo -> sql = "SELECT 
id_device_grp,
device_grp
FROM _device_grp";
$gdbo -> getRec();
$listDeviceGroup = $gdbo -> dbData;
# Users
$gdbo -> sql ="SELECT
a.id_hr_emp,
concat(b.cust_contact_givenName,' ',b.cust_contact_familyName) as \"fullname\" 
FROM
_hr_emp as a
LEFT JOIN _cust_contact b ON a.id_cust_contact=b.id_cust_contact
ORDER BY b.cust_contact_givenName,b.cust_contact_familyName";
$gdbo -> getRec();
$listUsers = $gdbo -> dbData;
# Status
$gdbo -> sql = "SELECT 
id_sys_status,
sys_status
FROM _sys_status 
WHERE id_sys_status_tp=7";
$gdbo -> getRec();
$listStatus = $gdbo -> dbData;
# Configuration
$gdbo -> sql = "SELECT 
id_cfg,
cfg FROM _cfg;";
$gdbo -> getRec();
$listConfiguration = $gdbo -> dbData;
# Dependent Lists
## Devices
$filterDeviceGroup = ($rec[0]["id_device_grp"]!=""?" WHERE a.id_device_grp=".$rec[0]["id_device_grp"]:"");
$gdbo -> sql = "SELECT
a.id_device,
a.device,
a.id_cust_company,
b.cust_company
FROM _device a
LEFT JOIN _cust_company b ON a.id_cust_company = b.id_cust_company".$filterDeviceGroup;
$gdbo -> getRec();
$listDevice = $gdbo -> dbData;
# Filterable Objects
## Devices
$gdbo = new ArcDb;
$gdbo -> dbConStr=$globalDBCON;
$gdbo -> dbType = $globalDBTP;
$gdbo -> dbSchema = $globalDB;
$gdbo -> sql = "SELECT 
a.id_device,
a.device,
a.id_cust_company,
b.cust_company FROM _device a
LEFT JOIN _cust_company b ON a.id_cust_company = b.id_cust_company";
$gdbo -> dbFilter = "WHERE id_device_grp=";
$gdbo -> type = "list";
$gdbo -> id = "id_device";
$gdbo -> attributes = 'class="chDevice"';
$devicedboJSON = encrypt(json_encode((array)$gdbo));
# Actions
$deviceAction = array( array( array(null, 'deviceSubmit', 'onclick="prepSaveChanges(\'frmDevice\')"', 2, null, 'Save Changes'), array(null, 'deviceClose', 'onclick="clearPop()"', 2, null, 'Close')));
# Fieldsets
$deviceForm = 
array(
array(
array('Parent Configuration', 'id_cfg', null, 1, $listConfiguration, $rec[0]["id_cfg"]),
array('Key Enabled', 'cfg_device_keyenabled', null, 8, null, $rec[0]["cfg_device_keyenabled"]),
array(null, 'id_cfg_device',null,3,null,$_POST["id_cfg_device"]),
),
array (
array('Device Label', 'cfg_device', null, 0,null,$rec[0]["cfg_device"]),
array('Operating System', 'cfg_device_os', null, 0,null,$rec[0]["cfg_device_os"]),
array('Administrator', 'id_hr_emp',null,1,$listUsers,$rec[0]["id_hr_emp"]),
),
array(
array('Type', 'id_device_grp', 'onchange="filterRec(this,\''.$devicedboJSON.'\',\'dDevice\')"', 1, $listDeviceGroup, $rec[0]["id_device_grp"]), 
array('Device', 'id_device', 'class="chDevice"', 1, $listDevice, $rec[0]["id_device"],'dDevice'), 
), 
array(
array('Serial Number', 'cfg_device_serialnumber', null, 0, null, $rec[0]["cfg_device_serialnumber"]),
array('Status', 'id_sys_status', null, 1, $listStatus, $rec[0]["id_sys_status"]),
),
array(
array('Date Created',null, 'disabled=\"disabled\"', 0, null,$rec[0]["cfg_device_dc"]),
array('Date Updated',null, 'disabled=\"disabled\"', 0, null,$rec[0]["cfg_device_du"]),
),
array(
array('Notes', 'cfg_device_notes', 'onkeydown="detectTab(this,event)"', 6, null, $rec[0]["cfg_device_notes"]),
),
array(
array('Management Key', 'cfg_device_key', null, 6, null, $rec[0]["cfg_device_key"]),
)
);
?>
<div id="popBox">
<form name="frmDevice" id="frmDevice" method="post" action="javascript:submitFrmVals('popWindow','/_mod/smod_18/sql.php','id_sys_status,id_device_grp,id_device','&action=edit&form=frmDevice','frmDevice')">
<fieldset id="Edit Device">
<legend>
Edit Device
</legend>
<?= frmElements($deviceAction); ?>
<?=frmElements($deviceForm); ?>
<h3>Configured Interfaces</h3>
<div id="tblinterface" class="fieldsettable">
<?php include("list_interface.php");?></div>
</fieldset>
</form>
</div>
<script type="text/javascript">
CKEDITOR.replace("cfg_device_notes");
</script>
