<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once ("_lib/php/auth.php");
if(!isset($_POST["id_cfg"])){
$_POST["id_cfg"]="";
}
$device=new ArcTbl;
$device->dbConStr=$globalDBCON;
$device->dbLimit=10;
$device->dbOffset=0;
$device->dbType = $globalDBTP;
$device->dbSchema=$globalDB;
$device->recIndex="id_cfg_device";
$device->recOrder="asc";
$device -> actionDestination = "popWindow";
$device->recOrderCol="Label";
$device -> recLink= $path."edit_cfgdevice.php";
$device -> actionFilterKey="id_cfg_device";
$device->recQuery="
SELECT
a.cfg_device as \"Device Label\",
a.id_cfg_device,
c.device_grp as \"Type\",
b.device as \"Device\",
a.cfg_device_os as \"OS\",
concat(f.cust_contact_familyName,',',f.cust_contact_givenName) as \"Admin\",
a.cfg_device_serialnumber as \"Serial Number\",
UNHEX(a.cfg_device_notes) as \"Notes\",
FROM_UNIXTIME(UNIX_TIMESTAMP(a.cfg_device_dc)+".$gmtOffset.",'%Y-%m-%d') as \"Date Entered\",
FROM_UNIXTIME(UNIX_TIMESTAMP(a.cfg_device_du)+".$gmtOffset.") as \"Last Update\",
d.sys_status as \"Status\",
CASE
WHEN a.cfg_device_keyenabled = 1
THEN 'Yes'
ELSE 'No'
END as \"Manageable\",
a.id_cfg,
concat('
<input type=\"button\" value=\"Interfaces\" onclick=\"arc(''popWindow'',''/_mod/smod_18/manage_interface.php'',''id_cfg=',cast(id_cfg as char),'&id_cfg_device=',cast(a.id_cfg_device as char),'&id_device=',cast(a.id_device as char),''',1,1)\"/>
<input type=\"button\" value=\"Delete\" onclick=\"delCfgDevice(',cast(id_cfg as char),',',cast(a.id_cfg_device as char),')\"/>
') as \"\" FROM _cfg_device a
LEFT JOIN
_device b ON a.id_device=b.id_device
LEFT JOIN
_device_grp c ON b.id_device_grp=c.id_device_grp
LEFT JOIN
_sys_status d ON a.id_sys_status=d.id_sys_status
LEFT JOIN
_hr_emp e ON a.id_hr_emp = e.id_hr_emp
LEFT JOIN
_cust_contact f ON e.id_cust_contact = f.id_cust_contact
WHERE a.id_cfg=".$_POST["id_cfg"]." ORDER by a.cfg_device";
$device->ajDestination="list18-0";
$device->ajPage="/_mod/smod_18/list_cfgdevice.php";
$device->recOrder="asc";
$device->recOrderCol="Device";
$device->ignoreCols=array("id_cfg","Notes","Date Entered","id_cfg_device");
$device->ignoreFilterCols=array("id_cfg","id_cfg_device","");
$device->build();
echo hex2str($device->tblNav);
echo $device->dataTable;
?>
