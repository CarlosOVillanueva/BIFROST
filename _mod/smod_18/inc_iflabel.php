<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once ("_lib/php/auth.php");
$ajCfgDevice = null;
$ajCfgDevice = (isset($_POST["id_cfg_device"])?"&id_cfg_device=".$_POST["id_cfg_device"]:"");
$ajCfgDevice.= (isset($_POST["id_device"])?"&id_device=".$_POST["id_device"]:"");
$ajCfgDevice.= (isset($_POST["id_cfg"])?"&id_cfg=".$_POST["id_cfg"]:"");
/**************************************************/
$iflabel=new ArcDb;
$iflabel->dbConStr=$globalDBCON;
$iflabel->dbType = $globalDBTP;
$iflabel->dbSchema=$globalDB;
$iflabel->sql="SELECT id_cfg_device_netgroup,cfg_device_netgroup from _cfg_device_netgroup order by cfg_device_netgroup";
$iflabel->getRec();
$listiflabel=$iflabel->dbData;
/**************************************************/
$buttonAdd ='<div class="elementIconBox" onclick="prepSaveChanges(\'frmIFLabel\')"><i class="fa fa-plus"></i></div>';
/**************************************************/
$mnuiflabel = array(
array( 
array("Interface Label", "device_interfacelabel", 'class="elementIcon"', 0,null,null,null,$buttonAdd,null,'Network Interface'), 
)
);
/**************************************************/
?>
<legend>
Add Inteface Label
</legend>
<form name="frmIFLabel" id="frmIFLabel" method="post" action="javascript:submitFrmVals('fs_labels','/_mod/smod_18/sql.php','device_interfacelabel','<?=$ajCfgDevice?>&action=insert&form=frmIFLabel','frmIFLabel')">
<?=frmElements($mnuiflabel); ?>
</form>
<h3>Configured Interface Labels</h3>
<div id="tbliflabel" class="fieldsettable">
<?php include("list_iflabel.php"); ?>
</div>
