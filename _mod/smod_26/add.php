<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once("_lib/php/auth.php");
require_once("_includes/optionstoolbar.php");
# Company 
$gdbo -> sql = "SELECT id_cust_company,cust_company FROM _cust_company where id_cust_company_tp=7";
$gdbo -> getRec();
$listCompany = $gdbo -> dbData;
# Device Groups
$gdbo -> sql = "SELECT id_device_grp,device_grp FROM _device_grp";
$gdbo -> getRec();
$listDeviceGroup = $gdbo -> dbData;
# Filterable Objects
## Internal Parts
$partsDBO = new ArcDb;
$partsDBO -> dbConStr=$globalDBCON;
$partsDBO -> dbType = $globalDBTP;
$partsDBO -> dbSchema = $globalDB;
$partsDBO -> sql = "SELECT id_part_model,part_model_internalpartnumber FROM _part_model";
$partsDBO -> dbFilter = "WHERE part_model_partnumber=";
$partsDBO -> type = "text";
$partsDBO -> id = "id_part_model_";
$partsDBO -> attributes = 'class="chPart"';
$partsDBOJSON =encrypt(json_encode((array)$partsDBO));
# Fieldsets
$fs=
array(
array(
array('Manufacturer', 'id_cust_company',null,1,$listCompany),
array('Device Group', 'id_device_grp',null,1,$listDeviceGroup),
),
array(
array('Model', 'device',null,0),
array('Label', 'device_altname',null,0),
),
array(
array('Manufacturer Part Number', 'device_mfrpartnumber','onkeyup="filterRec(this,\''.$partsDBOJSON.'\',\'dInternalPart\')"',0),
array('Internal Part Number', 'id_part_model_','disabled="disabled" class="chPart"',0,null,null,'dInternalPart'),
),
array(
array('Description', 'device_descr',null,6),
)
);
?>
<script type="text/javascript">$(".icoadd").remove();</script>
<form method="post" name="frmDevice" id="frmDevice" action="javascript:submitFrmVals('content','/_mod/smod_26/sql.php','device,id_cust_company','&form=frmDevice&action=insert','frmDevice')">
<fieldset id="Device">
<legend>Device</legend>
<?=frmElements($fs);?>
</fieldset>
</form>
<script type="text/javascript">
CKEDITOR.replace("device_descr");
</script>
