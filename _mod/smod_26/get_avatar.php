<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once("_lib/php/auth.php");
$gdbo -> sql = "SELECT CONCAT(fso) as fso FROM _fso WHERE fso_originalname='avatar' 
AND fso_pk=".$_POST["id_device"]." AND fso_pkcol='id_device'";
$gdbo -> getRec();
$fsoRec = $gdbo -> dbData;
$base64=shell_exec("base64 ".$avatarWritePath.$fsoRec[0][0]);
$avatar="data:image/png;base64,".$base64;
?>
<img class="avatarMedium" src="<?=$avatar;?>" height="200" width="200"/>
