<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once("_lib/php/auth.php");
require_once("_includes/optionstoolbar.php");
# Model Details
$gdbo -> sql = "SELECT 
a.id_device,
a.device,
a.id_device_grp,
a.device_enabled,
a.device_altname,
UNHEX(a.device_descr) as device_descr,
a.device_mfrpartnumber,
a.id_cust_company,
a.id_part_model,
b.part_model_internalpartnumber
FROM _device a
LEFT JOIN _part_model b ON a.id_part_model = b.id_part_model
WHERE a.id_device=".$_POST["id_device"];
$gdbo -> getRec();
$rec = $gdbo -> getAssociative();
# Company 
$gdbo -> sql = "SELECT id_cust_company,cust_company FROM _cust_company where id_cust_company_tp=7";
$gdbo -> getRec();
$listCompany = $gdbo -> dbData;
# Device Groups
$gdbo -> sql = "SELECT id_device_grp,device_grp FROM _device_grp";
$gdbo -> getRec();
$listDeviceGroup = $gdbo -> dbData;
# Filterable Objects
## Internal Parts
$partsDBO = new ArcDb;
$partsDBO -> dbConStr=$globalDBCON;
$partsDBO -> dbType = $globalDBTP;
$partsDBO -> dbSchema = $globalDB;
$partsDBO -> sql = "SELECT id_part_model,part_model_internalpartnumber FROM _part_model";
$partsDBO -> dbFilter = "WHERE part_model_partnumber=";
$partsDBO -> type = "text";
$partsDBO -> id = "id_part_model_";
$partsDBO -> attributes = 'class="chPart"';
$partsDBOJSON =encrypt(json_encode((array)$partsDBO));
# Fieldsets
$fs=
array(
array(
array('Manufacturer', 'id_cust_company',null,1,$listCompany,$rec[0]['id_cust_company']),
array('Device Group', 'id_device_grp',null,1,$listDeviceGroup,$rec[0]['id_device_grp']),
array(null, 'id_device',null,3,null,$rec[0]['id_device']),
),
array(
array('Model', 'device',null,0,null,$rec[0]['device']),
array('Label', 'device_altname',null,0,null,$rec[0]['device_altname']),
),
array(
array('Manufacturer Part Number', 'device_mfrpartnumber','onkeyup="filterRec(this,\''.$partsDBOJSON.'\',\'dInternalPart\')"',0,null,$rec[0]['device_mfrpartnumber']),
array('Internal Part Number', 'id_part_model_','disabled="disabled" class="chPart"',0,null,$rec[0]['part_model_internalpartnumber'],'dInternalPart',null,null,null,'<input type="hidden" name="id_part_model" id="id_part_model" value="'.$rec[0]['id_part_model'].'">'),
),
);
$fsDescription = array(
array(
array('Description', 'device_descr',null,6,null,$rec[0]['device_descr']),
)
);
?>
<form method="post" name="frmDevice" id="frmDevice" action="javascript:submitFrmVals('content','/_mod/smod_26/sql.php','device,id_cust_company','&form=frmDevice&action=update','frmDevice')">
<fieldset id="Device">
<legend>Device</legend>
<div class="frmrow">
<div style="float:left">
<?=frmElements($fs);?>
</div>
<?php
$gdbo -> sql = "SELECT fso FROM _fso WHERE fso_pk=".$_POST["id_device"]." AND fso_originalname='avatar' AND fso_pkcol='id_device'";
$gdbo -> getRec();
$fsoRec = $gdbo -> dbData;
if (isset($fsoRec[0][0]) && $fsoRec[0][0]) {
$base64=shell_exec("base64 ".$avatarWritePath.$fsoRec[0][0]);
$avatar="data:image/png;base64,".$base64;
} else {
$avatar="/_img/device.png";
}
?>
<div style="float:right;text-align:center;"><?php include("add_avatar.php");?></div>
</div>
<?=frmElements($fsDescription);?>
</fieldset>
</form>
<form action="#">
<fieldset id="Files">
<legend>Files</legend>
<div id="listFiles">
<?php
require("listFiles.php");
?>
</div></fieldset></form>
<script type="text/javascript">
oREC = '{"oRec":[{"module":"26","filter":"id_device","recid":"<?=$_POST["id_device"]?>","formname":"frmDevice"}]}';
<?=$icoDelete;?>
<?=$icoUpload;?>
<?=$icoCollapse;?>
$("#oOptions").append(icoDelete+icoCollapse+icoUpload);
collapseTabs();
</script>
<script type="text/javascript">
CKEDITOR.replace("device_descr");
</script>
