<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
# Includes
require_once("_lib/php/auth.php");
# Parse Form Action
$action = $_POST['action'];
$form = $_POST['form'];
$formAction = $form . "," . $action;
# Begin SQL Functions
switch ($formAction) {
case "frmDevice,insert":
/*****************************************************************************/
# Insert Part Model
$gdbo -> dbTable = "_device";
$gdbo -> insertRec();
$id_device = $gdbo -> insertedID;
$_POST['id_device']=$id_device;
include('edit.php');
/*****************************************************************************/
break;
case "frmDevice,update":
/*****************************************************************************/
# Update Part Model
$gdbo -> dbTable = "_device";
$gdbo -> updateRec("id_device=".$_POST['id_device']);
include('edit.php');
/*****************************************************************************/
break;
case "frmDevice,delete":
/*****************************************************************************/
$gdbo -> dbTable = "_device";
$gdbo -> deleteRec("id_device=".$_POST["id_device"]);
/*****************************************************************************/ 
include("index.php");
break; 
default:
break;
}
include("_error/status.php");
?>
