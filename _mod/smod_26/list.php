<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once("_lib/php/auth.php");
$arctbl = new ArcTbl;
$arctbl -> dbConStr=$globalDBCON;
$arctbl -> dbOffset = 0;$arctbl -> dbLimit = 23;
$arctbl -> dbType = $globalDBTP;
$arctbl -> dbSchema = $globalDB;
$arctbl -> recLink= $path."edit.php";
$arctbl -> actionFilterKey="id_device";
$arctbl -> recIndex="id_device";
$arctbl -> ignoreCols=array("id_device","Description","id_part_model");
$arctbl -> recQuery = "
SELECT
a.id_device,
b.cust_company as \"Manufacturer\",
a.device as \"Device\",
a.device_altname as \"Alt Label\",
UNHEX(a.device_descr) as \"Description\",
a.device_mfrpartnumber as \"MFR Part#\",
a.id_part_model
FROM _device a
LEFT JOIN
_cust_company b ON a.id_cust_company = b.id_cust_company";
$arctbl -> actionDestination = "content";
$arctbl -> ajDestination = "list26";
$arctbl -> ajPage = $path."list.php";
$arctbl -> build();
echo hex2str($arctbl -> tblNav);
echo $arctbl->dataTable;
?>
