<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once("_lib/php/auth.php");
$curYear = date('Y',strtotime($dtTimeCurrent));
$curMonth = date('n',strtotime($dtTimeCurrent));
$dayCount = cal_days_in_month(CAL_GREGORIAN, $curMonth, $curYear);
$start = strtotime("$curYear-$curMonth-1 00:00:00 ");
$end = strtotime("$curYear-$curMonth-$dayCount 23:59:59");
$arc = new ArcDb;
$arc -> dbConStr=$globalDBCON;
$arc -> dbOffset = 0;
$arc -> dbType = $globalDBTP;
$arc -> dbSchema = $globalDB;
if (isset($_POST["start_date"])&&isset($_POST["end_date"])){
$start=strtotime($_POST["start_date"]);
$end=strtotime($_POST["end_date"]);}
$daterange="$start and $end";
$daterangeFriendly=date('F j, Y',$start). " through " . date('F j, Y',$end);
$filter=($start!=""?"a.tm_ts between $daterange and":"");
$arc -> sql = "
SELECT * FROM(
SELECT
FROM_UNIXTIME(a.tm_ts+$gmtOffset,'%m-%d-%y') name,
sum(cast((a.tm_te-a.tm_ts-(a.tm_deduction*3600))/3600 as DECIMAL(12,2))) as data
FROM _tm a WHERE $filter id_hr_emp=$id_hr_emp_derived group by name order by name asc) as derived WHERE data >=1";
$arc->getRec();
$arcData=$arc->getAssociative();
$arcJSON=json_encode($arcData);
?>
<script type="text/javascript">
var chartData=<?=$arcJSON?>;
var chart = AmCharts.makeChart("chartdiv", {
"theme": "none",
"type": "serial",
"dataProvider": chartData,
"chartCursor": {},
"chartScrollbar": {},
"pathToImages": "/_thirdparty/amcharts/images/",
"valueAxes": [{
"title": "Time Spent, Hours"
}],
"graphs": [{
"balloonText": "Time applied:[[value]]",
"fillAlphas": 1,
"lineAlpha": 0.2,
"title": "Days",
"type": "column",
"valueField": "data"
}],
"rotate": true,
"categoryField": "name",
"categoryAxis": {
"gridPosition": "start",
"fillAlpha": 0.05,
"position": "left",
"title": "Date",
}
});
chart.validateNow();
</script>
<?php if ($arc->dbRows > 1) { ?>
<div id="chartdiv" style="width: 100%; height: 435px;font-size:11pt"></div>
<?php } else {?>
No data available.
<div id="chartdiv" style="display:none"></div>
<?php }?>
