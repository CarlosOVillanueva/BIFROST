<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once("_lib/php/auth.php");
require_once("_includes/heading.php");
$gdbo -> sql = "SELECT TABLE_NAME as a1, TABLE_NAME as a2 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA='".$globalDB."'" ;
$gdbo -> getRec();
$sdList = $gdbo -> dbData;
foreach ($sdList as $key => $col) {
$sch[$key] = $col[0];
$schLabel[$key] = $col[1];
$buttonAdd ='<div class="elementIconBox" onclick="arc(\'add\',\''.$path.'sqlResults.php\',\'table=\'+document.getElementById(\'schList\').options[document.getElementById(\'schList\').selectedIndex].value+\'&edit=1\',1,1)"><i class="fa fa-plus"></i></div>';
}
array_multisort($schLabel, SORT_ASC,$sdList);
?>
<script type="text/javascript">$(".icoadd").remove();</script>
<fieldset>
<legend>Manage System Tables</legend>
<div class="frmrow">
<div class="frmcol-x">
<label>Tables</label>
<?php echo selList($sdList,"schList","arc('list12','".$path."sqlResults.php','table='+document.getElementById('schList').options[document.getElementById('schList').selectedIndex].value+'&edit=0',1,1);clearPop('add')","style=\"width:300px !important\"");?>
<?=$buttonAdd?>
</div>
</div>
<div id="add"></div>
<div id="list12Nav"></div>
<div id="list12"></div>
<div id="status"></div>
</fieldset>
<script type="text/javascript">
$('#list12').css('width',($('fieldset').innerWidth()-20)+"px");
$('#list12').css('overflow','auto');
</script>
