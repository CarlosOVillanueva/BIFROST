<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once("_lib/php/auth.php");
$gdbo -> dbTable = $_POST["table"];
switch ($_POST["edit"]) {
case 0:
$gdbo -> insertRec();
include("list.php");
break;
/* ********************************************************************** */
case 1:
/* ********************************************************************** */
$gdbo -> updateRec($_POST["col"]."='".$_POST["key"]."'");
include("_error/status.php");
break;
/* ********************************************************************** */
case 2:
$gdbo -> deleteRec($_POST["col"]."='".$_POST["key"]."'");
include("list.php");
break;
/* ********************************************************************** */
}
include("_error/status.php");
?>
<script type="text/javascript">
(function(){
clearPop("add");
})();
</script>
