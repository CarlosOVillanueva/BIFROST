<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once("_lib/php/auth.php");
filterQryPost("table");
filterQryPost("edit");
if($edit != "") {
$frm = "";
switch ($edit) {
case 0:
$frm. "<div id=\"list12\">";
include("list.php");
$frm. "</div>";
break;
case 1:
$gdbo -> sql = "SELECT COLUMN_NAME,DATA_TYPE FROM COLUMNS WHERE TABLE_SCHEMA='$globalDB' AND TABLE_NAME='".$table."'";
$gdbo -> dbSchema ="INFORMATION_SCHEMA";
$gdbo -> getRec();
$aschema = $gdbo -> dbData;
$frm.= '<form method="post" name="frmAdd" action="javascript:submitFrmVals(\'list12\',\''.$path.'sqlInsert.php\',null,\'&table='.$table.'&edit=0\',\'frmAdd\')">';
$frm.= '<table style="clear:both" class="dataGrid"><tr>';
for ($i = 0; $i < count($aschema); $i++) { if ($i != 0) { $frm.= '<th>'.$aschema[$i][0].'</th>';}}
$frm.= '<th>&nbsp</th></tr><tr>';
for ($i = 0; $i < count($aschema); $i++) { if ($i != 0) {
if($aschema[$i][1] == "text" || $aschema[$i][1] == "blob") {
$frm.= '<td><textarea name="'.$aschema[$i][0].'" id="'.$aschema[$i][0].'"/>&nbsp;</textarea>'; 
} else {
$frm.= '<td><input type="text" name="'.$aschema[$i][0].'" id="'.$aschema[$i][0].'"/></td>';
}
}}
$frm.= '<td><input type="submit" value="Add"/><input type="button" onclick="clearPop(\'add\')" value="Cancel"/></td>';
$frm.= '</tr></table></form>';
break;
default:
break;
}} else { $frm = "<div>Please select a table from the list.</div>"; }
echo $frm;
?>
