<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once("_lib/php/auth.php");
$arctbl = new ArcTbl;
$arctbl -> dbType = $globalDBTP;
$arctbl -> dbConStr=$globalDBCON;
$arctbl -> dbSchema = $globalDB;
$arctbl -> control = '/_mod/smod_12/sqlInsert.php';
$arctbl -> editable = true;
$arctbl -> dbOffset = 0;
$arctbl -> dbLimit = 23;
if (isset($_POST["table"])){
$table = $_POST["table"];
$arctbl -> dbTable = $table;
$arctbl -> recQuery = "SELECT * FROM ".$table ;
$arctbl -> recIndex = $arctbl -> getPrimaryKey();
} 
$arctbl -> ajDestination = "list12";
$arctbl -> actionDestination = "status";
$arctbl -> ajPage = $path."list.php";
$arctbl -> build();
echo $arctbl->dataTable;
?>
<script type="text/javascript">
$('#list12Nav').html(hex2a('<?=($arctbl -> tblNav);?>'));
</script>
