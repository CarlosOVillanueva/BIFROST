<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once("_lib/php/auth.php");
$arctbl = new ArcTbl;
$arctbl -> dbConStr=$globalDBCON;
$arctbl -> dbOffset = 0;
$arctbl -> dbLimit = 23;
$arctbl -> dbType = $globalDBTP;
$arctbl -> dbSchema = $globalDB;
$arctbl -> recLink= $path."edit.php";
$arctbl -> actionFilterKey="id_sys_user";
$arctbl -> recIndex="id_sys_user";
$arctbl -> recFilter=" WHERE id_sys_status !=22";
$arctbl -> ignoreCols=array("id_sys_user","id_sys_status");
$arctbl -> ignoreFilterCols=array("id_sys_user","id_sys_status");
$arctbl -> recQuery = "
SELECT
a.id_sys_user,
a.sys_user as 'User Name',
id_sys_status,
concat(e.cust_contact_familyName,', ',e.cust_contact_givenName) as Employee
FROM _sys_user a
LEFT JOIN _sys_user_emp c
ON a.id_sys_user=c.id_sys_user
LEFT JOIN _cust_contact e
ON c.id_cust_contact=e.id_cust_contact";
$arctbl -> actionDestination = "content";
$arctbl -> ajDestination = "list14";
$arctbl -> ajPage = $path."list.php";
$arctbl -> build();
echo hex2str($arctbl -> tblNav);
echo $arctbl->dataTable;
?>
