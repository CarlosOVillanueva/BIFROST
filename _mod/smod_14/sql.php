<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
# Includes
require_once ("_lib/php/auth.php");
# Parse Form Action
$action = $_POST['action'];
$form = $_POST['form'];
$formAction = $form . "," . $action;
if ($_POST["id_sys_status"] == 22) {
$_POST["sys_user_dr"] = $dtTimeUTC;
}
$_POST["sys_user_du"] = $dtTimeUTC;
# Begin SQL Functions
switch ($formAction) {
case "frmUser,insert" :
/*****************************************************************************/
$_POST["sys_user_pw"] = encrypt($_POST["sys_user_pw"]);
$gdbo -> dbTable = "_sys_user";
$gdbo -> insertRec();
$_POST["id_sys_user"] = $gdbo -> insertedID;
# Employee Map
$gdbo -> dbTable = "_sys_user_emp";
$gdbo -> insertRec();
# Permissions
## Gather permissions
$permissions = array();
$permissionssql = "insert into _sys_permission (id_sys_module_sub,id_sys_user,sys_permission) values ";
foreach ($_POST as $key => $value) {
if (substr($key, 0, 4) == "78af") {
$decrypted = decrypt($key);
$keypair = explode("=", $decrypted);
$sysmodulesubkey = $keypair[1];
$permission = decrypt($value);
$permissions[] = array($sysmodulesubkey, $_POST["id_sys_user"], $permission);
}
}
for ($i = 0; $i < count($permissions); $i++) {
$comma = ($i != 0 ? "," : "");
$permissionssql .= $comma . '(' . $permissions[$i][0] . ',' . $permissions[$i][1] . ',' . $permissions[$i][2] . ')';
}
## Insert Permissions
$gdbo -> dbTable = "_sys_permission";
$gdbo -> deleteRec("id_sys_user=" . $_POST["id_sys_user"]);
$gdbo -> sql = $permissionssql;
$gdbo -> execQuery();
include ('edit.php');
/*****************************************************************************/
break;
case "frmUser,update" :
/*****************************************************************************/
$gdbo -> dbTable = "_sys_user";
$gdbo -> updateRec("id_sys_user=" . $_POST["id_sys_user"]);
# Employee Map
$gdbo -> dbTable = "_sys_user_emp";
$gdbo -> updateRec("id_sys_user=" . $_POST["id_sys_user"]);
# Permissions
## Gather permissions
$permissions = array();
$permissionssql = "insert into _sys_permission (id_sys_module_sub,id_sys_user,sys_permission) values ";
foreach ($_POST as $key => $value) {
if (substr($key, 0, 4) == "78af") {
$decrypted = decrypt($key);
$keypair = explode("=", $decrypted);
$sysmodulesubkey = $keypair[1];
$permission = decrypt($value);
$permissions[] = array($sysmodulesubkey, $_POST["id_sys_user"], $permission);
}
}
for ($i = 0; $i < count($permissions); $i++) {
$comma = ($i != 0 ? "," : "");
$permissionssql .= $comma . '(' . $permissions[$i][0] . ',' . $permissions[$i][1] . ',' . $permissions[$i][2] . ')';
}
## Insert Permissions
$gdbo -> dbTable = "_sys_permission";
$gdbo -> deleteRec("id_sys_user=" . $_POST["id_sys_user"]);
$gdbo -> sql = $permissionssql;
$gdbo -> execQuery();
include ('edit.php');
/*****************************************************************************/
break;
case "frmUser,delete" :
/*****************************************************************************/
$gdbo -> dbTable = "_sys_user";
$gdbo -> deleteRec("id_sys_user=" . $_POST["id_sys_user"]);
/*****************************************************************************/
# Purge all records that may be linked to this company
# IMPORTANT! All records associated with the branch will be purged
$gdbo -> sql = "SELECT TABLE_NAME FROM information_schema.columns WHERE COLUMN_NAME='id_sys_user'";
$gdbo -> getRec();
$delTables = $gdbo -> dbData;
foreach ($delTables as $row => $column) {
foreach ($column as $data => $value) {
$gdbo -> dbTable = $value;
$gdbo -> deleteRec("id_sys_user=" . $_POST["id_sys_user"]);
}
}
include ("index.php");
break;
default :
break;
}
include ("_error/status.php");
?>
