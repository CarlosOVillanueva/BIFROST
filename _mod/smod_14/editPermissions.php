<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once("_lib/php/auth.php");
require_once("_includes/optionstoolbar.php");
# system modules
$gdbo -> sql = "SELECT
a.id_sys_module,
a.sys_module,
b.id_sys_module_sub,
b.sys_module_sub
FROM
_sys_module a
LEFT JOIN
_sys_module_sub b ON a.id_sys_module = b.id_sys_module
WHERE b.sys_module_sub_enabled = 1
ORDER BY a.sys_module,b.sys_module_sub";
$gdbo -> getRec();
$recSubModules = $gdbo -> dbData;
# modules assigned to user
$gdbo -> sql = "SELECT 
sys_permission,
id_sys_module_sub,
id_sys_user,
id_sys_group
FROM _sys_permission
WHERE id_sys_user=".$_POST["id_sys_user"];
$gdbo -> getRec();
$recPermissions = $gdbo -> getAssociative();
$recRows = $gdbo -> dbRows;
$ptable = '
<table style="width:100%" id="permissionTable">
<caption></caption>
<tr>
<th>Module</th><th>Blocked</th><th>View</th><th>Alter</th><th>Delete</th>';
for ($i=0;$i<count($recSubModules);$i++){
$end = (int)count($recSubModules)-1;
$next = $i+1;
$prev = $i-1;
$boo = 0;
if($recRows > 0){
foreach($recPermissions as $record => $data){
if ($data["id_sys_module_sub"] == $recSubModules[$i][2]) {
$boo = $data["sys_permission"];
}
}}
if ($i==0 || $recSubModules[$i][0] != $recSubModules[$prev][0]) {
$ptable.='<tr>
<td class="clPermissionModule">'.$recSubModules[$i][1].'</td>
<td class="clPermissionModule" style="text-align:center"><i onclick="toggleCheck(this,\'0'.$recSubModules[$i][0].'\')" title="  Toggle Radio Buttons" class="fa fa-check-square"></i></td>
<td class="clPermissionModule" style="text-align:center"><i onclick="toggleCheck(this,\'1'.$recSubModules[$i][0].'\')" title="  Toggle Radio Buttons" class="fa fa-check-square"></i></td>
<td class="clPermissionModule" style="text-align:center"><i onclick="toggleCheck(this,\'2'.$recSubModules[$i][0].'\')" title="  Toggle Radio Buttons" class="fa fa-check-square"></i></td>
<td class="clPermissionModule" style="text-align:center"><i onclick="toggleCheck(this,\'3'.$recSubModules[$i][0].'\')" title="  Toggle Radio Buttons" class="fa fa-check-square"></i></td>
</tr>';
$seed = 0;
}
$altColor = ($seed % 2 == 0 ? "clEven" : "clOdd");
$ptable.='<tr class="dataRow '.$altColor.'"><td>'.$recSubModules[$i][3].'</td>
<td class="check"><input class="0'.$recSubModules[$i][0].'" type="radio" '.isBoo($boo,0).' name="'.encrypt("id_sys_sub_module=".$recSubModules[$i][2]).'" value="'.encrypt(0).'"/></td>
<td class="check"><input class="1'.$recSubModules[$i][0].'" type="radio" '.isBoo($boo,1).' name="'.encrypt("id_sys_sub_module=".$recSubModules[$i][2]).'" value="'.encrypt(1).'"/></td>
<td class="check"><input class="2'.$recSubModules[$i][0].'" type="radio" '.isBoo($boo,2).' name="'.encrypt("id_sys_sub_module=".$recSubModules[$i][2]).'" value="'.encrypt(2).'"/></td>
<td class="check"><input class="3'.$recSubModules[$i][0].'" type="radio" '.isBoo($boo,3).' name="'.encrypt("id_sys_sub_module=".$recSubModules[$i][2]).'" value="'.encrypt(3).'"/></td></tr>';
$seed = $seed +1;
}
$ptable.="</table>";
echo $ptable;
?>
