<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once("_lib/php/auth.php");
require_once("_includes/optionstoolbar.php");
# Company
$gdbo -> sql = "SELECT id_cust_company,cust_company FROM _cust_company ORDER BY cust_company asc";
$gdbo -> getRec();
$listCompany = $gdbo -> dbData;
# Filterable Objects
## filterDBO
$filterDBO = new ArcDb;
$filterDBO -> dbConStr=$globalDBCON;
$filterDBO -> dbType = $globalDBTP;
$filterDBO -> dbSchema = $globalDB;
$filterDBO -> sql = "SELECT 
id_cust_contact, 
concat(cust_contact_familyName,',',cust_contact_givenName) as cust_contact FROM _cust_contact";
$filterDBO -> dbFilter = "WHERE id_cust_company=";
$filterDBO -> type = "list";
$filterDBO -> id = "id_cust_contact";
$filterDBO -> attributes = "class='chContact'";
$contactJSON =encrypt(json_encode((array)$filterDBO));
# Fieldsets
$fsSystemUser=array(
array(
array("Username","sys_user",null,0),
array(null,"id_sys_status",null,3,null,21),
),
array(
array("Organization","id_cust_company",'onchange="filterRec(this,\''.$contactJSON.'\',\'dContact\')"',1,$listCompany),
array("User Record","id_cust_contact",'disabled="disabled" class="chContact"',1,null,null,'dContact'),
),
array(
array("Password","sys_user_pw_",'onblur="validateElement(\'password\',this)"',7),
array("Confirm Password","sys_user_pw",'onblur="validateElement(\'password\',this)"',7),
),
array(
array("E-Mail","sys_user_recovery",'onblur="validateElement(\'email\',this)"',0),
array("Time Approval E-Mail","sys_user_tmEmail",'onblur="validateElement(\'email\',this)"',0),
),
);
?>
<script type="text/javascript">$(".icoadd").remove();</script>
<form method="post" name="frmUser" id="frmuser" action="javascript:submitFrmVals('content','/_mod/smod_14/sql.php','sys_user,sys_user_pw,id_hr_emp','&form=frmUser&action=insert','frmUser')">
<fieldset id="user_management">
<legend>User Management</legend>
<?= frmElements($fsSystemUser);?>
</fieldset>
<fieldset id="permissions">
<legend>
Permissions
</legend>
<?php include("insertPermissions.php");?>
</fieldset>
</form>
