<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once ("_lib/php/auth.php");
require_once ("_includes/optionstoolbar.php");
# Recordset
$gdbo -> sql = "SELECT 
a.id_sys_user,
a.sys_user,
a.sys_user_dc,
a.sys_user_du,
a.sys_user_dr,
a.sys_user_eBy,
a.sys_user_uBy,
a.sys_user_rBy,
a.id_sys_status,
a.sys_user_pw,
a.sys_user_recovery,
a.sys_user_tmEmail,
c.id_sys_user_emp,
c.id_cust_contact,
b.id_cust_contact_tp,
b.id_cust_branch,
b.id_name_title,
b.cust_contact_familyName,
b.cust_contact_middleName,
b.cust_contact_givenName,
b.cust_contact_maidenName,
b.cust_contact_commonName,
b.id_name_suffix,
b.cust_contact_role,
b.cust_contact_dc,
b.cust_contact_dr,
b.cust_contact_notes,
b.cust_contact_bdate,
b.cust_department,
b.id_cust_company,
b.id_addr
FROM _sys_user a
LEFT JOIN _sys_user_emp c
ON a.id_sys_user = c.id_sys_user
LEFT JOIN _cust_contact b
ON c.id_cust_contact = b.id_cust_contact
WHERE a.id_sys_user=" . $_POST["id_sys_user"];
$gdbo -> getRec();
$rec = $gdbo -> getAssociative();
# Status
$gdbo -> sql = "SELECT 
id_sys_status,
sys_status
FROM _sys_status 
WHERE id_sys_status_tp=5";
$gdbo -> getRec();
$listStatus = $gdbo -> dbData;
# Company
$gdbo -> sql = "SELECT id_cust_company,cust_company FROM _cust_company ORDER BY cust_company asc";
$gdbo -> getRec();
$listCompany = $gdbo -> dbData;
# Dependent Arrays
if (!empty($rec[0]["id_cust_company"])) {
$gdbo -> sql = "SELECT id_cust_contact, concat(cust_contact_familyName,',',cust_contact_givenName) as cust_contact FROM _cust_contact WHERE id_cust_company=" . $rec[0]["id_cust_company"];
$gdbo -> getRec();
$listContact = $gdbo -> dbData;
} else {$listContact = array();
}
# Filterable Objects
## filterDBO
$filterDBO = new ArcDb;
$filterDBO -> dbConStr=$globalDBCON;
$filterDBO -> dbType = $globalDBTP;
$filterDBO -> dbSchema = $globalDB;
$filterDBO -> sql = "SELECT 
id_cust_contact, 
concat(cust_contact_familyName,',',cust_contact_givenName) as cust_contact FROM _cust_contact";
$filterDBO -> dbFilter = "WHERE id_cust_company=";
$filterDBO -> type = "list";
$filterDBO -> id = "id_cust_contact";
$filterDBO -> attributes = "class='chContact'";
$contactJSON = encrypt(json_encode((array)$filterDBO));
# Fieldsets
$fsSystemUser = array( array( 
array("Username", "sys_user", null, 0, null, $rec[0]["sys_user"]), 
array("Status", "id_sys_status", null, 1, $listStatus, $rec[0]["id_sys_status"]), 
array(null, "id_sys_user", null, 3, null, $rec[0]["id_sys_user"]), ), 
array( array("Organization", "id_cust_company", 'onchange="filterRec(this,\'' . $contactJSON . '\',\'dContact\')"', 1, $listCompany, $rec[0]["id_cust_company"]), 
array("User Record", "id_cust_contact", 'class="chContact"', 1, $listContact, $rec[0]["id_cust_contact"], 'dContact'), ), 
array( array("E-Mail", "sys_user_recovery", 'onblur="validateElement(\'email\',this)"', 0, null, $rec[0]["sys_user_recovery"]), 
array("Time Approval E-Mail", "sys_user_tmEmail", 'onblur="validateElement(\'email\',this)"', 0, null, $rec[0]["sys_user_tmEmail"]), ), );
?>
<form method="post" name="frmUser" id="frmuser" action="javascript:submitFrmVals('content','/_mod/smod_14/sql.php','sys_user,sys_user_pw,id_hr_emp','&form=frmUser&action=update','frmUser')">
<fieldset id="user_management">
<legend>
User Management
</legend>
<?= frmElements($fsSystemUser); ?>
</fieldset>
<fieldset id="permissions">
<legend>
Permissions
</legend>
<?php include("editPermissions.php");?>
</fieldset>
</form>
<script type="text/javascript">
<?=$icoCollapse;?>
$("#oOptions").append(icoCollapse);
collapseTabs();
</script>
