<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once("_lib/php/auth.php");
$curYear = date('Y',strtotime($dtTimeCurrent));
$curMonth = date('n',strtotime($dtTimeCurrent));
$dayCount = cal_days_in_month(CAL_GREGORIAN, $curMonth, $curYear);
$start = strtotime("$curYear-$curMonth-1 00:00:00 ");
$end = strtotime("$curYear-$curMonth-$dayCount 23:59:59");
$arc = new ArcDb;
$arc -> dbConStr=$globalDBCON;
$arc -> dbOffset = 0;
$arc -> dbType = $globalDBTP;
$arc -> dbSchema = $globalDB;
if (isset($_POST["start_date"])&&isset($_POST["end_date"])){
$start=strtotime($_POST["start_date"]);
$end=strtotime($_POST["end_date"]);}
$daterange="$start and $end";
$daterangeFriendly=date('F j, Y',$start). " through " . date('F j, Y',$end);
$filter=($start!=""?"where UNIX_TIMESTAMP(a.srv_dc)+$gmtOffset between ".$daterange:"");
$arc -> sql = "
SELECT * FROM(
SELECT
FROM_UNIXTIME(UNIX_TIMESTAMP(a.srv_dc)+$gmtOffset,'%m-%d-%y') name,
count(id_srv) as data
FROM _srv a $filter group by name order by name desc) as derived WHERE data >=1";
$arc->getRec();
$arcData=$arc->getAssociative();
$arcJSON=json_encode($arcData);
?>
<script type="text/javascript">
var chartData=<?=$arcJSON?>;
var chart = AmCharts.makeChart("chartdiv", {
"theme": "none",
"type": "serial",
"dataProvider": chartData,
"chartCursor": {},
"pathToImages": "/_thirdparty/amcharts/images/",
"valueAxes": [{
"title": "Tickets"
}],
"graphs": [{
"balloonText": "Tickets Opened:[[value]]",
"fillAlphas": 1,
"lineAlpha": 0.2,
"title": "Days",
"type": "column",
"valueField": "data"
}],
"rotate": false,
"categoryField": "name",
"categoryAxis": {
"gridPosition": "start",
"fillAlpha": 0.05,
"position": "left",
"title": "Date",
},
"exportConfig":{
"menuTop":"20px",
"menuRight":"20px",
"menuItems": [{
"icon": '/_thirdparty/amcharts/images/export.png',
"buttonTitle" : "Save chart as an image",
"format": 'png' 
}] 
}
});
chart.validateNow();
</script>
<?php if ($arc->dbRows > 1) { ?>
<div id="chartdiv" style="width: 100%; height: 435px;font-size:11pt"></div>
<?php } else {?>
No data available.
<div id="chartdiv" style="display:none"></div>
<?php }?>
