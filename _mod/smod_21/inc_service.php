<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once ("_lib/php/auth.php");
require_once ("_includes/optionstoolbar.php");
$params = "";
$mnuservice = array( array( array("Start Date", "start_date", 'class="dateFld" onblur="validateElement(\'date\',this)"', 0), array("data:image/png;base64, iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAn9JREFUeNqMU0tIVFEY/s655965JehozSLHyl4mIURg5qMQnTFLpNfkA6RNq6BFklIRRFi0S2gTRos2LaxE20Q1pYQwDo5CgQTVRsWkFoJDRT7m3rmn/5xpMbTQDvyce879v+//znf+wxL93bIi+RgAp2BYe0gKDxP552AwTycz+aBQInSf9r3/w3MqNHIRMEymAAIGbS68Bz4P0ZqtoYLQkqL0DDRGQOezlqdf5aPWIsykac3XEUAidxD4/LN52JZgnAgESyVhowhTg0NEwIk0WwFVQIZVUvW0m0JpewQstUgiSDzl8pTraEWW4AidPQXTMBCKnIQpBMKRE7BFLhojzbDNXIQJrHJTbhqO68FJexArtFA1LctC4mUUPp+NxKth2L4NmHw9jthSLeJPMh6bL2Jobq7BikvVeQ75aYIvE4sawhAINTXCJ/Jw9HiYZj8ajlXqA9RXEZg+mgisxnI6AL+3E3neVlLguHrTtHxIvPuA6EIF3vZTRTLcHp3RNxdLAMlVoKWP6csImkBa4p4h0clX3IwCi85cX3dAH6euKtNWR2qL0Vo8h327e2DlkILDQE9bF8L0v8CPS0TSx5UH0P3BEB/7CMU3MgY4NI/Hv9C+g4GJmyjdDvxaAso23cWP30DJNlLh4QJf/UugbqG6pgwdez6ht0PqubJ6L0zT0LIPFnah85DUuWou33JV9xUL3nkj5683rNlA7Q8ZykvIPLLrRlji9jDDRvJhaprMZ8s/RwO3orUOuSblv1BqLJ6L/QWnMfv9OYKbgbHZaxCUN/2NCBcxqDzbReFf5xmhrRdX7Hy0qttRzarAA924vN77yx5kGwJZ6wWKuT8CDAC10dPLWbJjmwAAAABJRU5ErkJggg==", null, 'alt="add date" height="16" width="16" onclick="openCalendar(this,\'interface\',\'start_date\')"', 5), array("End Date", "end_date", 'class="dateFld" onblur="validateElement(\'date\',this)"', 0), array("data:image/png;base64, iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAn9JREFUeNqMU0tIVFEY/s655965JehozSLHyl4mIURg5qMQnTFLpNfkA6RNq6BFklIRRFi0S2gTRos2LaxE20Q1pYQwDo5CgQTVRsWkFoJDRT7m3rmn/5xpMbTQDvyce879v+//znf+wxL93bIi+RgAp2BYe0gKDxP552AwTycz+aBQInSf9r3/w3MqNHIRMEymAAIGbS68Bz4P0ZqtoYLQkqL0DDRGQOezlqdf5aPWIsykac3XEUAidxD4/LN52JZgnAgESyVhowhTg0NEwIk0WwFVQIZVUvW0m0JpewQstUgiSDzl8pTraEWW4AidPQXTMBCKnIQpBMKRE7BFLhojzbDNXIQJrHJTbhqO68FJexArtFA1LctC4mUUPp+NxKth2L4NmHw9jthSLeJPMh6bL2Jobq7BikvVeQ75aYIvE4sawhAINTXCJ/Jw9HiYZj8ajlXqA9RXEZg+mgisxnI6AL+3E3neVlLguHrTtHxIvPuA6EIF3vZTRTLcHp3RNxdLAMlVoKWP6csImkBa4p4h0clX3IwCi85cX3dAH6euKtNWR2qL0Vo8h327e2DlkILDQE9bF8L0v8CPS0TSx5UH0P3BEB/7CMU3MgY4NI/Hv9C+g4GJmyjdDvxaAso23cWP30DJNlLh4QJf/UugbqG6pgwdez6ht0PqubJ6L0zT0LIPFnah85DUuWou33JV9xUL3nkj5683rNlA7Q8ZykvIPLLrRlji9jDDRvJhaprMZ8s/RwO3orUOuSblv1BqLJ6L/QWnMfv9OYKbgbHZaxCUN/2NCBcxqDzbReFf5xmhrRdX7Hy0qttRzarAA924vN77yx5kGwJZ6wWKuT8CDAC10dPLWbJjmwAAAABJRU5ErkJggg==", null, 'alt="add date" height="16" width="16" onclick="openCalendar(this,\'interface\',\'end_date\')"', 5), array(null, 'frmservicesubmit', ' onclick="prepSaveChanges(\'frmservice\')"', 2, null, 'Filter')));
?>
<form name="frmservice" id="frmservice" method="post" action="javascript:submitFrmVals('ch_service','/_mod/smod_21/ch_service.php','','<?=$params?>&action=qry&form=frmservice','frmservice')">
<fieldset id="service_records">
<legend>Service Records</legend>
<div style="margin-bottom:10px"><?php buildElements($mnuservice);?></div>
<div id="ch_service">
<?php
include ("ch_service.php");
?>
</div>
</fieldset>
</form>
