<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
# Includes
require_once("_lib/php/auth.php");
# Parse Form Action
$action = $_POST['action'];
$form = $_POST['form'];
$formAction = $form . "," . $action;
# Begin SQL Functions
switch ($formAction) {
case "frmDepartment,insert":
/*****************************************************************************/
$gdbo -> dbTable = "_cust_department";
$gdbo -> insertRec();
$_POST["id_cust_department"] = $gdbo -> insertedID;
include('edit.php');
/*****************************************************************************/
break;
case "frmDepartment,update":
/*****************************************************************************/
$gdbo -> dbTable = "_cust_department";
$gdbo -> updateRec("id_cust_department=".$_POST["id_cust_department"]);
include('edit.php');
/*****************************************************************************/
break;
case "frmDepartment,delete":
/*****************************************************************************/
$gdbo -> dbTable = "_cust_department";
$gdbo -> deleteRec("id_cust_department=".$_POST["id_cust_department"]);
/*****************************************************************************/ 
# Purge all records that may be linked to this company
# IMPORTANT! All records associated with the branch will be purged
$gdbo -> sql ="SELECT TABLE_NAME FROM information_schema.columns WHERE COLUMN_NAME='id_cust_department'";
$gdbo -> getRec();
$delTables = $gdbo -> dbData;
foreach($delTables as $row => $column) {
foreach($column as $data => $value) {
$gdbo -> dbTable = $value;
$gdbo -> deleteRec("id_cust_department=".$_POST["id_cust_department"]);
}
}
include("index.php");
break; 
default:
break;
}
include("_error/status.php");
?>
