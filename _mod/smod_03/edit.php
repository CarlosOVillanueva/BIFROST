<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once("_lib/php/auth.php");
require_once("_includes/heading.php");
require_once("_includes/optionstoolbar.php");
# Recordset
$gdbo -> sql ="SELECT 
a.id_cust_department,
a.id_cust_branch,
a.cust_department
FROM _cust_department a
WHERE a.id_cust_department=".$_POST["id_cust_department"];
$gdbo -> getRec();
$recDepartment=$gdbo -> getAssociative();
# Branch
require("_model/dboParentBranch.php");
$gdbo -> getRec();
$listBranch = $gdbo -> dbData;
# Fieldsets
$fsDepartment=
array(
array(
array('*Branch', 'id_cust_branch',null,1,$listBranch,$recDepartment[0]["id_cust_branch"]),
array('*Department', 'cust_department',null,0,null,$recDepartment[0]["cust_department"]),
array(null, 'id_cust_department',null,3,null,$recDepartment[0]["id_cust_department"]),
),
);
$requiredFields = "id_cust_branch,cust_department";
?>
<form method="post" name="frmDepartment" id="frmDepartment" action="javascript:submitFrmVals('content','/_mod/smod_03/sql.php','<?=$requiredFields?>','&form=frmDepartment&action=update','frmDepartment')">
<fieldset id="department_detail">
<legend>Department Detail</legend>
<?=frmElements($fsDepartment)?>
</fieldset>
</form>
</div>
<script type="text/javascript">
oREC = '{"oRec":[{"module":"03","filter":"id_cust_department","recid":"<?=$_POST["id_cust_department"]?>","formname":"frmDepartment"}]}';
<?=$icoDelete;?>
<?=$icoPreview;?> 
$("#oOptions").append(icoDelete+icoPreview);
</script>
