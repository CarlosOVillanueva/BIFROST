<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once("_lib/php/auth.php");
$arctbl = new ArcTbl;
$arctbl -> dbConStr=$globalDBCON;
$arctbl -> dbOffset = 0;
$arctbl -> dbLimit = 23;
$arctbl -> dbType = $globalDBTP;
$arctbl -> dbSchema = $globalDB;
$arctbl -> recLink= $path."edit.php";
$arctbl -> actionFilterKey="id_cust_department";
$arctbl -> recIndex="id_cust_department";
$arctbl -> ignoreCols=array("id_cust_department");
$arctbl -> recQuery = "
SELECT
a.id_cust_department,
a.cust_department as \"Department\",
c.cust_branch_tp as \"Type\",
b.cust_branch as \"Branch\"
FROM _cust_department a
JOIN _cust_branch b ON a.id_cust_branch=b.id_cust_branch
JOIN _cust_branch_tp c ON b.id_cust_branch_tp=c.id_cust_branch_tp";
$arctbl -> actionDestination = "content";
$arctbl -> ajDestination = "list03";
$arctbl -> ajPage = $path."list.php";
$arctbl -> build();
echo hex2str($arctbl -> tblNav);
echo $arctbl->dataTable;
?>
