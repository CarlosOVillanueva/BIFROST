<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#Application Path
$AppPath = "/opt/bifrost"; #Change this value to match "/parent/folder"

#File Parent Path
$FilePath = $AppPath; #Change this value to match "/parent/folder"

#File Folder
$FileFolder = "_uploads";

#Avatar Folder
$AvatarFolder = "_avatar";

#Database Properties
$Database = "bifrost";
$DatabaseServer = "localhost";
$Port = "3306";
$User = "root";
$Password = null;

############# DO NOT EDIT BEYOND THIS POINT ################
$avatarWritePath = "$AppPath/$AvatarFolder/";
$avatarfolder = "/$AvatarFolder";
$filefolder = "/$FileFolder/";
$fileWritePath = "$FilePath/$FileFolder/";
$serverrootpath = "$AppPath/";
$fsorootpath = $AppPath;
$globalDB = $Database;
$globalDBTP = "mysql";
$globalDBCON = "$DatabaseServer,$Port,$User,$Password";
?>
