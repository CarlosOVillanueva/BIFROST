<form id="popBox">
<fieldset>
<legend>Invoice Series</legend>
<?php
require_once("_lib/php/auth.php");
$buttonAdd ='<div class="elementIconBox" onclick="arc(\'listInvoiceSeries\',\'/_mod/smod_27/controls/sql_invoice_series.php\',\'invoice_series=\'+$(\'#invoice_series\').val()+\'&form=frminvoiceseries&action=insert\')"><i class="fa fa-plus"></i></div>';
$deviceAction = array( array( array(null, 'deviceClose', 'onclick="clearPop()"', 2, null, 'Close')));
$fs = array(
array(
array('Series Name','invoice_series','class="elementIcon"',0,null,null,null,$buttonAdd),
),
);
?>
<?=frmElements($fs);?>
<div id="listInvoiceSeries">
<?php require("_mod/smod_27/models/list_invoice_series.php");?>
</div>
<?=frmElements($deviceAction);?>
</fieldset>
</form>