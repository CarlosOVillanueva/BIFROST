<?php
require_once("_lib/php/auth.php");
$arctbl = new ArcTbl;
$arctbl -> dbType = $globalDBTP;
$arctbl -> dbConStr=$globalDBCON;
$arctbl -> dbSchema = $globalDB;
$arctbl -> editable = true;
$arctbl -> dbOffset = 0;
$arctbl -> dbLimit = 23;
$arctbl -> dbTable = "_invoice_series";
$arctbl -> recQuery = "
SELECT id_invoice_series,invoice_series as `Series` FROM _invoice_series";
$arctbl -> recIndex = "id_invoice_series";
$arctbl -> primaryKey = "id_invoice_series";
$arctbl -> ajDestination = "listInvoiceSeries";
$arctbl -> actionDestination = "statusBlock";
$arctbl -> control = "/_mod/smod_27/controls/sql_invoice_series.php";
$arctbl -> ajPage = "/_mod/smod_27/models/list_invoice_series.php";
$arctbl -> build();
echo hex2str($arctbl->tblNav);
echo $arctbl->dataTable;
?>
