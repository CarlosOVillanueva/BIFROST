<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

require_once("_lib/php/auth.php");
// A list of permitted file extensions
#$allowed = array('pdf','doc','docx','pdf','ppt','pptx','png', 'jpg', 'gif','zip','gz','tar');
if(isset($_FILES['upl']) && $_FILES['upl']['error'] == 0){
#if(!in_array(strtolower($extension), $allowed)){
#echo '{"status":"error"}';
#exit;
#}
$writepath=$fileWritePath;
$extension = pathinfo($_FILES['upl']['name'], PATHINFO_EXTENSION);
$filename = pathinfo($_FILES['upl']['name'], PATHINFO_FILENAME);
$filenameparts = bin2hex($_POST["fso_pkcol"]).'_'.$_POST["fso_pk"];
$newfilename = $filenameparts."_".bin2hex($filename).".".$extension;
$_POST["fso_filesize"]=$_FILES['upl']['size'];
$_POST["fso_filesizeraw"]=$_FILES['upl']['size'];
$_POST["fso_filesize"]=formatFileSize($_POST["fso_filesize"]);
$_POST["fso"]=$newfilename;
$_POST["fso_ext"]=$extension;
$_POST["fso_modifiedby"]=$id_cust_contact_derived;
$_POST["fso_path"]=$filefolder;
$_POST["fso_originalname"]=$_FILES['upl']['name'];
$_POST["fso_datemodified"]=$dtTimeUnix;
# Check if exists
$gdbo -> sql = "SELECT * FROM _fso WHERE fso='".$newfilename."'";
$gdbo -> getRec();
$recFSO = $gdbo -> getAssociative();
if ($gdbo -> dbRows >0){
# Record exist
#if ($recFSO[0]["fso_filesizeraw"]!=$_POST["fso_filesizeraw"]){
# File was updated
# Remove old file
unlink($writepath.$newfilename);
if(move_uploaded_file($_FILES['upl']['tmp_name'],$writepath.$newfilename)){
# Update record with new size
$gdbo -> dbTable = "_fso";
$_POST["fso_md5"]=md5_file($writepath.$newfilename);
$gdbo -> sql="fso_modifiedby=".$_POST["fso_modifiedby"].",fso_md5='".md5_file($writepath.$newfilename)."',fso_filesizeraw=".$_POST["fso_filesizeraw"].",fso_filesize='".$_POST["fso_filesize"]."'";
# $gdbo -> sql ="fso_filesize = '".$_POST["fso_filesize"]."'";
$gdbo -> updateRec("fso='".$newfilename."'");
echo '{"status":"success"}';
exit;
} else {
echo '{"status":"error"}';
exit;
}
#}
} else {
# Record does not exist
if(move_uploaded_file($_FILES['upl']['tmp_name'],$writepath.$newfilename)){
$_POST["fso_md5"]=md5_file($writepath.$newfilename);
$gdbo -> dbTable = "_fso";
$gdbo -> insertRec();
echo '{"status":"success"}';
exit;
} else {
echo '{"status":"error"}';
exit;
}
}
} else {
echo '{"status":"error"}';
exit;
}
?>
