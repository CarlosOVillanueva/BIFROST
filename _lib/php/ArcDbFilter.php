<?php 
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

require_once("_lib/php/auth.php");
$arctbl = json_decode(hex2str($_POST["arctbl"]), TRUE);
$cols = json_decode(hex2str($_POST["arccols"]), TRUE);
foreach ($cols as $key => $col) {
$coltype[$key] = $col["type"];
$collabel[$key] = $col["name"];}
array_multisort($collabel, SORT_ASC,$cols);
$colFullArray = array();
foreach ($cols as $column) {$colFullArray[] = array($column["type"], $column["name"]);}
?>
<div id="popBox">
<form method="POST" id="frmFilter" name="frmFilter" action="javascript:buildLocalFilter('<?php echo $arctbl["ajPage"]?>','<?php echo $arctbl["ajDestination"]?>','<?php echo $_POST["arctbl"]?>','<?php echo $arctbl["dbType"]?>')">
<script>
document.getElementById('frmFilter').addEventListener('keypress', function(event) {
if (event.keyCode == 13) {
event.preventDefault();}});
</script>
<fieldset><legend>Filter</legend>
<div class="multiRow" id="multi">
<div class="multimenu">
<input type="button" id="phonesAddButton" onclick="multiRowAddElement(this,'frmrow',null,null);hideMFControls(this,'cRemove');booCheck()" class="clAddButton" value="Add">
<input type="button" onclick="multiRowRemoveLastElement(this,'frmrow');" value="Remove Last">
</div>
<div class="frmrow">
<div class="frmcol">
<label>Column</label>
<?php echo selList($colFullArray, "x1_filtercol", "getSelFilter(this)", "class=\"mFilterCol\"") ?>
</div>
<div class="frmcol">
<label>Filter</label>
<?php
$placeholder=array();
echo selList($placeholder, "x1_filterby", "setFilterValue(this)", "disabled=\"disabled\" class=\"mFilterBy\"");
?>
</div>
<div class="frmcol">
<label>Value</label>
<input type="text" id="x1_filterval" name="x1_filterval" class="mFilterVal" disabled="disabled">
</div>
<div class="frmcol">
<label>Filter Group</label>
<select name="x1_grpID" id="x1_grpID" class="mGrpBy">
<?php
$i = 0;
while ($i< 10) {
echo "<option value='$i'>Group $i</option>";
$i++;}
?>
</select>
</div>
<div class="frmcol" style="display:none;padding-top:14px;vertical-align:middle">
<input type="radio" name="x1_boo" disabled="disabled" class="mFilterBoo" value="AND"/>
AND
<input type="radio" name="x1_boo" disabled="disabled" class="mFilterBoo" value="OR"/>
OR
<i id="x1_remove" style="display:none;margin-left:10px" class="cRemove fa fa-trash" onclick="multiRowRemoveActiveElement(this,'frmrow')"></i>
</div>
</div>
</div>
<div class="frmrow">
<div class="frmcol">
<input type="submit" value="Filter"/>
<input type="button" onclick="document.getElementById('popWindowFilter').style.display='none'" value="Close"/>
</div>
</div>
<div style="display:none">
<textarea name="hiddenVars" id="hiddenVars"><?php echo $defaultFilter ?></textarea>
</div>
</fieldset>
</form>
</div>