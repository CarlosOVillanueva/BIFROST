<?php
require_once("_lib/php/auth.php");
$dbo = new ArcDB;
$dboData = (array) json_decode(decrypt($_POST["DBO"]));
unset($dboData["dbCols"]);
unset($dboData["dbData"]);
$inc=1;
foreach ($dboData as $Column => $Data){
$comma=($inc != 1?",":"");
$dbo -> $Column = $Data;
$inc = $inc + 1;
}
if(isset($dbo -> dependents)){
$dependents = json_decode($dbo -> dependents);
foreach($dependents as $element => $name) {
echo '<script>$("'.$name.'").prop("disabled",true);</script>';
}
}
$attributes= (isset($dbo -> attributes) ?$dbo -> attributes :"");
$recID = (isset($dbo -> id) ?$dbo -> id :"");
if(!empty($_POST["VAL"])) {
$dbo -> sql = $dbo -> sql." ".$dbo -> dbFilter.hex2str($_POST["VAL"]);
$dbo -> getRec();
$results = $dbo -> dbData;
$count = $dbo -> dbRows;
$disabled="";
} else {
$results = array();
$disabled='disabled="disabled"'; 
}
if($count == 0) {
$disabled='disabled="disabled"';
$first = null;
$text = null;
} else {$first = $results[0][0];$text=$results[0][1];}
switch ($dbo -> type) {
case "list":
echo selList($results , $recID, null, $attributes.' '.$disabled);
break;
case "text":
echo '<input type="text" name="'.$recID.'" id="'.$recID.'" '.$attributes.' readonly="readonly" '.$disabled.' value="'.$text.'" />';
echo '<input type="hidden" name="'.rtrim($recID, "_").'" id="'.rtrim($recID, "_").'" value="'.$first.'" />';
break;
}
?>