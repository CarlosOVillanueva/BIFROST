<?php /*
* @dependency lib_ip.js
* @dependency ArcJS-13.3.0.1.js
* @dependency ArcCore-13.3.0.0.php
* @dependency ArcDb-13.3.0.0.php
* @dependency ArcTbl-13.3.0.0.php
* @dependency ArcDbFilter.php
* @dependency auth.php (Contains references for preceding *.php)
*
* */
require_once ("_lib/php/auth.php");
/* define database */
$ip=new ArcDb;
$ip->dbConStr=$globalDBCON;
$ip->dbType=$globalDBTP;
$ip->dbSchema=$globalDB;
/* return all interfaces with the same address */
$ip -> sql = "
SELECT
id_cfg_device_interface,
id_cfg_device_ip4netaddress,
a.id_cfg_device,
a.id_device_interfacelabel,
id_device_ifspeedduplex,
cfg_device_interface_ip4hostaddress,
cfg_device_interface_enabled,
d.device,
e.cfg,
c.device_interfacelabel
FROM
_cfg_device_interface a
INNER JOIN
_cfg_device b 
ON a.id_cfg_device=b.id_cfg_device
INNER JOIN
_device_interfacelabel c 
ON a.id_device_interfacelabel=c.id_device_interfacelabel
INNER JOIN
_device d 
ON b.id_device=d.id_device
INNER JOIN
_cfg e 
ON b.id_cfg=e.id_cfg
WHERE
cfg_device_interface_enabled=1
AND cfg_device_interface_ip4hostaddress=" . $_POST["cfg_device_interface_ip4hostaddress"];
$ip -> getRec();
if ($ip -> dbRows > 0) {
$existingInterfaceAssignments = $ip -> getAssociative();
} else {
die('<script>prepSaveChanges("'.$_POST["form"].'")</script>');
}
/* evaluate returned results */
if (count($existingInterfaceAssignments) > 0) {
/* matches were found */
/* * search array for network id passed via POST */
$netIsDefined = multiArraySearch($existingInterfaceAssignments, $_POST["id_cfg_device_ip4netaddress"]);
if ($netIsDefined !== false) {
$errorMsg = long2ip($_POST["cfg_device_interface_ip4hostaddress"]) . " already exists.";
$errorMsg .= "</br> The address is assigned to the following device interface.";
$errorMsg .= "<pre>" . print_r($existingInterfaceAssignments[$netIsDefined], true) . "</pre>";
} else {
die('<script>prepSaveChanges("'.$_POST["form"].'")</script>');
}
}
?>
<div class="ipError">
<?=$errorMsg ?>
</div>