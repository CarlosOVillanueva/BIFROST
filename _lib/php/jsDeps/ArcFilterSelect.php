<?php
/*
* Author: Carlos Villanueva
* Date Created: JUL 23, 2011
* Dependency: /_lib/_js/fn_fltrSelect.js
* POST variables
* a - label
* b - schema connection
* c - schema
* d - table
* e - PK Col, Descriptive Col
* f - node id
* g - script
* h - dbms bit (1=mssql,0=mysql)
* i - node id of destination
* j - object
* k - where clause
* l - order by clause
* m - attributes
* n - debug
*/
require_once("_lib/php/auth.php");
filterQryPost("n");
$debug=(isset($n)?$n:0);
if ($debug == 1) {
printArray($_POST);
}
# convert all POST variables into PHP
foreach ($_POST as $key => $value) {
filterQryPost($key);
}
# javascript parse
# only supports a single function with multiple parameters
if (!is_null($g)&&$g!="null"){
$g = str_replace("!qut!", "'", $g);
$g = str_replace("!sem!", ";", $g);
$g = str_replace("!com!", ",", $g);
;
}
if (!is_null($e)&&$e!="null"){
$e = str_replace("!squt!", "'", $e);
$e = str_replace("!sem!", ";", $e);
$e = str_replace("!com!", ",", $e);
;
}
else {
$g=null;}
# connections
require("_con/" . $b . ".php");
eval("$" . "con=$" . $b . ";");
# query
$l=(isset($l)?"ORDER BY $l":"");
$e = str_replace("!squt!", "'", $e);
$sql = "SELECT $e FROM $d WHERE $k $l";
if ($debug == 1) {
echo $sql;
}
# test if MSSQL or MySQL
if ($h == 0) {
myRec($r, $con, $c, $sql);
rec2arrayMY($array, $r);
mysql_close($con);
} else {
msRec($r, $con, $c, $sql);
rec2arrayMS($array, $r);
mssql_close($con);
}
if ($m=="null")
$m=null;
else
$m= str_replace("x34","\"",$m);
if (isset($a) ) {
$label="<label>$a</label>";
}
else {
$label="";}
if (count($array)>0) {
echo $label.selList($array, $f,$g,$m);
printArray($a);
}
else
echo $label."<select disabled=disabled id=\"$f\"><option>--</option></select>";
?>
