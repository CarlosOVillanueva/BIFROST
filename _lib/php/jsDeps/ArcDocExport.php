<style type="text/css">
.frmcol {
float: left;
width: 200px;
}
.frmrow {
clear: both;
margin-bottom: 20px;
overflow: hidden;
}
.label {
color: #686868;
font-size: 10pt;
display: block;
font-weight: bold;
}
fieldset {
border: 0;
}
h1 {
color: #464646;
font-size: 12pt;
text-transform: uppercase;
border-bottom: 1px solid #464646;
}
body {
font-size: 10pt;
font-family:'Open Sans', sans-serif;
color: #545454;
}
span {
display: inline-bock;
margin: 0 10px 0 0;
}
span.radio {
margin: 0 5px 0 0;
font-weight: bold;
}
span.element {
margin: normal;
}
pre {white-space: pre0!important;}
img {
border: 1px solid #464646;
padding: 2px;
background-color: #fff;
}
td {border:1px solid #999;padding:10px}
table {width:100%}
.syntaxhighlighter table td.code .line {
white-space: pre-wrap !important;
word-wrap: break-word !important;
}
@media screen and (max-width: 680px) {
.syntaxhighlighter .gutter {
display:none;
}
</style>
<?php
/*
Copyright (C) © 2010-2013 BIFRÖST SOFTWARE LLC
Author: Carlos Omar Villanueva
*/
require ("_lib/php/auth.php");
$data = hex2str($_POST["DBO"]);
?>
<html>
<head>
<title></title>
<link href="/_thirdparty/syntaxhighlighter_3.0.83/styles/shThemeDefault.css" rel="stylesheet" type="text/css"/>
<link href="/_thirdparty/syntaxhighlighter_3.0.83/styles/shCoreDefault.css" rel="stylesheet" type="text/css"/>
<script src="/_thirdparty/ckeditor/ckeditor.js" type="text/javascript"></script>
<script src="/_lib/jquery/0-jquery.js" type="text/javascript"></script>
<script src="/_lib/js/ArcJS.js" type="text/javascript"></script>
<script src="/_thirdparty/syntaxhighlighter_3.0.83/scripts/shCore.js" type="text/javascript"></script>
<script src="/_thirdparty/syntaxhighlighter_3.0.83/scripts/shLegacy.js" type="text/javascript"></script>
<script src="/_thirdparty/syntaxhighlighter_3.0.83/scripts/shBrushJScript.js" type="text/javascript"></script>
<script src="/_thirdparty/syntaxhighlighter_3.0.83/scripts/shBrushXml.js" type="text/javascript"></script>
</head><body>
<?=$data?>
</body>
<script type="text/javascript">
$("pre").addClass("brush: js"); 
SyntaxHighlighter.highlight();
SyntaxHighlighter.defaults['auto-links'] = false;
SyntaxHighlighter.defaults['smart-tabs'] = false;
SyntaxHighlighter.defaults['tab-size'] = 4;
SyntaxHighlighter.defaults['html-script'] = true;
$(".toolbar").remove();
lineWrap()
</script></html>

