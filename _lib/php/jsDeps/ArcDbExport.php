<?php
/*
Author: Carlos Omar Villanueva
*/
require_once ("_lib/php/auth.php");
$dbo = new ArcDB;
$dboData = (array) json_decode(decrypt($_POST["DBO"]));
unset($dboData["dbCols"]);
unset($dboData["dbData"]);
$inc = 1;
$debug = 1;
foreach ($dboData as $Column => $Data) {
$comma = ($inc != 1 ? "," : "");
$dbo -> $Column = $Data;
$inc = $inc + 1;
}
$dbo -> dbLimit = null;
$dbo -> getRec();
$aRecordset_cols = $dbo -> dbCols;
$aRecordset = $dbo -> getAssociative();
header("Pragma: public");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Content-Disposition: attachment;filename=export.csv");
header("Content-Transfer-Encoding: binary ");
$start = microtime(TRUE);
set_time_limit(330);
$report="";
for ($i = 0; $i < count($aRecordset_cols); $i++) {
if ($aRecordset_cols[$i]['name'] != "Action" && $aRecordset_cols[$i]['name'] != "") {
$report.=$aRecordset_cols[$i]['name'] . ",";
}
}
$report.="\n";
if (count($aRecordset) > 0) {
foreach ($aRecordset as $row => $cols) {
foreach ($cols as $key => $value) {
if ($key != "Action" && $key != "") {
$content = strip_tags($value);
$content = str_replace("&#x0D;", " ", $content);
$content = str_replace("\n", " ", $content);
$content = str_replace(",", " ", $content);
$report.="\"" . $content . "\"" . ",";
}
}
$report.="\n";
}
}
echo $report;
?>
