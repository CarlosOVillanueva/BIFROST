<?php
/* 
Author: Carlos Omar Villanueva
*/
require_once('_lib/php/auth.php');
$curYear = date('Y',strtotime($dtTimeCurrent));
$curDay = date('j',strtotime($dtTimeCurrent));
$curMonth = date('n',strtotime($dtTimeCurrent));
$daysOfWeek = array(array("Sunday", "Su", 0),
array("Monday", "Mo", 1),
array("Tuesday", "Tu", 2),
array("Wednesday", "We", 3),
array("Thursday", "Th", 4),
array("Friday", "Fr", 5),
array("Saturday", "Sa", 6)
);
filterQryPost("yearID");
$yearID = (isset($yearID) ? $yearID : $curYear);
filterQryPost("monthID");
$monthID = (isset($monthID) ? $monthID : $curMonth);
$dateToFind = cal_to_jd(CAL_GREGORIAN, $monthID, 1, $yearID);
$firstDayofMonth = jddayofweek($dateToFind, 1);
echo "<div style=\"text-align:center\" class=\"closeButton\" onclick=\"selectCalendarDate('".$yearID . "-" . $monthID . "-" . $curDay."')\">Today</div>";
$monthBackward = ($monthID != 1 ? "<span class=\"lArrow\" onclick=\"moveDate(0,0,$monthID,$yearID)\">◄</span>" : "<span class=\"lArrow\" style=\"visibility:hidden\">◄</span>");
$monthForward = ($monthID != 12 ? "<span class=\"rArrow\" onclick=\"moveDate(1,0,$monthID,$yearID)\">►</span>" : "<span class=\"rArrow\" style=\"visibility:hidden\">►</span>");
echo "<div class=\"calRow\"><span class=\"calTitle\">$monthBackward" . date('F', strtotime("$yearID-$monthID-01")) . "$monthForward</span></div>";
echo "<div class=\"calRow\"><span class=\"calTitle\"><span class=\"lArrow\" onclick=\"moveDate(0,1,$monthID,$yearID)\">◄</span>" . $yearID . "<span class=\"rArrow\" onclick=\"moveDate(1,1,$monthID,$yearID)\">►</span></span></div>";
$days = cal_days_in_month(CAL_GREGORIAN, $monthID, $yearID);
for ($i = 0; $i< count($daysOfWeek); $i++) {
if ($daysOfWeek[$i][0] == $firstDayofMonth)
$startDate = $daysOfWeek[$i][2];
}
for ($i = 0; $i< count($daysOfWeek); $i++) {
echo "<span class=\"dayHeading\">" . $daysOfWeek[$i][1] . "</span>";
}
echo "<br>";
global $d;
for ($d = 0; $d< $startDate; $d++) {
echo "<span class=\"dayNull\">&nbsp;</span>";
}
for ($i = 0; $i< $days; $i++) {
$day = $i + 1;
$d = $d + 1;
$jsDateValue = $yearID . "-" . $monthID . "-" . $day;
$break = ($d % 7 == 0 ? "<br>" : "");
$dayClass=($day==$curDay&&$monthID==$curMonth&&$yearID==$curYear?"dayActive":"day");
echo "<span class=\"$dayClass\" onclick=\"selectCalendarDate('$jsDateValue')\">" . $day . "</span>" . $break;
}
echo "<div style=\"text-align:center\" class=\"closeButton\" onclick=\"hideCalendar()\">Close</div>";
?>
