<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/*************************************************/
#includes
/*************************************************/
require("_lib/php/bootstrap.php");
/*************************************************/
#session id
/*************************************************/
if (!isset($_COOKIE["PHPSESSIDU"])) {header('location:/index.php');}
/*************************************************/
#authentication
/*************************************************/
$gdbo -> sql = "
SELECT id_sys_user,sys_user_log_sessionID FROM _sys_user_log WHERE id_sys_user=" . decrypt($_COOKIE["PHPSESSIDU"]) . " AND sys_user_log_sessionID='" . session_id() . "' ORDER BY sys_user_log_dc DESC limit 1";
$gdbo -> getRec();
$auth = $gdbo -> getAssociative();
printArray($auth);
/*************************************************/
#login test
/*************************************************/
$login = ($gdbo -> dbRows > 0 ? true : false);
if ($login === false) {header('location:/index.php');}
/*************************************************/
#auth success
/*************************************************/
$gdbo -> sql = "
SELECT
b.id_hr_emp,
c.id_cust_company,
a.id_cust_contact,
concat(c.cust_contact_familyName,', ',c.cust_contact_givenName) as fullNameSystem,
concat(c.cust_contact_givenName,' ',c.cust_contact_familyName) as fullName,
b.hr_emp_payFrequency,
d.sys_user_recovery
FROM
_sys_user_emp a 
LEFT JOIN _hr_emp b ON a.id_cust_contact=b.id_cust_contact
LEFT JOIN _cust_contact c ON a.id_cust_contact=c.id_cust_contact
LEFT JOIN _sys_user d ON a.id_sys_user=d.id_sys_user
WHERE d.id_sys_status=21 AND a.id_sys_user=" . decrypt($_COOKIE["PHPSESSIDU"]);
$gdbo -> getRec();
$user = $gdbo -> getAssociative();
printArray($user);
/*************************************************/
#date time parameters for user
/*************************************************/
$dateTimeZoneUser = new DateTimeZone("America/Chicago");
$dateTimeUser = new DateTime("now", $dateTimeZoneUser);
$gmtOffset = $dateTimeZoneUser -> getOffset($dateTimeUTC);
$dtTimeCurrent = date("Y-m-d H:i:s", (strtotime(date("Y-m-d H:i:s")) + $gmtOffset));
/*************************************************/
#user parameters
/*************************************************/
$id_sys_user = $auth[0]["id_sys_user"];
$id_cust_company_derived = $user[0]["id_cust_company"];
$usermail = $user[0]["sys_user_recovery"];
$id_hr_emp_derived = $user[0]["id_hr_emp"];
$id_cust_contact_derived = $user[0]["id_cust_contact"];
$fullNameSystem_derived = $user[0]["fullNameSystem"];
$fullName_derived = $user[0]["fullName"];
$payFrequency = $user[0]["hr_emp_payFrequency"];
$isEmployee = ($id_hr_emp_derived!=""?true:false);
/*************************************************/
#company data
/*************************************************/
$gdbo ->sql = "SELECT * FROM _sys_company";
$gdbo -> getRec();
$sysCompany = $gdbo -> getAssociative();
printArray($sysCompany);
/*************************************************/
#user permissions
/*************************************************/
$gdbo -> sql = "
SELECT 
a.id_sys_module_sub,
a.sys_permission,
b.id_sys_module
FROM _sys_permission a
LEFT JOIN 
_sys_module_sub b ON a.id_sys_module_sub = b.id_sys_module_sub
WHERE id_sys_user=".$id_sys_user;
$gdbo -> getRec();
$sysPermissions = $gdbo -> getAssociative();
/*************************************************/
#group permissions
/*************************************************/
$gdbo -> sql = "
SELECT 
id_sys_group
FROM _sys_group_user
WHERE id_sys_user=".$id_sys_user;
$gdbo -> getRec();
$userGroups = $gdbo -> getAssociative();

if($gdbo -> dbRows>0) {
#find permissions for groups
$gdbo -> dbData = null;
$gdbo -> recursiveData = true;
for ($i=0;$i<count($userGroups);$i++) { 
	$gdbo -> sql = "
	SELECT 
	a.id_sys_module_sub,
	a.sys_permission,
	b.id_sys_module
	FROM _sys_permission a
	LEFT JOIN
	_sys_module_sub b ON a.id_sys_module_sub = b.id_sys_module_sub
	WHERE a.id_sys_group=".$userGroups[$i]["id_sys_group"]." ORDER BY a.id_sys_module_sub ASC";
	$gdbo -> getRec();
}
$groupPermissions = $gdbo -> dbData;
$gdbo -> recursiveData = false;

#sort group permissions
foreach ($groupPermissions as $key => $col) {
    $sysModule[$key] = $col[0];
    $sysPermission[$key] = $col[1];
}
array_multisort($sysModule, SORT_ASC, $sysPermission, SORT_DESC, $groupPermissions);

#pull most restrictive permission
$groupPermissionMaster = array();
for ($i=0;$i<count($groupPermissions);$i++) {
	$next = $i + 1;$prev = $i - 1;
	if ($i == (count($groupPermissions) - 1) || $groupPermissions[$i][0] != $groupPermissions[$next][0]) {
	$groupPermissionMaster[] = array("id_sys_module_sub" => $groupPermissions[$i][0],"sys_permission" => $groupPermissions[$i][1],"id_sys_module" =>$groupPermissions[$i][2]);
	}
}
/*************************************************/
#combine group and user permissions
/*************************************************/
$masterPermissions = array();
$masterPermissions = array_merge($groupPermissionMaster,$sysPermissions);
#sort permissions
foreach ($masterPermissions as $key => $col) {
    $sysModule[$key] = $col["id_sys_module_sub"];
    $sysPermission[$key] = $col["sys_permission"];
}
array_multisort($sysModule, SORT_ASC, $sysPermission, SORT_DESC, $masterPermissions);

$permissionsTemp = array();
foreach($masterPermissions as $key => $col) {
	$permissionsTemp[] = array($col['id_sys_module_sub'],$col['sys_permission'],$col['id_sys_module']);
}

#pull most restrictive permission
$permissions = array();
for ($i=0;$i<count($permissionsTemp);$i++) {
	$next = $i + 1;$prev = $i - 1;
	if ($i == (count($permissionsTemp) - 1) || $permissionsTemp[$i][0] != $permissionsTemp[$next][0]) {
	$permissions[] = array("id_sys_module_sub" => $permissionsTemp[$i][0],"sys_permission" => $permissionsTemp[$i][1],"id_sys_module" =>$permissionsTemp[$i][2]);
	}
}} else {
	$permissions = $sysPermissions;
}
/*************************************************/
#configure module links
/*************************************************/
$mod = str_replace("_mod/smod_", "", $path);
$mod = (int) $mod;
/*************************************************/
#apply permissions to modules
/*************************************************/
if (isset($mod)){
	foreach ($permissions as $row => $data){
		if ($data["id_sys_module_sub"] == $mod) {
			if ($data["sys_permission"] >1) {
				$icoSave = $icoSave;
				$icoAdd = $icoAdd;
				$icoUpload = $icoUpload;
			} else {
				$icoSave = "icoSave = \"\";";
				$icoAdd = "icoAdd = \"\";";
				$icoUpload = "icoUpload = \"\";";
			}
		} 
		if ($data["id_sys_module_sub"] == $mod) {
			if ($data["sys_permission"] == 3) {
				$icoDelete = $icoDelete;
			} else {
				$icoDelete = "icoDelete = \"\";";
			}
		}
	}
}
?>