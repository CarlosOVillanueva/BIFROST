<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

require_once("_lib/php/auth.php");
$gdbo -> dbTable = "_fso";
# Get Data About the File
if (!empty($_POST["id_fso"])) {
$filter="WHERE id_fso=".$_POST["id_fso"];
} else {
$filter="WHERE fso='".$_POST["fso"]." and fso_filesize=".$_POST["fso_filesize"];
}
$gdbo -> sql = "SELECT * FROM _fso ".$filter;
$gdbo -> getRec();
$FSO = $gdbo -> getAssociative();
$_POST["modRecID"] = (!empty($_POST["id_fso"])?$FSO[0]["fso_pk"]:$_POST["fso_pk"]);
$listPath = "_mod/smod_".$_POST["fso_smod"]."/listFiles.php";
$FSOPath =$GLOBALS["fileWritePath"];
switch ($_POST["action"]) {
case "update":
include("$listPath");
break;
case "delete":
$gdbo -> deleteRec("id_fso=".$_POST["id_fso"]);
include($listPath);
unlink($FSOPath.$FSO[0]["fso"]);
break;
}
?>