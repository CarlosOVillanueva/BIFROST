<!--
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
-->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>
BIFRÖST SOFTWARE LLC
</title>
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<meta http-equiv="Cache control" content="no-cache"/>
<meta name="COPYRIGHT" content="&copy; 2014 Bifrost Softrware LLC">
<link href="/_thirdparty/syntaxhighlighter_3.0.83/styles/shCoreEmacs.css" rel="stylesheet" type="text/css"/>
<link href="/_thirdparty/syntaxhighlighter_3.0.83/styles/shThemeEmacs.css" rel="stylesheet" type="text/css"/> 
<script type="text/javascript" src="/_thirdparty/syntaxhighlighter_3.0.83/scripts/0-shCore.js"></script>
<script type="text/javascript" src="/_thirdparty/syntaxhighlighter_3.0.83/scripts/shBrushBash.js"></script>
</head>
<body>
<pre class="brush: bash">
<?php
require_once("_lib/php/auth.php");
$debug =1;
$cards = shell_exec('cat /var/git/arcgpl/_lib/php/vcard.txt');
$cards = str_replace('END:VCARD','!-!',$cards);
$cards = preg_replace('/BEGIN\:VCARD\s*VERSION\:[0-9]\.[0-9]/i','',$cards);
$cards = preg_replace('/\n/','!N!',$cards);
$cardArray = explode('!-!',$cards);
$filteredArray = array();
#printArray($cardArray);
foreach ($cardArray as $data => $value) {
$break = explode ('!N!',$value);
$break = array_values(array_filter($break));
$filteredArray[] = $break;
}
$kill = array();
for ($i=0;$i<count($filteredArray);$i++) {
for ($ii=0;$ii<count($filteredArray[$i]);$ii++){
if (strrpos($filteredArray[$i][$ii],"PHOTO") !== false) {
$start = $ii;
for ($iii=$start+1;$iii<count($filteredArray[$i]);$iii++) {
$filteredArray[$i][$ii].=$filteredArray[$i][$iii];
$filteredArray[$i][$ii] = preg_replace('/\s/','', $filteredArray[$i][$ii]);
$kill[] = array($i,$iii);
}
}
}
}
#printArray(array_values($filteredArray));
#printArray($kill);
for ($i=0;$i<count($kill);$i++){
unset($filteredArray[$kill[$i][0]][$kill[$i][1]]);
}
print_r(array_values($filteredArray));
?>
</pre>
<script> SyntaxHighlighter.all();</script>
</body>
</html>