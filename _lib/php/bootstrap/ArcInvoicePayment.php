<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/



class InvoicePayment {
public $filter = null;
public $elements = null;
public $id = null;
const deleteRow = '<span class="elementIconBox" onclick="multiRowRemoveActiveElement(this,\'frmgroup\');disableRemoveLast(\'#payment\',\'.frmgroup\',\'.clRemoveButton\')"><i class="fa fa-trash"></i></span>';
const dateReceived ='<div class="elementIconBox" onclick="openCalendar(this,\'interface\',null,\'clDateReceived\')"><i class="fa fa-calendar"></i></div>';
const dateCleared='<div class="elementIconBox" onclick="openCalendar(this,\'interface\',null,\'clDateCleared\')"><i class="fa fa-calendar"></i></div>';
function getRecords() {
$gdbo = new ArcDb;
$gdbo -> dbConStr=$GLOBALS['globalDBCON'];
$gdbo -> dbType = $GLOBALS['globalDBTP'];
$gdbo -> dbSchema = $GLOBALS['globalDB'];
if (is_null($this -> filter) || is_null($this -> id)) {
return false;
}
$gdbo -> sql = "SELECT 
_invoice_payment.id_invoice_payment,
_invoice_payment.invoice_payment,
_invoice_payment.invoice_payment_refNumber,
_invoice_payment.id_sys_user,
id_contract,
invoice_payment_dreceived,
invoice_payment_dcleared,
_invoice_payment.id_invoice_payment_tp
FROM _invoice_payment
WHERE ".$this -> filter ."=" . $this -> id.
" ORDER BY invoice_payment_dreceived ASC";
$gdbo -> getRec();
$results= $gdbo -> getAssociative();
return $results;
}
function getLists() {
$gdbo = new ArcDb;
$gdbo -> dbConStr=$GLOBALS['globalDBCON'];
$gdbo -> dbType = $GLOBALS['globalDBTP'];
$gdbo -> dbSchema = $GLOBALS['globalDB'];
$gdbo -> sql = "SELECT id_invoice_payment_tp, invoice_payment_tp FROM _invoice_payment_tp";
$gdbo -> getRec();
$this -> listPaymentTp = $gdbo -> dbData;
$gdbo -> sql = "SELECT
a.id_contract,
a.contract,
a.id_cust_company,
b.cust_company
FROM _contract a
LEFT JOIN _cust_company b
ON a.id_cust_company = b.id_cust_company
ORDER by b.cust_company
";
$gdbo -> getRec();
$this -> listContract = $gdbo -> dbData;
return true;
}
function getBaseElement(){
$elements=
array(
array(
array("*Payment",null,'class="clPayment" onblur="validateElement(\'money\',this)"',0),
array("*Payment Type",null,null,1,$this -> listPaymentTp),
array("Reference ID",null,'class="elementIcon"',0,null,null,null,self::deleteRow),
),
array(
array("Payment Received",null,'class="clDateReceived elementIcon" onblur="validateElement(\'date\',this),calcPayments()" class="dateFld" validateElement(\'date\',this)',0,null,null,null,self::dateReceived,null,'YYYY-MM-DD'),
array("Payment Cleared",null,'class="clDateCleared elementIcon" onblur="validateElement(\'date\',this)" class="dateFld" validateElement(\'date\',this)',0,null,null,null,self::dateCleared,null,'YYYY-MM-DD'),
array("Contract",null,null,1,$this -> listContract),
),
);
$baseElements='<div class="frmgroup" style="display:none">';
$baseElements.= frmElements($elements);
$baseElements.='</div>';
return $baseElements;
}
function getElements(){
$dataElements=(isset($dataElements)?$dataElements:"");
$rec = $this -> getRecords();
if ($rec == false) {
return false;
}
for ($i=0;$i<count($rec);$i++) {
$elements=
array(
array(
array("*Payment",null,'class="clPayment" onblur="validateElement(\'money\',this),calcPayments()"',0,null,$rec[$i]["invoice_payment"]),
array("*Payment Type",null,null,1,$this -> listPaymentTp,$rec[$i]["id_invoice_payment_tp"]),
array("Reference ID",null,'class="elementIcon"',0,null,$rec[$i]["invoice_payment_refNumber"],null,self::deleteRow),
),
array(
array("Payment Received",null,'class="clDateReceived elementIcon" onblur="validateElement(\'date\',this)" class="dateFld" validateElement(\'date\',this)',0,null,$rec[$i]["invoice_payment_dreceived"],null,self::dateReceived,null,'YYYY-MM-DD'),
array("Payment Cleared",null,'class="clDateCleared elementIcon" onblur="validateElement(\'date\',this)" class="dateFld" validateElement(\'date\',this)',0,null,$rec[$i]["invoice_payment_dcleared"],null,self::dateCleared,null,'YYYY-MM-DD'),
array("Contract",null,null,1,$this -> listContract,$rec[$i]["id_contract"]),
));
$dataElements.='<div class="frmgroup">';
$dataElements.=frmElements($elements);
$dataElements.='</div>';
}
return $dataElements;
}
function getFooter() {
$elements = 
array(
array(
array(null,'payment_mText',null,6,null,null,null,null,' style="display:none" '),
)
);
$footerElements=frmElements($elements);
return $footerElements;
}
function build() {
$this->getLists();
$fieldset = '<fieldset id="payment" class="multiRow">';
$fieldset .= '<legend>Payment</legend>';
$fieldset .= '<div class="multimenu">';
$fieldset .= '<input type="button" onClick="multiRowAddElement(this,\'frmgroup\');disableRemoveLast(\'#payment\',\'.frmgroup\',\'.clRemoveButton\')" class="clAddButton" value="Add"/>';
$fieldset .= '<input type="button" onClick="multiRowRemoveLastElement(this,\'frmgroup\');disableRemoveLast(\'#payment\',\'.frmgroup\',\'.clRemoveButton\')" class="clRemoveButton" value="Remove Last"/>';
$fieldset .= '</div>';
$fieldset .= $this -> getBaseElement() ;
$fieldset .= $this -> getElements();
$fieldset .= $this -> getFooter();
$fieldset .= '</fieldset><script>disableRemoveLast("#payment",".frmgroup",".clRemoveButton")</script>';
echo $fieldset;
}
}
?>