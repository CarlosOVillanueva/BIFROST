<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/



class contactMethods {
public $filter = null;
public $elements = null;
public $id = null;
const deleteRow = '<span class="elementIconBox" onclick="multiRowRemoveActiveElement(this,\'frmgroup\')"><i class="fa fa-trash"></i></span>';
function getRecords() {
if (is_null($this -> filter) || is_null($this -> id)) {
return false;
}
$gdbo = new ArcDB;
$gdbo -> dbConStr=$GLOBALS['globalDBCON'];
$gdbo -> dbType = $GLOBALS['globalDBTP'];
$gdbo -> dbSchema = $GLOBALS['globalDB'];
$gdbo -> sql = "
SELECT 
_contact_method.id_contact_method_tp,
_contact_method_tp.contact_method_tp_jsMap,
_contact_method.contact_method,
_contact_method.id_cust_branch,
_contact_method.contact_method_notes
FROM _contact_method
LEFT JOIN _contact_method_tp 
ON _contact_method.id_contact_method_tp = _contact_method_tp.id_contact_method_tp
WHERE ".$this -> filter ."=" . $this -> id;
$gdbo -> getRec();
return $gdbo -> getAssociative();
}
function getMethods() {
$gdbo = new ArcDB;
$gdbo -> dbConStr=$GLOBALS['globalDBCON'];
$gdbo -> dbType = $GLOBALS['globalDBTP'];
$gdbo -> dbSchema = $GLOBALS['globalDB'];
$gdbo -> sql = "
SELECT
a.id_contact_method_tp,
a.contact_method_tp,
b.id_contact_method_tp_grp,
b.contact_method_tp_grp
FROM
_contact_method_tp a
JOIN
_contact_method_tp_grp b ON a.id_contact_method_tp_grp=b.id_contact_method_tp_grp
ORDER BY contact_method_tp_grp,contact_method_tp
";
$gdbo -> getRec();
$results = $gdbo -> dbData;
return $results;
}
function getBaseElement() {
/*
* Element Attributes
* 0-Label str
* 1-NodeID str
* 2-Node Attributes str
* 3-Node Type int
* 4-Value List array
* 5-Default Value str
* 6-DivID
* 7-Text
*/
if (!is_null($this -> elements)){
return frmElements($this -> elements);
} else { 
$listMethods = $this -> getMethods();
$elements=
array(
array(
array("*Method",null,"onchange=\"setElementValidation(this)\"",1,$listMethods),
array("*Contact",null,null,0),
array("Tag",null,'class="elementIcon"',0,null,null,null,self::deleteRow),
)
);
$baseElements='<div class="frmgroup" style="display:none">';
$baseElements.= frmElements($elements);
$baseElements.='</div>';
return $baseElements;
};
}
function getElements() {
if (!is_null($this -> elements)) {
return frmElements($this -> elements);
} else {
$dataElements = "";
$listMethods = $this -> getMethods();
$records = $this -> getRecords();
if ($records == false) { return false;}
for ($i=0;$i<count($records);$i++) {
$elements = 
array(
array(
array("*Method",null,"onchange=\"setElementValidation(this)\"",1,$listMethods,$records[$i]["id_contact_method_tp"]),
array("*Contact",null,'onblur="validateElement(\''.$records[$i]["contact_method_tp_jsMap"].'\',this)"',0,null,$records[$i]["contact_method"]),
array("Tag",null,'class="elementIcon"',0,null,$records[$i]["contact_method_notes"],null,self::deleteRow),
));
$dataElements.='<div class="frmgroup" >';
$dataElements.=frmElements($elements);
$dataElements.='</div>';
}
return $dataElements;
}
}
function getFooter() {
$elements = 
array(
array(
array(null,'contact_method_mText',null,6,null,null,null,null,' style="display:none" '),
)
);
$footerElements=frmElements($elements);
return $footerElements;
}
function build() {
$fieldset = '<fieldset id="contact_method" class="multiRow">';
$fieldset .= '<legend>Contact Method</legend>';
$fieldset .= '<div class="multimenu">';
$fieldset .= '<input type="button" onClick="multiRowAddElement(this,\'frmgroup\');disableRemoveLast(\'#contact_method\',\'.frmgroup\',\'.clRemoveButton\')" class="clAddButton" value="Add"/>';
$fieldset .= '<input type="button" onClick="multiRowRemoveLastElement(this,\'frmgroup\');disableRemoveLast(\'#contact_method\',\'.frmgroup\',\'.clRemoveButton\')" class="clRemoveButton" value="Remove Last"/>';
$fieldset .= '</div>';
$fieldset .= $this -> getBaseElement() ;
$fieldset .= $this -> getElements();
$fieldset .= $this -> getFooter();
$fieldset .= '</fieldset><script>disableRemoveLast("#contact_method",".frmgroup",".clRemoveButton")</script>';
echo $fieldset;
}
}
?>