<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/



class ArcAvatarUpload extends ArcTbl{
public $rootpath = null;
public $folderpath = null; /*In the event uploads is not set*/
public $smodID = null;
public $recordFilter = null;
public $recordID = null;
function buildView() {
$view = '<style>input.clKnob,input.clKnob:active,input.clKnob:hover,input.clKnob:focus {-moz-box-shadow:none!important;-webkit-box-shadow:none!important;background:none!important;border:0!important;box-shadow:none!important;clear:both!important;cursor:default!important;display:inline-block!important;margin:16px 0 0 -39px !important;padding:0!important;position:absolute!important;transition:none!important;vertical-align:middle!important;white-space:normal!important;width:29px!important}</style>';
$view.= '<div class="popBox"><form id="upload" method="post" action="/_lib/php/ArcAvatarUpload.Control.php" enctype="multipart/form-data"><fieldset id="files"><legend>Files</legend><div id="uploadcontainer"><div id="drop"><p>Drag\'n drop file into this box</p><p>or</p><p><input id="browse" type="button" value="Select File"/><input type="button" value="Close" onclick="clearPop()"/></p><input type="hidden" name="fso_pk" value="'.$this -> recordID.'"/><input type="hidden" name="fso_pkcol" value="'.$this -> recordFilter.'"/><input id="filebutton" type="file" name="upl"/><p style="font-size:10pt"><cite>Please only upload PNG, GIF or JPG</cite></p></div><table id="uploaded"></table></div></fieldset><div id="ulDebug"></div></form></div>';
$script = '<script type="text/javascript">$(function(){function t(e){if(typeof e!=="number"){return""}if(e>=1e9){return(e/1e9).toFixed(2)+" GB"}if(e>=1e6){return(e/1e6).toFixed(2)+" MB"}return(e/1e3).toFixed(2)+" KB"}var e=$("#uploaded");$("#browse").click(function(){$("#filebutton").click()});$("#upload").fileupload({dropZone:$("#drop"),add:function(n,r){var i=$(\'<tr class="working"><td style="width:50px"><input type="text" value="0" data-width="50" data-height="50"\'+\' data-fgColor="#39516b" data-readOnly="1" data-bgColor="#F6F6F6" class="clKnob"/></td><td style="text-align:left"><span class="clDataSize"></span></td><td style="text-align:right"><input type="button" class="clDataStatus" value="Cancel"></td></tr>\');i.find("span.clDataSize").text(r.files[0].name).append("</br><i>"+t(r.files[0].size)+"</i>");r.context=i.appendTo(e);i.find(".clKnob").knob();i.find(".clDataStatus").click(function(){if(i.hasClass("working")){s.abort()}i.fadeOut(function(){i.remove()})});var s=r.submit()},progress:function(e,t){var n=parseInt(t.loaded/t.total*100,10);t.context.find(".clKnob").val(n).change();if(n==100){
arc("avatar","/_mod/smod_'.$this -> smodID.'/get_avatar.php","'.$this -> recordFilter.'='.$this -> recordID.'",1,1);t.context.removeClass("working");t.context.find(".clDataStatus").val("Done")}},fail:function(e,t){t.context.addClass("error")}});$(document).on("drop dragover",function(e){e.preventDefault()})})</script>';
return $view.$script;
}
function getFiles() {
$fullPath = $this -> rootpath;
$files = scandir($fullPath);
$objects = "";
$i=0;
foreach ($files as $fso) {
if ($i > 1) {
$path = encrypt($this -> folderpath . $fso);
$oFile = stat($fullPath);
$dc = date("D, d M Y H:i:s", $oFile['ctime']);
$objects.= '<div><a href="javascript:window.open(\'/_lib/php/downloadFSO.php?path='.$path.'\',\'_self\')">'.$fso.'</a>-'.$dc.'</div>';
}
$i++;
}
return $objects;
}
}
?>