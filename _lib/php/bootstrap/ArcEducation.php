<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/



class Education {
public $filter = null;
public $elements = null;
public $id = null;
const deleteRow = '<span class="elementIconBox" onclick="multiRowRemoveActiveElement(this,\'frmgroup\');disableRemoveLast(\'#education\',\'.frmgroup\',\'.clRemoveButton\')"><i class="fa fa-trash"></i></span>';
const dateStarted ='<div class="elementIconBox" onclick="openCalendar(this,\'interface\',null,\'clDateStarted\')"><i class="fa fa-calendar"></i></div>';
const dateAttained='<div class="elementIconBox" onclick="openCalendar(this,\'interface\',null,\'clDateAttained\')"><i class="fa fa-calendar"></i></div>';
function getRecords() {
$gdbo = new ArcDb;
$gdbo -> dbConStr=$GLOBALS['globalDBCON'];
$gdbo -> dbType = $GLOBALS['globalDBTP'];
$gdbo -> dbSchema = $GLOBALS['globalDB'];
if (is_null($this -> filter) || is_null($this -> id)) {
return false;
}
$gdbo -> sql = "
SELECT
id_hr_degree,
id_hr_degree_level,
id_hr_emp,
hr_degree_ds,
hr_degree_da,
id_hr_degree_status,
hr_degree,
id_cust_company,
id_cust_branch,
hr_degree_hours
FROM
_hr_degree
WHERE ".$this -> filter ."=" . $this -> id;
$gdbo -> getRec();
$results= $gdbo -> getAssociative();
return $results;
}
function getLists() {
$gdbo = new ArcDb;
$gdbo -> dbConStr=$GLOBALS['globalDBCON'];
$gdbo -> dbType = $GLOBALS['globalDBTP'];
$gdbo -> dbSchema = $GLOBALS['globalDB'];
# Degree Level
require("_model/dboDegreeLevel.php");
$gdbo -> getRec();
$this -> listDegreeLevel = $gdbo -> dbData;
# Campuses
require("_model/dboCampus.php");
$gdbo -> getRec();
$this -> listCampus = $gdbo -> dbData;
$this -> listCampusJSON = json_encode($this -> listCampus);
echo '<script>window.listCampusJSON=\''.$this -> listCampusJSON.'\'</script>';
# Education Status
require("_model/dboEducationStatus.php");
$gdbo -> getRec();
$this -> listEducationStatus = $gdbo -> dbData;
# University
require("_model/dboUniversity.php");
$gdbo -> getRec();
$this -> listUniversity = $gdbo -> dbData;
return true;
}
function getBaseElement(){
$elements=
array(
array(
array("*Status",null,null,1,$this -> listEducationStatus),
array("*Degree Level",null,null,1,$this -> listDegreeLevel),
array("Hours Completed",null,null,0),
array("*Degree",null,'class="elementIcon"',0,null,null,null,self::deleteRow),
),
array(
array("*University",null,'onchange="filterRecJS(this,window.listCampusJSON,\'frmgroup\',\'chCampus\')"',1,$this -> listUniversity),
array("*Campus",null,'class="chCampus"',1),
array("*Start Date",null,'class="clDateStarted elementIcon" validateElement(\'date\',this)',0,null,null,null,self::dateStarted,null,'YYYY-MM-DD'),
array("Date Attained",null,'class="clDateAttained elementIcon" validateElement(\'date\',this)',0,null,null,null,self::dateAttained,null,'YYYY-MM-DD'),
)
);
$baseElements='<div class="frmgroup" style="display:none">';
$baseElements.= frmElements($elements);
$baseElements.='</div>';
return $baseElements;
}
function getElements(){
$dataElements=(isset($dataElements)?$dataElements:"");
$records = $this -> getRecords();
if ($records == false) {
return false;
}
for ($i=0;$i<count($records);$i++) {
$campusFiltered = array();
$listCampus = $this -> listCampus;
for ($ii=0;$ii<count($listCampus);$ii++) {
if($listCampus[$ii][2]==$records[$i]["id_cust_company"]) {
$campusFiltered[]=array($listCampus[$ii][0],$listCampus[$ii][1]);
}
}
$elements=
array(
array(
array("*Status",null,null,1,$this -> listEducationStatus,$records[$i]["id_hr_degree_status"]),
array("*Degree Level",null,null,1,$this -> listDegreeLevel,$records[$i]["id_hr_degree_level"]),
array("Hours Completed",null,null,0,null,$records[$i]["hr_degree_hours"]),
array("*Degree",null,'class="elementIcon"',0,null,$records[$i]["hr_degree"],null,self::deleteRow),
),
array(
array("*University",null,'onchange="filterRecJS(this,window.listCampusJSON,\'frmgroup\',\'chCampus\')"',1,$this -> listUniversity,$records[$i]["id_cust_company"]),
array("*Campus",null,'class="chCampus"',1,$campusFiltered,$records[$i]["id_cust_branch"]),
array("*Start Date",null,'onblur="validateElement(\'date\',this)" class="clDateStarted elementIcon" validateElement(\'date\',this)',0,null,$records[$i]["hr_degree_ds"],null,self::dateStarted,null,'YYYY-MM-DD'),
array("Date Attained",null,'onblur="validateElement(\'date\',this)" class="clDateAttained elementIcon" validateElement(\'date\',this)',0,null,$records[$i]["hr_degree_da"],null,self::dateAttained,null,'YYYY-MM-DD'),
)
);
$dataElements.='<div class="frmgroup" >';
$dataElements.=frmElements($elements);
$dataElements.='</div>';
}
return $dataElements;
}
function getFooter() {
$elements = 
array(
array(
array(null,'degree_mText',null,6,null,null,null,null,' style="display:none" '),
)
);
$footerElements=frmElements($elements);
return $footerElements;
}
function build() {
$this->getLists();
$fieldset = '<fieldset id="education" class="multiRow">';
$fieldset .= '<legend>Education</legend>';
$fieldset .= '<div class="multimenu">';
$fieldset .= '<input type="button" onClick="multiRowAddElement(this,\'frmgroup\');disableRemoveLast(\'#education\',\'.frmgroup\',\'.clRemoveButton\')" class="clAddButton" value="Add"/>';
$fieldset .= '<input type="button" onClick="multiRowRemoveLastElement(this,\'frmgroup\');disableRemoveLast(\'#education\',\'.frmgroup\',\'.clRemoveButton\')" class="clRemoveButton" value="Remove Last"/>';
$fieldset .= '</div>';
$fieldset .= $this -> getBaseElement() ;
$fieldset .= $this -> getElements();
$fieldset .= $this -> getFooter();
$fieldset .= '</fieldset><script>disableRemoveLast("#education",".frmgroup",".clRemoveButton")</script>';
echo $fieldset;
}
}
?>