<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/



function getAssociativeArray($cols,$data) {
if ($cols == null || $data == null) {
return false;
}
$colsTrimmed = array();
foreach ($cols as $row) {
$colsTrimmed[] = $row['name'];
}
$combined = array();
foreach ($data as $row) {
$newrow = array_combine($colsTrimmed, $row);
$combined[] = $newrow;
}
return $combined;
}
function array2sqlstring($arrayObject = null,$required=null){
if (!is_null($arrayObject)) {
if (!is_null($required)) {
$required = explode(",",$required);
}
$cleanArray = array();
foreach($arrayObject as $data => $row) {
$fail = false;
if(!empty($required)){
foreach($required as $key => $id) {
if (!isset($arrayObject[$data][$id]) || $arrayObject[$data][$id]=="" || $arrayObject[$data][$id]=="--") {
$fail = true;
} 
}}
if ($fail==false){
$cleanArray[]=$arrayObject[$data]; 
}
}
$inc = 1;
$sqlString="";
foreach ($cleanArray as $row => $data) {
$comma = ($inc != 1?",":"");
$inc = 1 + $inc;
$childinc = 1;
$sqlString.=$comma;
$sqlString.="(";
foreach($data as $column => $value) {
$childcomma = ($childinc != 1?",":"");
$childinc = 1 + $childinc;
$sqlString.=$childcomma;
if (!empty($value)){
$sqlString.="'".$value."'";
} else {
$sqlString.='NULL';
}
}
$sqlString.=")";
}
return $sqlString;
}
else {
return null;
}
}
function frmElements($multiArray) {
/*
* Element Attributes
* 0-Label str
* 1-NodeID str
* 2-Node Attributes str
* 3-Node Type int
* 4-Value List array
* 5-Default Value str
* 6-DivID
* 7-Text
* 8-Row Attributes
* 9-Place Holder text
* 10-Hidden
* 11-parentattr
*/
$foo = "";
foreach($multiArray as $pArray => $arrayName){
$rowAttributes = (isset($arrayName[0][8])?$arrayName[0][8]:null);
$foo .= '<div class="frmrow" ' . $rowAttributes . '>';
foreach ($arrayName as $element) {
$divID = (isset($element[6]) ? ' id="' .$element[6] . '"':null);
$default = (isset($element[5]) ? $element[5] : null);
$attributes = (isset($element[2]) ? $element[2] : null);
$id = (isset($element[1]) ? $element[1] :null);
$label = (isset($element[0]) ? ' <label>' . $element[0] . '</label>':null);
$nameAttr = (!is_null($id) ? 'name="' . $id . '"':null);
$idAttr = (!is_null($id) ? 'id="' . $id . '"':null);
$value = (!is_null($default)? 'value="' . $default . '"':null);
$sideText = (isset($element[7]) ? $element[7] :null);
$placeholder = (isset($element[9]) ? 'placeholder="'.$element[9].'"' :null);
$hiddenText = (isset($element[10]) ? $element[10] :null);
$parentattr = (isset($element[11]) ? $element[11] :null);
switch($element[3]) {
case 0 :
$foo.= '<div class="frmcol" '.$parentattr.'>' . $label . '<span ' . $divID . '>' . '<input type="text" ' . $attributes . ' '. $placeholder . $idAttr . $nameAttr . $value . '/>'.$hiddenText .'</span>'.$sideText.'</div>';
break;
case 1 :
$foo.= '<div class="frmcol">' . $label . '<span ' . $divID . '>' . selList($element[4], $id , null, $attributes, null, null, $default) . '</span>'.$sideText.'</div>';
break;
case 2 :
$foo.= '<input ' . $idAttr . ' type="button" ' . $attributes . $value . '/>'.$sideText;
break;
case 3 :
$foo.= '<input ' . $idAttr . $nameAttr . $attributes . ' type="hidden" '. $value . '/>';
break;
case 4 :
$foo.= '<div class="frmcol">' . $label;
foreach($element[4] as $radList => $radValue){
$selected=(isset($element[5]) && $element[5]==$radValue[0]?'checked="checked"':null);
$value = 'value="'.$radValue[0].'"';
$foo.= '<input type="radio" '. $selected . $nameAttr . $value . '/><span>'.$radValue[1]."</span>";
}
$foo.= $sideText.'</div>'; 
break;
case 5:
$foo.= '<div class="frmcol">' . $label . '<img '. $idAttr . $attributes . ' src="' . $default . '"/>'.$sideText.'</div>';
break;
case 6 :
$foo.= '<div class="text">' . $label . '<textarea ' . $idAttr .' '. $nameAttr . ' ' . $attributes .'>' . $default . '</textarea>'.$sideText.'</div>';
break;
case 7:
$foo.= '<div class="frmcol" '.$parentattr.'>' . $label . '<span ' . $divID . '>' . '<input type="password" ' . $attributes . ' '. $placeholder . $idAttr . $nameAttr . $value . '/>'.$hiddenText .'</span>'.$sideText.'</div>';
break;
case 8:
$selected=($default == 1?'checked="checked"':'');
$foo.= '<div class="frmcol">' . $label . '<input type="checkbox" ' . $selected .' '. $nameAttr . ' ' .$attributes . ' value="1"/>' . $sideText . '</div>';
break;
case 9:
$foo.= '<div class="frmcol" '.$id.' '.$attributes.'>'.$element[0].'</div>';
break;
}
}
$foo.= '</div>';
}
return $foo;
}
function buildElements($multiArray) {
/*
* Element Attributes
* 0-Label str
* 1-NodeID str
* 2-Node Attributes str
* 3-Node Type int
* 4-Value List array
* 5-Default Value str
* 6-DivID
* 7-Text
*/
foreach($multiArray as $pArray => $arrayName){
echo '<div class="frmrow" >';
foreach ($arrayName as $element) {
$divID = (isset($element[6]) ? "id='".$element[6]."'" : null);
$default = (isset($element[5]) ? $element[5] : null);
switch($element[3]) {
case 0 :
/*text*/
if ($default!=null){
$value="value=\"".$default."\"";
} else {
$value="";
}
echo "<div class=\"frmcol\"><label>" . $element[0] . "</label><input " . $element[2] . " type=\"text\" id=\"" . $element[1] . "\" name=\"" . $element[1] . "\"" .$value."/></div>";
break;
case 1 :
/*list*/
echo "<div class=\"frmcol\"><label>" . $element[0] . "</label><span $divID >" . selList($element[4], $element[1], null, $element[2], null, null, $default) . "</span></div>";
break;
case 2 :
/*button*/
echo '<div class="frmcol"><div>&nbsp;</div><input id="' . $element[1] . '" type="button" ' . $element[2] . ' value="' . $default . '" /></div>';
break;
case 3 :
/*hidden*/
echo "<div class=\"frmcol\"><label>" . $element[0] . "</label><input " . $element[2] . " type=\"hidden\" id=\"" . $element[1] . "\" name=\"" . $element[1] . "\"/></div>";
break;
case 4 :
/*radio*/
echo "<div class=\"frmcol\"><label>" . $element[0] . "</label>";
foreach($element[4] as $radList => $radValue){
$selected=($element[5]==$radValue[0]?"checked=\"checked\"":"");
echo '<input type="radio" '.$selected.' name="'.$element[1].'" value="'.$radValue[0].'"/>'.$radValue[1];
}
echo "</div>"; 
break;
case 5:
/*image*/
echo '<div class="frmcol"><img id="' . $element[1] . '"' . $element[2] . ' src="' . $element[0] . '"/></div>';
break;
case 6 :
/*textarea*/
if ($default!=null){
$value=hex2str($default);
} else {
$value="";
}
echo "<div class=\"frmcol\"><label>" . $element[0] . "</label><textarea " . $element[2] . " id=\"" . $element[1] . "\" name=\"" . $element[1] . "\">" .$value."</textarea></div>";
break;
case 7:
/*password*/
if ($default!=null){
$value="value=\"".$default."\"";
} else {
$value="";
}
echo "<div class=\"frmcol\"><label>" . $element[0] . "</label><input " . $element[2] . " type=\"password\" id=\"" . $element[1] . "\" name=\"" . $element[1] . "\"" .$value."/></div>";
break;
case 8:
/*checkbox*/
$element[7]=(!isset($element[7])?'':$element[7]);
$selected=($element[5]==1?"checked=\"checked\"":"");
echo '<div '.$divID.' class="frmcol"><label>' . $element[0] . '</label><input type="checkbox" '.$selected.' name="'.$element[1].'" value="1"/>'.$element[7].'</div>';
break;
}
}
echo '</div>';
}
}
function browser() {
$ua = strtolower($_SERVER['HTTP_USER_AGENT']);
if (preg_match('/(chromium)[ \/]([\w.]+)/', $ua))
$browser = 'chromium';
elseif (preg_match('/(chrome)[ \/]([\w.]+)/', $ua))
$browser = 'chrome';
elseif (preg_match('/(safari)[ \/]([\w.]+)/', $ua))
$browser = 'safari';
elseif (preg_match('/(opera)[ \/]([\w.]+)/', $ua))
$browser = 'opera';
elseif (preg_match('/(msie)[ \/]([\w.]+)/', $ua))
$browser = 'msie';
elseif (preg_match('/(mozilla)[ \/]([\w.]+)/', $ua))
$browser = 'mozilla';
preg_match('/(' . $browser . ')[ \/]([\w]+)/', $ua, $version);
return $browser;
}
function printArray($array) {
extract($GLOBALS);
if(isset($debug) && $debug==1)
echo "<pre>" . print_r($array, true) . "</pre>";
}
function arrayIndexSize(&$arrayName) {
return (count($arrayName, COUNT_RECURSIVE) - count($arrayName)) / count($arrayName);
}
function dateMod($time, $inc = null) {
$date = new DateTime($time);
if (isset($inc)) {
date_modify($date, '-' . $inc . ' hour');
}
return date_format($date, "Y-m-d H:i");
}
function isChecked($val) {
if ($val != 0) {
echo " checked='checked' ";
} else {
echo "";
}
}
function isBoo($record,$row) {
if ($record == $row) {
return 'checked="checked"';
}
}
function nullTimeStamp($timstamp) {
if ($timstamp == "0000-00-00 00:00:00") {
return "";
} else {
return $timstamp;
}
}
function selList(&$arrayName, $recID = null, $scr = null, $attrib = null, $stVal = null, $altID = null, $edtVal = null, $override=false) {
$stVal = (!is_null($stVal) ? $stVal : "--");
$altID = (!is_null($altID) ? $altID : $recID);
$recID = (!is_null($recID) ? "id=\"$altID\" name=\"$recID\"" : "");
$attrib = (!is_null($attrib) ? str_replace("%3D", "=", $attrib) : "");
$onCh = (!is_null($scr) ? "onchange=\"$scr\"" : "");
$edtVal = (!is_null($edtVal) ? $edtVal : "");
$options = "";
$frmSelect = "<select " . $attrib . " " . $recID . " " . $onCh . ">";
if ($override === false) {
$frmSelect .= "<option value=\"\">" . $stVal . "</option>";
}
$fsHeader = "";
$fsFooter = "";
for ($i = 0; $i < count($arrayName); $i++) {
if (isset($arrayName[$i][2])) {
$cntAhead = $i + 1;
$cntBack = $i - 1;
$cnt = ($i == 0 ? $cntAhead : $cntBack);
if ($i == 0 || $arrayName[$i][3] != $arrayName[$cnt][3]) {
$fsHeader = "<optgroup label=\"" . $arrayName[$i][3] . "\">";
$fsFooter = "";
} elseif ($i > 0 && $arrayName[$i][2] != $arrayName[$cnt][2]) {
$fsHeader = "</optgroup><optgroup label=\"" . $arrayName[$i][3] . "\">";
$fsFooter = "";
} elseif ($i == 0 && count($arrayName) == 1) {
$fsHeader = "<optgroup label=\"" . $arrayName[$i][3] . "\">";
$fsFooter = "</optgroup >";
} elseif ($i == (count($arrayName) - 1) && $arrayName[$i][2] == $arrayName[$cnt][2]) {
$fsHeader = "";
$fsFooter = "</optgroup >";
} else {
$fsFooter = "";
$fsHeader = "";
}
} $objID = $arrayName[$i][0];
$selected = (($edtVal == $objID) ? "selected=\"selected\"" : "");
$frmSelect .= $fsHeader;
$frmSelect .= "<option value=\"" . $arrayName[$i][0] . "\" $selected>" . $arrayName[$i][1] . "</option>";
$frmSelect .= $fsFooter;
} $frmSelect .= "</select>";
return $frmSelect;
}
function multiArraySearch($haystack, $needle, $index = null) {
$aIt = new RecursiveArrayIterator($haystack);
$it = new RecursiveIteratorIterator($aIt);
while ($it -> valid()) {
if (((isset($index) and ($it -> key() == $index)) or (!isset($index))) and ($it -> current() == $needle)) {
return $aIt -> key();
} $it -> next();
}
return false;
}
function listEllipse($a) {
return (strlen($a) > 27 ? substr($a, 0, 24) . "..." : $a);
}
function sec2hms($sec, $padHours = false) {
$hms = "";
$hours = intval(intval($sec) / 3600);
$hms .= ($padHours) ? str_pad($hours, 2, "0", STR_PAD_LEFT) . ":" : $hours . ":";
$minutes = intval(($sec / 60) % 60);
$hms .= str_pad($minutes, 2, "0", STR_PAD_LEFT) . ":";
$seconds = intval($sec % 60);
$hms .= str_pad($seconds, 2, "0", STR_PAD_LEFT);
return $hms;
}
function dbDate($timezone = null) {
$timezone = (is_null($timezone) ? "UTC" : $timezone);
date_default_timezone_set($timezone);
return date("Y-m-d H:i:s");
}
function hex2str($hex) {
$str = '';
for ($i = 0; $i < strlen($hex) - 1; $i += 2) {
$str .= chr(hexdec($hex[$i] . $hex[$i + 1]));
}
return $str;
}
function array_unique_multi($input) {
$serialized = array_map('serialize', $input);
$unique = array_unique($serialized);
$new = array_intersect_key($input, $unique);
return array_values($new);
}
function ipHosts($b) {
$d = "";
$g = 32 - $b;
for ($i = 0; $i < $g; $i++) {
$d = $d . "1";
}
return $d;
}
function base64_encode_image($filename = 'string', $filetype = 'string') {
if ($filename) {
$imgbinary = fread(fopen($filename, "r"), filesize($filename));
return 'data:image/' . $filetype . ';base64,' . base64_encode($imgbinary);
}
}
function super_unique($array) {
$result = array_map("unserialize", array_unique(array_map("serialize", $array)));
foreach ($result as $key => $value) {
if (is_array($value)) {
$result[$key] = super_unique($value);
}
}
return $result;
}
function beginsWith($str, $sub) {
return (substr($str, 0, strlen($sub)) == $sub);
}
function endsWith($str, $sub) {
return (substr($str, strlen($str) - strlen($sub)) == $sub);
}
$salt = "232sd@$";
function encrypt($str) {
$key = $GLOBALS['salt'];
$block = mcrypt_get_block_size('des', 'ecb');
$pad = $block - (strlen($str) % $block);
$str .= str_repeat(chr($pad), $pad);
return bin2hex(mcrypt_encrypt(MCRYPT_DES, $key, $str, MCRYPT_MODE_ECB));
}
function decrypt($str) {
$key = $GLOBALS['salt'];
$str = hex2str($str);
$str = mcrypt_decrypt(MCRYPT_DES, $key, $str, MCRYPT_MODE_ECB);
$block = mcrypt_get_block_size('des', 'ecb');
$pad = ord($str[($len = strlen($str)) - 1]);
return substr($str, 0, strlen($str) - $pad);
}
function formatFileSize($bytes) {
if (!is_integer($bytes)) {
return '';
}
if ($bytes >= 1000000000) {
return round($bytes / 1000000000,2) . ' GB';
}
if ($bytes >= 1000000) {
return round($bytes / 1000000,2) . ' MB';
}
return round($bytes / 1000,2) . ' KB';
}
function filterQry($qrystr) {
if (isset($_GET[$qrystr])) {
$qry_data = $_GET[$qrystr];
$qry_data = str_replace(";", "", $qry_data);
$qry_data = str_replace(":", "", $qry_data);
$qry_data = str_replace("'", "''", $qry_data);
$qry_data = (string) $qry_data;
eval("global $" . $qrystr . ";");
eval("$" . $qrystr . "=\"" . $qry_data . "\";");
}
}
function filterQryPost($qrystr) {
if (isset($_POST[$qrystr])) {
$qry_data = $_POST[$qrystr];
$qry_data = str_replace(";", "", $qry_data);
$qry_data = str_replace(":", "", $qry_data);
$qry_data = str_replace("'", "''", $qry_data);
eval("global $" . $qrystr . ";");
eval("$" . $qrystr . "='" . $qry_data . "';");
} else {
eval("global $" . $qrystr . ";");
eval("$" . $qrystr . "=null;");
}
}
function downloadFile($fullPath,$auxname=null){

header('Content-Type: application/octet-stream');
header('Content-Length: ' . filesize($fullPath));
header('Content-Disposition: attachment; filename="'.$auxname.'"');
header('Content-Transfer-Encoding: binary');
header('Connection: Keep-Alive');
header('Expires: 0');
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
header('Pragma: public');
set_time_limit(0);
#ob_clean();
$file = $fullPath;
readfile("$file");
}
?>
