<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/



class Certification {
public $filter = null;
public $elements = null;
public $id = null;
const deleteRow = '<span class="elementIconBox" onclick="multiRowRemoveActiveElement(this,\'frmgroup\');disableRemoveLast(\'#certification\',\'.frmgroup\',\'.clRemoveButton\')"><i class="fa fa-trash"></i></span>';
const dateStarted ='<div class="elementIconBox" onclick="openCalendar(this,\'interface\',null,\'clDateStarted\')"><i class="fa fa-calendar"></i></div>';
const dateAttained='<div class="elementIconBox" onclick="openCalendar(this,\'interface\',null,\'clDateAttained\')"><i class="fa fa-calendar"></i></div>';
function getRecords() {
$gdbo = new ArcDb;
$gdbo -> dbConStr=$GLOBALS['globalDBCON'];
$gdbo -> dbType = $GLOBALS['globalDBTP'];
$gdbo -> dbSchema = $GLOBALS['globalDB'];
if (is_null($this -> filter) || is_null($this -> id)) {
return false;
}
$gdbo -> sql = "SELECT
id_hr_certification,
id_hr_certification_tp,
id_hr_emp,
id_hr_certification_status,
hr_certification_ds,
hr_certification_da,
id_hr_certification_provider,
hr_certification_hours
FROM
_hr_certification
WHERE ".$this -> filter ."=" . $this -> id;
$gdbo -> getRec();
$results= $gdbo -> getAssociative();
return $results;
}
function getLists() {
$gdbo = new ArcDb;
$gdbo -> dbConStr=$GLOBALS['globalDBCON'];
$gdbo -> dbType = $GLOBALS['globalDBTP'];
$gdbo -> dbSchema = $GLOBALS['globalDB'];
# Certification Type
require("_model/dboCertification.php");
$gdbo -> getRec();
$this -> listCertification = $gdbo -> dbData;
$this -> listCertificationJSON = json_encode($this -> listCertification);
echo '<script>window.listCertificationJSON=\''.$this -> listCertificationJSON.'\'</script>';
# Education Status
require("_model/dboEducationStatus.php");
$gdbo -> getRec();
$this -> listEducationStatus = $gdbo -> dbData;
# Provider
require("_model/dboCertificationProvider.php");
$gdbo -> getRec();
$this -> listProvider = $gdbo -> dbData;
return true;
}
function getBaseElement(){
$elements=
array(
array(
array("*Status",null,null,1,$this -> listEducationStatus),
array("*Provider",null,'onchange="filterRecJS(this,window.listCertificationJSON,\'frmgroup\',\'chCertificate\')"',1,$this -> listProvider),
array("*Certification",null,'class="elementIcon chCertificate elementIcon"',1,null,null,null,self::deleteRow),
),
array(
array("Certification Hours",null,null,0),
array("Date Started",null,'class="clDateStarted elementIcon" validateElement(\'date\',this)',0,null,null,null,self::dateStarted),
array("Date Attained",null,'onblur="validateElement(\'date\',this)" class="clDateAttained elementIcon" validateElement(\'date\',this)',0,null,null,null,self::dateAttained),
),
);
$baseElements='<div class="frmgroup" style="display:none">';
$baseElements.= frmElements($elements);
$baseElements.='</div>';
return $baseElements;
}
function getElements(){
$dataElements=(isset($dataElements)?$dataElements:"");
$records = $this -> getRecords();
if ($records == false) {
return false;
}
for ($i=0;$i<count($records);$i++) {
$campusFiltered = array();
$certificationFiltered = array();
$listCertification = $this -> listCertification;
for ($ii=0;$ii<count($listCertification);$ii++) {
if($listCertification[$ii][2]==$records[$i]["id_hr_certification_provider"]) {
$certificationFiltered[]=array($listCertification[$ii][0],$listCertification[$ii][1]);
}
}
$elements=
array(
array(
array("*Status",null,null,1,$this -> listEducationStatus,$records[$i]["id_hr_certification_status"]),
array("*Provider",null,'onchange="filterRecJS(this,window.listCertificationJSON,\'frmgroup\',\'chCertificate\')"',1,$this -> listProvider,$records[$i]["id_hr_certification_provider"]),
array("*Certification",null,'class="elementIcon chCertificate"',1,$certificationFiltered,$records[$i]["id_hr_certification_tp"],null,self::deleteRow),
),
array(
array("Certification Hours",null,null,0,null,$records[$i]["hr_certification_hours"]),
array("Date Started",null,'class="elementIcon dateFld" validateElement(\'date\',this)',0,null,$records[$i]["hr_certification_ds"],null,self::dateStarted),
array("Date Attained",null,'onblur="validateElement(\'date\',this)" class="elementIcon dateFld" validateElement(\'date\',this)',0,null,$records[$i]["hr_certification_da"],null,self::dateAttained),
),
);
$dataElements.='<div class="frmgroup" >';
$dataElements.=frmElements($elements);
$dataElements.='</div>';
}
return $dataElements;
}
function getFooter() {
$elements = 
array(
array(
array(null,'certification_mText',null,6,null,null,null,null,' style="display:none" '),
)
);
$footerElements=frmElements($elements);
return $footerElements;
}
function build() {
$this->getLists();
$fieldset = '<fieldset id="certification" class="multiRow">';
$fieldset .= '<legend>Certification</legend>';
$fieldset .= '<div class="multimenu">';
$fieldset .= '<input type="button" onClick="multiRowAddElement(this,\'frmgroup\');disableRemoveLast(\'#certification\',\'.frmgroup\',\'.clRemoveButton\')" class="clAddButton" value="Add"/>';
$fieldset .= '<input type="button" onClick="multiRowRemoveLastElement(this,\'frmgroup\');disableRemoveLast(\'#certification\',\'.frmgroup\',\'.clRemoveButton\')" class="clRemoveButton" value="Remove Last"/>';
$fieldset .= '</div>';
$fieldset .= $this -> getBaseElement() ;
$fieldset .= $this -> getElements();
$fieldset .= $this -> getFooter();
$fieldset .= '</fieldset><script>disableRemoveLast("#certification",".frmgroup",".clRemoveButton")</script>';
echo $fieldset;
}
}
?>