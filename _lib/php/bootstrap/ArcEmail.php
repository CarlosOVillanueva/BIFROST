<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/



class ArcEmail {
public $message = null;
public $subject = null;
public $fromEmail = null;
public $toEmail = null;
public $from = null;
public $headers = array();
public $identifier = null;
const salt = "232sd@$";
function hex2str($hex) {
$str = '';
for ($i = 0; $i < strlen($hex) - 1; $i += 2) {
$str .= chr(hexdec($hex[$i] . $hex[$i + 1]));
}
return $str;
}
function encrypt($str) {
$key = $self::salt;
$block = mcrypt_get_block_size('des', 'ecb');
$pad = $block - (strlen($str) % $block);
$str .= str_repeat(chr($pad), $pad);
return bin2hex(mcrypt_encrypt(MCRYPT_DES, $key, $str, MCRYPT_MODE_ECB));
}
function decrypt($str) {
$key = $self::salt;
$str = hex2str($str);
$str = mcrypt_decrypt(MCRYPT_DES, $key, $str, MCRYPT_MODE_ECB);
$block = mcrypt_get_block_size('des', 'ecb');
$pad = ord($str[($len = strlen($str)) - 1]);
return substr($str, 0, strlen($str) - $pad);
}
function getMessage() {
$message = $this -> message;
$message = hex2str($message);
$message = wordwrap($message, 70);
$this -> message = $message;
}
function getSubject() {
$subject = $this -> subject;
$this -> subject = $subject;
}
function buildHeaders() {
$headers = array();
$headers[] = "MIME-Version: 1.0";
$headers[] = "Content-type: text/html; charset=iso-8859-1";
$headers[] = "From: ".$this -> fromEmail;
$headers[] = "Subject: ".$this -> subject;
$headers[] = "X-Mailer: PHP/" . phpversion();
$this -> headers = $headers;
}
function addAuditRecord(){
$gdbo = new ArcDB;
$gdbo -> dbConStr=$GLOBALS['globalDBCON'];
$gdbo -> dbType = $GLOBALS['globalDBTP'];
$gdbo -> dbSchema = $GLOBALS['globalDB'];
$gdbo -> dbTable = "_email_log";
$_POST["id_sys_user"] = $GLOBALS['id_sys_user'];
$_POST["email_log_recipient"] = $this -> toEmail;
$_POST["email_log_msg"] = bin2hex($this -> message);
$_POST["email_log_subject"] = $this -> subject;
$gdbo -> insertRec();
}
function sendMessage() {
$this -> getMessage();
$this -> getSubject();
$this -> buildHeaders();
mail($this -> toEmail, $this -> subject, $this -> message, implode("\r\n", $this -> headers), "-f ".$this -> fromEmail);
$this -> addAuditRecord();
include("_error/emailstatus.php");
}
}
?>