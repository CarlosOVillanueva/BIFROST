<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/



class ArcAddress {
public $filter = null;
public $elements = null;
public $id = null;
function getRecords() {
$gdbo = new ArcDb;
$gdbo -> dbConStr=$GLOBALS['globalDBCON'];
$gdbo -> dbType = $GLOBALS['globalDBTP'];
$gdbo -> dbSchema = $GLOBALS['globalDB'];
if (is_null($this -> filter) || is_null($this -> id)) {
return false;
}
$gdbo -> sql = "SELECT
_addr.id_addr,
_addr.addr_line1,
_addr.addr_line2,
_addr.addr_line3,
_addr.id_loc_region,
_addr.loc_city,
_addr.loc_postal_code,
_addr.addr_dc,
_addr.addr_dr,
_addr.addr_label,
_addr.id_loc_country
FROM
_addr
WHERE ".$this -> filter ."=" . $this -> id;
$gdbo -> getRec();
$results= $gdbo -> getAssociative();
if ($gdbo -> dbRows >0) {
return $results; }
else {
return false;
}
}
function getLists() {
$gdbo = new ArcDb;
$gdbo -> dbConStr=$GLOBALS['globalDBCON'];
$gdbo -> dbType = $GLOBALS['globalDBTP'];
$gdbo -> dbSchema = $GLOBALS['globalDB'];
# Countries
$gdbo -> sql = "SELECT id_loc_country,loc_country FROM _loc_country";
$gdbo -> getRec();
$this -> listCountries = $gdbo -> dbData;
# Regions
$gdbo -> sql = "SELECT id_loc_region,loc_region,id_loc_country FROM _loc_region";
$gdbo -> getRec();
$this -> listRegions = $gdbo -> dbData;
# Filterable Objects
$gdbo = new ArcDb;
$gdbo -> dbConStr=$GLOBALS['globalDBCON'];
$gdbo -> dbType = $GLOBALS['globalDBTP'];
$gdbo -> dbSchema = $GLOBALS['globalDB'];
$gdbo -> sql = "SELECT id_loc_region,loc_region FROM _loc_region";
$gdbo -> dbFilter = "WHERE id_loc_country=";
$gdbo -> type = "list";
$gdbo -> id = "id_loc_region";
$gdbo -> attributes = 'class="chRegion"';
$this -> regionDBO = encrypt(json_encode((array)$gdbo));
return true;
}
function getBaseElement(){
$records = $this -> getRecords();
if ($records === false) {
$elements=
array(
array(
array('Country', 'id_loc_country','onchange="filterRec(this,\''.$this -> regionDBO.'\',\'dRegion\')"',1,$this -> listCountries),
array('State/Region', 'id_loc_region','disabled=\'disabled\' class=\'chRegion\'',1,null,null,"dRegion"),
),
array(
array('Address Line 1', 'addr_line1',null,0),
),
array(
array('Address Line 2', 'addr_line2',null,0),
),
array(
array('City', 'loc_city',null,0),
),
array(
array('Postal Code', 'loc_postal_code','onblur="validateElement(\'zip\',this)"',0),
)
);
$baseElements= frmElements($elements);
}
else {$baseElements = null;}
return $baseElements;
}
function getElements(){
$records = $this -> getRecords();
if ($records !== false) {
$dataElements="";
$regionFiltered = array();
$listRegions = $this -> listRegions;
for ($i=0;$i<count($listRegions);$i++) {
if($listRegions[$i][2]==$records[0]["id_loc_country"]) {
$regionFiltered[]=array($listRegions[$i][0],$listRegions[$i][1]);
}
}
$elements=
array(
array(
array('Country', 'id_loc_country','onchange="filterRec(this,\''.$this -> regionDBO.'\',\'dRegion\')"',1,$this->listCountries,$records[0]["id_loc_country"]),
array('State/Region', 'id_loc_region','class=\'chRegion\'',1,$regionFiltered,$records[0]["id_loc_region"],"dRegion"),
),
array(
array('Address Line 1', 'addr_line1',null,0,null,$records[0]["addr_line1"]),
array(null, 'id_addr',null,3,null,$records[0]['id_addr']),
),
array(
array('Address Line 2', 'addr_line2',null,0,null,$records[0]["addr_line2"]),
),
array(
array('City', 'loc_city',null,0,null,$records[0]["loc_city"]),
),
array(
array('Postal Code', 'loc_postal_code','onblur="validateElement(\'zip\',this)"',0,null,$records[0]["loc_postal_code"]),
)
);
$dataElements=frmElements($elements);
}else {$dataElements = null;}
return $dataElements;
}
function build() {
$this->getLists();
$fieldset = '<fieldset id="address">';
$fieldset .= '<legend>Address</legend>';
$fieldset .= $this -> getBaseElement() ;
$fieldset .= $this -> getElements();
$fieldset .= '</fieldset>';
echo $fieldset;
}
}
?>