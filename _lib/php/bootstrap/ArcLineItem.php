<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/



class LineItem {
/*
SELECT _invoice_item.id_invoice_item,
_invoice_item.invoice_item,
_invoice_item.invoice_item_qty,
_invoice_item.invoice_item_rate,
_invoice_item.id_invoice_qty_tp,
_invoice_item.invoice_item_dc,
_invoice_item.invoice_item_du,
_invoice_item.invoice_item_dservice,
_invoice_item.invoice_item_lastModifiedBy,
_invoice_item.id_invoice,
_invoice_item.id_contract,
_invoice_item.invoice_item_percentTax,
_invoice_item.invoice_item_amount,
_invoice_item.invoice_item_totalAmount,
_invoice_item.id_sys_user
FROM _invoice_item;
*/
public $filter = null;
public $elements = null;
public $id = null;
const deleteRow = '<span class="elementIconBox" onclick="multiRowRemoveActiveElement(this,\'frmgroup\');getTotals()"><i class="fa fa-trash"></i></span>';
const dateStarted ='<div class="elementIconBox" onclick="openCalendar(this,\'interface\',null,\'dateFld\')"><i class="fa fa-calendar"></i></div>';
function getrec() {
$gdbo = new ArcDb;
$gdbo -> dbConStr=$GLOBALS['globalDBCON'];
$gdbo -> dbType = $GLOBALS['globalDBTP'];
$gdbo -> dbSchema = $GLOBALS['globalDB'];
if (is_null($this -> filter) || is_null($this -> id)) {
return false;
}
$gdbo -> sql = "SELECT 
id_invoice_item,
invoice_item,
invoice_item_qty,
invoice_item_dservice,
invoice_item_rate,
id_invoice_qty_tp,
id_contract,
id_sys_user,
invoice_item_amount,
invoice_item_totalAmount,
invoice_item_percentTax,
invoice_item_lastModifiedBy,
id_invoice
FROM _invoice_item
WHERE ".$this -> filter ."=" . $this -> id. " ORDER BY id_invoice_item";
$gdbo -> getRec();
$rec= $gdbo -> getAssociative();
return $rec;
}
function getLists() {
$gdbo = new ArcDb;
$gdbo -> dbConStr=$GLOBALS['globalDBCON'];
$gdbo -> dbType = $GLOBALS['globalDBTP'];
$gdbo -> dbSchema = $GLOBALS['globalDB'];
$gdbo -> sql = "SELECT 
id_invoice_qty_tp,
invoice_qty_tp
FROM _invoice_qty_tp;
";
$gdbo -> getRec();
$this -> listQtyType = $gdbo -> dbData;
$gdbo -> sql = "SELECT
a.id_contract,
a.contract,
a.id_cust_company,
b.cust_company
FROM _contract a
LEFT JOIN _cust_company b
ON a.id_cust_company = b.id_cust_company
ORDER by b.cust_company
";
$gdbo -> getRec();
$this -> listContract = $gdbo -> dbData;
return true;
}
function getBaseElement(){
$elements=
array(
array(
array("*Item or Service",'x1_invoice_item','style="width:455px !important"',0,null,null,null,null,null,null,null,'style="width:455px !important"'),
array("Contract",'x1_id_contract',null,1,$this -> listContract),
array("Date",'x1_invoice_dservice','class="elementIcon dateFld" validateElement(\'date\',this)',0,null,null,null,self::dateStarted),
),
array(
array("*Quantity",'x1_invoice_item_qty','onblur="getLineTotal(this)"',0),
array("QTY Type",'x1_id_invoice_qty_tp',null,1,$this -> listQtyType),
array("*Rate",'x1_invoice_item_rate','onblur="getLineTotal(this)"',0,null),
array("Extension",'x1_invoice_item_amount','class="cLineAmount elementIcon" disabled="disabled"',0,null,null,null,self::deleteRow), 
),
array(
array("Tax Percentage",'x1_invoice_item_percentTax','onblur="getLineTotal(this)"',0),
array("Total Amount",'x1_invoice_item_totalAmount','class="cLineTotal" disabled="disabled"',null,0),
),
);
$baseElements='<div class="frmgroup" style="display:none">';
$baseElements.= frmElements($elements);
$baseElements.='</div>';
return $baseElements;
}
function getElements(){
$dataElements=(isset($dataElements)?$dataElements:"");
$rec = $this -> getrec();
if ($rec == false) {
return false;
}
for ($i=0;$i<count($rec);$i++) {
$inc=$i+2;
$elements=
array(
array(
array("*Item or Service",'x'.$inc.'_invoice_item','style="width:455px !important"',0,null,$rec[$i]["invoice_item"],null,null,null,null,null,'style="width:455px !important"'),
array("Contract",'x1_id_contract',null,1,$this -> listContract,$rec[$i]["id_contract"]),
array("Date",'x1_invoice_dservice','class="elementIcon dateFld" validateElement(\'date\',this)',0,null,$rec[$i]["invoice_item_dservice"],null,self::dateStarted),
),
array(
array("*Quantity",'x'.$inc.'_invoice_item_qty','onblur="getLineTotal(this)"',0,null,$rec[$i]["invoice_item_qty"]),
array("QTY Type",'x'.$inc.'_id_invoice_qty_tp',null,1,$this -> listQtyType,$rec[$i]["id_invoice_qty_tp"]),
array("*Rate",'x'.$inc.'_invoice_item_rate','onblur="getLineTotal(this)"',0,null,$rec[$i]["invoice_item_rate"]),
array("Extension",'x'.$inc.'_invoice_item_amount','class="cLineAmount elementIcon" disabled="disabled"',0,null,$rec[$i]["invoice_item_amount"],null,self::deleteRow),
),
array(
array("Tax Percentage",'x'.$inc.'_invoice_item_percentTax','onblur="getLineTotal(this)"',0,null,$rec[$i]["invoice_item_percentTax"]),
array("Total Amount",'x'.$inc.'_invoice_item_totalAmount','class="cLineTotal" disabled="disabled"',0,null,$rec[$i]["invoice_item_totalAmount"]),
),
);
$dataElements.='<div class="frmgroup">';
$dataElements.=frmElements($elements);
$dataElements.='</div>';
}
return $dataElements;
}
function getFooter() {
$elements = 
array(
array(
array(null,'lineItem_mText',null,6,null,null,null,null,' style="display:none" '),
)
);
$footerElements=frmElements($elements);
return $footerElements;
}
function build() {
$this->getLists();
$fieldset = '<fieldset id="line_items" class="multiRow">';
$fieldset .= '<legend>Line Items</legend>';
$fieldset .= '<div class="multimenu">';
$fieldset .= '<input type="button" onClick="multiRowAddElement(this,\'frmgroup\');disableRemoveLast(\'#line_items\',\'.frmgroup\',\'.clRemoveButton\')" class="clAddButton" value="Add"/>';
$fieldset .= '<input type="button" onClick="multiRowRemoveLastElement(this,\'frmgroup\');disableRemoveLast(\'#line_items\',\'.frmgroup\',\'.clRemoveButton\');getTotals()" class="clRemoveButton" value="Remove Last"/>';
$fieldset .= '</div>';
$fieldset .= $this -> getBaseElement() ;
$fieldset .= $this -> getElements();
$fieldset .= $this -> getFooter();
$fieldset .= '</fieldset><script>disableRemoveLast("#line_items",".frmgroup",".clRemoveButton")</script>';
echo $fieldset;
}
}
?>