<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/



class ArcTbl extends ArcDb {
public $ajDestination = null;
public $moreActions = null;
public $ajPage = null;
public $recPage = 1;
public $recLink = null;
public $recDetail = null;
public $ignoreCols = array();
public $recQuery = null;
public $recFilter = null;
public $recOrderCol = null;
public $recOrder = null;
public $nextPage = null;
public $previousPage = null;
public $recTotalRows = null;
public $tblID = null;
public $tblNav = null;
public $recIndex = null;
public $dataTable = null;
public $actionDestination = null;
public $actionFilterKey = null;
public $ignoreFilterCols = array();
public $updateAction = null;
public $deleteAction = null;
public $tblKey="";
public $editable=false;
public $elements=null;
public $control=null;

function set($arrayString) {
$arrayString = hex2str($arrayString);
$params = json_decode($arrayString);
$paramsArray = (array)$params;
$offset=$this->dbOffset;
foreach($paramsArray as $key => $value) {
eval('$this->'.$key.'= $params -> '.$key.';');
}
$this->dbOffset=$offset;
return $params;
}
function get() {
if (isset($_POST["arctbl"])) {
$params = $_POST["arctbl"];
$params = $this -> set($params);
} else {
$params = (object) array(
"dbSchema" => $this -> dbSchema,
"dbType" => $this -> dbType, 
"oUser"=>$this -> oUser,
"oPassword"=>$this -> oPassword,
"oHost"=>$this -> oHost,
"oPort"=>$this -> oPort,
"oOptions"=>$this -> oOptions,
"oCon"=>$this -> oCon,
"tblKey"=>$this -> tblKey,
"actionFilterKey"=>$this -> actionFilterKey,
"recIndex"=>$this -> recIndex,
"moreActions"=>$this -> moreActions,
"actionDestination"=>$this -> actionDestination,
"ajDestination" => $this -> ajDestination, 
"recFilter" => $this -> recFilter, 
"ajPage" => $this -> ajPage, 
"recPage" => $this -> recPage, 
"recLink" => $this -> recLink, 
"recDetail" => $this -> recDetail,
"ignoreCols" => $this -> ignoreCols, 
"recQuery" => $this -> recQuery, 
"recOrderCol" => $this -> recOrderCol,
"dbLimit" => $this -> dbLimit, 
"recOrder" => $this -> recOrder,
"editable" => $this -> editable,
"primaryKey" => $this -> primaryKey,
"elements" => $this -> elements,
"dbTable" => $this -> dbTable,
"control" => $this -> control
);
}
return $params;
}
function prep() {
$dbLimit = $this -> dbLimit;
$dbOffset = $this -> dbOffset;
$pgParams = $this -> get();
/***************************************************************************/
$this -> dbLimit = NULL;
$this -> dbOffset = NULL;
$recFilter = $pgParams -> recFilter;
/*Validate that empty filter wasn't being passed*/
if($recFilter=="where "){$recFilter="";}
$this -> sql = "SELECT count(*) as RowTotal FROM (" . $pgParams -> recQuery . ") as derived " . $recFilter;
$this -> getRec();
$rowCountResult = $this -> dbData;
$recOffset = ($dbOffset != 0 ? $dbOffset : 0);
$this -> recTotalRows = $rowCountResult[0][0] - $recOffset;
/***************************************************************************/
if ($rowCountResult[0][0] < 1) {
$this -> sql = "SELECT * FROM (" . $pgParams -> recQuery . ") as derived " . $recFilter;
#echo $this -> sql;
$this -> getRec();
if ($recFilter != null) {
$dbCols = $this -> dbCols;
$ignoreFilterCols = $this -> ignoreFilterCols;
$filterableCols = $dbCols;
for ($i = 0; $i < count($dbCols); $i++) {
foreach ($ignoreFilterCols as $key => $value) {
if ($dbCols[$i]["name"] == $value) {
unset($filterableCols[$i]);
}
}
}
$filterLink = "<a class=\"button\" onclick=\"arc('popWindowFilter','/_lib/php/ArcDbFilter.php','arccols=" . bin2hex(json_encode($filterableCols)) . "&arctbl=" . bin2hex(json_encode($pgParams)) . "',1,1)\"><i class=\"fa fa-filter fa-fw\"></i>Advanced Filter</a>";
$this -> tblNav = bin2hex("<div id=\"" . $this -> tblID . "_controlBar\" class=\"headingRow\"><div class=\"headingRight\">" . $filterLink . "</div></div>");
$errorMsg = '<span class="arctblerror">Your filter returned 0 results. Please try again.</span>';
} else {
$filterLink = "";
$this -> tblNav="";
$errorMsg = '<span class="arctblerror">There is currently no data available.</span>';
}
$this -> dataTable .= $errorMsg;
return FALSE;
} else {
$page = (empty($pgParams -> recPage) ? $page = 1 : $pgParams -> recPage);
$pageStart = ($page == 1 ? $dbOffset : ($dbLimit * $page) - $dbLimit + $dbOffset);
$this -> dbOffset = $pageStart;
$this -> dbLimit = $dbLimit;
$this -> sql = "SELECT * FROM (" . $pgParams -> recQuery . ") as derived " . $recFilter;
#echo $this -> sql;

if ($pgParams -> recOrderCol!="") {
$this -> dbOrder = $pgParams -> recOrderCol . " " . $pgParams -> recOrder;
} else {$this -> dbOrder = null;}
$this -> getRec();
return TRUE;
}
}
function buildNav($label,$page,$class=null) {
$pgParams = $this -> get();
$pgParams -> recPage = $page;
$button = '<a class="button '.$class.'" onclick="arc(\'' . $pgParams -> ajDestination . '\',\'' . $pgParams -> ajPage . '\',\'arctbl=' . bin2hex(json_encode($pgParams)) . '\',1,1)">'.$label.'</a>';
return $button;
}
function build() {
$pgParams = $this -> get();
#echo "<pre>".print_r($pgParams,true)."</pre>";
if ($this -> prep()) {
$dbType = $this -> dbType; 
$oUser=$this -> oUser;
$oPassword=$this -> oPassword;
$oHost=$this -> oHost;
$oPort=$this -> oPort;
$oOptions=$this -> oOptions;
$oCon=$this -> oCon;
$dbLimit = $this -> dbLimit;
$recTotalRows = $this -> recTotalRows;
$tblKey = $pgParams -> tblKey;
$recPage = $pgParams -> recPage;
$previousPage = $recPage - 1;
$nextPage = $recPage + 1;
$start = $this -> dbOffset + 1;
$end = $start + $this -> dbRows - 1;
$dbCols = $this -> dbCols;
$ignoreCols = $this -> ignoreCols;
$ignoreFilterCols = $this -> ignoreFilterCols;
$recIndex = $this -> recIndex;
$tableRows = $this -> getAssociative();
$filterableCols = $dbCols;
for ($i = 0; $i < count($dbCols); $i++) {
foreach ($ignoreFilterCols as $key => $value) {
if ($dbCols[$i]["name"] == $value) {
unset($filterableCols[$i]);
}
}
}
$tableColumns = array();
$dbColData = array();
foreach ($dbCols as $entry) {
$tableColumns[] = $entry["name"];
$dbColData[$entry["name"]]=array("type" => $entry["type"]);
}
$tableColumnsFiltered = array_diff($tableColumns, $ignoreCols);
if ($recTotalRows <= $dbLimit) {
$numPages = 1;
} else if (($recTotalRows % $dbLimit) == 0) {
$numPages = ($recTotalRows / $dbLimit);
} else {
$numPages = (int)(($recTotalRows / $dbLimit)) + 1;
}
$firstLink = (isset($previousPage) && $previousPage > 1 ? $this -> buildNav('<i class="fa fa-angle-double-left"></i>First', 1) : "");
$lastLink = $prevLink = (isset($previousPage) && $previousPage > 0 ? $this -> buildNav('<i class="fa fa-angle-left"></i>Previous', $previousPage) : "");
$nextLink = (isset($nextPage) && $nextPage <= $numPages ? $this -> buildNav('<i class="fa fa-angle-right"></i>Next', $nextPage) : "");
$lastLink = (isset($nextPage) && $nextPage <= $numPages ? $this -> buildNav('<i class="fa fa-angle-double-right"></i>Last', $numPages) : "");
$refreshLink =$this -> buildNav('<i class="fa fa-refresh" style="margin:0"></i>',1,'clTblRefresh');
if(isset($this->dbCatlog) && $this->dbCatalog != null){
$exportCatalog=",'".encrypt($this->dbType)."'";
}else {
$exportCatalog="";
}
$dbo =encrypt(json_encode((array)$this));
$exportLink = '<a class="button" onclick="qry2csv(\''.$dbo.'\' )"><i class="fa fa-download fa-fw"></i>Export</a>';
$pageSummary = "<strong>Page</strong> " . $recPage . " of " . $numPages;
$recordRange = "<strong>Record</strong> " . $start . " thru " . $end;
$recordSummary = $recordRange . " | <strong>Total</strong> " . $recTotalRows;
$filterLink = '<a class="button" onclick="arc(\'popWindowFilter\',\'/_lib/php/ArcDbFilter.php\',\'arccols=' . bin2hex(json_encode($filterableCols)) . '&arctbl=' . bin2hex(json_encode($pgParams)) . '\',1,1)"><i class="fa fa-filter fa-fw"></i>Advanced Filter</a>';
$this -> tblNav = bin2hex("
<div id=\"" . $this -> tblID . "_controlBar\" class=\"headingRow\">
<div class=\"headingLeft\">" . $recordSummary . " | " . $pageSummary . "</div>
<div class=\"headingRight\">".$firstLink.$prevLink.$nextLink.$lastLink.$exportLink.$filterLink.$refreshLink."</div>
</div>") ;
$pagingDebugValues = (object) array("recPage" => $recPage, "first" => $firstLink, "previous" => $previousPage, "nextPage" => $nextPage, "prevPage" => $previousPage, "last" => $lastLink, "numPages" => $numPages, "recTotalRows" => $recTotalRows);
if ($this -> debug === TRUE) {printArray($pagingDebugValues);}
$dataTable = "<table class=\"dataGrid\" id=\"" . $this -> tblID . "\"><tr><th>&nbsp;</th>";
$order = $pgParams -> recOrder;
switch ($this->dbType) {
case "mysql" :
$strBox = array("`", "`");
break;
case "mssql" :
$strBox = array("[", "]");
break;
case "pgsql" :
$strBox = array("\"", "\"");
break;
}
$orderCol = $pgParams -> recOrderCol;
if ($this -> debug === TRUE) {printArray($pgParams);
}
foreach ($tableColumnsFiltered as $record => $col) {
if (strtolower($col) == 'action' | $col == ''){
$actioncol='class="colaction"';
$actioncolid=$record;
} else {
$actioncol='';
$actioncolid = null;
}
if ($orderCol == $strBox[0] . $col . $strBox[1]) {
if ($order == "asc") {
$pgParams -> recPage = 1;
$pgParams -> recOrder = "desc";
$sortString = '<i class="fa fa-sort-alpha-asc"></i>';
} else {
$pgParams -> recPage = 1;
$pgParams -> recOrder = "asc";
$sortString = '<i class="fa fa-sort-alpha-desc"></i>';
}
} else {
$pgParams -> recOrder = "asc";
$sortString = '<i class="fa fa-sort"></i>';
}
$pgParams -> recOrderCol = $strBox[0] . $col . $strBox[1];
$thtext= ($col == $this -> primaryKey?'<i class="fa fa-key"></i> '.$col:$col);
$thdata = ($actioncol==''?"onclick=\"arc('" . $pgParams -> ajDestination . "','" . $pgParams -> ajPage . "','arctbl=" . bin2hex(json_encode($pgParams)) . "',1,1)\"><div style='float:left'>" . $thtext . "</div><div style='float:right;padding-right:4px'> " . $sortString . "</div>":">");
$dataTable .= "<th ".$actioncol.$thdata."</th>";
}
if ($this -> editable === true) {
$dataTable .= "<th>&nbsp;</th>";
}
$dataTable .= "</tr>";
foreach ($tableRows as $record) {
$rowIndex = (isset($recIndex) ? $record[$recIndex] : $start);
if (!is_null($pgParams -> recDetail)) {
$detailStr = $pgParams -> recDetail;
eval("\$detail=" . $detailStr . ";");
$action = ' onClick="$(\'#rd'. $rowIndex . '_'.$this->tblKey.'\').toggle()"';
} elseif (!is_null($pgParams -> recLink)) {
$detail = "&nbsp;";
$action = " onClick=\"arc('".$pgParams->actionDestination."','".$pgParams->recLink."','".$pgParams->actionFilterKey."=".$record[$recIndex]."')$pgParams->moreActions\"";
} 
else {
{
$detail = "&nbsp;";
$action = "";
}
}
$rowClass = ($start % 2 == 0 ? "class=\"evenRow\"" : "class=\"oddRow\"");
$dataTable .= "<tr $rowClass id=\"rc" . $rowIndex . "_".$tblKey. "\">";
$dataTable .= "<td class=\"rowID\">" . $start . "</td>";
if ($this -> editable === true) {
foreach ($tableColumnsFiltered as $col => $value) {
if ($this -> elements != null) {
$key = multiArraySearch($this -> elements, $tableColumnsFiltered[$col]);
$recColumn =($key !== false ? $this -> elements[$key][1] : $tableColumnsFiltered[$col]);
} else {$recColumn = $tableColumnsFiltered[$col];}
$dbColType = $dbColData[$tableColumnsFiltered[$col]]["type"];
$addAction = ($col != $actioncolid?$action:"");
$disabled = ($tableColumnsFiltered[$col] == $this -> primaryKey ? 'disabled=="disabed"' : '');
$colData = ($dbColType == "text" || $dbColType == "blob" ? hex2str($record[$value]) : $record[$value] );
switch ($dbColType) {
case "string":case "varchar":case "char":case "date":case "datetime":case "timestamp":case "time":
$dataTable .= "<td ".$addAction."><input $disabled onblur=\"isDefault(this)\" type=\"text\" name=\"".$recColumn ."\" value=\"" . $colData . "\"/></td>";
break;
case "int":case "bigint":case "numeric":case "decimal":
$dataTable .= "<td ".$addAction."><input $disabled onblur=\"isDefault(this)\" type=\"text\" name=\"".$recColumn ."\" value=\"" . $colData . "\"/></td>";
break; 
case "text":case "blob":
$dataTable .= "<td ".$addAction."><textarea $disabled onblur=\"isDefault(this)\" type=\"text\" name=\"".$recColumn ."\">".str_replace(">","&gt;",str_replace("<","&lt;",$colData ))."</textarea></td>";
break;
default:
$dataTable .= "<td>Not Supported.</td>"; 
break; 
}
}
} else {
foreach ($tableColumnsFiltered as $col => $value) {
$addAction = ($col != $actioncolid?$action:"");
$dataTable .= "<td ".$addAction.">" . $record[$value] . "</td>";
}}
if ($this -> editable === true) {
if ($this -> deleteAction === null) { $deleteAction = "updateRow('rc".$rowIndex."_".$tblKey. "','". $this -> dbTable."','".$this->primaryKey."',".$rowIndex .",2,'".$this->control."','".$this -> ajDestination."','".$this->actionDestination."')"; } else { eval("\$deleteAction = " . $this -> deleteAction . ";"); }
if ($this -> updateAction === null) { $updateAction = "updateRow('rc".$rowIndex."_".$tblKey. "','". $this -> dbTable."','".$this->primaryKey."',".$rowIndex .",1,'".$this->control."','".$this -> ajDestination."','".$this->actionDestination."')"; } else { eval("\$updateAction = " . $this -> updateAction . ";"); }
$dataTable .="<td><input type=\"button\" value=\"Update\" onclick=\"".$updateAction."\"/><input type=\"button\" value=\"Delete\" onclick=\"".$deleteAction."\"/></td>";
}
$dataTable .= "</tr>";
$colCount = count($tableColumnsFiltered) + 1;
$dataTable .= "<tr class=\"rdetail\" id=\"rd" . $rowIndex . "_".$tblKey."\" style=\"display:none\"><td colspan=\"" . $colCount . "\" >" . $detail . "</td></tr>";
$start = $start + 1;
}
$dataTable .= "</table>";
$this -> dataTable = $dataTable;
}
}
}
