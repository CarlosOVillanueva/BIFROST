<?php
// Created by Carlos Villanueva on 2013-02-25.
// Copyright 2013 BIFROST SOFTWARE LLC
// http://www.bifrostapps.com
$path = getcwd() . "/";
$path = str_replace($serverrootpath, "", $path);
$icoPreview = "icoPreview='<span onclick=\"printReport()\"><i class=\"fa fa-newspaper-o fa-fw\"></i>Preview</span>';";
$icoUpload =  "icoUpload='<span id=\"icoUpload\" onclick=\"uploadFiles(oREC)\"><i class=\"fa fa-cloud-upload fa-fw\"></i>Upload</span>';";
$icoDelete =  "icoDelete='<span id=\"icoDelete\" onclick=\"deleteRecord(oREC)\"><i class=\"fa fa-times-circle fa-fw\"></i>Delete</span>';";
$icoSave =   "icoSave='<span class=\"icoSave\" onclick=\"prepSaveChanges()\"><i class=\"fa fa-check-circle fa-fw\"></i>Save</span>';";
$icoEmail =  "icoEmail='<span onclick=\"$(\'#popWindow\').html(hex2bin(window.oEmailData));$(\'#popWindow\').toggle()\"><i class=\"fa fa-envelope fa-fw\"></i>Email</span>';";
$icoCollapse =  "icoCollapse='<span id=\"collapseTabs\" style=\"display:none\" onclick=\"collapseTabs()\"><i class=\"fa fa-list-alt fa-fw\"></i>Collapse Tabs</span>';";
$icoAdd =   "icoAdd='<span class=\"icoadd\" onclick=\"arc(\'content\',\'".$path."add.php\',null,1,1)\"><i class=\"fa fa-plus-circle fa-fw\"></i>Add</span>';";
$icoAddDevice = "<i class=\"fa fa-laptop fa-fw\"></i>";
$icoReportIcon = "<i class=\"fa fa-file-text fa-fw\"></i>";
$icoSubmitTimeIcon = "<i class=\'fa fa-clock-o fa-fw\'></i>";
$icoExportIcon = "<i class=\'fa fa-download fa-fw\'></i>";
?>