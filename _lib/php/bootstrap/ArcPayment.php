<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/



class Payment {
public $filter = null;
public $elements = null;
public $id = null;
const deleteRow = '<span class="elementIconBox" onclick="multiRowRemoveActiveElement(this,\'frmgroup\');disableRemoveLast(\'#payment\',\'.frmgroup\',\'.clRemoveButton\')"><i class="fa fa-trash"></i></span>';
const dateReceived ='<div class="elementIconBox" onclick="openCalendar(this,\'interface\',null,\'clDateReceived\')"><i class="fa fa-calendar"></i></div>';
const dateProcessed='<div class="elementIconBox" onclick="openCalendar(this,\'interface\',null,\'clDateProcessed\')"><i class="fa fa-calendar"></i></div>';
function getRecords() {
$gdbo = new ArcDb;
$gdbo -> dbConStr=$GLOBALS['globalDBCON'];
$gdbo -> dbType = $GLOBALS['globalDBTP'];
$gdbo -> dbSchema = $GLOBALS['globalDB'];
if (is_null($this -> filter) || is_null($this -> id)) {
return false;
}
$gdbo -> sql = "SELECT
_contract_payment.id_contract_payment,
_contract_payment.contract_payment,
contract_payment_dr,
contract_payment_dp,
_contract_payment.id_contract_payment_method_tp,
_contract_payment.id_contract
FROM _contract_payment
WHERE ".$this -> filter ."=" . $this -> id.
" ORDER BY contract_payment_dr ASC, id_contract_payment ASC";
$gdbo -> getRec();
$results= $gdbo -> getAssociative();
return $results;
}
function getLists() {
$gdbo = new ArcDb;
$gdbo -> dbConStr=$GLOBALS['globalDBCON'];
$gdbo -> dbType = $GLOBALS['globalDBTP'];
$gdbo -> dbSchema = $GLOBALS['globalDB'];
return true;
}
function getBaseElement(){
$elements=
array(
array(
array("*Payment",null,'class="clPayment" onkeyup="validateElement(\'money\',this)"',0),
array("Payment Received",null,'class="clDateReceived elementIcon" onkeyup="validateElement(\'date\',this),calcPayments()" class="dateFld" validateElement(\'date\',this)',0,null,null,null,self::dateReceived,null,'YYYY-MM-DD'),
array("Payment Processed",null,'class="clDateProcessed elementIcon" onkeyup="validateElement(\'date\',this)" class="dateFld" validateElement(\'date\',this)',0,null,null,null,self::dateProcessed,null,'YYYY-MM-DD'),
),
);
$baseElements='<div class="frmgroup" style="display:none">';
$baseElements.= frmElements($elements);
$baseElements.='</div>';
return $baseElements;
}
function getElements(){
$dataElements=(isset($dataElements)?$dataElements:"");
$records = $this -> getRecords();
if ($records == false) {
return false;
}
for ($i=0;$i<count($records);$i++) {
$elements=
array(
array(
array("*Payment",null,'class="clPayment" onkeyup="validateElement(\'money\',this),calcPayments()"',0,null,$records[$i]["contract_payment"]),
array("Payment Received",null,'class="clDateReceived elementIcon" onkeyup="validateElement(\'date\',this)" class="dateFld" validateElement(\'date\',this)',0,null,$records[$i]["contract_payment_dr"],null,self::dateReceived,null,'YYYY-MM-DD'),
array("Payment Processed",null,'class="clDateProcessed elementIcon" onkeyup="validateElement(\'date\',this)" class="dateFld" validateElement(\'date\',this)',0,null,$records[$i]["contract_payment_dp"],null,self::dateProcessed,null,'YYYY-MM-DD'),
));
$dataElements.='<div class="frmgroup">';
$dataElements.=frmElements($elements);
$dataElements.='</div>';
}
return $dataElements;
}
function getFooter() {
$elements = 
array(
array(
array(null,'payment_mText',null,6,null,null,null,null,' style="display:none" '),
)
);
$footerElements=frmElements($elements);
return $footerElements;
}
function build() {
$this->getLists();
$fieldset = '<fieldset id="payment" class="multiRow">';
$fieldset .= '<legend>Payment</legend>';
$fieldset .= '<div class="multimenu">';
$fieldset .= '<input type="button" onClick="multiRowAddElement(this,\'frmgroup\');disableRemoveLast(\'#payment\',\'.frmgroup\',\'.clRemoveButton\')" class="clAddButton" value="Add"/>';
$fieldset .= '<input type="button" onClick="multiRowRemoveLastElement(this,\'frmgroup\');disableRemoveLast(\'#payment\',\'.frmgroup\',\'.clRemoveButton\')" class="clRemoveButton" value="Remove Last"/>';
$fieldset .= '</div>';
$fieldset .= $this -> getBaseElement() ;
$fieldset .= $this -> getElements();
$fieldset .= $this -> getFooter();
$fieldset .= '</fieldset><script>disableRemoveLast("#payment",".frmgroup",".clRemoveButton")</script>';
echo $fieldset;
}
}
?>