<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/



class ArcDb {
	public $dbCatalog = null;
	public $dbCon = null;
	public $dbConStr = null;
	public $dbCols = array();
	public $dbData = array();
	public $dbLimit = null;
	public $dbOffset = null;
	public $dbOrder = null;
	public $dbRows = null;
	public $dbSchema = null;
	public $dbSettings = null;
	public $dbTable = null;
	public $dbType = null;
	public $debug = FALSE;
	public $insertedID = null;
	public $sql = null;
	public $recursiveData = FALSE;
	public $oCon=null;
	public $oUser=null;
	public $oPassword=null;
	public $oHost=null;
	public $oPort=null;
	public $oOptions=null;
	public $primaryKey=null;
private function setDefaults() {
	$dbSettings = $this -> dbSettings;
	if (isset($dbSettings)) {
		$pieces = explode(",", $this -> dbSettings);
		foreach ($pieces as $key => $value) {
			/* this allows you to assign the properties dynamically */
			list($name, $val) = explode("=", $value);
			$this -> $name = $val;
		}
	}
}
public function getCon() {
	$dbConStr = $this -> dbConStr;
	$dbCon = $this -> dbCon;
	$dbType = $this -> dbType;
	$dbCatalog = $this -> dbCatalog;
	if (is_null($dbConStr) && is_null($this -> oCon) ) { 
		if (!is_null($dbCon) && $dbCon != "") {
			switch ($dbCon) {
				case 1 :
				$conStr = self::VMMYSQL;
				break;
				default :
				$sqlError = ($this -> debug === true?"The defined connection string is empty." :"");
				die("$sqlError");
				break;
			}
			$this -> dbConStr = $conStr;
		} else {
			$this -> dbConStr = null;
		}
	}
	if (isset($this-> oCon)) {
		$host=$this-> oHost;
		$port=$this-> oPort;
		$user=$this-> oUser;
		$password=$this-> oPassword;
		$options=$this-> oOptions;
	} else {
		$conStr = explode(",", $this -> dbConStr);
		$host=$conStr[0];
		$port=(isset($conStr[1])&&$conStr[1]!=""?":".$conStr[1]:"");
		$user=(isset($conStr[2])&&$conStr[2]!=""?$conStr[2]:"");
		$password=(isset($conStr[3])&&$conStr[3]!=""?$conStr[3]:"");
		$options=(isset($conStr[4])&&$conStr[4]!=""?$conStr[4]:"");
	}
	switch ($dbType) {
		case 'mysql' :
		$con = mysql_connect($host.$port,$user,$password);
		break;
		case 'mssql' :
		$con = mssql_connect($host,$user,$password);
		break;
		case 'pgsql' :
		$database = (!is_null($this -> dbSchema)?"dbname=".$this -> dbSchema:"");
		$options=($options==""?"--client_encoding=UTF8":$options);
		$port=($port==""?"5132":$port);
		$con = pg_connect("host=$host user=$user password=$password port=$port options='$options' $database");
		break;
		default :
		$sqlError = ($this -> debug === true?"Invalid database type (mssql,mysql, or pgsql)." :"");
		die($sqlError);
		break;
	}
	if ($con === FALSE) {
		$sqlError = ($this -> debug === true?"Unable to connect to database." :"");
		die($sqlError);
	}
	return $con;
}
public function getSchemata($dbType, $dbSchema, $dbTable, $dbCon = null, $dbConStr = null, $dbCatalog = null) {
	switch ($dbType) {
		case "mysql" :
		$sql = "SELECT COLUMN_NAME,DATA_TYPE,ORDINAL_POSITION FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='" . $dbSchema . "' AND TABLE_NAME='" . $dbTable . "';";
		break;
		case "mssql" :
		$sql = "SELECT COLUMN_NAME,DATA_TYPE,ORDINAL_POSITION FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME='" . $dbTable . "';";
		break;
		case "pgsql" :
		$sql = "SELECT COLUMN_NAME,DATA_TYPE,ORDINAL_POSITION FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME='" . $dbTable . "';";
		$dbSchema = "information_schema";
		break;
	}
	$dbCatalogInsert = (!is_null($dbCatalog) ? ",dbCatalog=" . $dbCatalog : "");
	$schDbo = new ArcDb();
	$schDbo -> sql = $sql;
	$schDbo -> dbConStr = $dbConStr;
	$schDbo -> dbSettings = "dbSchema=" . $dbSchema . ",dbType=" . $dbType . ",dbCon=" . $dbCon . $dbCatalogInsert;
	$schDbo -> getRec();
	$schemadata=$schDbo -> dbData;
	$this -> primaryKey = $schemadata[0][0];
	return $schemadata;
}
public function insertRec() {
	$this -> setDefaults();
	$dbType = $this -> dbType;
	$dbSchema = $this -> dbSchema;
	$dbTable = $this -> dbTable;
	$sql = $this -> sql;
	$dbCatalog = $this -> dbCatalog;
	$this -> dbLimit = null;
	$this -> dbOffset = null;
	/* added this regex check to insure that the proper insert syntax was being used */
	preg_match('/^\(.*\)\s*values\s*\(.*\)$/i', $sql, $sqltest);
	if ($this -> debug === TRUE) {
		$debug = 1;
		printArray($sqltest);
	}
	if (empty($sql) || count($sqltest) < 1) {
		$schema = $this -> getSchemata($dbType, $dbSchema, $dbTable, $this -> dbCon, $this -> dbConStr, $dbCatalog);
		$sql = $this -> getInsertStr($schema);
	}
	switch ($dbType) {
		case 'mysql' :
		$activeCon = $this -> getCon();
		mysql_select_db($dbSchema, $activeCon);
		$sql = "INSERT INTO " . $dbTable . " " . $sql . ";";
		$inserted = mysql_query($sql);
		$this -> insertedID = mysql_insert_id();
		if ($this -> debug == true) {
			echo("Rows inserted: " . mysql_affected_rows());
		}
		mysql_close($activeCon);
		break;
		case 'mssql' :
		$activeCon = $this -> getCon();
		mssql_select_db($dbSchema, $activeCon);
		$sql = "INSERT INTO " . $dbTable . " " . $sql . " ;SELECT LAST_INSERT_ID=@@IDENTITY;";
		$inserted = mssql_query($sql);
		$insertedRow = mssql_fetch_assoc($inserted);
		$this -> insertedID = $insertedRow['LAST_INSERT_ID'];
		if ($this -> debug == true) {
			echo("Rows inserted: " . mssql_rows_affected($activeCon));
		}
		mssql_close($activeCon);
		break;
		case 'pgsql' :
		$schemaArray = $this -> getSchemata($dbType, $dbSchema, $dbTable, $this -> dbCon, $this -> dbConStr, $dbCatalog);
		$schemaPrimaryKey = $schemaArray[0][0];
		$activeCon = $this -> getCon();
		$sql = "INSERT INTO " . $dbTable . " " . $sql . " RETURNING $schemaPrimaryKey;";
		$inserted = pg_query($activeCon, $sql);
		$insertedRow = pg_fetch_row($inserted);
		$this -> insertedID = $insertedRow[0];
		if ($this -> debug == true) {
			echo("Rows inserted: " . pg_affected_rows($inserted));
		}
		pg_close($activeCon);
		break;
		default :
		$sqlError = ($this -> debug === true?"Invalid database type." :"");
		die($sqlError);
		break;
	}
	if ($this -> debug === TRUE) {
		echo($sql);
	}
}
public function updateRec($filter = null) {
	$this -> setDefaults();
	$dbType = $this -> dbType;
	$dbSchema = $this -> dbSchema;
	$dbTable = $this -> dbTable;
	$sql = $this -> sql;
	$dbCatalog = $this -> dbCatalog;
	$this -> dbLimit = null;
	$this -> dbOffset = null;
	preg_match('/^[\[`"A-Za-z].*[\]`"A-Za-z]=.*$/i', $sql, $sqltest);
	if ($this -> debug === TRUE) {
		$debug = 1;
		printArray($sqltest);
	}
	if (empty($sql) || count($sqltest) < 1) {
		$schema = $this -> getSchemata($dbType, $dbSchema, $dbTable, $this -> dbCon, $this -> dbConStr, $dbCatalog);
		$sql = $this -> getUpdateStr($schema);
	}
	$filter = (!is_null($filter) ? " WHERE " . $filter : "");
	switch ($dbType) {
		case 'mysql' :
		$activeCon = $this -> getCon();
		mysql_select_db($dbSchema, $activeCon);
		$sql = "UPDATE " . $dbTable . " SET " . $sql . " " . $filter . ";";
		$updated = mysql_query($sql);
		if ($this -> debug == TRUE) {
			echo("Rows updated: " . mysql_affected_rows());
		}
		mysql_close($activeCon);
		break;
		case 'mssql' :
		$activeCon = $this -> getCon();
		mssql_select_db($dbSchema, $activeCon);
		$sql = "UPDATE " . $dbTable . " SET " . $sql . " " . $filter . ";";
		$updated = mssql_query($sql);
		if ($this -> debug == TRUE) {
			echo("Rows updated: " . mssql_rows_affected($activeCon));
		}
		mssql_close($activeCon);
		case 'pgsql' :
		$activeCon = $this -> getCon();
		$sql = "UPDATE " . $dbTable . " SET " . $sql . " " . $filter . ";";
		$updated = pg_query($activeCon, $sql);
		if ($this -> debug == TRUE) {
			echo("Rows updated: " . pg_affected_rows($updated));
		}
		pg_close($activeCon);
		break;
		default :
		die("Unable to update table. Invalid database type.");
		break;
	}
	if ($this -> debug === TRUE) {
		echo($sql);
	}
}
public function deleteRec($filter = null) {
	$this -> setDefaults();
	$dbSchema = $this -> dbSchema;
	$dbType = $this -> dbType;
	$dbTable = $this -> dbTable;
	$filter = (!is_null($filter) ? " WHERE " . $filter : "");
	switch ($dbType) {
		case 'mysql' :
		$activeCon = $this -> getCon();
		mysql_select_db($dbSchema, $activeCon);
		$sql = "DELETE FROM " . $dbTable . $filter . ";";
		$deleted = mysql_query($sql);
#echo("Rows deleted: " . mysql_affected_rows());
		mysql_close($activeCon);
		break;
		case 'mssql' :
		$activeCon = $this -> getCon();
		mssql_select_db($dbSchema, $activeCon);
		$sql = "DELETE FROM " . $dbTable . $filter . ";";
		$deleted = mssql_query($sql);
#echo("Rows deleted: " . mssql_rows_affected($activeCon));
		mssql_close($activeCon);
		break;
		case 'pgsql' :
		$activeCon = $this -> getCon();
		$sql = "DELETE FROM " . $dbTable . $filter . ";";
		$deleted = pg_query($activeCon, $sql);
#echo("Rows deleted: " . pg_affected_rows($deleted));
		pg_close($activeCon);
		break;
		default :
		die("Unable to delete rows. Invalid database type.");
		break;
	}
	if ($this -> debug === TRUE) {
		echo($sql);
	}
}
function getPrimaryKey() {
	$schemaArray = $this -> getSchemata($this -> dbType,$this -> dbSchema,$this-> dbTable, $this -> dbCon, $this -> dbConStr, $this -> dbCatalog);
	$this -> primaryKey = $schemaArray[0][0];
	return $schemaArray[0][0];
}
public function getRec() {
	$this -> setDefaults();
	$activeCon = $this -> getCon();
	$dbType = $this -> dbType;
	$dbSchema = $this -> dbSchema;
	$sql = $this -> sql;
	$dbCols = $this -> dbCols;
	$dbDataTemp = array();
	$dbColsTemp = array();
	if ($this -> recursiveData === FALSE) {
		$dbCols = array();
		$dbData = array();
	} else {
		$dbCols = $this -> dbCols;
		$dbData = $this -> dbData;
	}
	$dbOffset = $this -> dbOffset;
	$dbLimit = $this -> dbLimit;
	$dbOrder = $this -> dbOrder;
	if ($this -> dbType != "mssql" && !is_null($dbOrder) && !empty($dbOrder) && strLen($dbOrder) > 1) {$sql = $sql . " order by " . $dbOrder . " ";
}
if (strpos(strtolower($sql), "delete from") !== FALSE) {
	die("This command is invalid. To delete, please use the deleteRec() method.");
}
switch ($dbType) {
	case 'mysql' :
	mysql_select_db($dbSchema, $activeCon);
	if (isset($dbOffset) && isset($dbLimit)) {
		$sql .= " LIMIT " . $dbOffset . "," . $dbLimit;
	}
	$rec = mysql_query($sql);
	if (mysql_error($activeCon)) {
		$sqlError = ($this->debug === true?"There was an errror with your query</br>. ".$sql."</br>" . mysql_error($activeCon):"");
		die($sqlError);
	}
	$fieldCount = mysql_num_fields($rec);
	while ($recAssoc = mysql_fetch_assoc($rec)) {
		$dbData[] = array_values($recAssoc);
	}
	for ($i = 0; $i < $fieldCount; $i++) {
		$dbColsFetch = mysql_fetch_field($rec, $i);
		$dbCols[] = (array)$dbColsFetch;
	}
	$dbRows = mysql_num_rows($rec);
	mysql_close($activeCon);
	break;
	case 'mssql' :
	/* $ansi = "SET NOCOUNT ON;SET ANSI_NULLS ON;SET ANSI_WARNINGS ON;"; */
	mssql_select_db($dbSchema, $activeCon);
	if (isset($dbOffset) && isset($dbLimit)) {
		$dbLimit = $dbLimit + $dbOffset;
		/* get first column for ordering */
		if (is_null($dbOrder)) {
			/* determine if an order by clause already exists */
			preg_match("/order by.*$/i", $sql, $ordermatch);
			if (count($ordermatch) > 0) {
				$sql = str_replace($ordermatch, "", $sql);
				$overCol = str_ireplace("order by", "", $ordermatch[0]);
			} else {
				$getColSql = "SELECT TOP 1 * FROM (" . $sql . ") as colDump;";
				$getColSqlRec = mssql_query($getColSql, $activeCon);
				if ($getColSqlRec === false) {
					$sqlError = ($this->debug === true?"There was an error with your query.":"");
					die($sqlError);
				}
				$getColFetch = mssql_fetch_field($getColSqlRec, 0);
				$getCol = (array)$getColFetch;
				$overCol = $getCol['name'];
			}
		} else {
			$overCol = $dbOrder;
		}
		$pattern = "/from.*/i";
		$testMatch = preg_match($pattern, $sql, $matches, PREG_OFFSET_CAPTURE);
		if ($testMatch === 0) {
			die("There was an error with your SQL Statement. Unable to paginate.");
		} else {
			$tempSQL = "SELECT count(*) FROM (" . $sql . ") as colCount";
			$tempRec = mssql_query($tempSQL, $activeCon);
			if ($tempRec === false) {
				die("There was an error with your query. Please be sure to omit ORDER BY from the ArcDb->sql property.");
			}
			$tempfieldCount = mssql_num_fields($tempRec);
			$sql = "SELECT * FROM (SELECT ROW_NUMBER() OVER (ORDER BY " . $overCol . ") AS RowNum,* FROM (" . $sql . ") as rowConstrainedChild) as rowConstrained
			WHERE RowNum > " . $dbOffset . " AND RowNum <= " . $dbLimit . " ORDER BY RowNum;";
		}
	}
	$rec = mssql_query($sql, $activeCon);
	if ($rec === false) {
		$sqlError = ($this->debug === true?"There was an error with your query.":"");
		die($sqlError);	
	}
	$fieldCount = mssql_num_fields($rec);
	while ($recAssoc = mssql_fetch_assoc($rec)) {
		$dbDataTemp[] = array_values($recAssoc);
	}
	for ($i = 0; $i < count($dbDataTemp); $i++) {
		$dbDataSub = array();
		foreach ($dbDataTemp[$i] as $col => $value) {
			$field = mssql_fetch_field($rec, $col);
			if ($field -> name != "RowNum") {
				if ($i == 0) {
					$dbCols[] = (array)$field;
				}
				$dbDataSub[$col] = $value;
			}
		}
		$dbData[] = $dbDataSub;
	}
	$dbRows = mssql_num_rows($rec);
	mssql_close($activeCon);
	break;
	case 'pgsql' :
	if (isset($dbOffset) && isset($dbLimit)) {
		$sql .= " LIMIT " . $dbLimit . " OFFSET " . $dbOffset;
	}
	$rec = pg_query($activeCon, $sql);
	if (pg_errormessage($activeCon)) {
		die("<p>There was an errror with your query.<br>" . pg_errormessage($activeCon) . "</p>");
	}
	$fieldCount = pg_num_fields($rec);
	while ($recAssoc = pg_fetch_assoc($rec)) {
		$dbData[] = array_values($recAssoc);
	}
	for ($i = 0; $i < $fieldCount; $i++) {
		$dbCols[] = array("name" => pg_field_name($rec, $i), "type" => pg_field_type($rec, $i));
	}
	$dbRows = pg_num_rows($rec);
	pg_close($activeCon);
	break;
	default :
	die("Invalid database type.");
	break;
}
$this -> dbCols = $dbCols;
$this -> dbData = $dbData;
$this -> dbRows = $dbRows;
if (is_null($this -> dbLimit)) {
	$this -> dbLimit = $dbRows;
}
if ($this -> debug === TRUE) {
	echo($sql);
}
}
public function getInsertStr($schemaArray) {
	$dbType = $this -> dbType;
	$insertArray = array();
	switch ($dbType) {
		case "mysql" :
		$strBox = array("`", "`");
		break;
		case "mssql" :
		$strBox = array("[", "]");
		break;
		case "pgsql" :
		$strBox = array("\"", "\"");
		break;
	}
	foreach ($schemaArray as $key => $col) {
		if (isset($_POST[$col[0]])) {
			$column = $strBox[0] . $col[0] . $strBox[1];
			$val = addslashes($_POST[$col[0]]);
			if (strpos(strtolower($col[1]), "int") === FALSE) {
				$val = "'" . $val . "'";
			}
			/*Clean Nulls*/
			//$val=str_replace("''",'NULL',$val);
			$val=($val=="" || $val=="''"?'NULL':$val);
			/*Add to Array*/
			$insertArray[] = array($column, $val);
		}
	}
	if (count($insertArray) > 0) {
		$columns = "";
		$vals = "";
		for ($i = 0; $i < count($insertArray); $i++) {
			$comma = ($i === 0 ? "" : ",");
			$columns .= $comma . $insertArray[$i][0];
			$vals .= $comma . $insertArray[$i][1];
		}
		return "(" . $columns . ") values (" . $vals . ")";
	} else {
		return false;
	}
}
public function getUpdateStr($schemaArray) {
	$dbType = $this -> dbType;
	$updateArray = array();
	switch ($dbType) {
		case "mysql" :
		$strBox = array("`", "`");
		break;
		case "mssql" :
		$strBox = array("", "");
		break;
		case "pgsql" :
		$strBox = array("\"", "\"");
		break;
	}
	foreach ($schemaArray as $key => $col) {
		if (isset($_POST[$col[0]]) && $col[2] != 1) {
			$column = $strBox[0] . $col[0] . $strBox[1];
			$val = addslashes($_POST[$col[0]]);
			if (strpos(strtolower($col[1]), "int") === FALSE) {
				$val = "'" . $val . "'";
			}
			//$val=str_replace("''",'NULL',$val);
			$val=($val=="" || $val=="''"?'NULL':$val);
			$updateArray[] = array($column . '=' . $val);
		}
	}
	if (count($updateArray) > 0) {
		$updateString = "";
		for ($i = 0; $i < count($updateArray); $i++) {
			$comma = ($i === 0 ? "" : ",");
			$updateString .= $comma . $updateArray[$i][0];
		}
		return $updateString;
	} 
}
public function getAssociative() {
	$cols = $this -> dbCols;
	$data = $this -> dbData;
	if ($cols == null || $data == null) {
		return false;
	}
	$colsTrimmed = array();
	foreach ($cols as $row) {
		$colsTrimmed[] = $row['name'];
	}
	$combined = array();
	foreach ($data as $row) {
		$newrow = array_combine($colsTrimmed, $row);
		$combined[] = $newrow;
	}
	return $combined;
}
/* Added 3.2.2014. This may not actually be needed, since all it is doing is executing whatever DB query I want, with little control. Use with caution!*/
public function execQuery() {
	$this -> setDefaults();
	$activeCon = $this -> getCon();
	$dbType = $this -> dbType;
	$dbSchema = $this -> dbSchema;
	$sql = $this -> sql;
	switch ($dbType) {
		case 'mysql' :
		mysql_select_db($dbSchema, $activeCon);
		$rec = mysql_query($sql);
		if (mysql_error($activeCon)) {
			echo "There was an errror with your query</br>. ".$sql."</br>" . mysql_error($activeCon);
			return;
		}
		mysql_close($activeCon);
		break;
		case 'mssql' :
		/* $ansi = "SET NOCOUNT ON;SET ANSI_NULLS ON;SET ANSI_WARNINGS ON;"; */
		mssql_select_db($dbSchema, $activeCon);
		$rec = mssql_query($sql, $activeCon);
		if ($rec === false) {
			die("There was an error with your query.");
		}
		mssql_close($activeCon);
		break;
		case 'pgsql' :
		$rec = pg_query($activeCon, $sql);
		pg_close($activeCon);
		break;
		default :
		die("Invalid database type.");
		break;
	}
}
}
?>
