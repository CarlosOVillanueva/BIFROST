<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/



class ArcEmailForm {
public $className = null;
public $senderid = null;
public $isBranch = false;
public $receiverid = null;
public $sendername = null;
public $path = "/_lib/php/submitEmail.php";
function getContactMethods($id,$override = false) {
$filter = ($this -> isBranch === false | $override === true?"id_cust_contact":"id_cust_branch");
$gdbo = new ArcDB;
$gdbo -> dbConStr=$GLOBALS['globalDBCON'];
$gdbo -> dbType = $GLOBALS['globalDBTP'];
$gdbo -> dbSchema = $GLOBALS['globalDB'];
$gdbo -> sql = "
SELECT 
_contact_method.contact_method as method,
_contact_method.contact_method
FROM _contact_method
LEFT JOIN _contact_method_tp 
ON _contact_method.id_contact_method_tp = _contact_method_tp.id_contact_method_tp
WHERE _contact_method_tp.id_contact_method_tp = 1 AND ".$filter."=".$id;
$gdbo -> getRec();
return $gdbo -> dbData;
}
function buildForm() {
$sender = $GLOBALS["usermail"];
$recepient = $this -> getContactMethods($this -> receiverid);
if (count($recepient) > 0 && count($recepient) > 0 && !empty($sender)) {
$formName = "email_".rand();
$fsEmail = 
array(
array(
array("From","fromEmail","readonly='readonly'",0,null,$sender),
array("To","toEmail",null,1,$recepient,null),
array(null,"from",null,3,null,$this->sendername),
),
array(
array("Subject","subject",'style="width:100% !important"',0,null,null,null,null,null,null,null,'style="width:100%"'),
),
array(
array("Message","message",null,6),
),
array(
array(null,null,"onclick='clearPop()'",2,null,'Close'),
array(null,null,'onclick="prepSaveChanges(\''.$formName.'\')"',2,null,'Send'),
),
);
$form= '<style>.frmcol.text {clear:both;width:100%}</style>';
$form.= '<form class="'. $this -> className .'" method="post" id="'.$formName.'" name="'.$formName.'" action="javascript:submitFrmVals(\'emailDebug\',\''.$this -> path.'\',\'email_to\',null,\''.$formName.'\')">';
$form.= '<fieldset><legend>Email</legend>';
$form.= frmElements($fsEmail);
$form.= '</fieldset></form><div id="emailDebug">&nbsp</div>';
$form.= '<script>CKEDITOR.replace( "message" ,{toolbarStartupExpanded : true});</script>';
return $form;
} else {
$fsEmail =
array(
array(
array(null,null,"onclick='clearPop()'",2,null,'Close'),
)
);
$form ='<form action="#" method="post" class="'. $this -> className .'">';
$form.= '<fieldset><legend>Email</legend>';
$form.= '<div class="clEmailError">Sorry. You have not defined an E-Mail address for either your account or the contact your are trying to reach.</div>';
$form.= frmElements($fsEmail);
$form.= '</fieldset></form>';
return $form;
}
}
}
?>