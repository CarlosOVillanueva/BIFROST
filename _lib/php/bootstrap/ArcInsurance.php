<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/



class Insurance {
public $filter = null;
public $elements = null;
public $id = null;
const deleteRow = '<span class="elementIconBox" onclick="multiRowRemoveActiveElement(this,\'frmgroup\');disableRemoveLast(\'#insurance\',\'.frmgroup\',\'.clRemoveButton\')"><i class="fa fa-trash"></i></span>';
const dateStarted ='<div class="elementIconBox" onclick="openCalendar(this,\'interface\',null,\'clDateStarted\')"><i class="fa fa-calendar"></i></div>';
const dateRenewed='<div class="elementIconBox" onclick="openCalendar(this,\'interface\',null,\'clDateRenewed\')"><i class="fa fa-calendar"></i></div>';
function getRecords() {
$gdbo = new ArcDb;
$gdbo -> dbConStr=$GLOBALS['globalDBCON'];
$gdbo -> dbType = $GLOBALS['globalDBTP'];
$gdbo -> dbSchema = $GLOBALS['globalDB'];
if (is_null($this -> filter) || is_null($this -> id)) {
return false;
}
$gdbo -> sql = "SELECT 
id_hr_insurance,
id_hr_emp,
id_cust_company,
id_hr_insurance_plan,
id_hr_insurance_coverage,
hr_insurance_ds,
hr_insurance_de
FROM _hr_insurance
WHERE ".$this -> filter ."=" . $this -> id;
$gdbo -> getRec();
$results= $gdbo -> getAssociative();
return $results;
}
function getLists() {
$gdbo = new ArcDb;
$gdbo -> dbConStr=$GLOBALS['globalDBCON'];
$gdbo -> dbType = $GLOBALS['globalDBTP'];
$gdbo -> dbSchema = $GLOBALS['globalDB'];
# Insurance Plan
$gdbo -> sql ="SELECT 
id_hr_insurance_plan,
hr_insurance_plan,
id_cust_company 
FROM _hr_insurance_plan
ORDER BY id_cust_company asc,hr_insurance_plan asc";
$gdbo -> getRec();
$this -> listInsurancePlan = $gdbo -> dbData;
$this -> listInsurancePlanJSON = json_encode($this -> listInsurancePlan);
echo '<script>window.listInsurancePlanJSON=\''.$this -> listInsurancePlanJSON.'\'</script>';
# Insurance Provider
require("_model/dboInsuranceProvider.php");
$gdbo -> getRec();
$this -> listInsuranceProvider = $gdbo -> dbData;
# Insurance Coverage
require("_model/dboInsuranceCoverage.php");
$gdbo -> getRec();
$this -> listInsuranceCoverage = $gdbo -> dbData;
return true;
}
function getBaseElement(){
$elements=
array(
array(
array("*Provider",null,'onchange="filterRecJS(this,window.listInsurancePlanJSON,\'frmgroup\',\'chPlan\')"',1,$this -> listInsuranceProvider),
array("*Plan",null,'class="chPlan"',1),
array("*Coverage",null,'class="elementIcon"',1,$this -> listInsuranceCoverage,null,null,self::deleteRow),
),
array(
array("*Start Date",null,'class="clDateStarted elementIcon" validateElement(\'date\',this)',0,null,null,null,self::dateStarted,null,'YYYY-MM-DD'),
array("Renewal Date",null,'class="clDateRenewed elementIcon" validateElement(\'date\',this)',0,null,null,null,self::dateRenewed,null,'YYYY-MM-DD'),
),
);
$baseElements='<div class="frmgroup" style="display:none">';
$baseElements.= frmElements($elements);
$baseElements.='</div>';
return $baseElements;
}
function getElements(){
$dataElements=(isset($dataElements)?$dataElements:"");
$records = $this -> getRecords();
if ($records == false) {
return false;
}
for ($i=0;$i<count($records);$i++) {
$campusFiltered = array();
$planFiltered = array();
$listInsurancePlan = $this -> listInsurancePlan;
for ($ii=0;$ii<count($listInsurancePlan);$ii++) {
if($listInsurancePlan[$ii][2]==$records[$i]["id_cust_company"]) {
$planFiltered[]=array($listInsurancePlan[$ii][0],$listInsurancePlan[$ii][1]);
}
}
$elements=
array(
array(
array("*Provider",null,'onchange="filterRecJS(this,window.listInsurancePlanJSON,\'frmgroup\',\'chPlan\')"',1,$this -> listInsuranceProvider,$records[$i]["id_cust_company"]),
array("*Plan",null,'class="chPlan"',1,$planFiltered,$records[$i]["id_hr_insurance_plan"]),
array("*Coverage",null,'class="elementIcon"',1,$this -> listInsuranceCoverage,$records[$i]["id_hr_insurance_coverage"],null,self::deleteRow),
),
array(
array("*Start Date",null,'class="clDateStarted elementIcon" validateElement(\'date\',this)',0,null,$records[$i]["hr_insurance_ds"],null,self::dateStarted,null,'YYYY-MM-DD'),
array("Renewal Date",null,'onblur="validateElement(\'date\',this)" class="clDateRenewed elementIcon" validateElement(\'date\',this)',0,null,$records[$i]["hr_insurance_de"],null,self::dateRenewed,null,'YYYY-MM-DD'),
),
);
$dataElements.='<div class="frmgroup" >';
$dataElements.=frmElements($elements);
$dataElements.='</div>';
}
return $dataElements;
}
function getFooter() {
$elements = 
array(
array(
array(null,'insurance_mText',null,6,null,null,null,null,' style="display:none" '),
)
);
$footerElements=frmElements($elements);
return $footerElements;
}
function build() {
$this->getLists();
$fieldset = '<fieldset id="insurance" class="multiRow">';
$fieldset .= '<legend>Insurance</legend>';
$fieldset .= '<div class="multimenu">';
$fieldset .= '<input type="button" onClick="multiRowAddElement(this,\'frmgroup\');disableRemoveLast(\'#insurance\',\'.frmgroup\',\'.clRemoveButton\')" class="clAddButton" value="Add"/>';
$fieldset .= '<input type="button" onClick="multiRowRemoveLastElement(this,\'frmgroup\');disableRemoveLast(\'#insurance\',\'.frmgroup\',\'.clRemoveButton\')" class="clRemoveButton" value="Remove Last"/>';
$fieldset .= '</div>';
$fieldset .= $this -> getBaseElement() ;
$fieldset .= $this -> getElements();
$fieldset .= $this -> getFooter();
$fieldset .= '</fieldset><script>disableRemoveLast("#insurance",".frmgroup",".clRemoveButton")</script>';
echo $fieldset;
}
}
?>