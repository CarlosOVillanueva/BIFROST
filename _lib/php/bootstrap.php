<?php
/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#session start
mb_internal_encoding("UTF-8");
if (session_id() == "" || !isset($_SESSION)) {
session_start();
}
#includes
require_once ("_lib/php/config.php");
require_once ("bootstrap/ArcCore.php");
require_once ("bootstrap/ArcDb.php");
require_once ("bootstrap/ArcCertification.php");
require_once ("bootstrap/ArcContactMethods.php");
require_once ("bootstrap/ArcEducation.php");
require_once ("bootstrap/ArcEmail.php");
require_once ("bootstrap/ArcEmailForm.php");
require_once ("bootstrap/ArcLineItem.php");
require_once ("bootstrap/ArcInsurance.php");
require_once ("bootstrap/ArcInvoicePayment.php");
require_once ("bootstrap/ArcPayment.php");
require_once ("bootstrap/ArcTbl.php");
require_once ("bootstrap/ArcAvatarUpload.php");
require_once ("bootstrap/ArcFileUpload.php");
require_once ("bootstrap/Base64Graphics.php");
require_once ("bootstrap/ArcAddress.php");
require_once ("bootstrap/ArcPOPayment.php");
require_once ("bootstrap/ArcPOLineItem.php");
#third-party includes
require_once ("bootstrap/mime_parser.php");
require_once ("bootstrap/rfc822_addresses.php");
require_once ("bootstrap/ScanDir.php");
require_once ('Net/IPv4.php');
##date and time
date_default_timezone_set('UTC');
###system - UTC
$dateTimeZoneUTC = new DateTimeZone("UTC");
$dateTimeUTC = new DateTime("now", $dateTimeZoneUTC);
$dtCurrent = date("l, F j, Y e");
$dtTimeUnix = strtotime(date("Y-m-d H:i:s"));
$dtTimeUTC = date("Y-m-d H:i:s", (strtotime(date("Y-m-d H:i:s"))));
#application database
$gdbo = new ArcDb;
$gdbo -> dbType = $globalDBTP;
$gdbo -> dbSchema = $globalDB;
$gdbo -> dbConStr = $globalDBCON;
?>
