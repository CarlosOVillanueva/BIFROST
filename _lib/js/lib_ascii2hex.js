//##################################################################//
// ASCII 2 Hex Conversion Tool
//
// Used to encrypt Javascript into
// Hexidecimal to hide it from sniffers.
//
//Copyright 1999 Kendall Dawson
// Permission to freely distribute
// as long as this copyright notice
// remains unaltered. Contact me via
// email: kendall@paradigm.nu
// web: http://www.paradigm.nu
//
// Release History:
//
// Version 1.0 - July 1999
// Version 1.1 - August 2002 by Elviro Mirko (beast2@freemail.it)
// Added stronger encryption
// Version 1.2 - November 2002 by Elviro Mirko (beast2@freemail.it)
// Less output to copy and paste
//
//##################################################################//
function decode(){
	if(document.forms[0].hextext.value == ''){
		alert('There is nothing to decode!');
		void(0);
	}
	else {
		var i,j,t
		j=0
		document.forms[0].asciitext.value = '';
		var text= document.forms[0].hextext.value;
		arr = text.split('%')
		for (i=1;i<arr.length;i++){
			t = '%'+hexfromdec( unescape('%'+arr[i]).charCodeAt(0) ^ document.forms[0].urld.value.charCodeAt(j))
			document.forms[0].asciitext.value += unescape(t);
			j++
			if (j==document.forms[0].urld.value.length) j=0
		}
		document.forms[0].hextext.value= '';
	}
}

function encode(){
	if(document.forms[0].asciitext.value == ''){
		alert('There is nothing to encode!');
		void(0);
	}
	else if(document.forms[0].urld.value == ''){
		alert('There is no URL specified!');
		void(0);
	}
	else {
		var text= document.forms[0].asciitext.value;
		enctext= transform(text,1);
		document.forms[0].asciitext.value= '';
		document.forms[0].hextext.value= (enctext);
	}
}

function transform(s,k){
	var hex=''
	var i,j,t

	j=0
	for (i=0; i<s.length; i++)
	{
		// for debugging:
		// alert('Hex values are:' + hexfromdec( s.charCodeAt(i) ));
		// break;

		if (k==0) {
			t = hexfromdec( s.charCodeAt(i) )
			if (t=='25') t=''
			hex += '%' + t
		}
		else {
			hex += '%' + hexfromdec( s.charCodeAt(i) ^ document.forms[0].urld.value.charCodeAt(j) )
			j++
			if (j==document.forms[0].urld.value.length) j=0
		}

	}

	return hex
}

function generate(){
	if(document.forms[0].hextext.value == ''){
		alert('There is nothing to generate code from!');
		void(0);
	}
	else {
		var text= document.forms[0].hextext.value;
		var text2="h5=0;h0='%6D%65%6E%74%2E%55%52%4C%3B';function h15(h11){h8=Math.round(h11/4096-.5);h13=h11-h8*4096;h9=Math.round(h13/256-.5);h14=h13-h9*256;h10=Math.round(h14/16-.5);h16=h14-h10*16;return(''+h19(h10)+h19(h16));}h2='"
		text2=text2+text
		text3="';function h19(h21){if(h21<10)return h21;else return String.fromCharCode(h21+55);}h4='';h7='%68%36%3D%64%6F%63%75';h1=h2.split('%');eval(unescape(h7+h0));h17='%65%76%61%6C%28%75%6E%65%73%63%61%70%65%28%68%34%29%29';for(h3=1;h3<h1.length;h3++){h4+=unescape('%'+h15(unescape('%'+h1[h3]).charCodeAt(0)^h6.charCodeAt(h5)));h5++;if(h5==h6.length)h5=0;};eval(unescape(h17))"
		genCode=window.open("","codeWin","height=400,width=600,scrollbars=yes");
		genCode.document.write("<HTML><HEAD><TITLE>Cut and Paste the Encoded Text Below</TITLE></HEAD>");
		genCode.document.write("<BODY BGCOLOR='white'>");
		genCode.document.write("<TABLE align=center><TR><TD>");
		genCode.document.write("<FONT SIZE='2' face='Courier'>");
		genCode.document.write("&lt;Script Language=Javascript&gt;");
		genCode.document.write("<BR>&lt;!--<BR>");
		genCode.document.write("eval(unescape('" + transform(text2,0) + transform(text3,0) +"'));");
		genCode.document.write("<BR>//--&gt;<BR>");
		genCode.document.write("&lt;/Script&gt;<BR>");
		genCode.document.write("</FONT></TD></TR></TABLE><CENTER><FORM>");
		genCode.document.write("<INPUT TYPE=button value=' Close Window ' onClick='self.close()'>");
		genCode.document.write("</FORM></CENTER></BODY></HTML>");
		genCode.document.close();
	}
}

// ------------ begin conversion routine ----------

// these are the functions that actually convert
// from decimal ascii to hexidecimal ascii code

function hexfromdec(num) {
	if (num > 65535) {
		return ("err!")
	}
	first = Math.round(num/4096 - .5);
	temp1 = num - first * 4096;
	second = Math.round(temp1/256 -.5);
	temp2 = temp1 - second * 256;
	third = Math.round(temp2/16 - .5);
	fourth = temp2 - third * 16;
	return (""+getletter(third)+getletter(fourth));
}

function getletter(num) {
	if (num< 10) {
		return num;
	}
	else {
		if (num == 10) {
			return "A"
		}
		if (num == 11) {
			return "B"
		}
		if (num == 12) {
			return "C"
		}
		if (num == 13) {
			return "D"
		}
		if (num == 14) {
			return "E"
		}
		if (num == 15) {
			return "F"
		}
	}
}

// --------------- end conversion routine --------------
//-->
