// Converts the binary representation of data to hex
// version: 812.316
// discuss at: http://phpjs.org/functions/bin2hex
// +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
// +   bugfixed by: Onno Marsman
// +   bugfixed by: Linuxworld
// *     example 1: bin2hex('Kev');
// *     returns 1: '4b6576'
// *     example 2: bin2hex(String.fromCharCode(0x00));
// *     returns 2: '00'
function bin2hex(b){var a,c=0,d=[];b+="";c=b.length;for(a=0;a<c;a++)d[a]=b.charCodeAt(a).toString(16).replace(/^([\da-f])$/,"0$1");return d.join("")};