/*
Copyright (c) 2010-2016 Carlos Omar Villanueva

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

function estTime() {
	var j = $(".timeFld");
	j = $.makeArray(j);
	var k = $(".dateFld");
	k = $.makeArray(k);
	if (j.length != 2) {
		return false
	}
	if (k.length != 2) {
		return false
	}
	var n = new Date($(k[0]).val() + " " + $(j[0]).val());
	var l = new Date($(k[1]).val() + " " + $(j[1]).val());
	DIFF = ((l - n) / 1000).toString();
	DIFF = parseFloat(DIFF / 3600);
	DIFF = DIFF - $("#tm_deduction").val();
	DIFF = (Math.round(DIFF * 100) / 100).toFixed(2);
	if (DIFF != "NaN") {
		$("#tm_total").val(DIFF)
	} else {
		$("#tm_total").val(0)
	}
}

function toggleCheck(j, k) {
	CHECKBOXES = $.find("." + k);
	$(CHECKBOXES).prop("checked", !$(CHECKBOXES).prop("checked"))
}

function printReport() {
	CONTENT = document.getElementById("content");
	CONTENTS = CONTENT.innerHTML;
	CONTENTS = CONTENTS.replace(/\n/gi, " ");
	CONTENTS = CONTENTS.replace(/<script.+?\/script>/gi, "");
	CONTENTS = CONTENTS.replace(/<form.+?>/gi, "");
	CONTENTS = CONTENTS.replace(/<\/form.+?>/gi, "");
	CONTENTS = CONTENTS.replace(/\*/gi, "");
	CONTENTWINDOW = document.createElement("div");
	$(CONTENTWINDOW).html(CONTENTS);
	$(CONTENTWINDOW).find("input,textarea,select,div").removeAttr("id");
	$(CONTENTWINDOW).find("input,textarea,select,div").removeAttr("name");
	$(CONTENTWINDOW).find(":button").remove();
	$(CONTENTWINDOW).find('input[type="hidden"]').remove();
	RADIO = $(CONTENTWINDOW).find('input[type="radio"],input[type="checkbox"]');
	RADIO = $.makeArray(RADIO);
	for ( i = 0; i < RADIO.length; i++) {
		if (RADIO[i].checked === true) {
			BOO = "Yes"
		} else {
			BOO = "No"
		}
		$(RADIO[i]).replaceWith('<span class="radio">' + BOO + "</span>")
	}
	TBLHEADINGS = $(CONTENTWINDOW).find(".headingRow");
	TBLHEADINGS = $.makeArray(TBLHEADINGS);
	for ( i = 0; i < TBLHEADINGS.length; i++) {
		$(TBLHEADINGS[i]).find(".headingRight").remove()
	}
	FORMCRAP = $(CONTENTWINDOW).find("input,textarea");
	FORMCRAP = $.makeArray(FORMCRAP);
	for ( i = 0; i < FORMCRAP.length; i++) {
		$(FORMCRAP[i]).replaceWith('<span class="element">' + $(FORMCRAP[i]).val() + "</span>")
	}
	FORMCRAP = $(CONTENTWINDOW).find("select");
	FORMCRAP = $.makeArray(FORMCRAP);
	for ( i = 0; i < FORMCRAP.length; i++) {
		$(FORMCRAP[i]).replaceWith('<span class="element">' + $(FORMCRAP[i]).find("option:selected").text() + "</span>")
	}
	LEGENDS = $(CONTENTWINDOW).find("legend");
	LEGENDS = $.makeArray(LEGENDS);
	for ( i = 0; i < LEGENDS.length; i++) {
		$(LEGENDS[i]).replaceWith("<h1>" + $(LEGENDS[i]).html() + "</h1>")
	}
	LABELS = $(CONTENTWINDOW).find("label");
	LABELS = $.makeArray(LABELS);
	for ( i = 0; i < LABELS.length; i++) {
		$(LABELS[i]).replaceWith('<div class="label">' + $(LABELS[i]).html() + "</div>")
	}
	$(CONTENTWINDOW).find(".text").removeClass("text");
	$(CONTENTWINDOW).find(".cke").remove();
	$(CONTENTWINDOW).find(".elementIconBox").remove();
	$(CONTENTWINDOW).find(".multimenu").remove();
	R = document.createElement("form");
	R.target = "_blank";
	R.method = "POST";
	R.action = "/_lib/php/jsDeps/ArcDocExport.php";
	R.style.display = "none";
	p = document.createElement("textarea");
	p.name = "DBO";
	p.value = bin2hex($(CONTENTWINDOW).html());
	R.appendChild(p);
	document.body.appendChild(R);
	R.submit();
	document.body.removeChild(R)
}

function getLineTotalPO(k) {
	ROWIDARRAY = k.name;
	ROWIDARRAY = ROWIDARRAY.split("_");
	ROWID = ROWIDARRAY[0];
	oRATE = document.getElementById(ROWID + "_purchaseorder_item_rate");
	oQTY = document.getElementById(ROWID + "_purchaseorder_item_qty");
	oTAX = document.getElementById(ROWID + "_purchaseorder_item_percentTax");
	var n = validateElement("money", oRATE);
	var l = validateElement("money", oQTY);
	var j = validateElement("money", oTAX);
	if (j === false || $(oTAX).val() == "") {
		$(oTAX).val(0)
	}
	if (n === true && l === true) {
		AMOUNT = $(oQTY).val() * $(oRATE).val();
		TAX = ($(oTAX).val() * AMOUNT) / 100;
		TOTALAMOUNT = AMOUNT + TAX;
		$("#" + ROWID + "_purchaseorder_item_totalAmount").val(TOTALAMOUNT);
		$("#" + ROWID + "_purchaseorder_item_amount").val(AMOUNT);
		getTotalsPO()
	} else {
		return false
	}
}

function getTotalsPO() {
	AMOUNT = 0;
	AMOUNTARRAY = $(".cLineAmount");
	AMOUNTARRAY = $.makeArray(AMOUNTARRAY);
	for ( i = 0; i < AMOUNTARRAY.length; i++) {
		ROWTOTAL = $(AMOUNTARRAY[i]).val();
		ROWTOTAL = (ROWTOTAL != "" ? parseFloat(ROWTOTAL) : 0);
		AMOUNT = AMOUNT + ROWTOTAL
	}
	AMOUNT = parseFloat(Math.round(AMOUNT * 100) / 100).toFixed(2);
	$("#purchaseorder_amount").val(AMOUNT);
	TOTALAMOUNT = 0;
	TOTALAMOUNTARRAY = $(".cLineTotal");
	TOTALAMOUNTARRAY = $.makeArray(TOTALAMOUNTARRAY);
	for ( i = 0; i < TOTALAMOUNTARRAY.length; i++) {
		ROWTOTAL = $(TOTALAMOUNTARRAY[i]).val();
		ROWTOTAL = (ROWTOTAL != "" ? parseFloat(ROWTOTAL) : 0);
		TOTALAMOUNT = TOTALAMOUNT + ROWTOTAL
	}
	TOTALAMOUNT = parseFloat(Math.round(TOTALAMOUNT * 100) / 100).toFixed(2);
	$("#purchaseorder_totalAmount").val(TOTALAMOUNT);
	TAX = TOTALAMOUNT - AMOUNT;
	TAX = parseFloat(Math.round(TAX * 100) / 100).toFixed(2);
	$("#purchaseorder_totalTax").val(TAX)
}

function getLineTotal(k) {
	ROWIDARRAY = k.name;
	ROWIDARRAY = ROWIDARRAY.split("_");
	ROWID = ROWIDARRAY[0];
	oRATE = document.getElementById(ROWID + "_invoice_item_rate");
	oQTY = document.getElementById(ROWID + "_invoice_item_qty");
	oTAX = document.getElementById(ROWID + "_invoice_item_percentTax");
	var n = validateElement("money", oRATE);
	var l = validateElement("money", oQTY);
	var j = validateElement("money", oTAX);
	if (j === false || $(oTAX).val() == "") {
		$(oTAX).val(0)
	}
	if (n === true && l === true) {
		AMOUNT = $(oQTY).val() * $(oRATE).val();
		TAX = ($(oTAX).val() * AMOUNT) / 100;
		TOTALAMOUNT = AMOUNT + TAX;
		$("#" + ROWID + "_invoice_item_totalAmount").val(TOTALAMOUNT);
		$("#" + ROWID + "_invoice_item_amount").val(AMOUNT);
		getTotals()
	} else {
		return false
	}
}

function getTotals() {
	AMOUNT = 0;
	AMOUNTARRAY = $(".cLineAmount");
	AMOUNTARRAY = $.makeArray(AMOUNTARRAY);
	for ( i = 0; i < AMOUNTARRAY.length; i++) {
		ROWTOTAL = $(AMOUNTARRAY[i]).val();
		ROWTOTAL = (ROWTOTAL != "" ? parseFloat(ROWTOTAL) : 0);
		AMOUNT = AMOUNT + ROWTOTAL
	}
	AMOUNT = parseFloat(Math.round(AMOUNT * 100) / 100).toFixed(2);
	$("#invoice_amount").val(AMOUNT);
	TOTALAMOUNT = 0;
	TOTALAMOUNTARRAY = $(".cLineTotal");
	TOTALAMOUNTARRAY = $.makeArray(TOTALAMOUNTARRAY);
	for ( i = 0; i < TOTALAMOUNTARRAY.length; i++) {
		ROWTOTAL = $(TOTALAMOUNTARRAY[i]).val();
		ROWTOTAL = (ROWTOTAL != "" ? parseFloat(ROWTOTAL) : 0);
		TOTALAMOUNT = TOTALAMOUNT + ROWTOTAL
	}
	TOTALAMOUNT = parseFloat(Math.round(TOTALAMOUNT * 100) / 100).toFixed(2);
	$("#invoice_totalAmount").val(TOTALAMOUNT);
	TAX = TOTALAMOUNT - AMOUNT;
	TAX = parseFloat(Math.round(TAX * 100) / 100).toFixed(2);
	$("#invoice_totalTax").val(TAX)
}

function triggerRefresh(j, k) {
	$("#" + j).find("." + k).trigger("click")
}

function disableRemoveLast(k, l, j) {
	if ($(k).find(l).length == 1) {
		$(k).find(j).prop("disabled", true)
	} else {
		$(k).find(j).prop("disabled", false)
	}
}

function lineWrap() {
	var k = function() {
		var l = document.getElementsByClassName("syntaxhighlighter");
		for (var n = 0; n < l.length; ++n) {
			var t = l[n];
			var A = t.getElementsByClassName("gutter")[0].getElementsByClassName("line");
			var C = t.getElementsByClassName("code")[0].getElementsByClassName("line");
			var B = 15;
			for (var q = 0; q < A.length; ++q) {
				var v = $(C[q]).height();
				if (v != B) {
					console.log(q);
					A[q].setAttribute("style", "height: " + v + "px !important;")
				}
			}
		}
	};
	var j = function() {
		if ($(".syntaxhighlighter").length === 0) {
			setTimeout(j, 800)
		} else {
			k()
		}
	};
	j()
}

function highlightCell(j) {
	$(j).parent().addClass("highlight")
}

function revertCell(j) {
	$(j).parent().removeClass("highlight")
}

function collapseTabs() {
	$("#content").find("fieldset").css("display", "block");
	$("#menu").find(".activeHeading").attr("class", "menuheadingbutton");
	document.getElementById("collapseTabs").style.display = "none"
}

function mkMMRows() {
	ROWDATA = $("#x1_row").html();
	NEWROW = '<div class="frmrow">' + ROWDATA + "</div>";
	COUNT = parseInt($("#mmQuantity").val());
	TARGET = $(".multiRow");
	for ( i = 0; i < COUNT; i++) {
		TARGET.append(NEWROW)
	}
}

function resizePopups() {
	$("#interface").height("auto");
	$("#interface").height($(document).height());
	$("#popWindow").height($(document).height());
	$("#popWindowFilter").height($(document).height());
}

function toMoney(j) {
	return Math.ceil(j * 100) / 100;
}

function calcPayments() {
	DATA = $(".clPayment");
	TOTAL = 0;
	for ( i = 0; i < DATA.length; i++) {
		if ($(DATA[i]).val() != "") {
			PAYMENT = parseFloat($(DATA[i]).val());
			TOTAL = TOTAL + PAYMENT
		}
		PAID = TOTAL;
		TOTAL = toMoney(TOTAL);
		$("#paid").val(TOTAL)
	}
	VALUE = parseFloat($("#contract_value").val());
	if (VALUE > PAID) {
		BALANCE = VALUE - PAID;
		BALANCE = toMoney(BALANCE)
	} else {
		BALANCE = ""
	}
	$("#balance").val(BALANCE);
	if (PAID < VALUE) {
		$("#paid").css("background-color", "#ffcccc")
	} else {
		$("#paid").css("background-color", "#ccffcc")
	}
}

function findTargetInRow(k, j, l) {
	ROW = k;
	while (ROW.className != j) {
		ROW = ROW.parentNode
	}
	DESTINATION = getElementsByClassName(l, null, ROW);
	DESTINATION = DESTINATION[0];
	return DESTINATION
}

function filterRecJS(k, l, j, n) {
	SELECTED = $(k).val();
	BASE = JSON.parse(l);
	DESTINATION = findTargetInRow(k, j, n);
	FILTERED = [];
	for ( i = 0; i < BASE.length; i++) {
		if (BASE[i][2] == SELECTED) {
			CHILD = [];
			CHILD.push(BASE[i][0]);
			CHILD.push(BASE[i][1]);
			FILTERED.push(CHILD)
		}
	}
	$(DESTINATION).find("option").remove();
	$("<option>--</option>").appendTo(DESTINATION);
	for ( i = 0; i < FILTERED.length; i++) {
		$('<option value="' + FILTERED[i][0] + '">' + FILTERED[i][1] + "</option>").appendTo(DESTINATION)
	}
}

function filterRec(k, j, l) {
	if ( typeof k === "object") {
		VALUE = "'" + $(k).val() + "'"
	} else {
		VALUE = "'" + k + "'"
	}
	arc(l, "/_lib/php/jsDeps/ArcFilterRec.php", "VAL=" + bin2hex(VALUE) + "&DBO=" + j, 0, 0)
}

function uploadFiles(j) {
	obj = JSON.parse(j);
	arc("popWindow", "_lib/php/ArcFileUpload.View.php", "filter=" + obj.oRec[0].filter + "&recid=" + obj.oRec[0].recid + "&module=" + obj.oRec[0].module, 1, 1)
}

function uploadFile(j) {
	obj = JSON.parse(j);
	arc("popWindow", "_lib/php/ArcAvatarUpload.View.php", "filter=" + obj.oRec[0].filter + "&recid=" + obj.oRec[0].recid + "&module=" + obj.oRec[0].module, 1, 1)
}

function deleteFSO(k, j) {
	CONFIRM = confirm("Are you sure you want to delete this File?");
	if (CONFIRM == 1) {
		arc("listFiles", "/_lib/php/ArcFileUpload.SQL.php", "&action=delete&fso_smod=" + j + "&id_fso=" + k, 1, 1)
	}
}

function deleteRecord(j) {
	var k = JSON.parse(j);
	var l = confirm("Are you sure you want to delete this record?\nThis may impact other records that reference it.");
	if (l == 1) {
		arc("content", "_mod/smod_" + k.oRec[0].module + "/sql.php", k.oRec[0].filter + "=" + k.oRec[0].recid + "&action=delete&form=" + k.oRec[0].formname, 1, 1)
	}
}

function toggleChild(j) {
	PARENT = $(j).parent();
	if ($(j).find("i").hasClass("fa-chevron-circle-up") == true) {
		$(j).find("i").addClass("fa-chevron-circle-down");
		$(j).find("i").removeClass("fa-chevron-circle-up")
	} else {
		$(j).find("i").addClass("fa-chevron-circle-up");
		$(j).find("i").removeClass("fa-chevron-circle-down")
	}
	$(PARENT).find(".childlist").toggle()
}

function buildSchemaView(j) {
	CON = j.options[j.selectedIndex].value;
	arc("queryBuilder", "/_mod/smod_19/model_query.php", "id_con=" + CON, 1, 1)
}

function buildTableView(j, l, k) {
	CON = j.options[j.selectedIndex].value;
	if (l != undefined) {
		l = "sql=" + bin2hex($(l).val()) + "&"
	} else {
		l = ""
	}
	arc("list19", "/_mod/smod_19/view_table.php", l + "id_con=" + CON, 1, 1)
}

function tabSubMenu(l, k, q, n, j) {
	SCOPE = document.getElementById(j);
	fsets = SCOPE.getElementsByTagName("fieldset");
	total = fsets.length;
	for ( i = 0; i < total; i++) {
		if (fsets[i].id != k) {
			fsets[i].style.display = "none"
		} else {
			$(fsets[i]).fadeIn("slow")
		}
	}
	$("#popBox").find("." + q).removeClass(q);
	$(l).addClass(q)
}

function tabMenu(j, n, l, k) {
	FIELDSETS = $("#" + j).find("fieldset");
	TABS = "";
	LABELS = [];
	for ( i = 0; i < FIELDSETS.length; i++) {
		if (FIELDSETS[i].id != "") {
			LABELS.push(FIELDSETS[i])
		}
	}
	for ( i = 0; i < LABELS.length; i++) {
		oID = LABELS[i].id;
		LABELID = "_" + LABELS[i].id;
		LABEL = LABELS[i].id.replace(/fs_|_/gi, " ");
		if (i == 0) {
			activeHead = l
		} else {
			activeHead = ""
		}
		TABS = TABS + '<div id="' + LABELID + '" class="tab ' + activeHead + '" onclick="tabSubMenu(this,\'' + oID + "','" + l + "','" + k + "','" + j + "')\">" + LABEL + "</div>";
		if (i != 0) {
			LABELS[i].style.display = "none"
		}
	}
	$("#" + n).html(TABS)
}

function childListFilter(j, q, v, A, n, t, k) {
	t = document.getElementById(t);
	switch(q) {
	case"list":
		base = $(j).val();
		break;
	case"text":
		base = $(j).val();
		break;
	case"radio":
		radName = j.name;
		radList = document.getElementsByTagName(radName);
		for (i in radList) {
			if (radList[i].checked === true) {
				base = radList[i].value;
				break
			}
		}
		break;
	case"checkbox":
		if (j.checked === true) {
			base = $(j).val()
		} else {
			return false
		}
		break
	}
	if (k != undefined) {
		dependents = getElementsByClassName(k);
		for (i in dependents) {
			if (base == undefined || base.length == 0) {
				switch(dependents[i].type) {
				case"select":
					dependents[i].selectedIndex = 0;
					break;
				case"radio":
				case"checkbox":
					dependents[i].checked = false;
					break;
				case"button":
				case"textarea":
					break;
				default:
					dependents[i].value = "";
					break
				}
				dependents[i].disabled = true
			} else {
				dependents[i].disabled = false
			}
		}
	}
	if (v == undefined || v.length == 0) {
		alert("The  list is missing for the node you are trying to populate.");
		return false
	}
	if (t == undefined) {
		alert("The destination node does not exists.");
		return false
	}
	if (n == undefined || n.length == 0) {
		alert("Please define an index value to extract from.");
		return false
	} else {
		extracted = n.split("-");
		for (i in extracted) {
			extracted[i] = trimLeftAndRight(extracted[i])
		}
		if (v[0][extracted[0]] == undefined || v[0][extracted[1]] == undefined) {
			alert("The defined indexes to extract do not exist.");
			return false
		} else {
			var B = extracted[0];
			var l = extracted[1]
		}
	}
	while (t.childNodes.length > 0) {
		t.removeChild(t.firstChild)
	}
	nullOption = document.createElement("option");
	nullOption.text = "--";
	t.appendChild(nullOption);
	for (i in v) {
		if (v[i][A] == base) {
			newOption = document.createElement("option");
			newOption.setAttribute("value", v[i][B]);
			newOption.innerHTML = v[i][l];
			t.appendChild(newOption)
		}
	}
	t.disabled = false
}

function recalcContract() {
	paidTotalArray = getElementsByClassName("contractPaid");
	totalPaid = 0;
	for (i in paidTotalArray) {
		totalPaid = parseFloat(totalPaid + paidTotalArray[i].value)
	}
	document.getElementById("contract_balance").value = parseFloat(document.getElementById("contract_value").value - totalPaid)
}

function delConfiguration(j) {
	b = confirm("Are you sure you want to delete this configuration?");
	if (b == true) {
		arc("content", "/_mod/smod_18/sql.php", "form=frmConfigInsert&action=delete&id=" + j, 1, 1)
	} else {
		return false
	}
}

function delCfgDevice(k, j) {
	b = confirm("Are you sure you want to delete this device?");
	if (b == true) {
		arc("list18-0", "/_mod/smod_18/sql.php", "form=frmDevice&action=delete&id_cfg=" + k + "&id_device=" + j, 1, 1)
	} else {
		return false
	}
}

function delContract(j) {
	b = confirm("Are you sure you want to delete this contract? All payments associated with it will be deleted as well?");
	if (b == true) {
		arc("content", "/_mod/smod_08/sql.php", "id_contract=" + j, 1, 1)
	} else {
		return false
	}
}

function delEmployee(j) {
	b = confirm("Are you sure you want to delete this employee? All records associated with that person will be deleted as well?");
	if (b == true) {
		arc("content", "/_mod/smod_05/insertSQL.php", "edit=2&id_hr_emp=" + j, 1, 1)
	} else {
		return false
	}
}

function delServiceticket(j) {
	b = confirm("Are you sure you want to delete this service ticket? All time associated with it will be deleted as well?");
	if (b == true) {
		arc("content", "/_mod/smod_10/sql.php", "id_srv=" + j, 1, 1)
	} else {
		return false
	}
}

function setAction(j) {
	setVal = document.getElementById(j+"_timeAction").options[document.getElementById(j + "_timeAction").selectedIndex].value;
	switch(setVal) {
	case"Approve":
		approveTimesheet(j);
		break;
	case"Reject":
		rejectTimesheet(j);
		break;
	case"Delete":
		deleteTimesheet(j);
		break
	}
}

function rejectTimesheet(j) {
	b = confirm("Are you sure you want to reject this timesheet?");
	if (b == true) {
		arc("content", "/_mod/smod_16/timecardapprovalsql.php", "id_tm_timesheet=" + j + "&tm_timesheet_approved=2&edit=1", 1, 1)
	} else {
		return false
	}
}

function approveTimesheet(j) {
	b = confirm("Are you sure you want to approve this timesheet?");
	if (b == true) {
		arc("content", "/_mod/smod_16/timecardapprovalsql.php", "id_tm_timesheet=" + j + "&tm_timesheet_approved=1&edit=1", 1, 1)
	} else {
		return false
	}
}

function deleteTimesheet(j) {
	b = confirm("Are you sure you want to delete this timesheet?");
	if (b == true) {
		arc("content", "/_mod/smod_16/timecardapprovalsql.php", "id_tm_timesheet=" + j + "&tm_timesheet_approved=1&edit=2", 1, 1)
	} else {
		return false
	}
}

function reviewTimesheet(j) {
	arc("popWindow", "/_mod/smod_16/timecardreview.php", "id_tm_timesheet=" + j, 1, 1)
}

function array2json(l) {
	l = l.split("&");
	var k = {};
	var j = [];
	for ( i = 0; i < l.length; i++) {
		j.push(l[i].split("="))
	}
	for ( i = 0; i < j.length; i++) {
		k[j[i][0]] = j[i][1]
	}
	return JSON.stringify(k)
}

function arc(n, j, q, k, t, l) {
	if (document.getElementById(n) == undefined) {
		return false
	}
	if (window.refreshOb != null) {
		clearInterval(window.refreshOb)
	}
	if (l == undefined || l == true) {
		document.getElementById(n).style.display = ""
	}
	if (t === 1) {
		document.getElementById(n).innerHTML = '<div class="loadingText"><i class="fa fa-cog fa-spin"></i> Loading...</div>'
	}
	if (q != undefined) {
		POSTDATA = array2json(q);
		POSTDATA = $.parseJSON(POSTDATA.toString())
	} else {
		POSTDATA = null
	}
	ARC = $.ajax({
		url : j,
		type : "POST",
		data : POSTDATA,
		dataType : "html"
	});
	ARC.done(function(v) {
		n = "#" + n;
		$(n).html(v);
		resizePopups();
	});
	ARC.fail(function(v, A) {
			resizePopups();
	
	});
}

function timedRefresh(B, C, t, A, v, k) {
	if (v != undefined) {
		v = v
	} else {
		v = 1
	}
	if (k != undefined) {
		k = k
	} else {
		k = 1
	}
	window.refreshOb = setInterval("arc('" + B + "','" + C + "','" + t + "'," + v + "," + k + ");", A)
}

function detectSave(n, k) {
	var l = k || window.event;
	if (l.ctrlKey && l.keyCode == 83 && document.forms[0] != undefined) {
		if (navigator.appName != "Microsoft Internet Explorer") {
			l.preventDefault()
		}
		prepSaveChanges();
		return false
	}
}

function detectEnter(n, k) {
	var l = k || window.event;
	if (l.keyCode == 13) {
		validate()
	}
}

function clearPop(k) {
	if (k == undefined) {
		k = "popWindow"
	}
	if (Object.prototype.toString.call(k) === "[object Array]") {
		for ( i = 0; i < k.length; i++) {
			if (document.getElementById(k[i]) != undefined) {
				document.getElementById(k[i]).style.display = "none";
				document.getElementById(k[i]).innerHTML = ""
			}
		}
	} else {
		if (document.getElementById(k) != undefined) {
			document.getElementById(k).style.display = "none";
			document.getElementById(k).innerHTML = ""
		}
	}
	resizePopups();
}

function cancel(j, k) {
	c = j.split(",");
	for ( i = 0; i < c.length; i++) {
		document.getElementById(c[i]).disabled = false
	}
	d = k.split(",");
	for ( i = 0; i < d.length; i++) {
		document.getElementById(d[i]).innerHTML = "";
		document.getElementById(d[i]).style.display = "none"
	}
}

function expireCookie(k) {
	var l = new Date();
	l.setTime(l.getTime() - 1);
	document.cookie = k += "=; expires=" + l.toGMTString()
}

function detectPrintScr(n, k) {
	var l = k || window.event;
	if (l.keyCode == 44) {
		if (navigator.appName != "Microsoft Internet Explorer") {
			l.preventDefault()
		}
		alert("An attempt to print the screen has been detected. Your username has been logged.")
	}
}

function toggle(j) {
	o = document.getElementById(j);
	if (o.style.display == "none") {
		o.style.display = ""
	} else {
		o.style.display = "none"
	}
}

function showStaticDetails(j) {
	var l = document.getElementById("row_" + j);
	var k = document.getElementById("col_" + j);
	if (k.style.display == "") {
		l.style.display = "none";
		k.style.display = "none"
	} else {
		l.style.display = "";
		k.style.display = ""
	}
	oPop = document.getElementById("popWindowFull");
	if (oPop != undefined && oPop.style.display == "") {
		oPopH = $(document).height();
		oPop.style.height = oPopH + "px"
	}
}

function filterData(n, l, t, j, v, k) {
	a = document.getElementById(n);
	b = a.innerHTML;
	if (b.search(k) == -1) {
		arc(n, l, t, j, v)
	} else {
		a.style.display = ""
	}
}

function qry2csv(j) {
	var k = document.createElement("form");
	k.target = "_blank";
	k.method = "POST";
	k.action = "/_lib/php/jsDeps/ArcDbExport.php";
	k.style.display = "none";
	var l = document.createElement("textarea");
	l.name = "DBO";
	l.value = j;
	k.appendChild(l);
	document.body.appendChild(k);
	k.submit();
	document.body.removeChild(k)
}

function setFilterValue(j) {
	SOURCEID = $(j).attr("id");
	SOURCEIDARRAY = SOURCEID.split("_");
	SOURCEVAL = $(j).val();
	DESTINATIONROWMARKER = SOURCEIDARRAY[0];
	DESTINATIONID = DESTINATIONROWMARKER + "_filterval";
	DESTINATION = $("#" + DESTINATIONID);
	DESTINATION.val("");
	$(DESTINATION).attr("disabled", false);
	if ($(j).val() > 6) {
		$(DESTINATION).attr("onblur", "validateElement('int',this)")
	} else {
		$(DESTINATION).removeAttr("onblur")
	}
}

function getSelFilter(j) {
	var l = [["", "--"], [1, "Begins With"], [2, "Contains"], [3, "Ends With"], [4, "Equals"], [5, "Not Equals"], [6, "Not Like"]];
	var k = [["", "--"], [7, "Greater Than"], [8, "Less Than"], [9, "Equals"], [10, "Not Equals"]];
	SOURCEID = $(j).attr("id");
	SOURCEIDARRAY = SOURCEID.split("_");
	SOURCEVAL = $(j).val();
	DESTINATIONROWMARKER = SOURCEIDARRAY[0];
	DESTINATIONID = DESTINATIONROWMARKER + "_filterby";
	DESTINATION = $("#" + DESTINATIONID);
	VALUEFIELDID = DESTINATIONROWMARKER + "_filterval";
	VALUEFIELD = $("#" + VALUEFIELDID);
	$(VALUEFIELD).attr("disabled", true);
	$(VALUEFIELD).val();
	$(DESTINATION).empty();
	switch(SOURCEVAL) {
	case"char":
	case"unknown":
	case"text":
	case"datetime":
	case"string":
	case"varchar":
	case"blob":
	case"timestamp":
		$.each(l, function(n, q) {
			$(DESTINATION).append('<option value="' + q[0] + '">' + q[1] + "</option>")
		});
		$(DESTINATION).attr("disabled", false);
		break;
	case"real":
	case"int":
	case"decimal":
	case"int4":
		$.each(k, function(n, q) {
			$(DESTINATION).append('<option value="' + q[0] + '">' + q[1] + "</option>")
		});
		$(DESTINATION).attr("disabled", false);
		break;
	default:
		break
	}
}

function buildLocalFilter(n, l, k, j) {
	filterArray = [];
	filterGroup = "";
	Q = "";
	H = 1;
	L = document.getElementById("hiddenVars").value;
	L = trimLeftAndRight(L);
	J = document.getElementById("multi");
	dataRow = getElementsByClassName("frmrow", null, J);
	for ( i = 0; i < dataRow.length; i++) {
		G = dataRow[i];
		col = "";
		S = "";
		N = "";
		P = "";
		M = "";
		I = "";
		filterColArray = getElementsByClassName("mFilterCol", null, G);
		filterValArray = getElementsByClassName("mFilterVal", null, G);
		filterByArray = getElementsByClassName("mFilterBy", null, G);
		filterBooArray = getElementsByClassName("mFilterBoo", null, G);
		filterGrpArray = getElementsByClassName("mGrpBy", null, G);
		for (a in filterGrpArray) {
			filterGroup = filterGrpArray[a].options[filterGrpArray[a].selectedIndex].value
		}
		for (a in filterColArray) {
			if (filterColArray[a].disabled === false && filterColArray[a].selectedIndex != 0) {
				switch(j) {
				case"mysql":
					quot = "`";
					col = quot + filterColArray[a].options[filterColArray[a].selectedIndex].text + quot;
					break;
				case"mssql":
					col = "[" + filterColArray[a].options[filterColArray[a].selectedIndex].text + "]";
					break;
				case"pgsql":
					quot = '"';
					col = quot + filterColArray[a].options[filterColArray[a].selectedIndex].text + quot;
					break
				}
			}
		}
		for (a in filterByArray) {
			if (filterByArray[a].disabled === false && filterByArray[a].selectedIndex != 0) {
				N = filterByArray[a].options[filterByArray[a].selectedIndex].value
			}
		}
		for (a in filterValArray) {
			if (filterValArray[a].disabled === false && filterValArray[a].value != "") {
				M = filterValArray[a].value;
				M = trimLeftAndRight(M);
				M = M.replace("'", "''")
			}
		}
		for (a in filterBooArray) {
			if (filterBooArray[a].checked == true) {
				P = " " + filterBooArray[a].value + " ";
				H = H + 1
			}
		}
		if (M != "") {
			switch(N) {
			case"7":
				I = ">" + M;
				break;
			case"8":
				I = "<" + M;
				break;
			case"9":
				I = "=" + M;
				break;
			case"10":
				I = "<>" + M;
				break;
			case"4":
				I = "='" + M + "'";
				break;
			case"5":
				I = "<>'" + M + "'";
				break;
			case"1":
				I = " like '" + M + "%'";
				break;
			case"2":
				I = " like '%" + M + "%'";
				break;
			case"3":
				I = " like '%" + M + "'";
				break;
			case"6":
				I = " not like '%" + M + "%'";
				break;
			default:
				I = "";
				break
			}
		} else {
			M = ""
		}
		if (col != "" && M != "") {
			S = P + col + I;
			filterSubGroup = [];
			filterSubGroup.push(filterGroup, trimLeftAndRight(P), col + I);
			filterArray.push(filterSubGroup)
		} else {
			S = ""
		}
		Q = Q + S
	}
	filterArray.sort(function(q, t) {
		if (q[0] < t[0]) {
			return -1
		}
		if (q[0] > t[0]) {
			return 1
		}
		if (q[0].length < t[1].length) {
			return -1
		}
		if (q[0].length > t[1].length) {
			return 1
		}
		return 0
	});
	customFilter = "";
	for (i in filterArray) {
		pre = parseInt(i) - 1;
		next = parseInt(i) + 1;
		end = parseInt(filterArray.length) - 1;
		if (i == 0 || (i != 0 && filterArray[i][0] != filterArray[pre][0])) {
			operator = (i != 0 ? filterArray[i][1] : "");
			customFilter = customFilter + operator + "(" + filterArray[i][2]
		} else {
			operator = (i != 0 ? " " + filterArray[i][1] + " " : "");
			customFilter = customFilter + operator + filterArray[i][2]
		}
		if (i == (end) || (i != (end) && filterArray[i][0] != filterArray[next][0])) {
			customFilter = customFilter + ")"
		}
	}
	Decoded = hex2bin(k);
	k = $.parseJSON(Decoded);
	k.dbOffset = "1";
	k.recPage = "1";
	k.recFilter = "where " + customFilter;
	k = JSON.stringify(k);
	k = bin2hex(k);
	if (H == dataRow.length) {
		clearPop("popWindowFilter");
		arc(l, n, "arctbl=" + k, 1, 1)
	} else {
		alert("There was an error with your request. Please select AND/OR or remove the additional filter.")
	}
}
JSON.stringify = JSON.stringify ||
function(A) {
	var q = typeof (A);
	if (q != "object" || A === null) {
		if (q == "string") {
			A = '"' + A + '"'
		}
		return String(A)
	} else {
		var B,
		    k,
		    l = [],
		    j = (A && A.constructor == Array);
		for (B in A) {
			k = A[B];
			q = typeof (k);
			if (q == "string") {
				k = '"' + k + '"'
			} else {
				if (q == "object" && k !== null) {
					k = JSON.stringify(k)
				}
			}
			l.push(( j ? "" : '"' + B + '":') + String(k))
		}
		return ( j ? "[" : "{") + String(l) + ( j ? "]" : "}")
	}
};
function pad(k, n) {
	return k.length < n ? pad("0" + k, n) : k
}

function bin2hex(k) {
	var n,
	    t,
	    l = 0,
	    j = [];
	k += "";
	l = k.length;
	for ( t = 0; t < l; t++) {
		j[t] = k.charCodeAt(t).toString(16).replace(/^([\da-f])$/, "0$1")
	}
	return j.join("")
}

function hex2bin(l) {
	var j = "";
	for (var k = 0; k < l.length; k += 2) {
		j += String.fromCharCode(parseInt(l.substr(k, 2), 16))
	}
	return j
}

function isNumeric(j) {
	return !isNaN(parseFloat(j)) && isFinite(j) && j.length > 0
}

function isNumericWhole(j) {
	return !isNaN(parseFloat(j)) && isFinite(j) && j > 0 && j.length > 0
}

function trimLeft(j) {
	return j.replace(/^\s+/, "")
}

function trimRight(j) {
	return j.replace(/\s+$/, "")
}

function trimLeftAndRight(j) {
	return j.replace(/^\s+|\s+$/g, "")
}

function setCommonValues(k, n, q) {
	o = k;
	o.id = "x";
	markerClass = o.className;
	while (o.className != "multiRow") {
		o = o.parentNode
	}
	eleArray = getElementsByClassName(n, null, o);
	markerArray = getElementsByClassName(markerClass, null, o);
	for ( i = 0; i < markerArray.length; i++) {
		if (markerArray[i].id === "x") {
			marker = i
		}
	}
	obj = eleArray[marker];
	objTag = obj.tagName;
	objType = obj.type;
	if (objTag == "select-one" || objTag == "SELECT") {
		indexVal = obj.selectedIndex;
		if (indexVal.selectedIndex != 0) {
			if (q == true) {
				for (x in eleArray) {
					eleArray[x].selectedIndex = indexVal
				}
			}
		}
	} else {
		if (objType == "text") {
			indexVal = eleArray[marker].value;
			if (indexVal.value != "") {
				if (q == true) {
					for (x in eleArray) {
						eleArray[x].value = indexVal
					}
				}
			}
		} else {
			if (obj.checked == true) {
				for (x in eleArray) {
					eleArray[x].checked = true
				}
			} else {
				for (x in eleArray) {
					eleArray[x].checked = false
				}
			}
		}
	}
	k.id = null
}

function enableButton(k, j) {
	switch(k.type) {
	case"select-one":
		if (k.selectedIndex != 0) {
			document.getElementById(j).disabled = false
		} else {
			document.getElementById(j).disabled = true
		}
		break;
	case"text":
		if (k.value != "") {
			document.getElementById(j).disabled = false
		} else {
			document.getElementById(j).disabled = true
		}
		break;
	default:
		break
	}
}

function getSelectValue(j) {
	b = document.getElementById(j);
	return b.options[b.selectedIndex].value
}

function disableChild(j, k) {
	c = k.split(",");
	voidFormElements(k);
	if (c.length > 0) {
		for ( i = 0; i < c.length; i++) {
			if (document.getElementById(c[i]) != null) {
				if (j.selectedIndex == 0) {
					document.getElementById(c[i]).selectedIndex = 0;
					document.getElementById(c[i]).disabled = true
				} else {
					document.getElementById(c[i]).disabled = false
				}
			}
		}
	}
}

function disableChildAbs(j) {
	c = j.split(",");
	voidFormElements(j);
	if (c.length > 0) {
		for ( i = 0; i < c.length; i++) {
			document.getElementById(c[i]).disabled = true
		}
	}
}

function addTimeStamp(j, k) {
	b = document.getElementById(j);
	if (b.value == "") {
		c = ""
	} else {
		c = "\n"
	}
	if (k != null) {
		user = k
	} else {
		user = ""
	}
	b.value = "[" + new Date() + "] : [" + user + "]\n----------------------------------------------------------------------\n\n" + b.value
}

function clearDefault(k) {
	if (k.defaultValue == k.value) {
		k.value = ""
	}
	if (k.style) {
		k.style.cssText = ""
	}
}

function cpSelToTxt(k, j, t, l) {
	if (k.selectedIndex != 0) {
		e = k.options[k.selectedIndex].text;
		if (t != undefined) {
			e = e.substring(0, t)
		}
		if (l != undefined) {
		}
		document.getElementById(j).value = e;
		document.getElementById(j).disabled = false
	} else {
		document.getElementById(j).value = "";
		document.getElementById(j).disabled = true
	}
}

function submitFrmVals(t, l, n, j, k, v) {
	if (k == null) {
		k = null
	} else {
		k = k
	}
	if (n != undefined) {
		validated = validateRequiredFields(n);
		if (validated === false) {
			return
		}
	}
	if (n == undefined || validated == true) {
		f = getFrmVals(k);
		if (j != undefined) {
			f = f + j
		}
		if (v != undefined && v == 1) {
			alert(f)
		}
		if (l != undefined) {
			arc(t, l, f, 1, 1)
		}
	}
}

function voidFormElements(k) {
	b = k.split(",");
	for ( i = 0; i < b.length; i++) {
		elms = document.getElementById(b[i]);
		switch(elms.type) {
		case"text":
			elms.value = "";
			break;
		case"textarea":
			elms.value = "";
			break;
		case"password":
			elms.value = "";
			break;
		case"radio":
			elms.checked = false;
			break;
		case"checkbox":
			break;
		case"select-one":
			elms.selectedIndex = 0;
			break;
		case"select-multiple":
			elms.selectedIndex = 0;
			break;
		case"select":
			elms.selectedIndex = 0;
			break
		}
	}
}

function setElementValidation(k) {
	c = $(k).val();
	switch(c) {
	case"1":
		d = "email";
		break;
	case"2":
		d = "phone";
		break;
	case"3":
		d = "phone";
		break;
	case"4":
		d = "twitter";
		break;
	case"5":
		d = "phone";
		break;
	case"6":
		d = "facebook";
		break;
	case"7":
		d = "url";
		break
	}
	ROW = k;
	while (ROW.className != "frmrow") {
		ROW = ROW.parentNode
	}
	FRMROWARRAY = $(ROW).find("input");
	FRMROWARRAY = $.makeArray(FRMROWARRAY);
	TARGET = FRMROWARRAY[0];
	$(TARGET).val("");
	PTAG = $(TARGET).parent();
	$(PTAG).html("");
	z = document.createElement("input");
	z.setAttribute("type", "text");
	z.setAttribute("onblur", "validateElement('" + d + "',this)");
	$(PTAG).append(z)
}

function voidFormElementsByType(q, l, k) {
	if (k == null) {
		k = ""
	} else {
		k = k
	}
	if (l == null) {
		l = document
	} else {
		l = l
	}
	b = q.split(",");
	for ( i = 0; i < b.length; i++) {
		eleType = b[i];
		switch(eleType) {
		case"radio":
			bb = l.getElementsByTagName("input");
			for ( ii = 0; ii < bb.length; ii++) {
				if (bb[ii].checked === true && bb[ii].className != k) {
					bb[ii].checked = false
				}
			}
			break;
		case"input.text":
			bb = l.getElementsByTagName("input");
			for ( ii = 0; ii < bb.length; ii++) {
				if (bb[ii].value !== "" && bb[ii].type === "text" && bb[ii].className != k) {
					bb[ii].value = ""
				}
			}
			break;
		case"select":
			bb = l.getElementsByTagName("select");
			for ( ii = 0; ii < bb.length; ii++) {
				if (bb[ii].selectedIndex > 0 && bb[ii].className != k) {
					bb[ii].selectedIndex = 0
				}
			}
			break;
		case"input.password":
			bb = l.getElementsByTagName("input");
			for ( ii = 0; ii < bb.length; ii++) {
				if (bb[ii].value !== "" && bb[ii].type === "password" && bb[ii].className != k) {
					bb[ii].value = ""
				}
			}
		}
	}
}

function detectTab(n, k) {
	var l = k || window.event;
	if (l.keyCode == 9) {
		if (navigator.appName != "Microsoft Internet Explorer") {
			l.preventDefault()
		}
		l.keyCode = 0;
		l.returnValue = false;
		l.preventDefault
		insertTab(n)
	}
}

function getCursorLocation(j) {
	var n,
	    l,
	    k,
	    v = -1;
	if ( typeof j.selectionStart == "number") {
		v = j.selectionStart
	} else {
		if (document.selection && j.createTextRange) {
			n = document.selection;
			if (n) {
				k = n.createRange();
				l = j.createTextRange();
				l.setEndPoint("EndToStart", k);
				v = l.text.length
			}
		}
	}
	return v
}

function insertTab(j) {
	obj = j;
	position = getCursorLocation(j);
	str = j.value;
	value = "    ";
	f = str.substring(0, position) + value + str.substring(position, str.length);
	j.value = f;
	j.focus();
	setSelectionRange(j, position + 1, position + 1)
}

function subForm(j) {
	document.getElementById(j).submit()
}

function utcCoding(t, v, q) {
	dateArray = v.split("-");
	timeArray = q.split(":");
	for (i in dateArray) {
		if (i != 1) {
			dateArray[i] = parseInt(dateArray[i])
		} else {
			dateArray[i] = parseInt(dateArray[i]) - 1
		}
	}
	for (i in timeArray) {
		timeArray[i] = parseInt(timeArray[i])
	}
	var k = new Date();
	k.setFullYear(dateArray[0], dateArray[1], dateArray[2]);
	k.setHours(timeArray[0], timeArray[1], timeArray[2]);
	u = k.getTime() + (t * 3600000);
	return (u)
}

function pad2time(k) {
	if (k < 10) {
		k = "0" + k
	}
	return k
}

function calcTime(k, n) {
	nd = new Date(n + (3600000 * k));
	h = pad2time(nd.getHours());
	m = pad2time(nd.getMinutes());
	s = pad2time(nd.getSeconds());
	return nd.toDateString() + " " + h + ":" + m + ":" + s
}

function filterDates(n, k) {
	month = document.getElementById("month");
	month1 = document.getElementById("month1");
	day = document.getElementById("day");
	day1 = document.getElementById("day1");
	year = document.getElementById("year");
	year1 = document.getElementById("year1");
	month = month.options[month.selectedIndex].value;
	month1 = month1.options[month1.selectedIndex].value;
	day = day.options[day.selectedIndex].value;
	day1 = day1.options[day1.selectedIndex].value;
	year = year.options[year.selectedIndex].value;
	year1 = year1.options[year1.selectedIndex].value;
	arc(n, k, "month=" + month + "&month1=" + month1 + "&day=" + day + "&day1=" + day1 + "&year=" + year + "&year1=" + year1, 1, 1)
}

function filterInsertedRow(l, k) {
	c = getElementsByClassName(k);
	z = c.length - 1;
	if (c[0].selectedIndex !== 0) {
		c[c.length - 1].remove(c[0].selectedIndex)
	}
}

function setConscFltrSelect(n, k, l) {
	if (n.selectedIndex != 0) {
		g = 0;
		d = getElementsByClassName(k);
		e = [];
		for ( i = 0; i < d.length; i++) {
			if (d[i] === n) {
				start = i + 1
			}
			if (d[i].selectedIndex > 0) {
				e[g] = d[i].options[d[i].selectedIndex].value;
				g = g + 1
			}
		}
		h = l.length - e.length - 1;
		f = new Array(h);
		for ( i = 0; i < f.length; i++) {
			f[i] = new Array(2)
		}
		window.filteredArray = [];
		filterArray(l, e, filteredArray);
		z = parseInt(n.options[n.selectedIndex].text);
		for ( i = 0; i < filteredArray.length; i++) {
			zz = parseInt(filteredArray[i][1]);
			if (filteredArray[i][1] >= z) {
				z = z + 1;
				if (zz === z) {
					d[start].value = filteredArray[i][0];
					start = start + 1
				}
			}
		}
		filterChild(k, l)
	}
}

function filterArray(l, n, t) {
	var k;
	var v;
	value2push = [];
	value2push[0] = null;
	value2push[1] = "--";
	t.push(value2push);
	for ( ii = 0; ii < l.length; ii++) {
		k = l[ii][0];
		v = l[ii][1];
		for ( i = 0; i < n.length; i++) {
			if (n[i][0] != k) {
				k = k;
				v = v
			} else {
				k = "";
				v = ""
			}
		}
		if (k != "" || v != "") {
			value2push = [];
			value2push[0] = k;
			value2push[1] = v;
			t.push(value2push)
		}
	}
	return t
}

function filterChild(n, l) {
	c = getElementsByClassName(n);
	selectedVars = [];
	for ( i = 0; i < c.length; i++) {
		selNode = c[i];
		if (selNode.selectedIndex !== 0) {
			if (selectedVars.length < 1) {
				g = 0
			} else {
				g = selectedVars.length + 1
			}
			currentVar = selNode.options[selNode.selectedIndex].value;
			selectedVars[g] = selNode.options[selNode.selectedIndex].value
		} else {
			currentVar = ""
		}
		while (selNode.childNodes.length > 0) {
			selNode.removeChild(selNode.firstChild)
		}
		for ( i1 = 0; i1 < l.length; i1++) {
			o = document.createElement("option");
			o.text = l[i1][1];
			o.value = l[i1][0];
			try {
				selNode.add(o, null)
			} catch(k) {
				selNode.add(o)
			}
		}
		if (currentVar !== "") {
			selNode.value = currentVar
		} else {
			selNode.selectedIndex = 0
		}
	}
	for ( i = 0; i < c.length; i++) {
		selNode = c[i];
		if (selNode.selectedIndex !== 0) {
			currentVar = selNode.options[selNode.selectedIndex].value
		} else {
			currentVar = ""
		}
		if (selectedVars.length > 0) {
			for ( ii = 0; ii < selectedVars.length; ii++) {
				for ( iii = 0; iii < selNode.length; iii++) {
					if (selectedVars[ii] != currentVar && selNode.options[iii].value == selectedVars[ii]) {
						selNode.remove(iii)
					}
				}
			}
		}
	}
}

function multiRowAddElement(k, v, l, t, n) {
	addBackground = false;
	while (k.className != "multiRow") {
		k = k.parentNode
	}
	c = getElementsByClassName(v, null, k);
	if (n != undefined) {
		totalRows = c.length;
		if (totalRows % 2 != 0) {
			addBackground = n
		} else {
			addBackground = ""
		}
	}
	if (l == undefined || c.length < l) {
		g = c[0].innerHTML;
		w = c.length + 1;
		g = g.replace(/x1_/gi, "x" + w + "_");
		h = document.createElement("div");
		if (addBackground != "") {
			h.setAttribute("style", n)
		} else {
			h.removeAttribute("style")
		}
		if (v != null) {
			h.setAttribute("class", v);
			h.setAttribute("className", v)
		}
		g = g.replace(":none", "inline");
		h.innerHTML = g;
		voidFormElementsByType("select,input.text", h, t);
		k.appendChild(h)
	} else {
		alert("You can only have " + l + " devices.")
	}
}

function incFields(l, v, k) {
	while (l.className != "multiRow") {
		l = l.parentNode
	}
	c = getElementsByClassName(v, null, l);
	elmStr = c[0].value;
	elmInt = parseInt(elmStr.match(/\d+/g));
	elmArray = getElementsByClassName(v, null, l);
	var t = [];
	if (k != null) {
		t = t.concat(k)
	}
	for ( xx = 0; xx < elmArray.length; xx++) {
		elmArrayStr = elmArray[xx].value;
		elmArrayInt = parseInt(elmArrayStr.match(/\d+/g));
		t.push(elmArrayInt)
	}
	for ( xx = 0; xx < elmArray.length; xx++) {
		for ( yy = 0; yy < t.length; yy++) {
			z = 0;
			while (t[yy] == elmInt + z) {
				z = z + 1;
				if (t[yy] != elmInt + z) {
					elmInt = elmInt + z;
					break
				}
			}
		}
		elmStr = elmStr.replace(/\d+/g, elmInt);
		elmArray[elmArray.length - 1].value = elmStr
	}
}

function multiRowRemoveLastElement(k, l) {
	while (k.className != "multiRow") {
		k = k.parentNode
	}
	b = getElementsByClassName(l, null, k);
	c = b.length;
	if (c > 1) {
		d = c - 1;
		k.removeChild(b[d])
	} else {
		alert("Nothing to remove.")
	}
}

function multiRowRemoveActiveElement(k, l, q) {
	while (k.className != l) {
		k = k.parentNode
	}
	b = k.parentNode;
	c = getElementsByClassName(l, null, b);
	if (c.length >= 1) {
		b.removeChild(k);
		f = getElementsByClassName(l, null, b);
		if (q != undefined) {
			for ( i = 0; i < f.length; i++) {
				c = i + 1;
				if (c % 2 == 0) {
					f[i].setAttribute("style", q)
				} else {
					f[i].removeAttribute("style")
				}
			}
		}
	} else {
		alert("Nothing to remove.")
	}
}

function hideMFControls(k, n) {
	while (k.className != "multiRow") {
		k = k.parentNode
	}
	row = getElementsByClassName(n, null, k);
	for ( i = 0; i < row.length; i++) {
		if (i != 0) {
			row[i].style.display = ""
		}
	}
}

function booCheck() {
	booArray = getElementsByClassName("mFilterBoo");
	for (i in booArray) {
		if (i > 1) {
			pob = booArray[i].parentNode;
			pob.style.display = "";
			booArray[i].disabled = false
		}
	}
}

function getObj(k) {
	if (document.getElementById) {
		this.obj = document.getElementById(k);
		this.style = document.getElementById(k).style
	} else {
		if (document.all) {
			this.obj = document.all[k];
			this.style = document.all[k].style
		}
	}
}

function validate() {
	var k = document.getElementById("uBit").value;
	if (k == 1) {
		if (document.getElementById("password_").value != document.getElementById("password").value || document.getElementById("password").disabled == true) {
			alert("Please change the password or click cancel");
			return false
		} else {
			document.forms[0].submit()
		}
	} else {
		document.forms[0].submit()
	}
}

function updatePWcancel(k) {
	document.getElementById("v_pw").style.display = "none";
	k.value = "Change Password";
	k.onclick = function() {
		updatePW(k)
	};
	document.getElementById("button_submit").value = "Login";
	document.getElementById("uBit").value = 0
}

function updatePW(k) {
	k.value = "Cancel";
	k.onclick = function() {
		updatePWcancel(k)
	};
	document.getElementById("v_pw").style.display = "";
	document.getElementById("button_submit").value = "Update Password and Login";
	document.getElementById("uBit").value = 1
}

function recoverCreds() {
	uAddress = prompt("Enter E-Mail Account");
	if (uAddress != null && uAddress.length > 0) {
		arc("recovery", "/_admin/recoverPassword.php", "uAddress=" + uAddress, 0)
	}
}

function setSelectionRange(k, l, n) {
	if (k.setSelectionRange) {
		k.focus();
		k.setSelectionRange(l, l)
	} else {
		if (k.createTextRange) {
			var j = k.createTextRange();
			j.collapse(true);
			j.moveEnd("character", n);
			j.moveStart("character", l);
			j.select()
		}
	}
}

function addCaption(j) {
	b = document.getElementById(j);
	c = document.getElementById("nav");
	if (c.style.display == "none") {
		document.getElementById("imgExpand").style.display = "";
		document.getElementById("imgCollapse").style.display = "none"
	} else {
		document.getElementById("imgExpand").style.display = "none";
		document.getElementById("imgCollapse").style.display = ""
	}
}

function addResource(j) {
	resources = $.makeArray($(".clRes"));
	checkedArray = [];
	for (i in resources) {
		if (resources[i].checked == true) {
			checkedArray.push(resources[i].value)
		}
	}
	if (checkedArray.length > 0) {
		arc("listResource", "/_mod/smod_10/sql_resources.php", "id_srv=" + j + "&uid=" + bin2hex(checkedArray.toString()) + "&form=frmResource&action=insert", 0, 0)
	}
}

function addZero(j) {
	if (j < 10) {
		j = "0" + j
	} else {
		j = j
	}
	return j
}

function collapseObj(A, v, j, q, t) {
	A = document.getElementById(A);
	if (A.style.display == "" && j != 1) {
		A.style.display = "none"
	} else {
		if (v != null) {
			if (j == null || j == 0) {
				j = getElementsByClassName(v)
			} else {
				j = document.getElementsByTagName(v)
			}
			for ( i = 0; i < j.length; i++) {
				j[i].style.display = "none"
			}
		}
		A.style.display = ""
	}
}

function conarc(E, j, n, D, q, k, l) {
	noCon = 0;
	switch(j) {
	case 1:
		if (E.value == n) {
			arc(D, q, l, 0, 0)
		} else {
			arc(D, k, l, 0, 0)
		}
		break;
	case 2:
		if (E.value != n) {
			arc(D, q, l, 0, 0)
		} else {
			arc(D, k, l, 0, 0)
		}
		break;
	case 3:
		if (E.value > n) {
			arc(D, q, l, 0, 0)
		} else {
			arc(D, k, l, 0, 0)
		}
	case 4:
		if (E.value < n) {
			arc(D, q, l, 0, 0)
		} else {
			arc(D, k, l, 0, 0)
		}
		break;
	default:
		noCon = 1;
		break
	}
	if (noCon == 1 || E.selectedIndex == 0) {
		document.getElementById(D).innerHTML = "";
		document.getElementById(D).style.display = "none"
	}
}

function delSrvBoard(j) {
	arc("popWindow", "/_mod/smod_09/delete.php", "id_srv_board=" + j, 1, 1)
}

function delSrvBoardOption(j) {
	destBoard = document.getElementById("id_srv_board_alt");
	destBoard.selectedIndex = 0;
	switch(j) {
	case 0:
		destBoard.disabled = false;
		break;
	case 1:
		destBoard.disabled = true;
		break;
	case 2:
		destBoard.disabled = true;
		break
	}
}

function delTm(k, j) {
	c = confirm("Are you sure you wish to delete this record?");
	if (c === true) {
		tmButtonControl(2);
		arc("blTime", "/_mod/smod_10/sql_timeEntries.php", "form=frmTime&action=delete&id_srv=" + j + "&id_tm=" + k, 1, 1)
	} else {
		return false
	}
}

function delTmFull(j) {
	c = confirm("Are you sure you wish to delete this record?");
	if (c === true) {
		arc("content", "/_mod/smod_11/insertSQL.php", "edit=2&id_tm=" + j, 1, 1)
	} else {
		return false
	}
}

function disableFieldset(k, j) {
	if (j.type == "checkbox" && j.checked == true) {
		alert("on")
	}
}

function disableSel(j) {
	if ( typeof (j) == "string") {
		j = document.getElementById(j)
	}
	if (j) {
		j.onselectstart = function() {
			return false
		};
		j.style.MozUserSelect = "none";
		j.style.KhtmlUserSelect = "none";
		j.unselectable = "on"
	}
}

function disableSelForClass(j) {
	c = j.split(",");
	for ( i = 0; i < c.length; i++) {
		b = getElementsByClassName(c[i]);
		for ( ii = 0; ii < b.length; ii++) {
			disableSel(b[ii])
		}
	}
}

function disableSelForTag(j) {
	c = j.split(",");
	for ( i = 0; i < c.length; i++) {
		b = document.getElementsByTagName(c[i]);
		for ( ii = 0; ii < b.length; ii++) {
			disableSel(b[ii])
		}
	}
}

function editRow(k, j) {
	data = document.getElementById(k + "_" + j).innerHTML;
	recparent = document.getElementById(k + "_" + j).parentNode;
	rechead = document.getElementById(k + "_head_" + j).innerHTML;
	recinput = document.getElementById("rid_" + k);
	recid = recinput.value;
	if (rechead.indexOf("[Private Note]") != -1) {
		document.getElementById("private_" + k).checked = true
	}
	if (recid != "") {
		recparentexists = document.getElementById(k + "_" + recid).parentNode;
		recparentexists.style.display = ""
	}
	recinput.value = j;
	recparent.style.display = "none";
	document.getElementById("ir_" + k).style.display = "";
	document.getElementById("it_" + k).value = data;
	moveScroll("it_" + k, 0, -34);
	document.getElementById("add_" + k).disabled = true;
	document.getElementById("save_" + k).disabled = false;
	document.getElementById("close_" + k).disabled = false;
	document.getElementById("delete_" + k).disabled = false;
	document.getElementById("private_" + k).disabled = false
}

function srvSave(j) {
	srvid = document.getElementById("sid_" + j).value;
	data = document.getElementById("it_" + j).value;
	srvprivate = document.getElementById("private_" + j).checked;
	recid = document.getElementById("rid_" + j).value;
	editbit = (recid == "" ? 0 : 1);
	arc("rec_" + j, "/_mod/smod_10/sql_" + j + ".php", "edit=" + editbit + "&pri=" + srvprivate + "&id=" + srvid + "&val=" + bin2hex(data) + "&rid=" + recid, 1, 1);
	document.getElementById("add_" + j).disabled = false;
	document.getElementById("save_" + j).disabled = true;
	document.getElementById("close_" + j).disabled = true;
	document.getElementById("delete_" + j).disabled = true;
	document.getElementById("private_" + j).disabled = true;
	document.getElementById("private_" + j).checked = false;
	document.getElementById("it_" + j).value = "";
	document.getElementById("ir_" + j).style.display = "none"
}

function srvAdd(j) {
	document.getElementById("add_" + j).disabled = true;
	document.getElementById("save_" + j).disabled = false;
	document.getElementById("close_" + j).disabled = false;
	document.getElementById("delete_" + j).disabled = true;
	document.getElementById("private_" + j).disabled = false;
	document.getElementById("ir_" + j).style.display = "";
	document.getElementById("rid_" + j).value = ""
}

function srvCancel(j) {
	document.getElementById("add_" + j).disabled = false;
	document.getElementById("save_" + j).disabled = true;
	document.getElementById("close_" + j).disabled = true;
	document.getElementById("delete_" + j).disabled = true;
	document.getElementById("private_" + j).disabled = true;
	document.getElementById("ir_" + j).style.display = "none";
	document.getElementById("it_" + j).value = "";
	recinput = document.getElementById("rid_" + j);
	if (recinput.value != "") {
		recparent = document.getElementById(j + "_" + recinput.value).parentNode;
		recparent.style.display = "";
		recinput.value = ""
	}
}

function srvDelete(j) {
	xo = confirm("Are you sure?");
	if (xo === true) {
		srvid = document.getElementById("sid_" + j).value;
		data = document.getElementById("it_" + j).value;
		recid = document.getElementById("rid_" + j).value;
		arc("rec_" + j, "/_mod/smod_10/sql_" + j + ".php", "edit=2&id=" + srvid + "&val=" + bin2hex(data) + "&rid=" + recid, 1, 1);
		document.getElementById("add_" + j).disabled = false;
		document.getElementById("save_" + j).disabled = true;
		document.getElementById("close_" + j).disabled = true;
		document.getElementById("delete_" + j).disabled = true;
		document.getElementById("private_" + j).disabled = true;
		document.getElementById("it_" + j).value = "";
		document.getElementById("ir_" + j).style.display = "none";
		document.getElementById("rid_" + j).value = "";
		return true
	} else {
		return false
	}
}

function filterAssoc(j) {
	x = j.options[j.selectedIndex].value;
	disableChildAbs("id_cust_contact");
	if (j.selectedIndex == 0) {
		document.getElementById("id_cust_company").selectedIndex = 0;
		document.getElementById("id_cust_company").disabled = true;
		return false
	} else {
		document.getElementById("id_cust_company").disabled = false
	}
	if (x == 1) {
		fltrSelect("Organization", "conbifrost", $globalDB, "_cust_company", "id_cust_company,cust_company", "id_cust_company", "fltrSelect(!qut!Association!qut!,!qut!conbifrost!qut!,!qut!_sys!qut!,!qut!_cust_contact!qut!,!qut!id_cust_contact,concat(cust_contact_familyName,!squt!,!squt!,cust_contact_givenName)!qut!,!qut!id_cust_contact!qut!,null,0,!qut!fldAssociation!qut!,this,!qut!cust_contact_dr is null AND id_cust_company%3D!qut!,null,null,null);", 0, "fldOrganization", document.getElementById("ignoreCompany"), " id_cust_company%3D", null, null, null)
	} else {
		fltrSelect("Organization", "conbifrost", $globalDB, "_cust_company", "id_cust_company,cust_company", "id_cust_company", "fltrSelect(!qut!Association!qut!,!qut!conbifrost!qut!,!qut!_sys!qut!,!qut!_cust_contact!qut!,!qut!id_cust_contact,concat(cust_contact_familyName,!squt!,!squt!,cust_contact_givenName)!qut!,!qut!id_cust_contact!qut!,null,0,!qut!fldAssociation!qut!,this,!qut!cust_contact_dr is null AND id_cust_company%3D!qut!,null,null,null);", 0, "fldOrganization", document.getElementById("ignoreCompany"), " id_cust_company !%3D", null, null, null)
	}
}

function fltrSelect(ac, ab, O, F, D, C, A, aa, T, E, q, B, U, K) {
	if (K != undefined) {
		K = K
	} else {
		K = 0
	}
	if (E.selectedIndex != 0) {
		if (U == undefined) {
			U = ""
		}
		arc(T, "/_lib/php/jsDeps/ArcFilterSelect.php", "a=" + ac + "&b=" + ab + "&c=" + O + "&d=" + F + "&e=" + D + "&f=" + C + "&g=" + A + "&h=" + aa + "&i=" + T + "&j=" + E + "&k=" + q + E.value + "&l=" + B + "&m=" + U + "&n=" + K, 0, 0)
	} else {
		document.getElementById(C).disabled = true
	}
}

function getFrmVals(D) {
	if (D == undefined) {
		D = 0
	} else {
		D = D
	}
	var E = document.forms[D].getElementsByTagName("input");
	var n = document.forms[D].getElementsByTagName("select");
	var B = document.forms[D].getElementsByTagName("textarea");
	var A = document.forms[D].getElementsByTagName("text");
	var l = "";
	for ( i = 0; i < E.length; i++) {
		if (E[i].name.length > 0 && ((E[i].type == "text" || E[i].type == "password")) || (E[i].type == "hidden") && E[i].type != "checkbox" && E[i] != "radio") {
			q = (l.length > 0 ? "&" : "");
			l = l + q + E[i].name + "=" + E[i].value
		}
		if (E[i].name.length > 0 && E[i].type == "checkbox") {
			q = (l.length > 0 ? "&" : "");
			if (E[i].checked == true) {
				l = l + q + E[i].name + "=1"
			} else {
				l = l + q + E[i].name + "=0"
			}
		}
		if (E[i].name.length > 0 && E[i].type == "radio") {
			q = (l.length > 0 ? "&" : "");
			if (E[i].checked == true) {
				l = l + q + E[i].name + "=" + E[i].value
			}
		}
	}
	for ( i = 0; i < n.length; i++) {
		if (n[i].id.length > 0) {
			q = (l.length > 0 ? "&" : "");
			if (n[i].options[n[i].selectedIndex].value == 0) {
				selValue = null
			} else {
				selValue = n[i].options[n[i].selectedIndex].value
			}
			l = l + q + n[i].name + "=" + selValue
		}
	}
	for ( i = 0; i < B.length; i++) {
		if (B[i].name.length > 0) {
			var q = (l.length > 0 ? "&" : "");
			fldName = B[i].name;
			if (fldName.indexOf("mText") != -1 && B[i].value.length > 0) {
				txtValue = B[i].value;
				l = l + q + B[i].name + "=" + txtValue
			} else {
				if (fldName.indexOf("_mText") != -1 && B[i].value.length == 0) {
					l = l
				} else {
					txtValue = bin2hex(B[i].value);
					l = l + q + B[i].name + "=" + txtValue
				}
			}
		}
	}
	if (l.length > 0) {
		return l
	} else {
		return "Fields are blank."
	}
}

function getLabel(j) {
	b = j[j.selectedIndex].text;
	c = b.substr(0, 3);
	document.getElementById("formDept").value = c
}

function hex2a(l) {
	var j = "";
	for (var k = 0; k < l.length; k += 2) {
		j += String.fromCharCode(parseInt(l.substr(k, 2), 16))
	}
	return j
}

function hideCalendar() {
	var j = document.getElementById("objCalendar");
	if (j.style.display == "") {
		j.style.display = "none";
		j.innerHTML = "";
		activeCal = "";
		document.body.removeAttribute("onresize")
	}
}

function hideViewport(k, j) {
	viewport = document.getElementById(k);
	if (j == 1) {
		viewport.style.visibility = "visible"
	} else {
		viewport.style.visibility = "hidden"
	}
}

function ieHeightResize() {
	var j = document.getElementById("banner");
	var E = j.clientHeight;
	var q = document.getElementById("menu");
	var k = q.clientHeight;
	var n = document.getElementById("oOptions");
	var l = n.clientHeight;
	var D = document.getElementById("contentcell");
	newHeight = document.body.clientHeight - (E + k + l);
	if (BrowserDetect.browser == "Explorer") {
		D.style.height = newHeight + "px"
	}
}

function insertTime(j, k) {
	if (k == undefined) {
		k = "timeFld"
	} else {
		k = k
	}
	c = findTargetInRow(j, "frmrow", k);
	currentTime = new Date();
	$(c).val(addZero(currentTime.getHours()) + ":" + addZero(currentTime.getMinutes()) + ":" + addZero(currentTime.getSeconds())).change()
}

function isDefault(k) {
	if (k.value != k.defaultValue) {
		k.style.backgroundColor = "#ffcccc"
	} else {
		k.style.backgroundColor = "#fff"
	}
}

function linkarc(n, l, k, j, E, D, q) {
	if (n.selectedIndex != 0) {
		arc(k, j, E)
	}
	if (l != undefined) {
		g = l.split(",");
		for ( i = 0; i < g.length; i++) {
			if (D != undefined && D == 1) {
				if (document.getElementById(g[i]).nodeName == "SELECT") {
					document.getElementById(g[i]).selectedIndex = 0
				} else {
					if (document.getElementById(g[i]).nodeName == "INPUT") {
						document.getElementById(g[i]).value = ""
					}
				}
				document.getElementById(g[i]).disabled = true
			}
		}
	} else {
		document.getElementById(q).disabled = true
	}
}

function logout() {
	expireCookie("PHPSESSID");
	expireCookie("PHPSESSIDU");
	expireCookie("PHPSESSIDG");
	window.location = "index.php"
}

function moveCalendar(j) {
	if (activeFld != "") {
		c = document.getElementById(j);
		b = findPosRelativeToViewport(activeFld, c);
		document.getElementById("objCalendar").style.top = b[1] + "px";
		document.getElementById("objCalendar").style.left = b[0] + "px"
	}
}

function moveDate(t, j, k, l) {
	if (j == 0) {
		if (t == 0) {
			k = k - 1
		} else {
			k = k + 1
		}
	} else {
		if (t == 0) {
			l = l - 1
		} else {
			l = l + 1
		}
	}
	arc("objCalendar", "_lib/php/jsDeps/ArcCalendar.php", "monthID=" + k + "&yearID=" + l, 1, 0)
}

function movedatenotes(t, q, l, v) {
	if (q == 0) {
		if (t == 0) {
			l = l - 1
		} else {
			l = l + 1
		}
	} else {
		if (t == 0) {
			v = v - 1
		} else {
			v = v + 1
		}
	}
	if (document.getElementById("opennotes") != undefined) {
		arc("objCalendar", "/_lib/php/libCalendarNotes.php", "monthID=" + l + "&yearID=" + v, null, 0, 0)
	} else {
		arc("popWindow", "/_lib/php/libComstatCalendarStatic.php", "monthID=" + l + "&yearID=" + v, null, 0, 0)
	}
}

function movepage(l, v, t, q) {
	dayblocks = getElementsByClassName("dayActive");
	for ( i = 0; i < dayblocks.length; i++) {
		dayblocks[i].setAttribute("class", "day");
		dayblocks[i].setAttribute("className", "day")
	}
	l.setAttribute("class", "dayActive");
	l.setAttribute("className", "dayActive");
	window.activitiesMonth = t;
	window.activitiesDay = q;
	window.activitiesYear = v;
	if (document.getElementById("openActivities") != undefined) {
		arc("todayActivities", "/_activities/todayActivities_table.php", "monthID=" + t + "&dayID=" + q + "&yearID=" + v, 0, 0);
		arc("openActivities", "/_activities/activities_table.php", "monthID=" + t + "&dayID=" + q + "&yearID=" + v, 0, 0)
	} else {
		arc("vpContent", "/_comstatViews/index.php", "monthID=" + t + "&dayID=" + q + "&yearID=" + v, 1, 1)
	}
}

function movepagenotes(l, v, t, q) {
	dayblocks = getElementsByClassName("dayActive");
	for ( i = 0; i < dayblocks.length; i++) {
		dayblocks[i].setAttribute("class", "day");
		dayblocks[i].setAttribute("className", "day")
	}
	l.setAttribute("class", "dayActive");
	l.setAttribute("className", "dayActive");
	window.notesMonth = t;
	window.notesDay = q;
	window.notesYear = v;
	if (document.getElementById("opennotes") != undefined) {
		arc("todaynotes", "/_notes/todaynotes_table.php", "monthID=" + t + "&dayID=" + q + "&yearID=" + v, 0, 0);
		arc("opennotes", "/_notes/notes_table.php", "monthID=" + t + "&dayID=" + q + "&yearID=" + v, 0, 0)
	} else {
		arc("vpContent", "/_comstatViews/index.php", "monthID=" + t + "&dayID=" + q + "&yearID=" + v, 1, 1)
	}
}

function moveScroll(l, k, n) {
	k = (k != undefined ? k : 0);
	n = (n != undefined ? n : 0);
	cursorPos = findPos(document.getElementById(l));
	window.scrollTo(cursorPos[0] + k, cursorPos[1] + n)
}

function navJump(k, j) {
	arc("content", k, "pTitle=" + j, 1, 1)
}

var activeFld = "";
function openCalendar(q, j, k, l) {
	if (l == undefined) {
		l = "dateFld"
	} else {
		l = l
	}
	if (k == undefined) {
		c = findTargetInRow(q, "frmrow", l)
	} else {
		c = document.getElementById(k)
	}
	if (activeFld != "") {
		hideCalendar()
	}
	activeFld = c;
	xx = document.getElementById(j);
	b = findPosRelativeToViewport(c, xx);
	d = document.getElementById("objCalendar");
	d.style.top = b[1] + "px";
	d.style.left = b[0] + "px";
	arc("objCalendar", "_lib/php/jsDeps/ArcCalendar.php", null, 0, 0);
	document.body.setAttribute("onresize", "moveCalendar('" + j + "')");
	xx.setAttribute("onscroll", "hideCalendar()")
}

function selectCalendarDate(j) {
	$(activeFld).val(j).change();
	hideCalendar()
}

function removeResource(j, l) {
	var k = confirm("Are you sure?");
	if (k === true) {
		arc("listResource", "/_mod/smod_10/sql_resources.php", "id_srv=" + l + "&id_srv_res=" + j + "&form=frmResource&action=delete", 0, 0)
	} else {
		return false
	}
}

function setActiveHeading(j) {
	cal = document.getElementById("objCalendar");
	if (cal != null && cal.style.display == "") {
		cal.style.display = "none"
	}
	labels = getElementsByClassName("activeHeading");
	for ( i = 0; i < labels.length; i++) {
		labels[i].setAttribute("class", "menuheadingbutton")
	}
	j.setAttribute("class", "activeHeading")
}

function subMenu(j, k) {
	$("#content").children("form").children("fieldset").css("display", "none");
	$("#content").find("#" + k).show();
	$("#collapseTabs").css("display", "inline-block");
	setActiveHeading(j)
}

function submitForApproval(j, l, k) {
	a = confirm("Are you sure you want to submit this timesheet?\nDoing so will prevent you from making any further modifications.");
	if (a == true) {
		data = tableToExcel(document.getElementById("tblPayPeriod"), "PayPeriod", 1);
		arc("popWindow", "/_mod/smod_11/timecardsubmit.php", "datestart=" + j + "&dateend=" + l + "&data=" + data, 1, 1)
	} else {
		return false
	}
}

function tmButtonControl(j) {
	buttons = getElementsByClassName("clTimeButton");
	switch(j) {
	case 0:
		buttons[0].disabled = true;
		if (buttons[1] != undefined) {
			buttons[1].disabled = false
		}
		if (buttons[1] != undefined) {
			buttons[2].disabled = true
		}
		if (buttons[1] != undefined) {
			buttons[3].disabled = true
		}
		break;
	case 1:
		buttons[0].disabled = false;
		if (buttons[1] != undefined) {
			buttons[1].disabled = true
		}
		if (buttons[2] != undefined) {
			buttons[2].disabled = false
		}
		if (buttons[3] != undefined) {
			buttons[3].disabled = true
		}
		break;
	case 2:
		buttons[0].disabled = true;
		if (buttons[1] != undefined) {
			buttons[1].disabled = false
		}
		if (buttons[2] != undefined) {
			buttons[2].disabled = true
		}
		if (buttons[3] != undefined) {
			buttons[3].disabled = true
		}
		break;
	case 3:
		buttons[0].disabled = false;
		if (buttons[1] != undefined) {
			buttons[1].disabled = true
		}
		if (buttons[2] != undefined) {
			buttons[2].disabled = false
		}
		if (buttons[3] != undefined) {
			buttons[3].disabled = false
		}
		break
	}
}

function tmButtons(k, j) {
	tmButtonControl(k);
	if (k == 1) {
		arc("blTime", "/_mod/smod_10/add_time.php", "id_srv=" + j, 1, 1)
	} else {
		arc("blTime", "/_mod/smod_10/listTimeEntries.php", "id_srv=" + j, 1, 1)
	}
}

function updateClass(k, j) {
	j.className = k
}

function updateTable(n, l, t, k, A, j, q, v) {
	if (A === 1) {
		ELEMENTS = $("#" + n).find("input,textarea");
		ELEMENTS = $.makeArray(ELEMENTS);
		TEMPARRAY = [];
		for ( i = 0; i < ELEMENTS.length; i++) {
			if (ELEMENTS[i].value != ELEMENTS[i].defaultValue) {
				ELEMENTS[i].style.backgroundColor = "#98FB98";
				INPUTVALUE = (ELEMENTS[i].type == "textarea" ? bin2hex(ELEMENTS[i].value) : ELEMENTS[i].value);
				TEMPARRAY.push(ELEMENTS[i].name + "=" + INPUTVALUE)
			}
		}
		POSTSTRING = "";
		for ( i = 0; i < TEMPARRAY.length; i++) {
			POSTSTRING = "&" + TEMPARRAY[i] + POSTSTRING
		}
		arc(q, j, t + "=" + k + POSTSTRING + v, 1, 1)
	} else {
		if (A === 2) {
			TEST = confirm("Are you sure?");
			if (TEST === true) {
				arc(q, j, t + "=" + k + v, 1, 1)
			} else {
				return false
			}
		} else {
			return false
		}
	}
}

function updateRow(q, B, t, n, k, v, A, l) {
	var j = "&col=" + t + "&key=" + n + "&table=" + B + "&edit=" + k;
	if (k === 1) {
		ELEMENTS = $("#" + q).find("input,textarea");
		ELEMENTS = $.makeArray(ELEMENTS);
		TEMPARRAY = [];
		for ( i = 0; i < ELEMENTS.length; i++) {
			if (ELEMENTS[i].value != ELEMENTS[i].defaultValue) {
				ELEMENTS[i].style.backgroundColor = "#98FB98";
				INPUTVALUE = (ELEMENTS[i].type == "textarea" ? bin2hex(ELEMENTS[i].value) : ELEMENTS[i].value);
				TEMPARRAY.push(ELEMENTS[i].name + "=" + INPUTVALUE)
			}
		}
		for ( i = 0; i < TEMPARRAY.length; i++) {
			j = "&" + TEMPARRAY[i] + j
		}
		arc(l, v, j, 1, 1)
	} else {
		if (k === 2) {
			TEST = confirm("Are you sure?");
			if (TEST === true) {
				arc(A, v, j, 1, 1)
			} else {
				return false
			}
		} else {
			return false
		}
	}
}

function uploadData() {
	var j = document.getElementById("newFile");
	formdata = false;
	file = j.files[0];
	if (window.FormData) {
		formdata = new FormData()
	} else {
		alert("false")
	}
	formdata.append("file", file);
	arc("msgWindow", "/_lib/objects/fso/upload_file.php", formdata, 0, 0)
}

function validateElement(n, l) {
	o = l;
	l = $(o).val();
	l = trimLeftAndRight(l);
	if (l.length > 0) {
		switch(n) {
		case"phone":
			if (l.length >= 10) {
				l = l.replace(/\D/g, "");
				d = isNumericWhole(l);
				o.value = l
			} else {
				d = false
			}
			error = "Not a valid phone number\nMust be at least 10 digits and only contain Numbers";
			break;
		case"time":
			if (l != "") {
				l = l.toLowerCase();
				if (l.indexOf("pm") != -1) {
					pm = true
				} else {
					pm = false
				}
				if (l.indexOf("am") != -1) {
					am = true
				} else {
					am = false
				}
				l = l.replace(/\D/g, "");
				l = l.replace(/\s/g, "");
				tmlen = l.length;
				leadingZero = l.charAt(0);
				if (leadingZero != 0 && tmlen == 2) {
					l = l.toString();
					l = l + "00";
					tmlen = 4
				}
				if (leadingZero != 0 && tmlen < 4) {
					l = pad("0" + l, 4);
					tmlen = 4
				}
				if (leadingZero == 0 && tmlen < 4) {
					l = l.toString();
					l = l + "00";
					tmlen = 4
				}
				if (tmlen > 3) {
					l = l.substr(0, 4)
				}
				firstTwo = l.substr(0, 2);
				if (firstTwo > 9 && firstTwo < 13) {
					if (firstTwo == 12 && am == true) {
						lastTwo = l.substr(2, 4);
						l = pad("0" + lastTwo, 4)
					}
					if (firstTwo == 12 && pm == true) {
						l = l
					}
					if (pm == true && firstTwo != 12) {
						l = parseFloat(l) + 1200
					}
				} else {
					if (firstTwo < 9) {
						if (pm == true) {
							l = parseFloat(l) + 1200
						}
					}
				}
				l = l.toString();
				hh = l.substr(0, 2);
				mm = l.substr(2, 4);
				l = hh + ":" + mm + ":00";
				$(o).val(l).change()
			}
			if (hh > 23) {
				d = false
			} else {
				d = true
			}
			error = "Please input a valid time.";
			break;
		case"url":
			var k = new RegExp("^(http|https|ftp)://[a-zA-Z0-9-.]+.[a-zA-Z]{2,3}(:[a-zA-Z0-9]*)?/?([a-zA-Z0-9-._?,'/\\+&%$#=~])*$");
			d = k.test(l);
			error = "Not a valid url\nExample: http://www.example.com";
			break;
		case"dateTime":
			l = l.toLowerCase();
			o.value = l;
			k = new RegExp("^(201[2-9])-(0[0-9]|1[0,1,2])-([0,1,2][0-9]|3[0,1]).([0-1][0-9]|[2][0-3])(:([0-5][0-9])){1,2}$");
			d = k.test(l);
			error = "Not a valid time format.\nExample: YYYY-MM-DD 00:00 in 24 Hour Format";
			break;
		case"email":
			l = l.toLowerCase();
			o.value = l;
			k = new RegExp("[a-z0-9!#$%&'*+/=?^_`|}~-]+(?:.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?");
			d = k.test(l);
			error = "Not a valid email account\nExample: user@host.com";
			break;
		case"date":
			if (n == "date") {
				k = new RegExp("^[0-9]{4}-[0-9]{1,2}-[0-9]{1,2}$");
				d = k.test(l)
			}
			error = "Not a valid date\nExample: YYYY-MM-DD";
			break;
		case"twitter":
			z = l.substr(0, 1);
			if (z == "@") {
				d = true
			} else {
				d = false;
				error = "Not a valid Twitter account\nExample: @twitteraccount"
			}
			break;
		case"facebook":
			z = l.search(/facebook.com/i);
			if (z != -1) {
				d = true
			} else {
				d = false;
				error = "Not a valid Facebook account\nExample: user@facebook.com"
			}
			break;
		case"zip":
			c = l.split("-");
			for ( i = 0; i < c.length; i++) {
				d = isNumericWhole(c[i])
			}
			if (c.length > 2) {
				d = false
			}
			error = "Not a valid postal code";
			break;
		case"ssn":
			l = l.replace(/\D/g, "");
			l = l.replace(/-/g, "");
			o.value = l;
			d = isNumericWhole(l);
			error = "Not a valid Social Security Number\nExample: 111111111 (9 Digits) or 111-11-1111";
			if (d === true && l.length != 9) {
				error = "Not a valid Social Security Number\nExample: 111111111 (9 Digits) or 111-11-1111";
				d = false
			}
			break;
		case"int":
			d = isNumericWhole(l);
			error = "Not a valid number.";
			break;
		case"money":
			l = l.replace(/[a-z$]/g, "");
			o.value = l;
			k = new RegExp("^[-+]?[0-9]+(.[0-9]+)?$");
			d = k.test(l);
			error = "Not a valid monetary value";
			break;
		case"ip":
			k = new RegExp("^([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}$");
			d = k.test(l);
			error = "Not a valid ip address\nExample: 192.168.23.2\nWhere each octet is less than 256";
			break;
		case"mac":
			l = l.toString();
			l = l.toUpperCase();
			l = l.replace(/[^A-F,^a-f,^0-9,^\s]/gi, "");
			l = l.substr(0, 12);
			o.value = l;
			if (l.length != 12) {
				d = false
			} else {
				d = true
			}
			error = "Not a valid MAC address\n";
			break;
		case"password":
			objValue = o.value;
			objId = o.id;
			objIdLastChar = objId.charAt(objId.length - 1);
			if (objIdLastChar == "_") {
				if (objValue.length < 8 || objValue == "") {
					d = false;
					primaryPasswordId = objId.substring(-1, objId.length - 1);
					primaryPassword = document.getElementById(primaryPasswordId);
					primaryPasswordValue = "";
					error = "Password is less than 8 characters";
					primaryPassword.disabled = true;
					secondaryPassword = o;
					secondaryPasswordValue = objValue.value;
					secondaryPassword.style.backgroundColor = "#FFDDDD"
				} else {
					primaryPasswordId = objId.substring(-1, objId.length - 1);
					primaryPassword = document.getElementById(primaryPasswordId);
					primaryPasswordValue = primaryPassword.value;
					k = /(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*\W).*$/;
					d = k.test(objValue);
					error = "Does not meet complexity requirements";
					if (d == true) {
						error = "";
						secondaryPassword = o;
						secondaryPasswordValue = objValue.value;
						primaryPassword.disabled = false;
						primaryPassword.focus()
					}
				}
			} else {
				d = true;
				secondaryPasswordId = objId + "_";
				primaryPassword = o;
				primaryPasswordValue = primaryPassword.value;
				secondaryPassword = document.getElementById(secondaryPasswordId);
				secondaryPasswordValue = secondaryPassword.value
			}
			if (d == true) {
				if (objId == primaryPasswordId && primaryPasswordValue != secondaryPasswordValue) {
					d = false;
					o.value = "";
					error = "Passwords do not match"
				} else {
					d = true
				}
			}
			break;
		default:
			d = false
		}
		if (d === false) {
			x = 1
		} else {
			x = 0
		}
		if (x !== 0) {
			alert(error);
			o.style.backgroundColor = "#FFDDDD";
			o.value = "";
			return false
		} else {
			o.style.backgroundColor = "#FFF";
			return true
		}
	} else {
		o.style.backgroundColor = "#FFF";
		return true
	}
}

function validateRequiredFields(a) {
	b = a.split(",");
	errorMsg = "";
	errors = 0;
	for ( i = 0; i < b.length; i++) {
		if (b[i].indexOf(".") == -1) {
			strfield = b[i];
			u = 0
		} else {
			strArray = b[i].split(".");
			strfieldset = strArray[0];
			fieldsetObj = document.getElementById(strfieldset);
			strfield = strArray[1];
			u = 1
		}
		x = document.getElementById(strfield);
		if (x != null) {
			if (x.type == "text" || x.type == "password") {
				y = trimLeftAndRight(x.value);
				z = 0
			} else {
				if (x.type == "select-one") {
					y = x;
					z = 1
				}
			}
			if ((z == 0 && y.length < 1) || (z == 1 && y.selectedIndex == 0)) {
				if (u == 1 && fieldsetObj.style.display == "none") {
					fieldsetObj.style.display = ""
				}
				errorMsg = "Please fill the highlighted required fields";
				document.getElementById(strfield).style.backgroundColor = "#ffdddd"
			} else {
				document.getElementById(strfield).style.backgroundColor = "#fff";
				if (document.getElementById(strfield).onblur != undefined) {
					c = document.getElementById(strfield).onblur;
					d = String(c);
					e = d.replace(/function onblur\(event\) \{/gi, "");
					e = e.replace(/function onblur\(\)/, "");
					e = e.replace(/\n/gi, "");
					e = e.replace(/\{/gi, "");
					f = e.replace(/\}/gi, "");
					f = f.replace(/this/, "document.getElementById('" + strfield + "')");
					g = eval(f);
					if (g == false) {
						errors = errors + 1
					} else {
						if (errors > 0) {
							errors = errors - 1
						} else {
							errors = 0
						}
					}
				}
			}
		}
	}
	if (errorMsg != "" || errors > 0) {
		alert(errorMsg);
		return false
	} else {
		return true
	}
}

function findPosRelativeToViewport(v, q) {
	var l = findPos(v);
	var t = getPageScroll(q);
	return [l[0] - t[0], l[1] - t[1]]
}

function findPos(k) {
	var j = curtop = 0;
	if (k.offsetParent) {
		do {
			j += k.offsetLeft;
			curtop += k.offsetTop
		} while(k=k.offsetParent)
	}
	return [j, curtop]
}

function moveScroll(l, k, n) {
	k = (k != undefined ? k : 0);
	n = (n != undefined ? n : 0);
	cursorPos = findPos(document.getElementById(l));
	window.scrollTo(cursorPos[0] + k, cursorPos[1] + n)
}

function getPageScroll(k) {
	var j,
	    l;
	if (k.pageYOffset) {
		l = k.pageYOffset;
		j = k.pageXOffset
	} else {
		if (k && k.scrollTop) {
			l = k.scrollTop;
			j = k.scrollLeft
		} else {
			if (k) {
				l = k.scrollTop;
				j = k.scrollLeft
			}
		}
	}
	return [j, l]
}

function getFormObjects(j) {
	oElements = new Array;
	oInput = j.getElementsByTagName("input");
	oSelect = j.getElementsByTagName("select");
	for ( a = 0; a < oInput.length; a++) {
		oElements.push(oInput[a])
	}
	for ( a = 0; a < oSelect.length; a++) {
		oElements.push(oSelect[a])
	}
	return oElements
}

function prepSaveChanges(j) {
	if (j != undefined) {
		FORMS = [];
		FORMS[0] = document.forms[j]
	} else {
		FORMS = [];
		FORMS[0] = document.forms[0]
	}
	for ( I = 0; I < FORMS.length; I++) {
		oFORM = FORMS[I];
		MULTI = getElementsByClassName("multiRow", null, oFORM);
		for ( r = 0; r < MULTI.length; r++) {
			MTEXT = "";
			RESULTS = [];
			oTextbox = $(MULTI[r]).find("textarea");
			for ( i = 0; i < oTextbox.length; i++) {
				TName = oTextbox[i].id;
				if (TName.indexOf("mText") != -1) {
					MTEXTNAME = "#" + TName
				} else {
					alert("There was an error");
					return false
				}
			}
			MTEXT = $(MTEXTNAME);
			GROUPING_FRMGRP = getElementsByClassName("frmgroup", null, MULTI[r]);
			if (GROUPING_FRMGRP.length > 0) {
				GROUPING = GROUPING_FRMGRP
			} else {
				GROUPING = getElementsByClassName("frmrow", null, MULTI[r])
			}
			VALS_FRMROW = [];
			VALS_FRMGRP = [];
			for ( i = 0; i < GROUPING.length; i++) {
				if (GROUPING[i].style.display == "") {
					ROWDATA = getFormObjects(GROUPING[i]);
					ROWRESULTS = [];
					for ( ii = 0; ii < ROWDATA.length; ii++) {
						ELEMENT = ROWDATA[ii];
						if ($(ELEMENT).attr("id") != TName) {
							NOMTEXT = true;
							if ($(ELEMENT).val() != undefined) {
								ROWRESULTS.push($(ELEMENT).val())
							}
						} else {
							NOMTEXT = false;
							break
						}
					}
					ROWRESULTS.push("~id~");
					if (i != 0 && NOMTEXT === true) {
						RESULTS.push(ROWRESULTS)
					}
				}
				strINSERT = bin2hex(JSON.stringify(RESULTS, null, 2));
				$(MTEXT).val(strINSERT)
			}
		}
		$(oFORM).submit();
		if (window.formTest === false) {
			alert("fail");
			break
		}
	}
}

function showPasswordText(j, n) {
	b = document.getElementById(j);
	if (b.type == "password") {
		b.type = "text";
		$(n).html('<i class="fa fa-eye-slash"></i>')
	} else {
		b.type = "password";
		$(n).html('<i class="fa fa-eye"></i>')
	}
};