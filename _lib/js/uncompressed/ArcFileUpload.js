$(function(){
        var ul = $('#uploaded');
        $('#browse').click(function(){
// Simulate a click on the file input button
// to show the file browser dialog
$('#filebutton').click();
});
// Initialize the jQuery File Upload plugin
$('#upload').fileupload({
// This element will accept file drag/drop uploading
dropZone: $('#drop'),
// This function is called when a file is added to the queue;
// either via the browse button, or via drag/drop:
add: function (e, data) {
    var tpl = $('<tr class="working"><td style="width:50px"><input style="border-radius:none;box-shadow:none;background:none;border:none;border-spacing:0;margin:0;padding:0;" class="clKnob" type="text" value="0" data-width="50" data-height="50"'+
        ' data-fgColor="#39516b" data-readOnly="1" data-bgColor="#F6F6F6" /></td><td style="text-align:left"><span class="clDataSize"></span></td><td style="text-align:right"><input type="button" class="clDataStatus" value="Cancel"></td></tr>');
// Append the file name and file size
tpl.find('span.clDataSize').text(data.files[0].name)
.append('</br><i>' + formatFileSize(data.files[0].size) + '</i>');
// Add the HTML to the UL element
data.context = tpl.appendTo(ul);
// Initialize the knob plugin
tpl.find('.clKnob').knob();
// Listen for clicks on the cancel icon
tpl.find('.clDataStatus').click(function(){
    if(tpl.hasClass('working')){
        jqXHR.abort();
    }
    tpl.fadeOut(function(){
        tpl.remove(); 
    });
});
// Automatically upload the file once it is added to the queue
var jqXHR = data.submit();
},
progress: function(e, data){
// Calculate the completion percentage of the upload
var progress = parseInt(data.loaded / data.total * 100, 10);
// Update the hidden input field and trigger a change
// so that the jQuery knob plugin knows to update the dial
data.context.find('.clKnob').val(progress).change();
if(progress == 100){
    data.context.removeClass('working');
    data.context.find('.clDataStatus').val('Done');
    arc("filesreturned","_lib/php/ArcFileList.php");
}
},
fail:function(e, data){
// Something has gone wrong!
data.context.addClass('error');
}
});
// Prevent the default action when a file is dropped on the window
$(document).on('drop dragover', function (e) {
    e.preventDefault();
});
// Helper function that formats the file sizes
function formatFileSize(bytes) {
    if (typeof bytes !== 'number') {
        return '';
    }
    if (bytes >= 1000000000) {
        return (bytes / 1000000000).toFixed(2) + ' GB';
    }
    if (bytes >= 1000000) {
        return (bytes / 1000000).toFixed(2) + ' MB';
    }
    return (bytes / 1000).toFixed(2) + ' KB';
}
});