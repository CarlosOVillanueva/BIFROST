// ArcJS-13.3.0.1.js;


//
/* ********************Sta** IP ADDRESS FUNCTIONS ******************ro***** */
function bit2dec(bits) {
	dec = Math.pow(2, bits);
	return dec;
}

/* http://www.name-generators.com/javascript-2/how-to-make-binary-decimal-conversion-in-javascript.htm*/
function convertBinary(bin) {
	var binaryNumber = bin;
	binaryNumber = parseInt(binaryNumber, 2);
	var decimalNumber = binaryNumber.toString(10);
	return (decimalNumber)
}

function convertDecimal(dec) {
	var decimalNumber = dec;
	decimalNumber = parseInt(decimalNumber);
	var binaryNumber = decimalNumber.toString(2);
	return (binaryNumber)
}

/* http://javascript.about.com/library/blipconvert.htm */
function dot2num(dot) {
	var d = dot.split('.');
	return ((((((+d[0]) * 256) + (+d[1])) * 256) + (+d[2])) * 256) + (+d[3]);
}

function num2dot(num) {
	var d = num % 256;
	for (var i = 3; i > 0; i--) {
		num = Math.floor(num / 256);
		d = num % 256 + '.' + d;
	}
	return d;
}

/* page specific functions */
function net2zones(fieldset, dep) {
	if (dep == 0 || dep == undefined) {
		filter = "updateZone"
	} else {
		filter = "updateZoneDependents"
	}
	/*global variables*/
	if (window.zones == undefined) {
		window.zones = "";
	}
	if (window.supNet == undefined) {
		window.supNet = "";
	}
	if (window.supNM == undefined) {
		window.supNM = "";
	}
	if (window.nmStaged == undefined) {
		window.nmStaged = new Array();
	}
	/*set application scope*/
	zoneParent = document.getElementById(fieldset);
	/*get application type*/
	zoneParentType = zoneParent.className;
	/*results window*/
	wResults = getElementsByClassName("cZoneResults", null, zoneParent);
	wResults = wResults[0];
	/*zone count*/
	zonesOb = getElementsByClassName("cZoneCount", null, zoneParent);
	zonesOb = zonesOb[0];
	zones = parseInt(zonesOb.options[zonesOb.selectedIndex].value);
	/*network to divide*/
	supNetOb = getElementsByClassName("cNetwork", null, zoneParent);
	supNetOb = supNetOb[0];
	supNet = supNetOb.value
	if (supNet == "") {
		alert("Please enter a valid network");
		supNetOb.style.backgroundColor = "ffdddd";
		return false
	} else {
		supNetOb.style.backgroundColor = "fff"
	}
	/*network netmask*/
	supNMOb = getElementsByClassName("cNetmask", null, zoneParent);
	supNMOb = supNMOb[0];
	supNM = parseInt(supNMOb.options[supNMOb.selectedIndex].value);
	/* determine hosts from supernet*/
	supHostNM = 32 - supNM;
	supHostCount = bit2dec(supHostNM);
	supNet = dot2num(supNet);
	/*test whether values make up a valid subnet*/
	testNet = supNet % supHostCount;
	if (testNet > 0) {
		/*alert("The supernet entered is not valid. The system will compensate for your inability to assign a network")*/
	}
	supNet = supNet - testNet
	supNetOb.value = num2dot(supNet)

	if (zoneParentType == "zone_data" || dep == 1) {
		/* determine host ratio and VLAN Netmasks */
		switch(true) {
			case(supNM<22):
				vMgmt = 256;
				vServer = 128;
				vPrinter = 128;
				break;
			case(supNM==22):
				vMgmt = 128;
				vServer = 64;
				vPrinter = 64;
				break;
			case(supNM==23):
				vMgmt = 64;
				vServer = 32;
				vPrinter = 32;
				break;
			case(supNM==24):
				vMgmt = 32;
				vServer = 16;
				vPrinter = 16;
				break;
			default:
				vMgmt = 32;
				vServer = 0;
				vPrinter = 0;
				break;
		}
	} else {
		vMgmt = 0;
		vServer = 0;
		vPrinter = 0;
	}

	/* determine remaining hosts for user zones */
	zoneHostPool = supHostCount - vMgmt - vServer - vPrinter;
	hostsPerZone = zoneHostPool / zones;
	/* determine proper zone calculation */
	switch(true) {
		case(hostsPerZone<128):
			hostsPerZone = 64;
			break;
		case(hostsPerZone<256):
			hostsPerZone = 128;
			break;
		default:
			hostsPerZone = 256;
			break;
	}
	/* determine zone netmask */
	zoneHostBits = convertDecimal(hostsPerZone - 1)
	zoneHostBits = zoneHostBits.length;
	zoneNM = 32 - zoneHostBits;
	/* generate networks */
	zoneNetworks = new Array();
	zoneNetworks[0] = new Array(num2dot(supNet), zoneNM, "A");
	for ( i = 1; i < zones; i++) {
		/*ugh*/
		switch(true) {
			case(zoneNM>=24):
				oct = 3;
				break;
			case(zoneNM>=16 && zoneNM<24):
				oct = 2;
				break;
			case(zoneNM>=8 && zoneNM<16):
				oct = 1;
				break;
			case(zoneNM>=0 && zoneNM<8):
				oct = 3;
				break;
		}
		octHosts = zoneNetworks[i-1][0].split(".")
		octHost = parseInt(octHosts[oct])
		/* alert(zoneNetworks[i-1][0]+"-"+octHost+ "-"+parseInt(octHost+hostsPerZone)) */
		zoneNetworks.push(new Array(num2dot(dot2num(zoneNetworks[i-1][0]) + hostsPerZone), zoneNM, String.fromCharCode('A'.charCodeAt() + i)))
	}
	/* ********************************************************************************************************************************* */
	if (zoneParentType == "zone_data") {
		/* allocate static zones - from the end of the pool */
		/* end of pool */
		supEnd = supNet + supHostCount;
		/* used pool size */
		unusedHosts = supHostCount - (hostsPerZone * zones)
		/* determine which pools can be used */
		managementVLAN = false;
		serverVLAN = false;
		printerVLAN = false;
		switch(true) {
			case(unusedHosts<0):
				alert("Not enough hosts to subnet");
				managementVLAN = false;
				serverVLAN = false;
				printerVLAN = false;
				return false;
				break;
			case (unusedHosts<32):
				alert("Not enough hosts remaining to subnet static zones");
				break;
			case (unusedHosts<64 && unusedHosts>=32):
				managementVLAN = true;
				alert("Not enough hosts remaining to subnet Printer/Imaging and Server VLANs")
				break;
			case (unusedHosts<128 && unusedHosts>=64):
				switch(true) {
					case(supNM<23):
						alert("Not enough hosts remaining in subnet for static VLANs")
						break;
					case(supNM==23):
						managementVLAN = true;
						serverVLAN = true;
						alert("Not enough hosts remaining in subnet for Printer/Imaging VLANs")
						break;
					case(supNM>24):
						managementVLAN = true;
						serverVLAN = true;
						printerVLAN = true;
						break;
				}
				break;
			case (unusedHosts<256 && unusedHosts>=128):
				switch(true) {
					case(supNM<22):
						alert("Not enough hosts remaining in subnet for static VLANs")
						break;
					case(supNM==22):
						managementVLAN = true;
						serverVLAN = true;
						alert("Not enough hosts remaining in subnet for Printer/Imaging VLANs")
						break;
					case(supNM>=23):
						managementVLAN = true;
						serverVLAN = true;
						printerVLAN = true;
						break;
				}
				break;
			case (unusedHosts<384 && unusedHosts>=256):
				switch(true) {
					case(supNM<22):
						managementVLAN = true;
						alert("Not enough hosts remaining in subnet for Printer/Imaging and Server VLANs")
						break;
					case(supNM>=22):
						managementVLAN = true;
						serverVLAN = true;
						printerVLAN = true;
						break;
				}
				break;
			case (unusedHosts<512 && unusedHosts>=384):
				switch(true) {
					case(supNM<22):
						managementVLAN = true;
						serverVLAN = true;
						alert("Not enough hosts remaining in subnet for Printer/Imaging")
						break;
					case(supNM>=22):
						managementVLAN = true;
						serverVLAN = true;
						printerVLAN = true;
						break;
				}
				break;
			case (unusedHosts>=512):
				managementVLAN = true;
				serverVLAN = true;
				printerVLAN = true;
				break;

			case(unusedHosts>=0):
				managementVLAN = false;
				serverVLAN = false;
				printerVLAN = false;
				break;

		}
		/* management */
		if (managementVLAN == true) {
			mgmt = supEnd - vMgmt;
			mgmtHostBits = convertDecimal(vMgmt - 1)
			mgmtHostBits = mgmtHostBits.length;
			mgmtNM = 32 - mgmtHostBits;
		}
		if (serverVLAN == true) {
			/* server */
			server = mgmt - vServer;
			serverHostBits = convertDecimal(vServer - 1)
			serverHostBits = serverHostBits.length;
			serverNM = 32 - serverHostBits;
		}
		if (printerVLAN == true) {
			/* printer */
			imaging = server - vPrinter
			imagingHostBits = convertDecimal(vPrinter - 1)
			imagingHostBits = imagingHostBits.length;
			imagingNM = 32 - imagingHostBits;
		}
		/* append static zones */
		if (managementVLAN == true) {
			zoneNetworks.push(new Array(num2dot(imaging), imagingNM, "Printers"))
		}
		if (serverVLAN == true) {
			zoneNetworks.push(new Array(num2dot(server), serverNM, "Servers"))
		}
		if (printerVLAN == true) {
			zoneNetworks.push(new Array(num2dot(mgmt), mgmtNM, "Management"))
		}
	}
	/* ********************************************************************************************************************************* */

	/* begin creating elements */

	hcode = "";
	for ( b = 0; b < zoneNetworks.length; b++) {
		znet = "<input type=\"text\" name=\"" + zoneNetworks[b][2] + "_net_" + fieldset + "\" id=\"" + zoneNetworks[b][2] + "_net_" + fieldset + "\"class=\"superNET\" readonly=\"readonly\" value=\"" + zoneNetworks[b][0] + "\"/>";
		znm = buildNetmask(zoneNetworks[b][1], zoneNetworks[b][2], fieldset, filter);
		zlabel = "<input type=\"text\" readonly=\"readonly\" value=\"" + zoneNetworks[b][2] + "\"/>";
		hcode = hcode + "<div class=\"frmrow\"><div class=\"frmcol\"><label>Zone</label>" + zlabel + "</div><div class=\"frmcol\"><label>Network</label>" + znet + "</div><div class=\"frmcol\"><label>Netmask</label>" + znm + "</div></div>"
	}
	wResults.innerHTML = hcode
	/*populate nmStaged with select array values prior to modifications*/
	nmArray = getElementsByClassName("superNM", "select", zoneParent)
	for ( i = 0; i < nmArray.length; i++) {
		x = nmArray[i].options[nmArray[i].selectedIndex].value
		window.nmStaged.push(x);
	}
}

/* ******************************************************************************************************* */
function buildNetmask(selectedNetmask, zone, fieldset, filter) {
	listNetmask = "<select name=\"" + zone + "_nm_" + fieldset + "\" id=\"" + zone + "_nm_" + fieldset + "\" class=\"superNM\" onchange=\"" + filter + "('" + zone + "','" + fieldset + "',this.id)\">"
	for ( i = 24; i < 29; i++) {
		supHostCount = 0
		supHostNM = 32 - i;
		supHostCount = bit2dec(supHostNM);
		selected = (i == selectedNetmask ? "selected=\"selected\"" : "");
		printSupHostCount = supHostCount - 2;
		listNetmask = listNetmask + "<option value='" + i + "' " + selected + ">" + i + " - " + printSupHostCount + " Hosts</option>";
	}
	listNetmask = listNetmask + "</select>"
	return listNetmask;
}

/* ******************************************************************************************************* */
function updateZone(label, fieldset, obid) {
	/*set application scope*/
	zoneParent = document.getElementById(fieldset);
	nmArray = getElementsByClassName("superNM", "select", zoneParent)
	allNM = getElementsByClassName("superNM", "select", null)
	netArray = getElementsByClassName("superNET", "input", zoneParent)
	/*loop through array to find key for selected netmask*/
	for ( i = 0; i < nmArray.length; i++) {
		nmName = nmArray[i].name
		nmName = nmName.split("_")
		nmName = nmName[0]
		if (nmName == label) {
			changedNMKey = i
		}
	}
	/* get globals */
	/*zone count*/
	if (zones == "") {
		zonesOb = getElementsByClassName("cZoneCount", null, zoneParent);
		zonesOb = zonesOb[0];
		zones = parseInt(zonesOb.options[zonesOb.selectedIndex].value);
	}
	/*network to divide*/
	if (supNet == "") {
		supNetOb = getElementsByClassName("cNetwork", null, zoneParent);
		supNetOb = supNetOb[0];
		supNet = supNetOb.value;
	}
	/*network netmask*/
	if (supNM == "") {
		supNMOb = getElementsByClassName("cNetmask", null, zoneParent);
		supNMOb = supNMOb[0];
		supNM = parseInt(supNMOb.options[supNMOb.selectedIndex].value);
	}
	/* ******************** start calculations */
	/*determine hosts from supernet*/
	supHostNM = 32 - supNM;
	supHostCount = bit2dec(supHostNM);
	/*newly changed netmask*/
	changedNM = nmArray[changedNMKey].options[nmArray[changedNMKey].selectedIndex].value;
	changedNMHosts = parseInt(bit2dec(32 - changedNM))
	/* original netmask*/
	originalNM = nmStaged[changedNMKey];
	originalNMHosts = parseInt(bit2dec(32 - originalNM))
	//alert(changedNM+"-"+originalNM)
	/*get difference */
	diffHosts = originalNMHosts - changedNMHosts;
	/*identify if the zone is a static zone*/
	zoneName = ""
	zoneName = nmArray[changedNMKey].name
	zoneNamePart = zoneName.split("_")
	zoneNamePart = zoneNamePart[0]
	/*validate network - in the event that the netmask scope crosses another defined network*/
	if (changedNMKey + 1 < nmArray.length && changedNMKey > 0 && zoneNamePart.length == 1) {
		updateNW = dot2num(netArray[changedNMKey].value)
		updateNextNW = dot2num(netArray[changedNMKey + 1].value)
		updatePrevNW = dot2num(netArray[changedNMKey - 1].value)
		updatePrevDiff = updateNW - updatePrevNW
		updateNextDiff = updateNextNW - updateNW
		//alert(updateNextNW+"-"+updateNW+"="+updateNextDiff)
		// alert(changedNMHosts+">"+updateNextDiff)
		pretest = false;
		preNMHosts = bit2dec(32 - nmArray[changedNMKey-1].options[nmArray[changedNMKey - 1].selectedIndex].value);
		rem = 256 - (updatePrevNW % 256 + preNMHosts)

		if (changedNMHosts < updatePrevDiff || changedNMHosts <= rem) {
			ip = num2dot(updatePrevNW + preNMHosts)
			netArray[changedNMKey].value = ip
			pretest = true;
		}

		if (changedNMHosts > updateNextDiff && pretest == false) {
			chUpdateNW = num2dot(updateNW)
			chOctHosts = chUpdateNW.split(".")
			changedNMVal = nmArray[changedNMKey].options[nmArray[changedNMKey].selectedIndex].value
			switch(true) {
				case(changedNMVal>=24):
					oct = 3;
					break;
				case(changedNMVal>=16 && changedNMVal<24):
					oct = 2;
					break;
				case(changedNMVal>=8 && changedNMVal<16):
					oct = 1;
					break;
				case(changedNMVal>=0 && changedNMVal<8):
					oct = 3;
					break;
			}
			chOctHost = parseInt(chOctHosts[oct])
			updateDiff = 256 - chOctHost

			ip = num2dot(updateNW + updateDiff)
			netArray[changedNMKey].value = ip
		}

	}
	/* combine hostsAllocated with newly changed */
	hostsAllocated = 0

	for ( o = 0; o < nmArray.length; o++) {
		x = nmArray[o].options[nmArray[o].selectedIndex].value
		xhosts = parseInt(bit2dec(32 - x))

		hostsAllocated = hostsAllocated + xhosts
	}
	//alert(hostsAllocated+" and "+supHostCount)
	/* subract allocated hosts from total subnet */
	remainingHosts = supHostCount - hostsAllocated

	/*alert("Totol Supernet Count: "+supHostCount+"\nTotal Hosts Preceding Change: "+hostsAllocated+"\nRemaining Hosts: "+remainingHosts)*/
	if (remainingHosts < 0) {
		alert("This is an invalid configuration");
		for (a in allNM) {
			if (a != changedNMKey) {
				allNM[a].disabled = true;
			}
		}
		return false;
	} else {
		for (a in allNM) {
			allNM[a].disabled = false;

		}
	}

	/* loop through nmArray and get network sizes */
	hostRem = 0
	/*calculate if valid*/
	changedNMVal = nmArray[changedNMKey].options[nmArray[changedNMKey].selectedIndex].value
	switch(true) {
		case(changedNMVal>=24):
			oct = 3;
			break;
		case(changedNMVal>=16 && changedNMVal<24):
			oct = 2;
			break;
		case(changedNMVal>=8 && changedNMVal<16):
			oct = 1;
			break;
		case(changedNMVal>=0 && changedNMVal<8):
			oct = 3;
			break;
	}
	hostUp = false;
	for ( i = changedNMKey + 1; i < nmArray.length; i++) {
		netName = ""
		netName = nmArray[i].name
		netNamePart = netName.split("_")
		netNamePart = netNamePart[0]
		if (netNamePart.length == 1) {
			/* get netmask */
			rowNM = nmArray[i].options[nmArray[i].selectedIndex].value
			/* get hosts*/
			rowHostNM = 32 - rowNM;
			rowHostCount = bit2dec(rowHostNM);
			/* get network */
			prevNM = nmArray[i-1].options[nmArray[i - 1].selectedIndex].value
			preRowHostNM = 32 - prevNM;
			preRowHostCount = bit2dec(preRowHostNM);
			netArray[i].value = num2dot(dot2num(netArray[i - 1].value) + preRowHostCount)
			decNet = dot2num(netArray[i].value)
			decNetDot = num2dot(decNet)
			octHosts = decNetDot.split(".")
			octHost = parseInt(octHosts[oct])
			hostPred = octHost + rowHostCount
			if (hostPred > 256) {
				hostRem = 256 - octHost
			} else {
				hostRem = 0
			}
			netArray[i].value = num2dot(decNet + hostRem)
		} else {
			/* ********************************************************************************************************************************* */
			/* build end of network calculations for non user zones */
			/* ********************************************************************************************************************************* */
		}
	}
	nmArray = getElementsByClassName("superNM", "select", zoneParent)
	window.nmStaged.length = 0;
	for ( i = 0; i < nmArray.length; i++) {
		x = nmArray[i].options[nmArray[i].selectedIndex].value
		window.nmStaged.push(x);
	}
	return true
}

function buildZones(a, r) {
	y = a.split(",");
	x = 0;
	for ( i = 0; i < y.length; i++) {
		y[i] = document.getElementById(y[i]);
		if (i > 0) {
			/*zone count*/
			a = getElementsByClassName("cZoneCount", null, y[0]);
			a = a[0];
			a = a.selectedIndex;
			/*network netmask*/
			b = getElementsByClassName("cNetmask", null, y[0]);
			b = b[0];
			b = b.selectedIndex;
			/*network to divide*/
			c = getElementsByClassName("cNetwork", null, y[0]);
			c = c[0];
			cobj = c;
			c = c.value;
			if (c == "") {
				alert("Please enter a valid network address")
				cobj.style.backgroundColor = "ffcccc"
				return false;

			} else {
				cobj.style.backgroundColor = "fff"
			}
			c = dot2num(c);
			/*zone count*/
			d = getElementsByClassName("cZoneCount", null, y[i]);
			d = d[0];
			d.selectedIndex = a;
			/*network to divide*/
			e = getElementsByClassName("cNetwork", null, y[i]);
			e = e[0];
			NETMASK
			x = x + 65536;
			e.value = num2dot(x + c);
			/*network netmask*/
			f = getElementsByClassName("cNetmask", null, y[i]);
			f = f[0];
			f.selectedIndex = b;
		}
	}
	for (u in y) {
		net2zones(y[u].id, r);
	}
}

function updateZoneDependents(label, fieldset, g) {
	arr = new Array()
	z = document.getElementById(g)
	z = z.selectedIndex
	nmArray = getElementsByClassName("superNM", "select", null)
	for ( i = 0; i < nmArray.length; i++) {
		if (z.id != nmArray[i].id) {
			a = nmArray[i].id;
			o = nmArray[i];
			if (a.indexOf(label) != -1) {
				b = nmArray[i];
				while (b.className.indexOf('zone_') == -1) {
					b = b.parentNode;

				}
				o.selectedIndex = z
				x = b.id
				arr.push(x)

			}
		}

	}

	for (s in arr) {

		updateZone(label, arr[s], g);
		if (updateZone(label, arr[s], g) == false) {
			return false;

		}
	}

}
function IPIsUnique(nIP,rNETWORK,DEST,PAGE,SUBMITBUTTON,FORMNAME,PARAMS){
	pIP = true;
	PARAMS=(PARAMS!=undefined?PARAMS:"");
	IP=dot2num(nIP.value);
	/* validate IP */
	tIP=validateElement("ip",nIP);
	if (tIP==false){pIP = false;}

	nNETWORK=document.getElementById(rNETWORK);

	tInNetwork=IPInNetwork(nIP,nNETWORK);
	if (tInNetwork==false){pIP = false;}

	NETWORK=nNETWORK.options[nNETWORK.selectedIndex].value;
	DEST=(DEST===undefined||DEST===null?"intfInsertError":DEST);
	PAGE=(PAGE===undefined||PAGE===null?"/_lib/php/jsDeps/ArcUniqueIP.php":PAGE);
	/*jump to PHP function IPIsUnique.php*/	
	if(pIP == false) {
		noIP=confirm("You are submitting an interface with no IP assignment. Do you wish to continue?");
		if (noIP == 1) {
			prepSaveChanges(FORMNAME);
			return false;
		} else {
			return false;
		}
	}
	arc(DEST,PAGE,"cfg_device_interface_ip4hostaddress="+IP+"&id_cfg_device_ip4netaddress="+NETWORK+"&submit="+SUBMITBUTTON+"&form="+FORMNAME+PARAMS,0,0);
}

function validateNetwork(IP, CIDR, NETGROUP, EXISTING, DEST, oFORM) {
	/*@requirements: JQuery 1.9.0 or greater*/
	oIP = document.getElementById(IP);
	oFORM = document.getElementById(oFORM);
	IP = oIP.value;
	CIDR = document.getElementById(CIDR).options[document.getElementById(CIDR).selectedIndex].value;
	NETGROUP = oFORM.elements[NETGROUP].options[oFORM.elements[NETGROUP].selectedIndex].value;
	message = "";

	/*kill function if parameters are not set*/
	if (IP == "" || CIDR == "") {
		return false;
	}

	/*Convert IP to decimal*/
	ipDec = dot2num(IP);
	/*Convert CIDR to HOST BITS*/
	hostBits = 32 - CIDR;
	/*Convert hostBits to Decimal*/
	hosts = bit2dec(hostBits);
	/*Test IP*/
	if (ipDec % hosts != 0) {
		kill = true;
		document.getElementById("frmnetworksubmit").disabled = true;
		error = "This is not a valid network!";
	} else {
		kill = false
		error = "This is a valid network";
		document.getElementById("frmnetworksubmit").disabled = false;

	}
	/*Loop through EXISTING array and determine whether or not Network overlaps*/
	if (EXISTING != undefined && kill == false) {

		matchedResults = new Array();
		start = ipDec;
		end = ipDec + hosts;

		hostArray = new Array();
		for ( y = start; y < end; y++) {
			hostArray.push(y);
		}

		for ( i = 0; i < EXISTING.length; i++) {
			matched = false;
			eHosts = bit2dec(32 - EXISTING[i][1]);
			eNetwork = dot2num(EXISTING[i][0]);
			eNetGroup = EXISTING[i][2];
			eStart = eNetwork;
			eEnd = eNetwork + eHosts;
			existingRange = new Array();
			for ( e = eStart; e < eEnd; e++) {
				existingRange.push(e)
			}
			o = 0;
			if (parseInt(eNetGroup) === parseInt(NETGROUP)) {

				switch(true) {
					case (start<=eStart):
						if (end > eStart && end <= eEnd) {
							eTest = true;
							//alert(1+"--"+start+":"+eStart+"x"+end+":"+eEnd)
						} else {
							if (end >= eEnd) {
								eTest = true;
								//alert(2)
							} else {
								eTest = false;
							}
						}
						break;
					case (start==eStart):
						eTest = true;
						//alert(3)
						break;
					case (start>=eStart):
						if (end <= eEnd) {
							//alert(4)
							eTest = true;
						} else {
							if (end >= eEnd && start < eEnd) {
								//alert(5)
								eTest = true;
							} else {
								eTest = false;
							}
						}
						break;
					default:
						eTest = false;
						break;
				}
				if (eTest === true) {
					matchedResults.push(num2dot(eStart) + "/" + EXISTING[i][1]);
				}
			}
		}
		if (matchedResults.length > 0) {
			message = "\n--------------------------------\nERROR: The network entered overlaps the following network(s):\n";
			for (matched in matchedResults) {
				message = message + matchedResults[matched] + "\n";
			}
			/*clear ip*/
			oIP.value = "";
		}
	}
	alert("\nIP - " + IP + "\n" + error + message);
}
function IPInNetwork(addressField, networkField, network, netmask) {
	/*
	 * this function was originally written for console and will need to be parsed for universal use
	 *	 */
	if (networkField.selectedIndex == 0) {
		addressField.value = "";
		addressField.disabled=true;
	}else{
		addressField.disabled=false;
	}

	address = addressField.value;
	if (address == "" || networkField == undefined) {
		return false;
	}

	networkFieldText = networkField.options[networkField.selectedIndex].text;
	networkFieldArray = networkFieldText.split("/");
	network = trimLeftAndRight(networkFieldArray[0]);
	netmask = trimLeftAndRight(networkFieldArray[1]);

	//address=prompt("Enter an address");
	//network=prompt("Enter a network");
	//netmask=prompt("Enter a netmask in CIDR");
	hostmask = 32 - netmask;

	addressDec = dot2num(address);
	networkDec = dot2num(network);
	netmaskDec = bit2dec(netmask);
	hostmaskDec = bit2dec(hostmask);

	netOffset = (parseInt(netmask) < 31 ? 1 : 0);

	networkHostStartDec = networkDec + netOffset;
	networkHostEndDec = networkDec + hostmaskDec - 1 - netOffset;

	networkHostStart = num2dot(networkHostStartDec);
	networkHostEnd = num2dot(networkHostEndDec);

	if (addressDec >= networkHostStartDec && addressDec <= networkHostEndDec) {
		Test = "True";
		addressField.style.backgroundColor = "#FFF";
	} else {
		Test = "False";

		alert("The address entered does not pertain to the assigned network\n" + "Address:" + address + "\n" + "Network:" + network + "\n" + "Netmask:" + netmask + "\n" + "Address in Dec: " + addressDec + "\n" + "Network in Dec: " + networkDec + "\n" + "Netmask in Dec: " + netmaskDec + "\n" + "Hostmask in Dec: " + hostmaskDec + "\n" + "Network Host Start: " + networkHostStart + "\n" + "Network Host End: " + networkHostEnd + "\n" + "Test Result: " + Test)
		addressField.value = "";
		addressField.style.backgroundColor = "#FFDDDD";
		return false;
	}
}
